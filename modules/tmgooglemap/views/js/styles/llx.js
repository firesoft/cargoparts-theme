var google_map_style = [
  {
    "featureType": "all",
    "elementType": "labels.text",
    "stylers": [
      {
        "lightness": "-85"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "saturation": "-100"
      },
      {
        "lightness": "34"
      },
      {
        "hue": "#0092ff"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "lightness": "-100"
      },
      {
        "hue": "#ff0000"
      },
      {
        "saturation": "-100"
      },
      {
        "gamma": "0.00"
      },
      {
        "weight": "0"
      }
    ]
  },
  {
    "featureType": "all",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      },
      {
        "lightness": "2"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 20
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 17
      },
      {
        "weight": 1.2
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "labels",
    "stylers": [
      {
        "hue": "#ff0000"
      },
      {
        "saturation": "-100"
      },
      {
        "lightness": "-2"
      },
      {
        "gamma": "1"
      }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": "-87"
      },
      {
        "hue": "#008eff"
      },
      {
        "saturation": "-36"
      },
      {
        "gamma": "0.95"
      },
      {
        "weight": "1.00"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 21
      },
      {
        "color": "#21262a"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      },
      {
        "saturation": "-12"
      },
      {
        "lightness": "-12"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "off"
      },
      {
        "saturation": "-100"
      },
      {
        "lightness": "0"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "lightness": "-66"
      },
      {
        "hue": "#ff0000"
      },
      {
        "saturation": "-100"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#000000"
      },
      {
        "lightness": 29
      },
      {
        "weight": 0.2
      },
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "hue": "#ff0000"
      },
      {
        "lightness": "-26"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": "-79"
      },
      {
        "hue": "#0092ff"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": "-77"
      },
      {
        "hue": "#0092ff"
      },
      {
        "saturation": "11"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "lightness": "-33"
      },
      {
        "saturation": "-10"
      },
      {
        "weight": "1.50"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "visibility": "off"
      },
      {
        "weight": "0.59"
      },
      {
        "lightness": "-46"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": "-73"
      },
      {
        "visibility": "on"
      },
      {
        "hue": "#ff0000"
      },
      {
        "saturation": "-100"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text",
    "stylers": [
      {
        "lightness": "-15"
      },
      {
        "saturation": "-41"
      },
      {
        "gamma": "1.85"
      },
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "lightness": "-47"
      },
      {
        "saturation": "15"
      },
      {
        "hue": "#0092ff"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "on"
      },
      {
        "lightness": "-20"
      },
      {
        "saturation": "-100"
      },
      {
        "weight": "0.01"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": "-82"
      },
      {
        "hue": "#008eff"
      },
      {
        "saturation": "-89"
      },
      {
        "gamma": "1.06"
      }
    ]
  }
]