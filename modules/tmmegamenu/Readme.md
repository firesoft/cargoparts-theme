# TM Mega Menu

v 1.6.3
FIXED SORTING ISSUE IN MEGAMENU TYPE
v 1.7.0
ADDED MULTI-LANGUAGE CUSTOM URL FOR MENU TOP ITEMS
v 1.7.1
FIXED ISSUE WITH MULTI-LANGUAGE CUSTOM URL'S VALIDATION
v 1.7.2
REFACTORING: html code replaced from php to tpl
1.7.3
FIX: ADDED CHECKING FOR VARIABLE EXISTENCE
1.7.4
FIX: removed double escaping in categories-tree.tpl
1.7.5
FIX: small fix, replaced wrong html escape categories-tree.tpl