<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMFilters extends AdminModulesControllerCore
{
    /*
     *  Main class that responds for building filter parameters on the frontend filter form
     */
    public $db_prefix; // date base prefix used in store
    public $shop;      // shop information(like: id, name etc.)
    public $language;  // language information (like: id, name etc.)

    public function __construct($db_prefix, Shop $shop, Language $language)
    {
        $this->shop = $shop;
        $this->language = $language;
        $this->db_prefix = $db_prefix;
    }

    /**
     * Get tree categories
     * @return array $category_tree
     */
    public function filterTreeCategories($context)
    {
        $root = Category::getRootCategory();
        $tree = new HelperTreeCategories('categories_col');
        if ($selected_categories = $context->get('tmproductsmanager')) {
            $selected_categories = Tools::jsonDecode($selected_categories, true);
            if (isset($selected_categories['categoryBox'])) {
                $selected_categories = $selected_categories['categoryBox'];
                $tree->setUseCheckBox(true)
                    ->setAttribute('is_category_filter', $root->id)
                    ->setRootCategory($root->id)
                    ->setSelectedCategories($selected_categories)
                    ->setUseSearch(false);
            } else {
                $tree->setUseCheckBox(true)
                    ->setAttribute('is_category_filter', $root->id)
                    ->setRootCategory($root->id)
                    ->setUseSearch(true);
            }
        } else {
            $tree->setUseCheckBox(true)
                ->setAttribute('is_category_filter', $root->id)
                ->setRootCategory($root->id)
                ->setUseSearch(true);
        }
        $category_tree = $tree->render();

        return $category_tree;
    }

    public function filterManufacturers()
    {
        $sql = "SELECT m.`id_manufacturer` as id, m.`name`
                FROM {$this->db_prefix}manufacturer m
                LEFT JOIN {$this->db_prefix}manufacturer_shop ms
                ON(ms.`id_manufacturer` = m.`id_manufacturer`)
                WHERE ms.`id_shop` = {$this->shop->id}";

        return Db::getInstance()->executeS($sql);
    }

    public function filterSuppliers()
    {
        $sql = "SELECT s.`id_supplier` as id, s.`name`
                FROM {$this->db_prefix}supplier s
                LEFT JOIN {$this->db_prefix}supplier_shop ss
                ON(ss.`id_supplier` = s.`id_supplier`)
                WHERE ss.`id_shop` = {$this->shop->id}";

        return Db::getInstance()->executeS($sql);
    }

    public function filterAttributes(array $attributes = array())
    {
        $groups = AttributeGroup::getAttributesGroups($this->language->id);
        if ($groups) {
            foreach ($groups as $group) {
                $attributes[$group['id_attribute_group']]['info']['id'] = $group['id_attribute_group'];
                $attributes[$group['id_attribute_group']]['info']['name'] = $group['name'];
                $attributes[$group['id_attribute_group']]['info']['public_name'] = $group['public_name'];
                $related_attributes = AttributeGroup::getAttributes($this->language->id, $group['id_attribute_group']);
                if ($related_attributes) {
                    foreach ($related_attributes as $k => $attr) {
                        $attributes[$group['id_attribute_group']]['items'][$k] = $attr;
                        $attributes[$group['id_attribute_group']]['items'][$k]['id'] = $attributes[$group['id_attribute_group']]['items'][$k]['id_attribute'];
                    }
                }
            }
        }

        return $attributes;
    }

    public function filterFeatures(array $features = array())
    {
        $groups = Feature::getFeatures($this->language->id);
        if ($groups) {
            foreach ($groups as $group) {
                $features[$group['id_feature']]['info']['id'] = $group['id_feature'];
                $features[$group['id_feature']]['info']['name'] = $group['name'];
                $related_features = FeatureValue::getFeatureValuesWithLang($this->language->id, $group['id_feature']);
                if ($related_features) {
                    foreach ($related_features as $k => $feature) {
                        $features[$group['id_feature']]['items'][$k] = $feature;
                        $features[$group['id_feature']]['items'][$k]['id'] = $features[$group['id_feature']]['items'][$k]['id_feature_value'];
                        $features[$group['id_feature']]['items'][$k]['name'] = $features[$group['id_feature']]['items'][$k]['value'];
                    }
                }
            }
        }

        return $features;
    }

    public function filterConditions(array $result = array())
    {
        $result[] = array('id' => 'new', 'name' => $this->l('New'));
        $result[] = array('id' => 'used', 'name' => $this->l('Used'));
        $result[] = array('id' => 'refurbished', 'name' => $this->l('Refurbished'));

        return $result;
    }

    public function filterImages(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('yes'));
        $result[] = array('id' => 2, 'name' => $this->l('no'));

        return $result;
    }

    public function filterSale(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterActive(array $result = array())
    {
        $result[0] = array('id' => 0, 'name' => $this->l('No'));
        $result[1] = array('id' => 1, 'name' => $this->l('Yes'));

        return $result;
    }

    public function filterCustomizable(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterOnlineonly(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterOrder(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterSPrice(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterVirtual(array $result = array())
    {
        $result[] = array('id' => 1, 'name' => $this->l('Yes'));
        $result[] = array('id' => 0, 'name' => $this->l('No'));

        return $result;
    }

    public function filterVisibility(array $result = array())
    {
        $result[] = array('id' => 'both', 'name' => $this->l('Both'));
        $result[] = array('id' => 'catalog', 'name' => $this->l('Catalog'));
        $result[] = array('id' => 'search', 'name' => $this->l('Search'));
        $result[] = array('id' => 'none', 'name' => $this->l('None'));

        return $result;
    }
}
