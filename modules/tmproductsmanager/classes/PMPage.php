<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMPage extends ObjectModel
{
    public $id_shop;
    public $name;
    public $friendly_url;
    public $filters;
    public $tmexcludedproducts;
    public $products;
    public $description;
    public $page_image;
    public $layout;
    public $active;
    public $type;
    public $date_from;
    public $date_to;
    public $countdown;
    public $voucher;
    public $code;
    public $gift_product;
    public $gift_product_attribute;
    public $display_code;
    public $display_gift;
    public static $definition = array(
        'table'     => 'tmproductsmanager_pages',
        'primary'   => 'id_page',
        'multilang' => true,
        'fields' => array(
            'id_shop'                 => array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isunsignedInt'),
            'filters'                 => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString'),
            'tmexcludedproducts'      => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'products'                => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString'),
            'name'                    => array('type' => self::TYPE_STRING, 'lang' => true, 'required' => true, 'validate' => 'isGenericName'),
            'friendly_url'            => array('type' => self::TYPE_STRING, 'lang' => true, 'required' => true, 'validate' => 'isLinkRewrite'),
            'description'             => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'page_image'              => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrl'),
            'layout'                  => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString'),
            'active'                  => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'type'                    => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'date_from'               => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'date_to'                 => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'countdown'               => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'voucher'                 => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'code'                    => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'gift_product'            => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'gift_product_attribute'  => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'display_code'            => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'display_gift'            => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt')
        )
    );

    /**
     * Delete page and related banners + related voucher
     *
     * @return bool
     */
    public function delete()
    {
        $result = true;
        $banners = PMPageBanner::getPageBannersLite($this->id);
        if ($banners) {
            foreach ($banners as $banner) {
                $banner = new PMPageBanner($banner['id_banner']);
                $result &= $banner->delete();
            }
        }
        if ($this->voucher) {
            $voucher = new CartRule($this->voucher);
            $result &= $voucher->delete();
        }
        $result &= $this->clearPageProducts();
        $result &= parent::delete();

        return $result;
    }

    /**
     * Get all special's pages list for current shop
     *
     * @param      $id_shop
     * @param bool $id_lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getPagesList($id_shop, $id_lang = false)
    {
        $sql = 'SELECT tp.*, tpl.*
                FROM '._DB_PREFIX_.'tmproductsmanager_pages tp
                LEFT JOIN '._DB_PREFIX_.'tmproductsmanager_pages_lang tpl
                ON (tp.`id_page` = tpl.`id_page`)
                WHERE tp.`id_shop` = '.(int)$id_shop.'
                AND tpl.`id_lang` = '.(int)$id_lang;

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get and sort products for result's page list
     *
     * @param      $products_ids array of suitable products id's
     * @param      $id_lang current language
     * @param      $p page number
     * @param      $n number of products in a page
     * @param null $order_by
     * @param null $order_way
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public static function getProducts($products_ids, $id_lang, $p, $n, $order_by = null, $order_way = null)
    {
        $context = Context::getContext();
        if ($p < 1) {
            $p = 1;
        }
        if (empty($order_by) || $order_by == 'position') {
            $order_by = 'name';
        }
        if (empty($order_way)) {
            $order_way = 'ASC';
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by = pSQL($order_by[0]).'.`'.pSQL($order_by[1]).'`';
        }
        $alias = '';
        if ($order_by == 'price') {
            $alias = 'product_shop.';
        } elseif ($order_by == 'name') {
            $alias = 'pl.';
        } elseif ($order_by == 'manufacturer_name') {
            $order_by = 'name';
            $alias = 'm.';
        } elseif ($order_by == 'quantity') {
            $alias = 'stock.';
        } else {
            $alias = 'p.';
        }
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity'
            .(Combination::isFeatureActive(
            ) ? ', product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, IFNULL(product_attribute_shop.`id_product_attribute`,0) id_product_attribute' : '').'
			, pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`,
			pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`, image_shop.`id_image` id_image, il.`legend`, m.`name` AS manufacturer_name,
				DATEDIFF(
					product_shop.`date_add`,
					DATE_SUB(
						"'.date('Y-m-d').' 00:00:00",
						INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
					)
				) > 0 AS new'
            .' FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').
            (Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
						ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$context->shop->id.')' : '').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
				ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
				ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
				ON (m.`id_manufacturer` = p.`id_manufacturer`)
			'.Product::sqlStock('p', 0);
        $sql .= '
				WHERE p.`id_product` IN ('.implode(',', $products_ids).')
				AND product_shop.`active` = 1
				AND product_shop.`visibility` IN ("both", "catalog")
				GROUP BY p.id_product
				ORDER BY '.$alias.'`'.bqSQL($order_by).'` '.pSQL($order_way).'
				LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if (!$result) {
            return false;
        }
        if ($order_by == 'price') {
            Tools::orderbyPrice($result, $order_way);
        }

        return Product::getProductsProperties($id_lang, $result);
    }

    /**
     * Check if page has activated countdown
     *
     * @param $id_page
     *
     * @return bool
     */
    public static function getCountdown($id_page)
    {
        $page = new PMPage($id_page);
        if ($page->type == 'none') {
            return false;
        }

        return $page->date_to;
    }

    public function getAllShopVouchers($id_shop)
    {
        $sql = 'SELECT `voucher`
                FROM '._DB_PREFIX_.'tmproductsmanager_pages
                WHERE `voucher` > 0
                AND `id_shop` = '.(int)$id_shop;

        return Db::getInstance()->executeS($sql);
    }

    public function updatePageProducts($id_page, $products, $id_shop)
    {
        if (!$products) {
            return false;
        }
        if (!Db::getInstance()->delete('tmproductsmanager_pages_products', '`id_page` = '.(int)$id_page.' AND `id_shop` = '.(int)$id_shop)) {
            return false;
        }
        foreach ($products as $product) {
            if (!Db::getInstance()->insert('tmproductsmanager_pages_products', array('id_page' => (int)$id_page, 'id_product' => (int)$product, 'id_shop' => (int)$id_shop))) {
                return false;
            }
        }

        return true;
    }

    public function clearPageProducts()
    {
        if (!Db::getInstance()->delete('tmproductsmanager_pages_products', '`id_page` = '.(int)$this->id.' AND `id_shop` = '.(int)$this->id_shop)) {
            return false;
        }

        return true;
    }

    public function checkProductAction($id_product, $id_shop)
    {
        return Db::getInstance()->executeS('SELECT `id_page` FROM '._DB_PREFIX_.'tmproductsmanager_pages_products WHERE `id_product` = '.(int)$id_product.' AND `id_shop` = '.(int)$id_shop);
    }
}
