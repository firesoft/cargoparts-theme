<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMForms extends ModuleAdminController
{
    /**
     * Main class that builds a forms and listing tables on the last filter page and specific(Specials offers) pages
     */
    private $ids = array(); // ids of products which according to selected criteria
    protected $context; // get context of page (languages, currencies etc.)
    protected $helper; // build main part of helper object to avoid duplicating
    protected $tabInfo;
    public $module; // get module's class instance
    public $pages = array(); // create pages list array(for banners positioning)
    public $hooks = array(); // create hooks list array(for banners positioning)
    /**
     * Build class and set properties
     */
    public function __construct($context, $ids, $action, $info)
    {
        $this->context = $context;
        $this->module = new Tmproductsmanager();
        $this->ids = Tools::jsonDecode($ids, true); // decode id's array to json(mainly used like json)
        $this->helper = $this->buildHelper($action);
        $this->tabInfo = $info;
        $this->pages = $this->getPages();
        $this->hooks = array(
            array('id' => 'displayBanner', 'type' => 'displayBanner'),
            array('id' => 'displayTop', 'type' => 'displayTop'),
            array('id' => 'displayColumnTop', 'type' => 'displayTopColumn'),
            array('id' => 'displayHome', 'type' => 'displayHome'),
            array('id' => 'displayLeftColumn', 'type' => 'displayLeftColumn'),
            array('id' => 'displayRightColumn', 'type' => 'displayRightColumn'),
            array('id' => 'displayFooter', 'type' => 'displayFooter'),
            array('id' => 'displayProductFooter', 'type' => 'displayProductFooter'),
        );
    }

    /**
     * Get all available pages list
     * @return array
     */
    protected function getPages()
    {
        $pages = array();
        $pages[] = array('id' => 'all', 'type' => $this->l('Display in all pages'));
        $prestaspages = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
        foreach (array_keys($prestaspages) as $key) {
            $pages[] = array('id' => $key, 'type' => $key);
        }

        return $pages;
    }

    /**
     * Get instance of helper class
     *
     * @param $action current action to know which form will be use
     *
     * @return HelperForm
     */
    protected function buildHelper($action)
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = 'tmproductsmanager';
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTMPM';
        $helper->module = $this->module;
        $helper->currentIndex = $this->context->link->getAdminLink(
            'AdminTmProductsManager',
            false
        ).'&action='.$action.'&process';
        $helper->token = Tools::getAdminTokenLite('AdminTmProductsManager');
        $helper->tpl_vars = array(
            'fields_value' => false,
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id
        );

        return $helper;
    }

    /********************************************* Information tab ***************************************************/
    /**
     * Build "information tab" form "Enable/disable products"
     *
     * @return mixed
     */
    public function enableProducts()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) status'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'status',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    )
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=enableProducts&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Available for order"
     *
     * @return mixed
     */
    public function availableOrder()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) available for order'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'available_for_order',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=availableOrder&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Show price"
     *
     * @return mixed
     */
    public function showPrice()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->l('Show price selected products'),
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Show product(s) price'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'show_price',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=showPrice&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Online only"
     *
     * @return mixed
     */
    public function onlineOnly()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) online only'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'online_only',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=onlineOnly&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Condition"
     *
     * @return mixed
     */
    public function condition()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'name'    => 'condition',
                        'label'   => $this->l('Set product(s) condition'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id'   => 'new',
                                    'name' => $this->l('New')),
                                array(
                                    'id'   => 'used',
                                    'name' => $this->l('Used')),
                                array(
                                    'id'   => 'refurbished',
                                    'name' => $this->l('Refurbished'))
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=condition&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Description"
     *
     * @return mixed
     */
    public function description()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'name'    => 'choose_place',
                        'label'   => $this->l('Choose the place for adding description'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id'   => 'before_description',
                                    'name' => $this->l('Before the main description')),
                                array(
                                    'id'   => 'after_description',
                                    'name' => $this->l('After the main description')),
                                array(
                                    'id'   => 'replace_description',
                                    'name' => $this->l('Replace the whole description'))
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                    array(
                        'type'         => 'textarea',
                        'name'         => 'description',
                        'autoload_rte' => true,
                        'lang'         => true,
                        'label'        => $this->l('Enter description'),
                        'desc'         => $this->l('Description: "Changes will be applied to all listed products".')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=description&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Description short"
     *
     * @return mixed
     */
    public function descriptionShort()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'         => 'textarea',
                        'name'         => 'description_short',
                        'autoload_rte' => true,
                        'lang'         => true,
                        'label'        => $this->l('Set product(s) short description'),
                        'desc'         => $this->l('Selected short description will changed for all listed products.')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=descriptionShort&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Information tab" form "Tags"
     *
     * @return mixed
     */
    public function tags()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'  => 'tags',
                        'name'  => 'tags',
                        'lang'  => true,
                        'label' => $this->l('Set product(s) tags'),
                        'desc'  => $this->l('Each tag has to be followed by a comma. The following characters are forbidden: !<;>;?=+#"°{}_$%.')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=tags&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }
    /************************************************* end of Information tab *****************************************/
    /************************************************** Price tab *****************************************************/
    /**
     * Build "Price tab" form "Display "On Sale""
     *
     * @return mixed
     */
    public function onSale()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) on sale'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'on_sale',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=onSale&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Price tab" form "Change tax rules"
     *
     * @param array $options define array
     *
     * @return mixed
     */
    public function taxRules(array $options = array())
    {
        $tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
        $options[] = array('id' => 0, 'name' => $this->l('No Tax'));
        if ($tax_rules_groups) {
            foreach ($tax_rules_groups as $tax_rules_group) {
                $options[] = array('id' => $tax_rules_group['id_tax_rules_group'], 'name' => $tax_rules_group['name']);
            }
        }
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select tax rules'),
                        'name'    => 'tax_rules',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => $options,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=taxRules&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Price tab" form "Add new specific price"
     *
     * @return mixed
     */
    public function specificPrice()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'  => 'datetime',
                        'label' => $this->l('Start Date'),
                        'name'  => 'data_start',
                        'col'   => 6,
                    ),
                    array(
                        'type'  => 'datetime',
                        'label' => $this->l('End Date'),
                        'name'  => 'data_end',
                        'col'   => 6,
                    ),
                    array(
                        'type'     => 'text',
                        'label'    => $this->l('Discount price'),
                        'col'      => 2,
                        'required' => true,
                        'name'     => 'discount_price',
                    ),
                    array(
                        'type'    => 'select',
                        'name'    => 'reduction_type',
                        'options' => array(
                            'id'    => 'id_option',
                            'name'  => 'name',
                            'query' => array(
                                array(
                                    'id_option' => 'amount',
                                    'name'      => 'amount'
                                ),
                                array(
                                    'id_option' => 'percentage',
                                    'name'      => '%'
                                ),
                            )
                        )
                    ),
                    array(
                        'type'    => 'select',
                        'name'    => 'reduction_tax',
                        'options' => array(
                            'id'    => 'id_option',
                            'name'  => 'name',
                            'query' => array(
                                array(
                                    'id_option' => 0,
                                    'name'      => 'Tax excluded'
                                ),
                                array(
                                    'id_option' => 1,
                                    'name'      => 'Tax included'
                                ),
                            )
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=specificPrice&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /******************************************************* end of Price tab ****************************************/
    /***************************************************** Association tab *******************************************/
    /**
     * Build "Association tab" form "Change brand"
     *
     * @param array $options define array
     *
     * @return mixed
     */
    public function brand(array $options = array())
    {
        $options[] = array('id' => '', 'name' => '');
        $manufacturers = Manufacturer::getManufacturers(false, 0, false);
        if ($manufacturers) {
            foreach ($manufacturers as $manufacturer) {
                $options[] = array('id' => $manufacturer['id_manufacturer'], 'name' => $manufacturer['name']);
            }
        }
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select brand'),
                        'name'    => 'manufacturer',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => $options,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=brand&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Association tab" form "Change related category"
     *
     * @param bool $multi multi select marker
     *
     * @return mixed
     */
    public function categories($multi = true)
    {
        $root = Category::getRootCategory();
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=categories&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        if ($multi) {
            $fields_form['form']['input'][] = array(
                'type'    => 'select',
                'label'   => $this->l('Select method'),
                'name'    => 'method',
                'options' => array(
                    'query' => array(
                        array(
                            'id' => 'add', 'name' => $this->l('Add to categories and save old relations')
                        ),
                        array(
                            'id' => 'replace', 'name' => $this->l('Add to categories and remove old relations')
                        ),
                    ),
                    'id'    => 'id',
                    'name'  => 'name'
                )
            );
        }
        $fields_form['form']['input'][] = array(
            'type'  => 'categories',
            'label' => $this->l('Choose categories'),
            'name'  => 'id_parent',
            'tree'  => array(
                'id'                  => 'categories-tree',
                'selected_categories' => array(),
                'disabled_categories' => null,
                'root_category'       => $root->id,
                'use_checkbox'        => $multi,
                'use_search'          => true,
            )
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Build "Association tab" form "Change default category"
     *
     * @return mixed
     */
    public function defaultCategory()
    {
        return $this->categories(false);
    }

    /**
     * Build "Association tab" form "Add accessories"
     *
     * @return mixed
     */
    public function accessories()
    {
        $module = new Tmproductsmanager();
        $this->context->smarty->assign(
            'action',
            AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                'AdminTmProductsManager'
            ).'&action=accessories&process'
        );
        $this->context->smarty->assign(
            'back',
            AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                'AdminTmProductsManager'
            ).'&action=accessories&filter'
        );

        return $module->display($module->path, 'views/templates/admin/_forms/accessories.tpl');
    }
    /*************************************** end of Association tab ***************************************************/
    /**************************************** Quantity tab ************************************************************/
    /**
     * Build "Quantity tab" form "Change quantity"
     *
     * @return mixed
     */
    public function quantity()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select method'),
                        'name'    => 'method',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'increase', 'name' => $this->l('Increase current quantity on')
                                ),
                                array(
                                    'id' => 'decrease', 'name' => $this->l('Decrease current quantity on')
                                ),
                                array(
                                    'id' => 'set', 'name' => $this->l('Set current quantity')
                                ),
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                    array(
                        'type'  => 'text',
                        'label' => $this->l('Quantity'),
                        'name'  => 'quantity',
                        'desc'  => $this->l('Type a quantity value(only integer)'),
                        'col'   => 3
                    )
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=quantity&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }
    /**************************************** end of Quantity tab *****************************************************/

    /**************************************** start New Actions *******************************************************/
    /**
     * Build "Actions tab" form "Add action"
     *
     * @return string
     */
    public function newaction()
    {
        $content = $this->actionsPageForm();
        if (Tools::getValue('id_page')) {
            $content .= $this->actionsBannerForm();
            $content .= $this->actionsBannersList();
        }
        return $content;
    }

    /**
     * Main form of generating page
     *
     * @return mixed
     */
    public function actionsPageForm()
    {
        $pm = new Tmproductsmanager(); // Tmproductsmanager class's instance
        $id_page = Tools::getValue('id_page');
        // get all voucher(CartRule) info if it exists for current page
        $voucher_amount = false;
        $gift_product_filter = false;
        $gift_product_select = false;
        $gift_product_attribute_select = false;
        $hasAttribute = false;
        $page = new PMPage($id_page);
        if ($page && ($page->type != 'none' && $page->type != 'time') && $page->voucher) {
            $voucher_amount = new CartRule($page->voucher);
            if ($voucher_amount->gift_product) {
                $product = new Product($voucher_amount->gift_product, false, $this->context->language->id);
                $hasAttribute = $product->hasAttributes();
                $gift_product_filter = (!empty($product->reference) ? $product->reference : $product->name);
                $search_products = $this->searchProducts($gift_product_filter);
                if (isset($search_products['products']) && is_array($search_products['products'])) {
                    foreach ($search_products['products'] as $product) {
                        $gift_product_select .= '<option value="'.$product['id_product'].'" '.($product['id_product'] == $voucher_amount->gift_product ? 'selected="selected"' : '').'>
						'.$product['name'].(count($product['combinations']) == 0 ? ' - '.$product['formatted_price'] : '').'</option>';
                        if (count($product['combinations'])) {
                            $gift_product_attribute_select .= '<select class="control-form id_product_attribute " id="ipa_'.$product['id_product'].'" name="ipa_'.$product['id_product'].'" '.($voucher_amount->gift_product != $product['id_product'] ? 'style="display:none;"' : '').'>';
                            foreach ($product['combinations'] as $combination) {
                                $gift_product_attribute_select .= '<option '.($combination['id_product_attribute'] == $voucher_amount->gift_product_attribute ? 'selected="selected"' : '').' value="'.$combination['id_product_attribute'].'">
								'.$combination['attributes'].' - '.$combination['formatted_price'].'</option>';
                            }
                            $gift_product_attribute_select .= '</select>';
                        }
                    }
                }
            }
        }

        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'input'   => array(
                    array(
                        'type'    => 'text',
                        'label'   => $this->l('Page name'),
                        'name'    => 'name',
                        'desc'    => $this->l('Enter a page name.'),
                        'required' => true,
                        'col'     => '3',
                        'lang'    => true,
                    ),
                    array(
                        'type'    => 'text',
                        'label'   => $this->l('Friendly URL'),
                        'name'    => 'friendly_url',
                        'desc'    => $this->l('Enter friendly URL.'),
                        'required' => true,
                        'col'     => '3',
                        'lang'    => true,
                        'hint'    => $this->l('Only letters, numbers, underscore (_) and the minus (-) character are allowed.')
                    ),
                    array(
                        'type'    => 'text'.($id_page ? '' : ' hidden'),
                        'label'   => $this->l('Front-end URL'),
                        'name'    => 'front_url',
                        'desc'    => $this->l('This URL will be using in front-end. Use it if you want add custom link anywhere.'),
                        'col'     => '6',
                        'lang'    => true
                    ),
                    array(
                        'type' => 'textarea',
                        'label'   => $this->l('Page description'),
                        'name'    => 'description',
                        'desc'    => $this->l('Enter a page description.'),
                        'lang'    => true,
                        'autoload_rte' => true
                    ),
                    array(
                        'type'  => 'img_filemanager',
                        'name'  => 'page_image',
                        'label' => $this->l('Page image'),
                        'description'  => $this->l('Select an image which will display in top of the page.')
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'type',
                        'label' => $this->l('Type of offer'),
                        'desc' => $this->l('Select special offer type'),
                        'options'          => array(
                            'query' => array(
                                array('id' => 'none', 'type' => $this->l('Simple')),
                                array('id' => 'time', 'type' => $this->l('Limited time offer')),
                                array('id' => 'voucher_amount', 'type' => $this->l('Amount voucher')),
                                array('id' => 'voucher_percent', 'type' => $this->l('Percent voucher')),
                                array('id' => 'free_shipping', 'type' => $this->l('Free shipping')),
                                array('id' => 'free_gift', 'type' => $this->l('Free gift'))
                            ),
                            'id'    => 'id',
                            'name'  => 'type'
                        )
                    ),
                    array(
                        'form_group_class' => 'hideable time-field',
                        'type' => 'datetime',
                        'name' => 'date_from',
                        'required' => true,
                        'label' => $this->l('Select start date')
                    ),
                    array(
                        'form_group_class' => 'hideable time-field',
                        'type' => 'datetime',
                        'name' => 'date_to',
                        'required' => true,
                        'label' => $this->l('Select end date'),
                    ),
                    array(
                        'form_group_class' => 'hideable voucher_amount-field free_shipping-field voucher_percent-field time-field free_gift-field',
                        'type'   => 'switch',
                        'label'  => $this->l('Display countdown'),
                        'name'   => 'countdown',
                        'values' => array(
                            array(
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'form_group_class' => 'hideable voucher_amount-field free_shipping-field free_gift-field voucher_percent-field',
                        'type' => 'select',
                        'name' => 'display_code',
                        'label' => $this->l('Display voucher code'),
                        'desc' => $this->l('Select where voucher code will be displayed in the product information.'),
                        'options'          => array(
                            'query' => array(
                                array('id' => 0, 'type' => $this->l('Nowhere')),
                                array('id' => 1, 'type' => $this->l('Listing: this product page')),
                                array('id' => 2, 'type' => $this->l('Listing: every pages')),
                                array('id' => 3, 'type' => $this->l('Listing: not this product page')),
                                array('id' => 4, 'type' => $this->l('Listing: this product page; + Product page')),
                                array('id' => 5, 'type' => $this->l('Listing: every pages; + Product page')),
                                array('id' => 6, 'type' => $this->l('Listing: not this product page; + Product page')),
                                array('id' => 7, 'type' => $this->l('Product page'))
                            ),
                            'id'    => 'id',
                            'name'  => 'type'
                        )
                    ),
                    array(
                        'form_group_class' => 'hideable free_gift-field',
                        'type'  => 'select',
                        'name'  => 'display_gift',
                        'label' => $this->l('Display gift information'),
                        'desc'  => $this->l('Select where voucher gift product\'s information will be displayed in the product information.'),
                        'options'          => array(
                            'query' => array(
                                array('id' => 0, 'type' => $this->l('Nowhere')),
                                array('id' => 1, 'type' => $this->l('Listing: this product page')),
                                array('id' => 2, 'type' => $this->l('Listing: every pages')),
                                array('id' => 3, 'type' => $this->l('Listing: not this product page')),
                                array('id' => 4, 'type' => $this->l('Listing: this product page; + Product page')),
                                array('id' => 5, 'type' => $this->l('Listing: every pages; + Product page')),
                                array('id' => 6, 'type' => $this->l('Listing: not this product page; + Product page')),
                                array('id' => 7, 'type' => $this->l('Product page'))
                            ),
                            'id'    => 'id',
                            'name'  => 'type'
                        )
                    ),
                    array(
                        'form_group_class' => 'hideable voucher_amount-field',
                        'type'   => 'voucher_amount',
                        'label'  => $this->l('Set voucher'),
                        'data'   => $voucher_amount,
                        'name'   => ''
                    ),
                    array(
                        'form_group_class' => 'hideable voucher_percent-field',
                        'type'   => 'voucher_percent',
                        'label'  => $this->l('Set voucher'),
                        'data'   => $voucher_amount,
                        'name'   => ''
                    ),
                    array(
                        'form_group_class' => 'hideable free_shipping-field',
                        'type'   => 'free_shipping',
                        'label'  => $this->l('Set voucher'),
                        'data'   => $voucher_amount,
                        'name'   => ''
                    ),
                    array(
                        'form_group_class' => 'hideable free_gift-field',
                        'type'   => 'free_gift',
                        'label'  => $this->l('Set voucher'),
                        'data'   => $voucher_amount,
                        'name'   => ''
                    ),
                    array(
                        'type'        => 'layouts',
                        'name'        => 'layout',
                        'label'       => $this->l('Select page layout'),
                        'description' => $this->l('Select makeup which will be display in the front-end'),
                        'layouts'      => $pm->getAdminPageLayouts() // get templates for layouts(back-end preview) located in views/templates/admin/_layouts/
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Active'),
                        'name'   => 'active',
                        'values' => array(
                            array(
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'filters'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'products'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'tmexcludedproducts'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_page'
                    )
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'name' => 'saveNstay',
                        'type' => 'submit',
                        'title' => $this->l('Save and Stay'),
                        'icon'  => 'process-icon-save',
                        'class' => 'btn btn-default pull-right'
                    ),
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=newaction&filter'.(Tools::getValue('id_page') ? '&id_page='.Tools::getValue('id_page') : ''),
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;
        $helper->tpl_vars = array(
            'fields_value'                  => $this->getPageFormValues(),
            'languages'                     => $this->context->controller->getLanguages(),
            'id_language'                   => $this->context->language->id,
            'img_src'                       => __PS_BASE_URI__,
            'currencies'                    => Currency::getCurrencies(false, true),
            'giftProductFilter'           => $gift_product_filter,
            'gift_product_select'           => $gift_product_select,
            'gift_product_attribute_select' => $gift_product_attribute_select,
            'hasAttribute'                  => $hasAttribute
        );

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Main form of page's banner creating
     *
     * @return mixed
     */
    public function actionsBannerForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => (Tools::getIsset('id_banner') && Tools::getValue('id_banner')
                        ? $this->l('Update banner')
                        : $this->l('Create banner for this page')),
                    'icon'  => 'icon-cogs',
                ),
                'input'   => array(
                    array(
                        'type'    => 'text',
                        'label'   => $this->l('Banner name'),
                        'name'    => 'banner_name',
                        'desc'    => $this->l('Enter the banner name.'),
                        'required' => true,
                        'col'     => '3',
                        'lang'    => true,
                    ),
                    array(
                        'form_group_class' => 'tmproductsmanager-actions-form short-multiselect',
                        'type' => 'multiselect',
                        'name' => 'pages',
                        'filter' => $this->pages,
                        'label' => $this->l('Display on page(s)'),
                        'description' => $this->l('Select pages on which banner will be display.')
                    ),
                    array(
                        'form_group_class' => 'tmproductsmanager-actions-form',
                        'type' => 'multiselect',
                        'name' => 'position',
                        'label' => $this->l('Display on position(s)'),
                        'filter' => $this->hooks,
                        'description' => $this->l('Select positions on which banner will be display.')
                    ),
                    array(
                        'type' => 'img_filemanager',
                        'name' => 'page_banner',
                        'label' => $this->l('Banner image'),
                        'description' => $this->l('Select an image which will be a banner the will refer to for this page.')
                    ),
                    array(
                        'type' => 'textarea',
                        'label'   => $this->l('Banner description'),
                        'name'    => 'banner_description',
                        'desc'    => $this->l('Enter a banner description.'),
                        'lang'    => true,
                        'autoload_rte' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Specific class'),
                        'name'  => 'specific_class',
                        'col' => 3
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Display countdown'),
                        'name'   => 'banner_countdown',
                        'desc'   => $this->l('Will display countdown in banner if it exists for current page'),
                        'values' => array(
                            array(
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Active'),
                        'name'   => 'banner_active',
                        'values' => array(
                            array(
                                'id'    => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_page'
                    ),
                    array(
                        'type' => 'hidden',
                        'name' => 'id_banner'
                    )
                ),
                'buttons' => array(
                    array(
                        'name' => (Tools::getIsset('id_banner') && Tools::getValue('id_banner') ? 'TMPMUpdateBanner' : 'TMPMAddBanner'),
                        'type' => 'submit',
                        'title' => (Tools::getIsset('id_banner') && Tools::getValue('id_banner') ?  $this->l('Update banner') : $this->l('Add banner')),
                        'icon'  => 'process-icon-save',
                        'class' => 'btn btn-default pull-right'
                    ),
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=newaction&filter'.(Tools::getValue('id_page') ? '&id_page='.Tools::getValue('id_page') : ''),
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;
        $helper->tpl_vars = array(
            'fields_value' => $this->getBannerFormValues(),
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id,
            'img_src'  => __PS_BASE_URI__
        );

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Fill page form
     *
     * @return array
     */
    public function getPageFormValues()
    {
        $front_url = false;
        $id_page = Tools::getValue('id_page');
        if ($id_page) {
            $page = new PMPage((int)$id_page);
            foreach ($page->friendly_url as $key => $url) {
                $front_url[$key] = Tmproductsmanager::getTMProductsmanagerLink('tmproductsmanager', array('id_page' => $id_page, 'title' => $url), $key);
            }
        } else {
            $page = new PMPage();
        }
        $fields_values = array(
            'id_page'            => Tools::getValue('id_page', $id_page),
            'filters'            => Tools::getValue('filters', $page->filters),
            'tmexcludedproducts' => Tools::getValue('tmexcludedproducts', $page->tmexcludedproducts),
            'products'           => Tools::getValue('products', $page->products),
            'name'               => Tools::getValue('name', $page->name),
            'friendly_url'       => Tools::getValue('friendly_url', $page->friendly_url),
            'description'        => Tools::getValue('description', $page->description),
            'page_image'         => Tools::getValue('page_image', $page->page_image),
            'layout'             => Tools::getValue('layout', ($page->layout ? $page->layout : 'default')),
            'active'             => Tools::getValue('active', $page->active),
            'type'               => Tools::getValue('type', $page->type),
            'date_from'          => Tools::getValue('date_from', $page->date_from),
            'date_to'            => Tools::getValue('date_to', $page->date_to),
            'countdown'          => Tools::getValue('countdown', $page->countdown),
            'front_url'          => $front_url,
            'display_code'       => Tools::getValue('display_code', $page->display_code),
            'display_gift'       => Tools::getValue('display_gift', $page->display_gift)
        );

        return $fields_values;
    }

    /**
     * Fill banner form
     *
     * @return array
     */
    public function getBannerFormValues()
    {
        $id_banner = Tools::getValue('id_banner');
        if ($id_banner) {
            $banner = new PMPageBanner((int)$id_banner);
        } else {
            $banner = new PMPageBanner();
        }
        $fields_values = array(
            'id_banner'          => Tools::getValue('id_banner', $id_banner),
            'banner_name'        => Tools::getValue('banner_name', $banner->banner_name),
            'id_page'            => Tools::getValue('id_page', $banner->id_page),
            'position'           => Tools::getValue('position', explode(',', $banner->position)),
            'pages'              => Tools::getValue('pages', explode(',', $banner->pages)),
            'banner_description' => Tools::getValue('banner_description', $banner->banner_description),
            'page_banner'        => Tools::getValue('page_banner', $banner->page_banner),
            'specific_class'     => Tools::getValue('specific_class', $banner->specific_class),
            'banner_active'      => Tools::getValue('banner_active', $banner->banner_active),
            'banner_countdown'   => Tools::getValue('banner_countdown', $banner->banner_countdown)
        );

        return $fields_values;
    }

    /**
     * Generate list of created banners
     *
     * @return bool
     */
    public function actionsBannersList()
    {
        $id_page = Tools::getValue('id_page');
        $banners = PMPageBanner::getPageBanners($id_page, $this->context->language->id);

        if (!$banners) {
            return false;
        }

        $fields_list = array(
            'id_banner' => array(
                'title' => $this->l('ID Banner'),
                'type'  => 'text',
            ),
            'banner_name'       => array(
                'title' => $this->l('Banner name'),
                'type'  => 'text',
            ),
            'position'       => array(
                'title' => $this->l('Banner position(s)'),
                'type'  => 'text',
                'class' => 'pm-fixed'
            ),
            'pages'       => array(
                'title' => $this->l('Banner page(s)'),
                'type'  => 'text',
                'class' => 'pm-fixed'
            ),
            'banner_active' => array(
                'title' => $this->l('Active'),
                'type'   => 'bool',
                'align'  => 'center',
                'active' => 'status'
            )
        );
        $helper = new HelperList();
        $helper->title = $this->l('List of banners for current page');
        $helper->shopLinkType = '';
        $helper->identifier = 'id_banner';
        $helper->table = 'updatebanner';
        $helper->actions = array('edit', 'delete');
        $helper->listTotal = count($banners);
        $helper->show_toolbar = true;
        $helper->simple_header = true;
        $helper->show_filters = false;
        $helper->token = Tools::getAdminTokenLite('AdminTmProductsManager');
        $helper->currentIndex = AdminController::$currentIndex.'&action=newaction&process&updatetmproductsmanager_pages&id_page='.(int)$id_page;
        return $helper->generateList($banners, $fields_list);
    }

    protected function searchProducts($search)
    {
        if ($products = Product::searchByName((int)$this->context->language->id, $search)) {
            foreach ($products as &$product) {
                $combinations = array();
                $productObj = new Product((int)$product['id_product'], false, (int)$this->context->language->id);
                $attributes = $productObj->getAttributesGroups((int)$this->context->language->id);
                $product['formatted_price'] = Tools::displayPrice(Tools::convertPrice($product['price_tax_incl'], $this->context->currency), $this->context->currency);

                foreach ($attributes as $attribute) {
                    if (!isset($combinations[$attribute['id_product_attribute']]['attributes'])) {
                        $combinations[$attribute['id_product_attribute']]['attributes'] = '';
                    }
                    $combinations[$attribute['id_product_attribute']]['attributes'] .= $attribute['attribute_name'].' - ';
                    $combinations[$attribute['id_product_attribute']]['id_product_attribute'] = $attribute['id_product_attribute'];
                    $combinations[$attribute['id_product_attribute']]['default_on'] = $attribute['default_on'];
                    if (!isset($combinations[$attribute['id_product_attribute']]['price'])) {
                        $price_tax_incl = Product::getPriceStatic((int)$product['id_product'], true, $attribute['id_product_attribute']);
                        $combinations[$attribute['id_product_attribute']]['formatted_price'] = Tools::displayPrice(Tools::convertPrice($price_tax_incl, $this->context->currency), $this->context->currency);
                    }
                }

                foreach ($combinations as &$combination) {
                    $combination['attributes'] = rtrim($combination['attributes'], ' - ');
                }
                $product['combinations'] = $combinations;
            }
            return array(
                'products' => $products,
                'found' => true
            );
        } else {
            return array('found' => false, 'notfound' => Tools::displayError('No product has been found.'));
        }
    }
    /***************************************** end New Actions ********************************************************/
}
