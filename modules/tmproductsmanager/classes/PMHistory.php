<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMHistory extends AdminController
{
    public $db_prefix;
    public $user;
    public $value;
    public $shop;

    public function __construct($db_prefix, $user)
    {
        $this->db_prefix = $db_prefix;
        $this->user = $user->employee->id_profile;
        $this->shop = $user->shop->id;
    }

    /**
     * Get value by key
     *
     * @param $key
     *
     * @return false|null|string
     */
    public function get($key)
    {
        $sql = "SELECT `value`
            FROM {$this->db_prefix}tmproductsmanager_history
            WHERE `id_employee` = {$this->user}
            AND `key` = '{$key}'
            AND `id_shop` = {$this->shop}";

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Check if key value already exists
     *
     * @param $key
     *
     * @return false|null|string
     */
    public function exists($key)
    {
        $sql = "SELECT `id_tmproductsmanager`
            FROM {$this->db_prefix}tmproductsmanager_history
            WHERE `id_employee` = {$this->user}
            AND `key` = '{$key}'
            AND `id_shop` = {$this->shop}";

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Set new key value
     *
     * @param $key
     * @param $value
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    public function set($key, $value)
    {
        $result = true;
        if (!$this->exists($key)) {
            $result &= Db::getInstance()->insert(
                'tmproductsmanager_history',
                array(
                    'id_shop' => $this->shop,
                    'id_employee' => $this->user,
                    'key' => $key,
                    'value' => $value,
                    'date' => date('Y/m/d')
                )
            );
        } else {
            $result &= $this->update($key, $value);
        }

        return $result;
    }

    /**
     * Update existed key value
     *
     * @param $key
     * @param $value
     *
     * @return bool
     */
    public function update($key, $value)
    {
        return Db::getInstance()->update(
            'tmproductsmanager_history',
            array(
                'value' => $value,
                'date' => date('Y/m/d')
            ),
            '`id_employee` = '.$this->user.' AND `key` ="'. $key.'" AND `id_shop` = '.$this->shop
        );
    }

    /**
     * Delete key value
     *
     * @param $key
     *
     * @return bool
     */
    public function delete($key)
    {
        return Db::getInstance()->delete(
            'tmproductsmanager_history',
            '`id_employee` = '.$this->user.' AND `key` = "'.$key.'" AND `id_shop` = '.$this->shop
        );
    }

    /**
     * Clear all history table
     *
     * @return bool
     */
    public function clear()
    {
        return Db::getInstance()->delete(
            'tmproductsmanager_history',
            '`id_employee` = '.$this->user.' AND `id_shop` = '.$this->shop
        );
    }
}
