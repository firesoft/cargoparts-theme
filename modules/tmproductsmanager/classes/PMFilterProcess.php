<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMFilterProcess
{
    /*
     *  Main class responsible for filtering products by selected parameters
     */
    public $db_prefix;    // date base prefix used in store
    public $shop;         // shop information(like: id, name etc.)
    public $language;     // language information (like: id, name etc.)
    public $query;        // type of date base query
    public $excluded_ids; // ids of products that are excluded from result

    public function __construct($db_prefix, Shop $shop, Language $language, $excluded_ids)
    {
        $this->shop = $shop;
        $this->language = $language;
        $this->db_prefix = $db_prefix;
        $this->query = 'SELECT ';
        $this->excluded_ids = $excluded_ids;
    }

    /**
     * Query start: defines main static query parameters
     * @return string
     */
    public function start()
    {
        return "SELECT DISTINCT p.`id_product`, pl.name
                FROM {$this->db_prefix}product p
                LEFT JOIN {$this->db_prefix}product_lang pl
                ON p.`id_product` = pl.`id_product`
                LEFT JOIN {$this->db_prefix}product_shop ps
                ON p.`id_product` = ps.`id_product`";
    }

    /**
     * Query end: defines main static query parameters(like ordering) and excludes defined products ids
     * @return string
     */
    public function end()
    {
        $excluded = '';
        if (isset($this->excluded_ids) && $this->excluded_ids) {
            $excluded = " AND p.`id_product` NOT IN ($this->excluded_ids)";
        }

        return $excluded." AND pl.`id_lang` = {$this->language->id} AND ps.`id_shop` = {$this->shop->id} AND pl.name != '' ORDER BY p.`id_product`";
    }

    /**
     * Adds information about selected products' category
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joincategories($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT DISTINCT cp.`id_product` FROM {$this->db_prefix}category_product cp
                    WHERE cp.`id_category` IN ({$ids})) id_cat
                    ON p.`id_product` = id_cat.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' manufacturers
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinmanufacturers($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT DISTINCT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`id_manufacturer` IN ({$ids})) id_man
                    ON p.`id_product` = id_man.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' suppliers
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinsuppliers($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT DISTINCT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`id_supplier` IN ({$ids})) id_sup
                    ON p.`id_product` = id_sup.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' attributes
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinattributes($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT DISTINCT pa.`id_product` FROM {$this->db_prefix}product_attribute pa
                    LEFT JOIN {$this->db_prefix}product_attribute_combination pac
                    ON pa.`id_product_attribute` = pac.`id_product_attribute` WHERE pac.`id_attribute` IN ({$ids})) id_pac
                    ON p.`id_product` = id_pac.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' features
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinfeatures($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT DISTINCT fp.`id_product` FROM {$this->db_prefix}feature_product fp
                    WHERE fp.`id_feature_value` IN ({$ids})) id_feat
                    ON p.`id_product` = id_feat.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' conditions
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinconditions($ids)
    {
        $ids = '"'.implode('","', $ids).'"';
        $query = " JOIN (
                    SELECT DISTINCT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`condition` IN ($ids)) id_cond
                    ON p.`id_product` = id_cond.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' images
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinimages($ids)
    {
        if ($ids[0] == 1) {
            $query = " JOIN (
                        SELECT DISTINCT p.`id_product` FROM {$this->db_prefix}product p
                        RIGHT JOIN {$this->db_prefix}image i
                        ON i.`id_product` = p.`id_product`
                        WHERE i.`id_image` IS NOT NULL) img_stat
                    ON p.`id_product` = img_stat.`id_product`";
        } else {
            $query = " JOIN (
                        SELECT p.`id_product`, i.`id_image` FROM {$this->db_prefix}product p
                        LEFT JOIN {$this->db_prefix}image i
                        ON i.`id_product` = p.`id_product`
                        WHERE i.`id_image` IS NULL) img_stat
                    ON p.`id_product` = img_stat.`id_product`";
        }

        return $query;
    }

    /**
     * Adds information about selected products' on sale
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinonsale($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`on_sale` IN ($ids)) id_sale
                   ON p.`id_product` = id_sale.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' active
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinactive($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`active` IN ($ids)) id_active
                   ON p.`id_product` = id_active.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' customizable
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joincustomizable($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`customizable` IN ($ids)) id_customizable
                   ON p.`id_product` = id_customizable.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' online only
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinonlineonly($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`online_only` IN ($ids)) id_online
                   ON p.`id_product` = id_online.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' that are available for order
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinorder($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`available_for_order` IN ($ids)) id_order
                   ON p.`id_product` = id_order.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' that are displays price
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinsprice($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`show_price` IN ($ids)) show_price
                   ON p.`id_product` = show_price.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' that are virtual or not
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinvirtual($ids)
    {
        $ids = implode(',', $ids);
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`is_virtual` IN ($ids)) id_virtual
                   ON p.`id_product` = id_virtual.`id_product`";

        return $query;
    }

    /**
     * Adds information about selected products' that visible or not
     * @param $ids that are selected in a filter
     *
     * @return string
     */
    public function joinvisibility($ids)
    {
        $ids = '"'.implode('","', $ids).'"';
        $query = " JOIN (
                    SELECT p.`id_product` FROM {$this->db_prefix}product p
                    WHERE p.`visibility` IN ($ids)) id_visibility
                   ON p.`id_product` = id_visibility.`id_product`";

        return $query;
    }

    /**
     * Run built SQL query
     * @param $query
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public function process($query)
    {
        return DB::getInstance()->executeS($query);
    }
}
