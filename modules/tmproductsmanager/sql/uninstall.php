<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

/**
* In some cases you should not drop the tables.
* Maybe the merchant will just try to reset the module
* but does not want to loose all of the data associated to the module.
*/

$sql = array();

$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_history`';
$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_pages`';
$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_pages_products`';
$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_pages_lang`';
$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_banners`';
$sql[] = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'tmproductsmanager_banners_lang`';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
