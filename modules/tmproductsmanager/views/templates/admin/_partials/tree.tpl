{**
* 2002-2017 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<div class="col-xs-5 indent-bottom">
  <a id="tmproductsmanagerSelectedCategories" class="btn btn-sm btn-default" data-toggle="modal" data-target="#myModal">
    <span>
      {if isset($selected['categoryBox']) && $selected['categoryBox']}
        {$selected['categoryBox']|count}
        {l s='selected' mod='tmproductsmanager'}
      {else}
        {l s='empty' mod='tmproductsmanager'}
      {/if}
    </span>
    <b class="caret"></b>
  </a>
</div>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{l s='Select categories' mod='tmproductsmanager'}</h4>
      </div>
      <div class="modal-body">
        {$filter}
      </div>
      <div class="modal-footer clearfix">
        <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Done' mod='tmproductsmanager'}</button>
      </div>
    </div>
  </div>
</div>
