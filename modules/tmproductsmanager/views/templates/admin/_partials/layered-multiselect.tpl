{**
* 2002-2017 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}


{assign var="selected_name" value="tmpm_`$name`"}
<div class="row">
  {foreach from=$filter key=index item=filter_ell}
    <div class="multiselect-box clearfix">
      <label class="col-xs-6">{$filter_ell.info.name|escape:'html':'UTF-8'}{if isset($filter_ell.info.public_name) && $filter_ell.info.public_name}({$filter_ell.info.public_name|escape:'html':'UTF-8'}){/if}</label>
      <div class="col-xs-5 indent-bottom">
        <select id="multiselect-{$name|escape:'html':'UTF-8'}-{$index|escape:'html':'UTF-8'}" class="multiselect-{$name|escape:'html':'UTF-8'}" multiple="multiple" name="tmpm_{$name|escape:'html':'UTF-8'}[]">
          {if $filter_ell.items}
            {foreach from=$filter_ell.items item=ell}
              <option {if isset($selected[$selected_name]) && $ell.id|in_array:$selected[$selected_name]}selected="selected"{/if} value="{$ell.id|escape:'htmlall':'UTF-8'}">{$ell.name|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
          {else}
            <optgroup label="{l s='nothing to select' mod='tmproductsmanager'}"></optgroup>
          {/if}
        </select>
      </div>
      <div class="hidden empty-text" data-empty-text="{l s='empty' mod='tmproductsmanager'}"></div>
    </div>
    <script type="text/javascript">
      $(document).ready(function() {
        tmproductsmanager.multiselectInit($('#multiselect-{$name|escape:"html":"UTF-8"}-{$index|escape:"html":"UTF-8"}'), '{$name|escape:"html":"UTF-8"}');
      });
    </script>
  {/foreach}
</div>