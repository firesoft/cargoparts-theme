{**
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{extends file="helpers/list/list_footer.tpl"}
{block name="footer"}
  <div class="panel-footer">
    {foreach from=$toolbar_btn item=btn key=k}
      <a id="desc-{$table|escape:'html':'UTF-8'}-{if isset($btn.imgclass)}{$btn.imgclass|escape:'html':'UTF-8'}{else}{$k|escape:'html':'UTF-8'}{/if}" class="btn btn-default{if isset($btn.target) && $btn.target}  _blank{/if} {if isset($btn.class) && $btn.class}{$btn.class|escape:'html':'UTF-8'}{/if}"{if isset($btn.href)} href="{$btn.href|escape:'html':'UTF-8'}"{/if}{if isset($btn.js) && $btn.js} onclick="{$btn.js|escape:'html':'UTF-8'}"{/if}>
        <i class="process-icon-{if isset($btn.type)}{$btn.type|escape:'html':'UTF-8'}{/if}"></i>
        <span {if isset($btn.force_desc) && $btn.force_desc == true } class="locked" {/if}>{$btn.desc|escape:'html':'UTF-8'}</span>
      </a>
    {/foreach}
  </div>
{/block}
