{*
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     TemplateMonster
* @copyright  2002-2017 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{extends file="helpers/form/form.tpl"}
{block name="field"}
  {if $input.type == 'img_filemanager'}
    {foreach from=$languages item=language}
      {if $languages|count > 1}
        <div class="translatable-field lang-{$language.id_lang|escape:'htmlall':'UTF-8'}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
      {/if}
      <div class="col-lg-6">
        {if $fields_value[$input.name][$language.id_lang]}
          <div class="img-manager-image">
            <img class="img-responsive {$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}" src="{$img_src|escape:'html':'UTF-8'}{$fields_value[$input.name][$language.id_lang]|escape:'html':'UTF-8'}" alt="{$input.name|escape:'html':'UTF-8'}"/>
          </div>
        {/if}
        <div class="row">
          <div class="input-group">
            <input
                    disabled="disabled"
                    data-name="bgimg"
                    class="form-control"
                    name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}"
                    id="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}"
                    value="{$fields_value[$input.name][$language.id_lang|escape:'htmlall':'UTF-8']}"/>
            <input type="hidden" name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}" value="{$fields_value[$input.name][$language.id_lang|escape:'htmlall':'UTF-8']}"/>
            <span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
            <span class="input-group-addon">
              <a
                      href="filemanager/dialog.php?type=1&field_id={$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}"
                      data-input-name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}"
                      type="button"
                      class="iframe-btn">
                <span class="icon-file"></span>
              </a>
            </span>
          </div>
          <p class="help-block">{$input.description|escape:'html':'UTF-8'}</p>
        </div>
        {literal}
        <script type="text/javascript">
          var base_url = '{/literal} {$base_url|escape:"html":"UTF-8"} {literal}';
        </script> {/literal}
      </div>
      {if $languages|count > 1}
        <div class="col-lg-2">
          <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
            {$language.iso_code|escape:'htmlall':'UTF-8'}
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            {foreach from=$languages item=lang}
              <li>
                <a href="javascript:hideOtherLanguage({$lang.id_lang|escape:'htmlall':'UTF-8'});" tabindex="-1">{$lang.name|escape:'htmlall':'UTF-8'}</a>
              </li>
            {/foreach}
          </ul>
        </div>
      {/if}
      {if $languages|count > 1}
        </div>
      {/if}
    {/foreach}
  {/if}
  {if $input.type == 'multiselect'}
    <div class="col-lg-9">
      <div class="multiselect-box">
        <select id="multiselect-{$input.name|escape:'html':'UTF-8'}" class="multiselect-{$input.name|escape:'html':'UTF-8'}" multiple="multiple" name="{$input.name|escape:'html':'UTF-8'}[]">
          {foreach from=$input.filter item='filter'}
            <option {if isset($fields_value[$input.name]) && $filter.id|in_array:$fields_value[$input.name]}selected="selected"{/if} value="{$filter.id|escape:'htmlall':'UTF-8'}">{$filter.type|escape:'htmlall':'UTF-8'}</option>
          {/foreach}
        </select>
        <div class="hidden empty-text" data-empty-text="{l s='empty' mod='tmproductsmanager'}"></div>
      </div>
      {if isset($input.description)}<p class="help-block">{$input.description|escape:'htmlall':'UTF-8'}</p>{/if}
    </div>
    {addJsDefL name='tmproductsmanagerSelectAllText'}{l s='Select all' mod='tmproductsmanager'}{/addJsDefL}
    <script type="text/javascript">
      $(document).ready(function() {
        tmproductsmanager.multiselectInit($('#multiselect-{$input.name|escape:"html":"UTF-8"}'), '{$input.name|escape:"html":"UTF-8"}');
      });
    </script>
  {/if}
  {if $input.type == 'layouts'}
    <div class="col-lg-7">
      <button id="tmpm-layout-select" class="btn btn-default" type="button">
        {l s='Select layout' mod='tmproductsmanager'}
      </button>
      <ul class="layouts-list">
        {foreach from=$input.layouts key=name item='layout'}
          <li data-layout="{$name|escape:'html':'UTF-8'}" class="{if $fields_value[$input.name] != $name}hide{else}active{/if}">{$layout|escape:'quotes':'UTF-8'}</li>
        {/foreach}
      </ul>
      <input type="hidden" name="layout" value="{$fields_value[$input.name]|escape:'html':'UTF-8'}"/>
    </div>
  {/if}
  {if $input.type == 'voucher_amount'}
    <div class="col-lg-6 voucher-area">
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='This is the code users should enter to apply the voucher to a cart. Either create your own code or generate one by clicking on "Generate".' mod='tmproductsmanager'}">{l s='Code' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <div class="input-group col-lg-12">
                <input type="text" name="voucher_amount_code" value="{if isset($input.data->code) && $input.data->code}{$input.data->code|escape:'quotes':'UTF-8'}{/if}"/>
                <span class="input-group-btn">
                  <a class="btn btn-default generate-code" data-name="voucher_amount_code" href="#">
                    <i class="icon-random"></i>
                    {l s='Generate' mod='tmproductsmanager'}
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Cart rules are applied by priority. A cart rule with a priority of "1" will be processed before a cart rule with a priority of "2".' mod='tmproductsmanager'}">{l s='Priority' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_amount_priority" value="{if isset($input.data->priority) && $input.data->priority}{$input.data->priority|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">{l s='Amount' mod='tmproductsmanager'}</label>
            <div class="col-lg-8">
              <input
                      type="text"
                      name="voucher_amount_amount"
                      value="{if isset($input.data->reduction_amount) && $input.data->reduction_amount}{Tmproductsmanager::convertToInt($input.data->reduction_amount|escape:'html':'UTF-8')}{/if}" />
            </div>
          </div>
        </div>
        <div class="col-lg-2">
          <div class="col-lg-12">
            <select name="voucher_amount_reduction_currency">
              {foreach from=$currencies item=currency}
                <option {if isset($input.data->reduction_currency) && $input.data->reduction_currency == $currency.id_currency}selected="selected"{/if} value="{$currency.id_currency|escape:'html':'UTF-8'}">{$currency.name|escape:'html':'UTF-8'}({$currency.iso_code|escape:'html':'UTF-8'})</option>
              {/foreach}
            </select>
          </div>
        </div>
        <div class="col-lg-2">
          <div class="col-lg-12">
            <select name="voucher_amount_reduction_tax">
              <option {if isset($input.data->reduction_tax) && $input.data->reduction_tax == 0}selected="selected"{/if} value="0">{l s='Tax excluded' mod='tmproductsmanager'}</option>
              <option {if isset($input.data->reduction_tax) &&  $input.data->reduction_tax == 1}selected="selected"{/if} value="1">{l s='Tax included' mod='tmproductsmanager'}</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The cart rule will be applied to the first "X" customers only.' mod='tmproductsmanager'}">
                {l s='Total available' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_amount_quantity" value="{if isset($input.data->quantity) && $input.data->quantity}{$input.data->quantity|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='A customer will only be able to use the cart rule "X" time(s).' mod='tmproductsmanager'}">
                {l s='Total available for each user' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_amount_quantity_per_user" value="{if isset($input.data->quantity_per_user) && $input.data->quantity_per_user}{$input.data->quantity_per_user|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-10">
          <label class="control-label col-lg-3 required">
            <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The default period is one month.' mod='tmproductsmanager'}">
              {l s='Valid' mod='tmproductsmanager'}
            </span>
          </label>
          <div class="col-lg-9">
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="voucher_amount_date_from" value="{if isset($input.data->date_from) && $input.data->date_from}{$input.data->date_from|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="voucher_amount_date_to" value="{if isset($input.data->date_to) && $input.data->date_to}{$input.data->date_to|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  {/if}
  {if $input.type == 'voucher_percent'}
    <div class="col-lg-6 voucher-area">
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='This is the code users should enter to apply the voucher to a cart. Either create your own code or generate one by clicking on "Generate".' mod='tmproductsmanager'}">{l s='Code' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <div class="input-group col-lg-12">
                <input type="text" name="voucher_percent_code" value="{if isset($input.data->code) && $input.data->code}{$input.data->code|escape:'qoutes':'UTF-8'}{/if}"/>
                <span class="input-group-btn">
                  <a class="btn btn-default generate-code" data-name="voucher_percent_code" href="#">
                    <i class="icon-random"></i>
                    {l s='Generate' mod='tmproductsmanager'}
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Cart rules are applied by priority. A cart rule with a priority of "1" will be processed before a cart rule with a priority of "2".' mod='tmproductsmanager'}">{l s='Priority' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_percent_priority" value="{if isset($input.data->priority) && $input.data->priority}{$input.data->priority|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">{l s='Value' mod='tmproductsmanager'}</label>
            <div class="col-lg-8">
              <div class="input-group">
                <span class="input-group-addon">%</span>
                <input
                        type="text"
                        name="voucher_percent_percent"
                        value="{if isset($input.data->reduction_percent) && $input.data->reduction_percent}{Tmproductsmanager::convertToInt($input.data->reduction_percent|escape:'html':'UTF-8')}{/if}" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The cart rule will be applied to the first "X" customers only.' mod='tmproductsmanager'}">
                {l s='Total available' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_percent_quantity" value="{if isset($input.data->quantity) && $input.data->quantity}{$input.data->quantity|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='A customer will only be able to use the cart rule "X" time(s).' mod='tmproductsmanager'}">
                {l s='Total available for each user' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="voucher_percent_quantity_per_user" value="{if isset($input.data->quantity_per_user) && $input.data->quantity_per_user}{$input.data->quantity_per_user|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-10">
          <label class="control-label col-lg-3 required">
            <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The default period is one month.' mod='tmproductsmanager'}">
              {l s='Valid' mod='tmproductsmanager'}
            </span>
          </label>
          <div class="col-lg-9">
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="voucher_percent_date_from" value="{if isset($input.data->date_from) && $input.data->date_from}{$input.data->date_from|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="voucher_percent_date_to" value="{if isset($input.data->date_to) && $input.data->date_to}{$input.data->date_to|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  {/if}
  {if $input.type == 'free_shipping'}
    <div class="col-lg-6 voucher-area">
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='This is the code users should enter to apply the voucher to a cart. Either create your own code or generate one by clicking on "Generate".' mod='tmproductsmanager'}">{l s='Code' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <div class="input-group col-lg-12">
                <input type="text" name="free_shipping_code" value="{if isset($input.data->code) && $input.data->code}{$input.data->code|escape:'qoutes':'UTF-8'}{/if}"/>
                <span class="input-group-btn">
                  <a class="btn btn-default generate-code" data-name="free_shipping_code" href="#">
                    <i class="icon-random"></i>
                    {l s='Generate' mod='tmproductsmanager'}
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Cart rules are applied by priority. A cart rule with a priority of "1" will be processed before a cart rule with a priority of "2".' mod='tmproductsmanager'}">{l s='Priority' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_shipping_priority" value="{if isset($input.data->priority) && $input.data->priority}{$input.data->priority|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The cart rule will be applied to the first "X" customers only.' mod='tmproductsmanager'}">
                {l s='Total available' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_shipping_quantity" value="{if isset($input.data->quantity) && $input.data->quantity}{$input.data->quantity|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='A customer will only be able to use the cart rule "X" time(s).' mod='tmproductsmanager'}">
                {l s='Total available for each user' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_shipping_quantity_per_user" value="{if isset($input.data->quantity_per_user) && $input.data->quantity_per_user}{$input.data->quantity_per_user|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-10">
          <label class="control-label col-lg-3 required">
            <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The default period is one month.' mod='tmproductsmanager'}">
              {l s='Valid' mod='tmproductsmanager'}
            </span>
          </label>
          <div class="col-lg-9">
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="free_shipping_date_from" value="{if isset($input.data->date_from) && $input.data->date_from}{$input.data->date_from|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="free_shipping_date_to" value="{if isset($input.data->date_to) && $input.data->date_to}{$input.data->date_to|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  {/if}
  {if $input.type == 'free_gift'}
    <div class="col-lg-6 voucher-area">
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='This is the code users should enter to apply the voucher to a cart. Either create your own code or generate one by clicking on "Generate".' mod='tmproductsmanager'}">{l s='Code' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <div class="input-group col-lg-12">
                <input type="text" name="free_gift_code" value="{if isset($input.data->code) && $input.data->code}{$input.data->code|escape:'qoutes':'UTF-8'}{/if}"/>
                <span class="input-group-btn">
                  <a class="btn btn-default generate-code" data-name="free_gift_code" href="#">
                    <i class="icon-random"></i>
                    {l s='Generate' mod='tmproductsmanager'}
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div id="free_gift_div" class="form-group">
            <label class="control-label col-lg-4">{l s='Search a product' mod='tmproductsmanager'}</label>
            <div class="col-lg-8">
              <div class="input-group col-lg-12">
                <input type="text" id="giftProductFilter" value="{$giftProductFilter}" />
                <span class="input-group-addon"><i class="icon-search"></i></span>
              </div>
            </div>
          </div>
          <div id="gift_products_found" {if $gift_product_select == ''}style="display:none"{/if}>
            <div id="gift_product_list" class="form-group">
              <label class="control-label col-lg-4">{l s='Matching products' mod='tmproductsmanager'}</label>
              <div class="col-lg-8">
                <select name="gift_product" id="gift_product" onclick="tmproductsmanager.displayProductAttributes();" class="control-form">
                  {$gift_product_select}
                </select>
              </div>
            </div>
            <div id="gift_attributes_list" class="form-group" {if !$hasAttribute}style="display:none"{/if}>
              <label class="control-label col-lg-4">{l s='Available combinations' mod='tmproductsmanager'}</label>
              <div class="col-lg-8" id="gift_attributes_list_select">
                {$gift_product_attribute_select}
              </div>
            </div>
          </div>
          <div id="gift_products_err" class="alert alert-warning" style="display:none"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='Cart rules are applied by priority. A cart rule with a priority of "1" will be processed before a cart rule with a priority of "2".' mod='tmproductsmanager'}">{l s='Priority' mod='tmproductsmanager'}</span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_gift_priority" value="{if isset($input.data->priority) && $input.data->priority}{$input.data->priority|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The cart rule will be applied to the first "X" customers only.' mod='tmproductsmanager'}">
                {l s='Total available' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_gift_quantity" value="{if isset($input.data->quantity) && $input.data->quantity}{$input.data->quantity|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8">
          <div class="form-group">
            <label class="control-label col-lg-4 required">
              <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='A customer will only be able to use the cart rule "X" time(s).' mod='tmproductsmanager'}">
                {l s='Total available for each user' mod='tmproductsmanager'}
              </span>
            </label>
            <div class="col-lg-8">
              <input type="text" name="free_gift_quantity_per_user" value="{if isset($input.data->quantity_per_user) && $input.data->quantity_per_user}{$input.data->quantity_per_user|escape:'html':'UTF-8'}{/if}"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-10">
          <label class="control-label col-lg-3 required">
            <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="{l s='The default period is one month.' mod='tmproductsmanager'}">
              {l s='Valid' mod='tmproductsmanager'}
            </span>
          </label>
          <div class="col-lg-9">
            <div class="row">
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="free_gift_date_from" value="{if isset($input.data->date_from) && $input.data->date_from}{$input.data->date_from|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="input-group">
                  <span class="input-group-addon">{l s='From' mod='tmproductsmanager'}</span>
                  <input type="text" data-hex="true" class="datetimepicker" name="free_gift_date_to" value="{if isset($input.data->date_to) && $input.data->date_to}{$input.data->date_to|escape:'html':'UTF-8'}{/if}" />
                  <span class="input-group-addon">
                    <i class="icon-calendar-empty"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  {/if}
  {$smarty.block.parent}
{/block}