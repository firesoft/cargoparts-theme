{**
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $offers}
  {foreach from=$offers item='offer'}
    {if isset($offer.code) || isset($offer.gift)}
      {assign var='friendly_url' value=Tmproductsmanager::getPageFriendlyURL($offer['id_page'], $id_lang)}
      <span class="offer-container">
        <a href="{Tmproductsmanager::getTMProductsmanagerLink('tmproductsmanager', ['id_page' => $offer['id_page']|escape:'htmlall':'UTF-8', 'title' => $friendly_url|escape:'htmlall':'UTF-8'])}">
          {if isset($offer.code) && $offer.code}
            <span class="code-content">
              <strong>{l s='Promotional code:' mod='tmproductsmanager'}</strong>
              <span class="code-name">{$offer.code|escape:'html':'UTF-8'}</span>
            </span>
          {/if}
          {if isset($offer.gift) && $offer.gift}
            <strong>{l s='Gift:' mod='tmproductsmanager'}</strong>
            <span class="gift-content">
              {assign var='image' value=$offer['gift']['id_image']}
              {if isset($offer.gift_attribute_image) && $offer.gift_attribute_image}
                {assign var='image' value=$offer['gift_attribute_image']}
              {/if}
              <img class="img-responsive" src="{$link->getImageLink($offer['gift']['link_rewrite'], $image, 'cart_default')|escape:'html':'UTF-8'}" alt="{$offer['gift']['name']}" />
              <span class="gift-name">
                {$offer['gift']['name']|escape:'html':'UTF-8'}
                {if $offer['gift_attributes']}
                  {foreach from=$offer['gift_attributes'] item='attribute'}
                    {$attribute.public_group_name|escape:'html':'UTF-8'}:{$attribute.attribute_name|escape:'html':'UTF-8'}
                  {/foreach}
                {/if}
              </span>
            </span>
          {/if}
        </a>
      </span>
    {/if}
  {/foreach}
{/if}