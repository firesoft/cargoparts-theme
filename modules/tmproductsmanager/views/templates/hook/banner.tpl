{**
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $banners}
  {foreach from=$banners item='banner'}
    {assign var='friendly_url' value=Tmproductsmanager::getPageFriendlyURL($banner.id_page, $banner.id_lang)}
    <div class="tmpm-banner {if $banner.specific_class}{$banner.specific_class|escape:'html':'UTF-8'}{/if}" data-href="{Tmproductsmanager::getTMProductsmanagerLink('tmproductsmanager', ['id_page' => $banner.id_page|escape:'htmlall':'UTF-8', 'title' => $friendly_url|escape:'htmlall':'UTF-8'])}">
      {if $banner.page_banner}
        <div class="tmpm-banner-image">
          <img src="{$img_path|escape:'htmlall':'UTF-8'}{$banner.page_banner|escape:'html':'UTF-8'}" alt="{$banner.banner_name|escape:'htmlall':'UTF-8'}" />
        </div>
      {/if}
      {if $banner.banner_description}
        <div class="tmpm-banner-description">
          {$banner.banner_description}
        </div>
      {/if}
      {if $banner.banner_countdown}
        {assign var='countdown' value=PMPage::getCountdown($banner.id_page)}
        {if $countdown}
          <div class="mcountdown" data-mcountdown="{$countdown|escape:'htmlall':'UTF-8'}" data-server="{$server_time|escape:'htmlall':'UTF-8'}"></div>
        {/if}
      {/if}
    </div>
  {/foreach}
{/if}