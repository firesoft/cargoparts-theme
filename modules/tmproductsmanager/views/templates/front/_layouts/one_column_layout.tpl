{**
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<div id="promo-page" class="row one-column">
  <div class="col-sm-12">
    {if isset($page->page_image) && $page->page_image}
      <div class="image-full-width" style="background-image: url('{$img_src|escape:'htmlall':'UTF-8'}{$page->page_image|escape:'htmlall':'UTF-8'}');">
        <div class="col-sm-12 content">
          {if $page->type != 'none' && $page->countdown}
            <div class="mcountdown" data-mcountdown="{$page->date_to|escape:'htmlall':'UTF-8'}" data-server="{$server_time|escape:'htmlall':'UTF-8'}"></div>
          {/if}
          <h1 class="page-heading product-listing">{$page->name|escape:'htmlall':'UTF-8'}</h1>
          {if $page->voucher && $page->code}
            <div class="promo-code">{$page->code|escape:'html':'UTF-8'}</div>
          {/if}
          {if isset($page->description) && $page->description}
            <div class="description">
              {$page->description}
            </div>
          {/if}
        </div>
      </div>
    {/if}
  </div>
</div>
