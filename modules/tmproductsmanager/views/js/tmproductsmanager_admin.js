/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

$(document).ready(function() {
    gift_product_search = $('#giftProductFilter').val();
    tmproductsmanager.multiselectInit();
    tmproductsmanager.openFileManager();
    tmproductsmanager.changeLayout();
    tmproductsmanager.displaySelectFields('time');
    $(document).on('change', 'select[name="type"]', function(e) {
      tmproductsmanager.displaySelectFields();
    });
    $(document).on('change', 'input[name="categoryBox[]"]', function(e) {
      tmproductsmanager.updateCategoriesSelected();
    });
    $(document).on('click', '#check-all-categories_col, #uncheck-all-categories_col', function(e) {
      tmproductsmanager.updateCategoriesSelected();
    });
    if ($('#product_autocomplete_input').length) {
      tmproductsmanager.autocompleteInit($('#product_autocomplete_input'));
      $('#divAccessories').delegate('.delAccessory', 'click', function() {
        tmproductsmanager.delAccessory($(this).attr('name'));
      });
    }
    $('select#reduction_type').bind('change', function(e) {
      if ($('select#reduction_type').val() == 'percentage') {
        $('select#reduction_tax').hide();
      }
      else {
        $('select#reduction_tax').show();
      }
    }).trigger('change');
    $('.filter-tab li a').mouseenter(function() {
      var content = $(this).parents('li').find('div.tab-info').html();
      $(this).parents('.tab-pane').find('.tab-info-holder').html(content);
    }).mouseleave(function() {
      $(this).parents('.tab-pane').find('.tab-info-holder').html('');
    });

    $(document).on('click', 'a.generate-code', function(e) {
      e.preventDefault();
      tmproductsmanager.gencode($(this).attr('data-name'));
    });
    $('#giftProductFilter').typeWatch({
      captureLength: 2,
      highlight: false,
      wait: 100,
      callback: function(){ tmproductsmanager.searchProducts(); }
    });
  }
);
tmproductsmanager = {
  updateCategoriesSelected : function() {
    var btn        = $('a#tmproductsmanagerSelectedCategories');
    var categories = $('input[name="categoryBox[]"]:checked');
    if (!categories.length) {
      btn.html('<span>' + tmproductsmanagerSelectedCategoriesEmptyText + '</span><b class="caret"></b>');
      return;
    } else {
      var categoriesCount = 0;
      categories.each(function() {
        categoriesCount++;
      });
    }
    btn.html('<span>' + categoriesCount + ' ' + tmproductsmanagerSelectedCategories + '</span><b class="caret"></b>');
  },
  multiselectInit          : function(element, name) {
    if (!$('.tmproductsmanager-form').length && !$('.tmproductsmanager-actions-form').length) {
      return;
    }
    var name = $(element).parents('.multiselect-box').find('.empty-text').attr('data-empty-text');
    $(element).multiselect({
      enableFiltering        : false,
      includeSelectAllOption : true,
      nonSelectedText        : name,
      selectAllText          : tmproductsmanagerSelectAllText,
    });
  },
  autocompleteInit         : function(block) {
    block.autocomplete('ajax_products_list.php?exclude_packs=0&excludeVirtuals=0', {
      minChars      : 3,
      autoFill      : true,
      max           : 20,
      matchContains : true,
      mustMatch     : false,
      scroll        : false,
      cacheLength   : 0,
      formatItem    : function(item) {
        return item[1] + ' - ' + item[0];
      }
    }).result(this.addAccessory);
    block.setOptions({
      extraParams : {
        excludeIds : this.getAccessoriesIds()
      }
    });
  },
  getAccessoriesIds        : function() {
    return $('#inputAccessories').val().replace(/\-/g, ',');
  },
  addAccessory             : function(event, data, formatted) {
    if (data == null) {
      return false;
    }
    var productId         = data[1];
    var productName       = data[0];
    var $divAccessories   = $('#divAccessories');
    var $inputAccessories = $('#inputAccessories');
    var $nameAccessories  = $('#nameAccessories');
    /* delete product from select + add product line to the div, input_name, input_ids elements */
    $divAccessories.html($divAccessories.html() + '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + productId + '"><i class="icon-remove text-danger"></i></button>&nbsp;' + productName + '</div>');
    $nameAccessories.val($nameAccessories.val() + productName + '¤');
    $inputAccessories.val($inputAccessories.val() + productId + '-');
    $('#product_autocomplete_input').val('');
    $('#product_autocomplete_input').setOptions({
      extraParams : {excludeIds : tmproductsmanager.getAccessoriesIds()}
    });
  },
  delAccessory             : function(id) {
    var div      = getE('divAccessories');
    var input    = getE('inputAccessories');
    var name     = getE('nameAccessories');
    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut  = name.value.split('¤');
    if (inputCut.length != nameCut.length) {
      return jAlert('Bad size');
    }
    // Reset all hidden fields
    input.value   = '';
    name.value    = '';
    div.innerHTML = '';
    for (i in inputCut) {
      // If empty, error, next
      if (!inputCut[i] || !nameCut[i]) {
        continue;
      }
      // Add to hidden fields no selected products OR add to select field selected product
      if (inputCut[i] != id) {
        input.value += inputCut[i] + '-';
        name.value += nameCut[i] + '¤';
        div.innerHTML += '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + inputCut[i] + '"><i class="icon-remove text-danger"></i></button>&nbsp;' + nameCut[i] + '</div>';
      }
      else {
        $('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
      }
    }
    $('#product_autocomplete_input').setOptions({
      extraParams : {excludeIds : tmproductsmanager.getAccessoriesIds()}
    });
  },
  openFileManager          : function() {
    $('input.disabled').attr('disabled', true);
    $('.iframe-btn').fancybox({
      'width'          : 900,
      'height'         : 600,
      'type'           : 'iframe',
      'autoScale'      : false,
      'autoDimensions' : false,
      'fitToView'      : false,
      'autoSize'       : false,
      onUpdate         : function() {
        $('.fancybox-iframe').contents().find('a.link').data('field_id', $(this.element).data("input-name"));
        $('.fancybox-iframe').contents().find('a.link').attr('data-field_id', $(this.element).data("input-name"));
      },
      afterShow        : function() {
        $('.fancybox-iframe').contents().find('a.link').data('field_id', $(this.element).data("input-name"));
        $('.fancybox-iframe').contents().find('a.link').attr('data-field_id', $(this.element).data("input-name"));
      },
      afterClose       : function() {
        var el    = $('input[name="' + $(this.element).data("input-name") + '"');
        var value = el.val();
        var value = value.substring(value.indexOf("img/"));
        el.val(value);
      }
    });
    $(document).on('click', 'a.clear-image', function(e) {
      $(this).parents('.input-group').find('input').val('');
      return false;
    });
  },
  changeLayout             : function() {
    $(document).on('click', '#tmpm-layout-select', function(e) {
      e.preventDefault;
      tmproductsmanager.openFancyBox();
    });
    $(document).on('click', '.layouts-list.show li', function(e) {
      $.fancybox.close();
      $('.layouts-list li.active').removeClass('active').addClass('hide');
      $('.layouts-list li[data-layout="' + $(this).attr('data-layout') + '"]').addClass('active').removeClass('hide');
      $('input[name="layout"]').val($(this).attr('data-layout'));
    });
  },
  openFancyBox             : function() {
    $.fancybox.open({
      type      : 'inline',
      autoScale : true,
      minHeight : 30,
      minWidth  : 480,
      maxWidth  : 1170,
      padding   : 0,
      content   : '<ul class="layouts-list show">' + $('.layouts-list').html() + '</ul>',
      helpers   : {
        overlay : {
          locked : false
        },
      }
    });
  },
  displaySelectFields      : function() {
    var switcher = $('#tmproductsmanager_form select[name="type"]').val();
    $('#tmproductsmanager_form .form-group.hideable').each(function() {
      if ($(this).hasClass(switcher+'-field')) {
        $(this).removeClass('hidden');
      } else {
        $(this).addClass('hidden');
      }
    });
  },
  gencode                  : function(name) {
    var value = '';
    /* There are no O/0 in the codes in order to avoid confusion */
    var chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
    for (var i = 1; i <= 16; ++i) {
      value += chars.charAt(Math.floor(Math.random() * chars.length));
      if (i%4 == 0 && i != 16) {
        value += '-';
      }
    }
    $('input[name="'+name+'"]').val(value);
  },
  searchProducts: function() {
    if ($('#giftProductFilter').val() == gift_product_search)
      return;
    gift_product_search = $('#giftProductFilter').val();

    $.ajax({
      type: 'POST',
      headers: { "cache-control": "no-cache" },
      url: 'ajax-tab.php' + '?rand=' + new Date().getTime(),
      async: true,
      dataType: 'json',
      data: {
        controller: 'AdminCartRules',
        token: currentToken,
        action: 'searchProducts',
        product_search: $('#giftProductFilter').val()
      },
      success : function(res)
      {
        var products_found = '';
        var attributes_html = '';
        stock = {};

        if (res.found)
        {
          $('#gift_products_err').hide();
          $('#gift_products_found').show();
          $.each(res.products, function() {
            products_found += '<option value="' + this.id_product + '">' + this.name + (this.combinations.length == 0 ? ' - ' + this.formatted_price : '') + '</option>';

            attributes_html += '<select class="id_product_attribute" id="ipa_' + this.id_product + '" name="ipa_' + this.id_product + '" style="display:none">';
            $.each(this.combinations, function() {
              attributes_html += '<option ' + (this.default_on == 1 ? 'selected="selected"' : '') + ' value="' + this.id_product_attribute + '">' + this.attributes + ' - ' + this.formatted_price + '</option>';
            });
            attributes_html += '</select>';
          });

          $('#gift_product_list #gift_product').html(products_found);
          $('#gift_attributes_list #gift_attributes_list_select').html(attributes_html);
          tmproductsmanager.displayProductAttributes();
        }
        else
        {
          $('#products_found').hide();
          $('#products_err').html(res.notfound);
          $('#products_err').show();
        }
      }
    });
  },
  displayProductAttributes: function() {
    if ($('#ipa_' + $('#gift_product option:selected').val() + ' option').length === 0)
      $('#gift_attributes_list').hide();
    else
    {
      $('#gift_attributes_list').show();
      $('.id_product_attribute').hide();
      $('#ipa_' + $('#gift_product option:selected').val()).show();
    }
  }
}