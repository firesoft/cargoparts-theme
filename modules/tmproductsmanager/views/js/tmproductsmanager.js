/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

$(document).ready(function() {
  $("[data-mcountdown]").each(function() {
    var $this = $(this), finalDate = $(this).data("mcountdown"), serverTime = $(this).data("server");
    $this.mcountdown(serverTime, finalDate, function(event) {
      $this.html(event.strftime('<span><span>%D</span>' + tmdd_msg_days + '</span><span><span>%H</span>' + tmdd_msg_hr + '</span><span><span>%M</span>' + tmdd_msg_min + '</span><span><span>%S</span>' + tmdd_msg_sec + '</span>'));
    });
  });

  $('.tmpm-banner').on('click', function() {
    var url = $(this).attr('data-href');
    window.location.href = url;
  });
});