<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TmProductsManagerListingModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        $id_page = Tools::getValue('id_page');
        if (!$id_page) {
            $this->context->smarty->assign(
                array(
                    'products'            => false,
                    'add_prod_display'    => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                    'nbProducts'          => (int)0,
                    'homeSize'            => Image::getSize(ImageType::getFormatedName('home')),
                    'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM')
                )
            );
        } else {
            $page = new PMPage($id_page, $this->context->language->id);
            if ($page->active && $page->products) {
                $ids = Tools::jsonDecode($page->products, true);
                $this->productSort();
                $product_per_page = isset($this->context->cookie->nb_item_per_page) ? (int)$this->context->cookie->nb_item_per_page : Configuration::get(
                    'PS_PRODUCTS_PER_PAGE'
                );
                $n = abs((int)(Tools::getValue('n', $product_per_page)));
                $p = abs((int)(Tools::getValue('p', 1)));
                $order_by = Tools::getValue('orderby', 'name');
                $order_way = Tools::getValue('orderway', 'asc');
                $nbProducts = count($ids);
                $this->pagination($nbProducts);
                $products = PMPage::getProducts(
                    $ids,
                    Context::getContext()->language->id,
                    $p,
                    $n,
                    $order_by,
                    $order_way
                );
                $this->addColorsToProductList($products);
                $pm = new Tmproductsmanager();
                $this->context->smarty->assign(
                    array(
                        'page'                => $page,
                        'products'            => $products,
                        'add_prod_display'    => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                        'nbProducts'          => (int)$nbProducts,
                        'homeSize'            => Image::getSize(ImageType::getFormatedName('home')),
                        'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM'),
                        'layout'              => $pm->getFrontLayout($page->layout, $page)
                    )
                );
            }
        }

        $this->setTemplate('listing.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_.'product_list.css');
    }
}
