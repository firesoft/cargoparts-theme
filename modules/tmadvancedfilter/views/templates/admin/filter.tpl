{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div role="tabpanel" class="tab-pane" id="{$type|escape:'htmlall':'UTF-8'}-advancedfilter">
  <div class="tmadvancedfilter-panel">
    {if $exist_filter}
      <div class="filter-container">
        <h4>{l s='Type' mod='tmadvancedfilter'}
          - {$exist_filter.type|escape:'htmlall':'UTF-8'} {if isset($exist_filter.name) && $exist_filter.name}{l s='Name' mod='tmadvancedfilter'} - {$exist_filter.name|escape:'htmlall':'UTF-8'}{/if}
          <a href="#" data-id-filter="{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" class="remove-filter narrow"><i class="icon-remove"></i></a>
          <a href="#" data-id-filter="{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" class="edit-filter narrow"><i class="icon-cog"></i></a>
          <a href="#" class="btn btn-default reindex{if !$exist_filter.indexed} need-indexation{/if}" data-filter-type="{$exist_filter.type|escape:'htmlall':'UTF-8'}">{l s='Reindex %s filter' mod='tmadvancedfilter' sprintf=[$exist_filter.type|escape:'htmlall':'UTF-8']}</a>
        </h4>
        <div id="filter-items-{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" data-id-filter="{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" class="clearfix filter-items">
          <div class="col-xs-12 col-sm-3 col-md-3">
            <h5>{l s='Available filters' mod='tmadvancedfilter'}</h5>
            <ul id="available-filters-{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" class="available-filters">
              {include file="./_parts/available_list.tpl" available_filters=Tmadvancedfilter::getAllFilters($exist_filter.id_filter, false, false)}
            </ul>
          </div>
          <div class="col-xs-12 col-sm-9 col-md-9">
            <h5>{l s='Selected filters' mod='tmadvancedfilter'}</h5>
            <ul id="used-filters-{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}" data-id-layout="{$exist_filter.id_layout|escape:'htmlall':'UTF-8'}" class="used-filters">
              {assign var='id_layout' value=$exist_filter.id_layout}
              {include file="./_layouts/layout_$id_layout.tpl" filters=Tmadvancedfilter::getBackTree($exist_filter.id_filter)}
            </ul>
          </div>
          <input type="hidden" name="id_filter" value="{$exist_filter.id_filter|escape:'htmlall':'UTF-8'}"/>
        </div>
      </div>
    {else}
      <a href="#" class="btn btn-default add-filter" data-filter-type="{$type|escape:'htmlall':'UTF-8'}">{l s='Create %s' mod='tmadvancedfilter' sprintf=[$name|escape:'htmlall':'UTF-8']}</a>
    {/if}
  </div>
</div>
