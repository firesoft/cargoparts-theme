{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if !isset($item.id_item) && $item}
  {assign var='item' value=Tmadvancedfilter::getFilterItemInfo($item)|escape:'htmlall':'UTF-8'}
{/if}
{if !isset($li)}<li class="" data-id="{$item.position_inside|escape:'htmlall':'UTF-8'}" data-id-item="{$item.id_item|escape:'htmlall':'UTF-8'}" data-sort-order="{$item.sort_order|escape:'htmlall':'UTF-8'}" data-type="{$item.type|escape:'htmlall':'UTF-8'}"><div class="li-inner">{/if}
  {assign var='elementInfo' value=Tmadvancedfilter::getItemInfo($item.type, $item.position_inside)}
  {if $item.type == 'category'}
    {l s='Category level' mod='tmadvancedfilter'}{$item.position_inside|escape:'htmlall':'UTF-8'}
  {else}
    {$elementInfo.name|escape:'htmlall':'UTF-8'}
  {/if} ({$item.name|escape:'htmlall':'UTF-8'})
  <div class="info-row">
    <span class="info-field-type">{l s='Field type : ' mod='tmadvancedfilter'}{$item.field_type|escape:'htmlall':'UTF-8'}</span>
    <span class="info-type">{l s='Type : ' mod='tmadvancedfilter'}{$item.type|escape:'htmlall':'UTF-8'}</span>
    <span class="info-label">{l s='Label : ' mod='tmadvancedfilter'}{if $item.label}{$item.label|escape:'htmlall':'UTF-8'}{else}--{/if}</span>
  </div>
  <div class="item-buttons">
    <a href="#" class="remove-item" data-id-item="{$item.id_item|escape:'htmlall':'UTF-8'}"><span class="icon-remove"></span></a>
    <a href="#" class="edit-item" data-id-item="{$item.id_item|escape:'htmlall':'UTF-8'}"><span class="icon-cog"></span></a>
  </div>
{if !isset($li)}</div></li>{/if}