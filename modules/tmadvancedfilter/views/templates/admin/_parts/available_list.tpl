{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{foreach from=$available_filters key=type item='filter_group'}
  <li class="no-drag">{$type|escape:'htmlall':'UTF-8'}</li>
  {foreach from=$filter_group item='filter'}
    <li class="" data-type="{$type|escape:'htmlall':'UTF-8'}" {if isset($filter.id) && $filter.id}data-id="{$filter.id|escape:'intval':'UTF-8'}"{/if}>
      {$filter.name|escape:'htmlall':'UTF-8'}
      {if isset($filter.public_name) && $filter.public_name}
        <i> ({$filter.public_name|escape:'htmlall':'UTF-8'})</i>
      {/if}
    </li>
  {/foreach}
{/foreach}