{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if isset($filters) && $filters}
  {foreach from=$filters item='filter'}
    {if isset($filter.children) && $filter.children && $filter.children|is_array}
      <li data-id="{$filter.position_inside|escape:'htmlall':'UTF-8'}" data-id-item="{$filter.id_item|escape:'htmlall':'UTF-8'}" data-sort-order="{$filter.sort_order|escape:'htmlall':'UTF-8'}" data-type="{$filter.type|escape:'htmlall':'UTF-8'}">
        <div class="li-inner">
          {include file='./item.tpl' li=false item=$filter}
        </div>
        <ul id="used-filter-{$exist_filter['id_filter']|escape:'htmlall':'UTF-8'}-{$filter.id_item|escape:'htmlall':'UTF-8'}" class="inner-sort-list">
          {include file='./selected_list.tpl' filters=$filter.children}
        </ul>
      </li>
    {else}
      {include file='./item.tpl' item=$filter}
    {/if}
  {/foreach}
{/if}