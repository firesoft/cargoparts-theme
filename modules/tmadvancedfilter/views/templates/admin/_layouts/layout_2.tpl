{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<li class="row">
  <ul id="used-filters-column-1" class="filter-column col-xs-6" data-filter-column="1">
    {if isset($filters[1]) && $filters[1]}
      {include file="../_parts/selected_list.tpl" filters=$filters[1]}
    {/if}
  </ul>
  <ul id="used-filters-column-2" class="filter-column col-xs-6" data-filter-column="2">
    {if isset($filters[2]) && $filters[2]}
      {include file="../_parts/selected_list.tpl" filters=$filters[2]}
    {/if}
  </ul>
</li>
