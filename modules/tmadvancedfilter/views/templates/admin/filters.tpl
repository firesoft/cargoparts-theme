{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="tmadvancedfilter">
  {if $all_filter_types}
    <ul id="tmadvancedfilter-tabs" class="nav nav-tabs" role="tablist">
      {foreach from=$all_filter_types key=type item=name}
        {assign var='filter' value=Tmadvancedfilter::getShopFilterByType($type)}
        <li role="presentation">
          <a {if $filter && !$filter->indexed}class="need-indexation"{/if} href="#{$type|escape:'htmlall':'UTF-8'}-advancedfilter" data-filter-type="{$type|escape:'htmlall':'UTF-8'}" aria-controls="{$type|escape:'htmlall':'UTF-8'}-advancedfilter" role="tab" data-toggle="tab">
            {$name|escape:'htmlall':'UTF-8'}
          </a>
        </li>
      {/foreach}
    </ul>
    <div class="tab-content">
      {if $exists_filters}
        {foreach from=$exists_filters item='exist_filter'}
          {include file="./filter.tpl" type=$exist_filter.type filter_info=$exist_filter}
        {/foreach}
      {/if}
      {if $filter_types}
        {foreach from=$filter_types key=type item='name'}
          {assign var="exist_filter" value=false}
          {include file="./filter.tpl" type=$type name=$name filter_info=$exist_filter}
        {/foreach}
      {/if}
    </div>
  {/if}
</div>
{addJsDefL name=tmadv_remaining_time_text}{l s='Remaining time' mod='tmadvancedfilter'}{/addJsDefL}
{addJsDefL name=tmadv_hours_text}{l s='h' mod='tmadvancedfilter'}{/addJsDefL}
{addJsDefL name=tmadv_min_text}{l s='m' mod='tmadvancedfilter'}{/addJsDefL}
{addJsDefL name=tmadv_sec_text}{l s='s' mod='tmadvancedfilter'}{/addJsDefL}
{addJsDefL name=tmadv_warning_message}{l s='Please, do not close or refresh the page. All progress will be lost!' mod='tmadvancedfilter'}{/addJsDefL}
