{*
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
* @author     TemplateMonster
* @copyright  2002-2017 TemplateMonster
* @license    http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{extends file="helpers/form/form.tpl"}
{block name="field"}
  {if $input.type == 'advanced_filter_layouts'}
    <label class="control-label col-lg-3">{l s='Select filter layout type' mod='tmadvancedfilter'}</label>
    <div class="col-lg-9">
      <div class="row">
        {for $i = 1 to $input.layouts_nums}
          {if $input.active == $i}
            <input type="radio" checked="checked" class="filter-layout-type" name="{$input.name|escape:'htmlall':'UTF-8'}" value="{$i|escape:'htmlall':'UTF-8'}"/>
            <img width="85" src="{$module_images_path|escape:'htmlall':'UTF-8'}layout-{$i|escape:'htmlall':'UTF-8'}.png" alt=""/>
          {else}
            <input type="radio" class="filter-layout-type" name="{$input.name|escape:'htmlall':'UTF-8'}" value="{$i|escape:'htmlall':'UTF-8'}"/>
            <img width="85" src="{$module_images_path|escape:'htmlall':'UTF-8'}layout-{$i|escape:'htmlall':'UTF-8'}.png" alt=""/>
          {/if}
        {/for}
      </div>
    </div>
  {/if}
  {$smarty.block.parent}
{/block}