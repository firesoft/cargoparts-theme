{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div id="top-filter-layout-3" class="row">
  <div class="col-xs-4">
    {if isset($filter_data[1]) && $filter_data[1]}
      {foreach from=$filter_data[1] item=element}
        {$element|escape:'quotes':'UTF-8'}
      {/foreach}
    {/if}
  </div>
  <div class="col-xs-4">
    {if isset($filter_data[2]) && $filter_data[2]}
      {foreach from=$filter_data[2] item=element}
        {$element|escape:'quotes':'UTF-8'}
      {/foreach}
    {/if}
  </div>
  <div class="col-xs-4">
    {if isset($filter_data[3]) && $filter_data[3]}
      {foreach from=$filter_data[3] item=element}
        {$element|escape:'quotes':'UTF-8'}
      {/foreach}
    {/if}
  </div>
</div>
