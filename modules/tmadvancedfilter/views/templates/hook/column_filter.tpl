{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $content.indexed}
  <form action="#" method="post">
    <div id="{$content.type|escape:'htmlall':'UTF-8'}-filter" class="{$content.type|escape:'htmlall':'UTF-8'}-filter tmadvancedfilter block">
      {if $content.filter_name}<h2 class="title_block">{$content.filter_name|escape:'htmlall':'UTF-8'}</h2>{/if}
      {if $content.filter_description}<div class="filter-description">{$content.filter_description|escape:'quotes':'UTF-8'}</div>{/if}
      {if isset($filter_parameters) && $filter_parameters}
        <div class="filter-parameters-row clearfix">
          <ul id="filter-selected-parameters">
            {foreach from=$filter_parameters key=field item=data}
              {if $data}
                {foreach from=$data item=parameter}
                  {if $parameter.field_type}
                    <li data-filter-field="{$field|escape:'htmlall':'UTF-8'}" data-filter-field-type="{$parameter.field_type|escape:'htmlall':'UTF-8'}" {if isset($parameter.id) && $parameter.id}data-filter-field-id="{$parameter.id|escape:'htmlall':'UTF-8'}"{/if}>{$parameter.name|escape:'htmlall':'UTF-8'}<i class="icon icon-remove"></i></li>
                  {/if}
                {/foreach}
              {/if}
            {/foreach}
          </ul>
        </div>
      {/if}
      {$content.filter|escape:'quotes':'UTF-8'}
      <button name="submitColumnTmAdvancedFilter" type="submit" class="btn btn-success btn-lg result">{l s='Show' mod='tmadvancedfilter'} <span class="count">{if isset($result) && $result}{$result|escape:'htmlall':'UTF-8'}{/if}</span></button>
      <input type="hidden" name="id_filter" value="{$content.id_filter|escape:'htmlall':'UTF-8'}" />
      <input type="hidden" name="type" value="column" />
    </div>
  </form>
{/if}
