{**
* 2002-2016 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{*{$id_filter_layout} - get filter layout type*}
{*{$filter_item.column} - get item column in layout*}
{if $items && $active_item}
  <div class="{$type|escape:'htmlall':'UTF-8'}-filter-row">
    {if $filter_item.name}
      <label class="parameter-name">{$filter_item.name|escape:'htmlall':'UTF-8'}
        {if $filter_item.label}<small>{$filter_item.label|escape:'htmlall':'UTF-8'}</small>{/if}
      </label>
    {/if}
    {if $filter_item.description}<div class="parameter-description"><small>{$filter_item.description|escape:'quotes':'UTF-8'}</small></div>{/if}
    <div class="row">
      {foreach from=$items item='item'}
        <div class="col-xs-12 {if $type == 'top' && $id_filter_layout == 1}col-sm-3 col-md-2{/if}">
          <div class="checkbox">
            <input
                    {*{if isset($item.element->possible_values) && !$item.element->possible_values}disabled="disabled"{/if}*}
                    type="checkbox"
                    {if $active_item && $item.element->is_checked}checked="checked"{/if}
                    id="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}_{$item.element->id|escape:'htmlall':'UTF-8'}"
                    name="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}[]"
                    value="{$item.element->id|escape:'htmlall':'UTF-8'}"
                    autocomplete="off" /> <label for="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}_{$item.element->id|escape:'htmlall':'UTF-8'}">{$item.element->name|escape:'htmlall':'UTF-8'} {if isset($item.element->possible_values) && $item.element->possible_values} ({$item.element->possible_values|escape:'htmlall':'UTF-8'}){/if}</label>
          </div>
        </div>
      {/foreach}
    </div>
  </div>
  <div class="clearfix"></div>
{/if}