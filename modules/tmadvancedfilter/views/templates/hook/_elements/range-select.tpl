{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{*{$id_filter_layout} - get filter layout type*}
{*{$filter_item.column} - get item column in layout*}
<div class="{$type|escape:'htmlall':'UTF-8'}-filter-row">
  {if $filter_item.name}
    <label class="parameter-name">{$filter_item.name|escape:'htmlall':'UTF-8'}
      {if $filter_item.label}<small>{$filter_item.label|escape:'htmlall':'UTF-8'}</small>{/if}
    </label>{/if}
  {if $filter_item.description}<div class="parameter-description">{$filter_item.description|escape:'quotes':'UTF-8'}</div>{/if}
  <div class="form-group">
    {assign var='from' value=Tmadvancedfilter::getActiveRange($filter_item.type, $filter_item.id_item, 'from')|escape:'htmlall':'UTF-8'}
    {assign var='to' value=Tmadvancedfilter::getActiveRange($filter_item.type, $filter_item.id_item, 'to')|escape:'htmlall':'UTF-8'}
    <div class="row">
      <div class="{if $type == 'top'}col-xs-6 col-md-3{else}col-xs-12{/if}">
        <label>{l s='from' mod='tmadvancedfilter'} ({$currency_sign|escape:'htmlall':'UTF-8'})</label>
        <select {if !$active_item}disabled="disabled"{/if} name="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}[]" data-type="price" data-element-type="select" data-type-value="from" class="form-control" autocomplete="off">
          <option value="0">--</option>
          {if $range_list}
            {foreach from=$range_list item='item'}
              <option {if $to != 0 && $item > $to}disabled="disabled"{/if} {if $active_item && $from == $item}selected="selected"{/if} value="{$item|escape:'htmlall':'UTF-8'}">{$item|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
          {/if}
        </select>
      </div>
      <div class="{if $type == 'top'}col-xs-6 col-md-3{else}col-xs-12{/if}">
        <label>{l s='to' mod='tmadvancedfilter'} ({$currency_sign|escape:'htmlall':'UTF-8'})</label>
        <select {if !$active_item}disabled="disabled"{/if} name="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}[]" class="form-control" data-type="price" data-element-type="select" data-type-value="to" autocomplete="off">
          <option value="0">--</option>
          {if $range_list}
            {foreach from=$range_list item='item'}
              <option {if $item < $from}disabled="disabled"{/if} {if $active_item && $to == $item}selected="selected"{/if} value="{$item|escape:'htmlall':'UTF-8'}">{$item|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
          {/if}
        </select>
      </div>
    </div>
  </div>
</div>