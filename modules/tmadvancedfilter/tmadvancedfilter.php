<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once(dirname(__FILE__).'/classes/FilterHelper.php');
require_once(dirname(__FILE__).'/classes/Filter.php');
require_once(dirname(__FILE__).'/classes/FilterItem.php');
require_once(dirname(__FILE__).'/classes/Indexer.php');
require_once(dirname(__FILE__).'/classes/FilterProcess.php');

class Tmadvancedfilter extends Module
{
    protected $config_form       = false;
    public $module_path;
    public $filter_types;
    public $item_types;
    public $frontData            = array();
    public $other_fields         = array();
    public $available_parameters = array();
    public $ajax                 = false;
    public $is_filter            = false;
    public $filter_layouts_nums  = 5;

    public function __construct()
    {
        $this->name = 'tmadvancedfilter';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'Template Monster';
        $this->need_instance = 1;
        $this->controllers = array('filter');
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('TM Advanced Filter');
        $this->description = $this->l('This module allows select products by criteria');
        $this->confirmUninstall = $this->l(
            'Are you sure you want to uninstall my module? All filters and settings will be lose!'
        );
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->module_path = $this->local_path;
        $this->filter_types = array(
            'top'    => 'Top filter',
            'column' => 'Column filter'
        );
        $this->item_types = array('slider', 'checkbox', 'radio', 'select', 'range-select', 'range-input');
        array_push($this->other_fields, array('id' => 1, 'type' => 'other', 'name' => $this->l('Price')));
        array_push($this->other_fields, array('id' => 2, 'type' => 'other', 'name' => $this->l('Manufacturers')));
        array_push($this->other_fields, array('id' => 3, 'type' => 'other', 'name' => $this->l('Suppliers')));
        array_push($this->other_fields, array('id' => 4, 'type' => 'other', 'name' => $this->l('On Sale')));
        array_push($this->other_fields, array('id' => 5, 'type' => 'other', 'name' => $this->l('Condition')));
        array_push($this->other_fields, array('id' => 6, 'type' => 'other', 'name' => $this->l('Image required')));
        array_push($this->other_fields, array('id' => 7, 'type' => 'other', 'name' => $this->l('Available only')));
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
        $this->registerHook('moduleRoutes') &&
        $this->registerHook('header') &&
        $this->registerHook('backOfficeHeader') &&
        $this->registerHook('actionAttributeDelete') &&
        $this->registerHook('actionAttributeGroupDelete') &&
        $this->registerHook('actionCategoryDelete') &&
        $this->registerHook('actionFeatureDelete') &&
        $this->registerHook('actionFeatureValueDelete') &&
        $this->registerHook('actionObjectManufacturerDeleteAfter') &&
        $this->registerHook('actionObjectSupplierDeleteAfter') &&
        $this->registerHook('actionProductAdd') &&
        $this->registerHook('actionProductDelete') &&
        $this->registerHook('actionProductSave') &&
        $this->registerHook('actionProductUpdate') &&
        $this->registerHook('actionOrderStatusPostUpdate') &&
        $this->registerHook('displayLeftColumn') &&
        $this->registerHook('displayRightColumn') &&
        $this->registerHook('displayTopColumn') &&
        $this->createAjaxController();
    }

    public function uninstall()
    {
        if (!$this->removeIndexedTables()) {
            return false;
        }
        include(dirname(__FILE__).'/sql/uninstall.php');

        return $this->removeAjaxController() &&
        parent::uninstall();
    }

    private function removeIndexedTables()
    {
        $indexer = new Indexer();
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            foreach ($filters as $filter) {
                if (!$indexer->dropIndexTable($filter['type']) || !$indexer->dropIndexPriceTable($filter['type'])) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     *  Create ajax controller for the module //AdminTMAdvancedFilterController.php
     *
     * @return bool
     */
    public function createAjaxController()
    {
        $tab = new Tab();
        $tab->active = 1;
        $languages = Language::getLanguages(false);
        if (is_array($languages)) {
            foreach ($languages as $language) {
                $tab->name[$language['id_lang']] = 'tmadvancedfilter';
            }
        }
        $tab->class_name = 'AdminTMAdvancedFilter';
        $tab->module = $this->name;
        $tab->id_parent = -1;

        return (bool)$tab->add();
    }

    /**
     *  Remove ajax controller for the module //AdminTMAdvancedFilterController.php
     *
     * @return bool
     */
    private function removeAjaxController()
    {
        if ($tab_id = (int)Tab::getIdFromClassName('AdminTMAdvancedFilter')) {
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        return true;
    }

    /**
     * Load main filter configuration template
     */
    public function getContent()
    {
        $output = '';
        if ($message = $this->getWarningMultishop()) {
            return $message;
        }
        // add url for ajax scripts (tmadvancedfilter_admin.js)
        Media::addJsDef(array('ajax_url' => $this->context->link->getAdminLink('AdminTMAdvancedFilter')));
        // add path for admin module parts (used in *.tpl)
        $this->context->smarty->assign('module_tpl_path', $this->module_path.'views/templates/admin/_parts/');
        // add language id(used in some *.tpl files, temporary)
        $this->context->smarty->assign('id_lang', $this->context->language->id);
        // get all available filters types
        $this->context->smarty->assign('all_filter_types', $this->filter_types);
        // get not used filter types to build available filter button(s)
        $this->context->smarty->assign('filter_types', $this->getAvailableFilterTypes($this->context->shop->id));
        // all exist filter to display on main template
        $this->context->smarty->assign(
            'exists_filters',
            Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id)
        );
        // make class "tmadvancedfilter" available to use in the *.tpl files
        $this->context->smarty->assign('tmadvancedfilter', $this);
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/filters.tpl');

        return $output;
    }

    /**
     *  Build backend tree by filter id
     *
     * @param $id_filter
     *
     * @return array
     */
    public static function getBackTree($id_filter)
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        if (!$filter_items = FilterItem::getFilterItems($id_filter, Context::getContext()->language->id, true)) {
            return false;
        }

        $tree = $tmadvancedfilter->createFilterTree($filter_items);

        return $tree;
    }

    /**
     * Create main tree structure by objects
     *
     * @param     $tree
     * @param int $root
     *
     * @return array
     */
    public function createFilterTree($tree, $root = 0)
    {
        $parents = array();
        foreach ($tree as $a) {
            $parents[$a['parent']][] = $a;
        }

        return Tmadvancedfilter::createFilterBranch($parents, $parents[$root], true);
    }

    /**
     * Create each tree branch
     *
     * @param $parents
     * @param $children
     *
     * @return array
     */
    public function createFilterBranch(&$parents, $children, $top = false)
    {
        $tree = array();
        foreach ($children as $child) {
            if (isset($parents[$child['id_item']])) {
                $child['children'] = Tmadvancedfilter::createFilterBranch($parents, $parents[$child['id_item']]);
            }
            if ($top) {
                $tree[$child['column']][] = $child;
            } else {
                $tree[] = $child;
            }
        }

        return $tree;
    }

    /**
     * Get available filter type for defined shop
     *
     * @param $id_shop
     *
     * @return array
     */
    public function getAvailableFilterTypes($id_shop)
    {
        $filters = array();
        foreach ($this->filter_types as $key => $type) {
            if (!Filter::checkFilterExists($id_shop, $key)) {
                $filters[$key] = $type;
            }
        }

        return $filters;
    }

    /**
     * Generate list of all available filters for the shop
     *
     * @param $id_shop
     * @param $id_lang
     *
     * @return array
     */
    public static function getAllFilters($id_filter, $id_shop, $id_lang)
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        if (!$id_shop) {
            $id_shop = Context::getContext()->shop->id;
        }
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $available_filters = array();
        $categories_depth = FilterHelper::getCategoriesDepth($id_shop);
        $attributes_array = AttributeGroup::getAttributesGroups($id_lang);
        $features_array = Feature::getFeatures($id_lang);
        foreach ($categories_depth as $key => $depth) {
            if (!FilterItem::checkItemInFilter($id_filter, 'category', $depth['level_depth'])) {
                $available_filters['category'][$key]['id'] = $depth['level_depth'];
                $available_filters['category'][$key]['name'] = sprintf(
                    $tmadvancedfilter->l('Categories level %s'),
                    $depth['level_depth']
                );
            }
        }
        foreach ($attributes_array as $key => $attribute) {
            if (!FilterItem::checkItemInFilter($id_filter, 'attribute', $attribute['id_attribute_group'])) {
                $available_filters['attribute'][$key]['id'] = $attribute['id_attribute_group'];
                $available_filters['attribute'][$key]['name'] = $attribute['name'];
                $available_filters['attribute'][$key]['public_name'] = $attribute['public_name'];
            }
        }
        foreach ($features_array as $key => $feature) {
            if (!FilterItem::checkItemInFilter($id_filter, 'feature', $feature['id_feature'])) {
                $available_filters['feature'][$key]['id'] = $feature['id_feature'];
                $available_filters['feature'][$key]['name'] = $feature['name'];
            }
        }
        foreach ($tmadvancedfilter->other_fields as $value) {
            if (!FilterItem::checkItemInFilter($id_filter, $value['type'], $value['id'])) {
                $available_filters[$value['type']][$value['id']] = array('id' => $value['id'], 'name' => $value['name']);
            }
        }

        return $available_filters;
    }

    /**
     * Get information about item by type of filter and item id
     *
     * @see  Tmadvancedfilter::getItemInfo('type', 'id_filter')
     * @used AdminTMAdvancedFilterController.php, filter.tpl
     *
     * @param $type    (string)
     * @param $id_item (int)
     *
     * @return array
     */
    public static function getItemInfo($type, $id_item)
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $id_lang = Context::getContext()->language->id;
        $info = array();
        switch ($type) {
            case 'category':
                $info = array();
                break;
            case 'feature':
                $feature = new Feature($id_item, $id_lang);
                $info['name'] = $feature->name;
                break;
            case 'attribute':
                $attribute = new AttributeGroup($id_item, $id_lang);
                $info['name'] = $attribute->name;
                break;
            case 'other':
                foreach ($tmadvancedfilter->other_fields as $item) {
                    if ($item['id'] == $id_item) {
                        $info['name'] = $item['name'];
                    }
                }
                break;
        }

        return $info;
    }

    /**
     * Get information about item by item id
     *
     * @param $id_item
     *
     * @return FilterItem
     */
    public static function getFilterItemInfo($id_item)
    {
        return FilterItem::getFilterItem($id_item, Context::getContext()->shop->id);
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    public function renderForm($item, $id_filter)
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues($item),
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm($item, $id_filter)));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm($item, $id_filter)
    {
        $parent_items = array();
        $parent_items[] = array('id' => 0, 'name' => '--');
        $filter_items = FilterItem::getFilterItems($id_filter, $this->context->language->id);
        if ($item->id) {
            $filter_item = new FilterItem($item->id);
        } else {
            $filter_item = new FilterItem();
        }
        foreach ($filter_items as $i) {
            if ($i['id_item'] != $item->id && $i['parent'] != $item->id) {
                if (count($children = $filter_item->getAllChildrenId()) && !in_array($i['id_item'], $children)) {
                    $parent_items[] = $this->buildParentItem($i);
                } elseif (!$children || !$item->id) {
                    $parent_items[] = $this->buildParentItem($i);
                }
            }
        }
        $form = array(
            'form' => array(
                'legend'  => array(
                    'title' => !isset($item->id) ? $this->l('Add filter') : $this->l('Update filter'),
                    'icon'  => 'icon-cogs',
                ),
                'input'   => array(
                    array(
                        'col'   => 6,
                        'type'  => 'text',
                        'desc'  => $this->l('Enter a name of filter value'),
                        'name'  => 'name',
                        'label' => $this->l('Name'),
                        'lang'  => true
                    ),
                    array(
                        'col'   => 6,
                        'type'  => 'text',
                        'desc'  => $this->l('Enter a label for filter value'),
                        'name'  => 'label',
                        'label' => $this->l('Label'),
                        'lang'  => true
                    ),
                    array(
                        'type'         => 'textarea',
                        'label'        => $this->l('Description'),
                        'desc'         => $this->l('Enter a description for filter value'),
                        'name'         => 'description',
                        'autoload_rte' => true,
                        'lang'         => true,
                    ),
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Parent filter'),
                        'name'    => 'parent',
                        'options' => array(
                            'query' => $parent_items,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Filter field type'),
                        'name'    => 'field_type',
                        'options' => array(
                            'query' => $this->getAvailableFieldType($item->type, $item->position_inside),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    )
                ),
                'buttons' => array(
                    array(
                        'class' => 'btn btn-success',
                        'title' => $this->l('Save'),
                        'type'  => 'submit',
                        'name'  => 'updateFilterItem',
                    ),
                ),
            ),
        );
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_item');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_filter');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'type');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'position_inside');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'sort_order');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'column');

        return $form;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues($item)
    {
        if (Validate::isLoadedObject($item)) {
            $item = new FilterItem($item->id);
        }
        $fields_values = array(
            'id_item'         => Tools::getValue('id_item', $item->id),
            'id_filter'       => Tools::getValue('id_filter', (int)$item->id_filter),
            'type'            => Tools::getValue('type', $item->type),
            'position_inside' => Tools::getValue('position_inside', $item->position_inside),
            'sort_order'      => Tools::getValue('sort_order', (int)$item->sort_order),
            'column'          => Tools::getValue('column', (int)$item->column),
            'parent'          => Tools::getValue('parent', (int)$item->parent),
            'field_type'      => Tools::getValue('field_type', pSql($item->field_type))
        );
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $fields_values['name'][$lang['id_lang']] = Tools::getValue(
                'name_'.(int)$lang['id_lang'],
                $item->name[$lang['id_lang']]
            );
            $fields_values['label'][$lang['id_lang']] = Tools::getValue(
                'label_'.(int)$lang['id_lang'],
                $item->label[$lang['id_lang']]
            );
            $fields_values['description'][$lang['id_lang']] = Tools::getValue(
                'description_'.(int)$lang['id_lang'],
                $item->description[$lang['id_lang']]
            );
        }

        return $fields_values;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    public function renderFilterForm($item)
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFilterFormValues($item),
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id,
            'module_images_path'  => $this->_path.'views/img/',
        );

        return $helper->generateForm(array($this->getConfigFilterForm($item)));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigFilterForm($item)
    {
        $form = array(
            'form' => array(
                'legend'  => array(
                    'title' => !$item->id ? $this->l('Add filter') : $this->l('Update filter'),
                    'icon'  => 'icon-cogs',
                ),
                'input'   => array(
                    array(
                        'col'   => 6,
                        'type'  => 'text',
                        'desc'  => $this->l('Enter filter name'),
                        'name'  => 'name',
                        'label' => $this->l('Name'),
                        'lang'  => true
                    ),
                    array(
                        'type'         => 'textarea',
                        'desc'         => $this->l('Enter filter description'),
                        'name'         => 'description',
                        'label'        => $this->l('Description'),
                        'lang'         => true,
                        'autoload_rte' => true
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Show counter'),
                        'name' => 'counter_status',
                        'is_bool' => true,
                        'desc' => $this->l('Display number of available products for this criteria'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Show selected filters'),
                        'name' => 'selected_filters',
                        'is_bool' => true,
                        'desc' => $this->l('Display list of selected criteria'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        )
                    ),
                ),
                'buttons' => array(
                    array(
                        'class' => 'btn btn-success',
                        'title' => $this->l('Save'),
                        'type'  => 'submit',
                        'name'  => 'updateFilter',
                    ),
                ),
            ),
        );
        if ($item->type == 'top') {
            $form['form']['input'][] = array(
                'type' => 'advanced_filter_layouts',
                'name' => 'id_layout',
                'layouts_nums' => $this->filter_layouts_nums,
                'active' => $item->id_layout
            );
        }
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_filter');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'type');
        $form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_shop');

        return $form;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFilterFormValues($item)
    {
        if (Validate::isLoadedObject($item)) {
            $item = new Filter($item->id);
        }
        $fields_values = array(
            'id_filter' => Tools::getValue('id_filter', (int)$item->id),
            'type'      => Tools::getValue('type', $item->type),
            'id_shop'   => Tools::getValue('id_shop', (int)$item->id_shop),
            'id_layout'   => Tools::getValue('id_layout', (int)$item->id_layout),
            'counter_status'   => Tools::getValue('counter_status', (int)$item->counter_status),
            'selected_filters'   => Tools::getValue('selected_filters', (int)$item->selected_filters)
        );
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $fields_values['name'][$lang['id_lang']] = Tools::getValue(
                'name_'.(int)$lang['id_lang'],
                $item->name[$lang['id_lang']]
            );
            $fields_values['description'][$lang['id_lang']] = Tools::getValue(
                'description_'.(int)$lang['id_lang'],
                $item->description[$lang['id_lang']]
            );
        }

        return $fields_values;
    }

    /**
     * Get available fields type for current element by element type
     *
     * @param $item_type
     * @param $position
     *
     * @return array
     */
    protected function getAvailableFieldType($item_type, $position)
    {
        $result = array();
        foreach ($this->item_types as $type) {
            if ($item_type == 'category' && $type != 'slider' && $type != 'range-select' && $type != 'range-input') {
                $result[] = array('id' => $type, 'name' => $type);
            }
            if ($item_type == 'feature' && $type != 'slider' && $type != 'range-select' && $type != 'range-input') {
                $result[] = array('id' => $type, 'name' => $type);
            }
            if ($item_type == 'attribute' && $type != 'slider' && $type != 'range-select' && $type != 'range-input') {
                $result[] = array('id' => $type, 'name' => $type);
            }
            if ($item_type == 'other') {
                if ($position == 1 && $type == 'range-select') { // price
                    $result[] = array('id' => $type, 'name' => $type);
                } elseif ($position == 1 && $type == 'range-input') {
                    $result[] = array('id' => $type, 'name' => $type);
                } elseif ($position != 1 && $type != 'slider' && $type != 'range-select' && $type != 'range-input') {
                    $result[] = array('id' => $type, 'name' => $type);
                }
            }
        }

        return $result;
    }

    /**
     * Build list for selecting parent element item
     *
     * @param $item
     *
     * @return array
     */
    protected function buildParentItem($item)
    {
        $name = $item['name'] ? $item['name'] : '--';
        $label = $item['label'] ? $item['label'] : '--';
        if ($item['parent']) {
            $parent_info = tmadvancedfilter::getFilterItemInfo($item['parent']);
            if ($parent_info[0]['type'] == 'category') {
                $parent = sprintf($this->l('Category level %s'), $parent_info[0]['position_inside']);
            } else {
                $parent = $parent_info[0]['name'];
            }
        } else {
            $parent = '--';
        }

        return array(
            'id'   => $item['id_item'],
            'name' => sprintf(
                $this->l('Type: %s; Name: %s; Label: %s; Parent: %s'),
                $item['type'],
                $name,
                $label,
                $parent
            )
        );
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name || Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJS(
                array(
                    _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                    _PS_JS_DIR_.'admin/tinymce.inc.js',
                )
            );
            $this->context->controller->addJqueryUI('ui.sortable');
            $this->context->controller->addJS($this->_path.'views/js/tmadvancedfilter_admin.js');
            $this->context->controller->addCSS($this->_path.'views/css/tmadvancedfilter_admin.css');
        }
    }

    public function hookActionCategoryDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $process = new FilterProcess();
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'category',
                    $params['category']->level_depth
                );
                if ($item_code) {
                    $products_ids = $process->filter(
                        $filter['type'],
                        array('category_'.$item_code => $params['category']->id_category)
                    );
                    if ($products_ids) {
                        $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products_ids, false);
                    }
                }
            }
        }
    }

    public function hookActionAttributeDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $process = new FilterProcess();
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $id_attribute_group = Indexer::checkAttributeIndex($filter['id_filter'], $params['id_attribute']);
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'attribute',
                    $id_attribute_group
                );
                if ($item_code) {
                    $products_ids = $process->filter(
                        $filter['type'],
                        array('attribute_'.$item_code => $params['id_attribute'])
                    );
                    if ($products_ids) {
                        $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products_ids, false);
                    }
                    Indexer::deleteAttributeValueIndex($filter['id_filter'], $params['id_attribute']);
                }
            }
        }
    }

    public function hookActionAttributeGroupDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'attribute',
                    $params['id_attribute_group']
                );
                if ($item_code) {
                    $indexer->dropIndexTableColumn($filter['type'], 'attribute_'.$item_code);
                    $filter_item = new FilterItem($item_code);
                    $filter_item_children = $filter_item->getAllChildrenId();
                    if ($filter_item_children) {
                        foreach ($filter_item_children as $child) {
                            $child_item = new FilterItem($child);
                            $indexer->dropIndexTableColumn($filter['type'], $child_item->type.'_'.$child_item->id);
                            $child_item->delete();
                        }
                    }
                    $filter_item->delete();
                    Indexer::deleteAttributeIndex($filter['id_filter'], $params['id_attribute_group']);
                }
            }
        }
    }

    public function hookActionFeatureDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'feature',
                    $params['id_feature']
                );
                if ($item_code) {
                    $indexer->dropIndexTableColumn($filter['type'], 'feature_'.$item_code);
                    $filter_item = new FilterItem($item_code);
                    $filter_item_children = $filter_item->getAllChildrenId();
                    if ($filter_item_children) {
                        foreach ($filter_item_children as $child) {
                            $child_item = new FilterItem($child);
                            $indexer->dropIndexTableColumn($filter['type'], $child_item->type.'_'.$child_item->id);
                            $child_item->delete();
                        }
                    }
                    $filter_item->delete();
                    Indexer::deleteFeatureIndex($filter['id_filter'], $params['id_feature']);
                }
            }
        }
    }

    public function hookActionFeatureValueDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $process = new FilterProcess();
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $id_feature = Indexer::checkFeatureIndex($filter['id_filter'], $params['id_feature_value']);
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'feature',
                    $id_feature
                );
                if ($item_code) {
                    $products_ids = $process->filter(
                        $filter['type'],
                        array('feature_'.$item_code => $params['id_feature_value'])
                    );
                    if ($products_ids) {
                        $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products_ids, false);
                    }
                    Indexer::deleteFeatureValueIndex($filter['id_filter'], $params['id_feature_value']);
                }
            }
        }
    }

    public function hookActionObjectManufacturerDeleteAfter($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $process = new FilterProcess();
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'other',
                    2
                );
                if ($item_code) {
                    $products_ids = $process->filter(
                        $filter['type'],
                        array('other_'.$item_code => $params['object']->id)
                    );
                    if ($products_ids) {
                        $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products_ids, false);
                    }
                }
            }
        }
    }

    public function hookActionObjectSupplierDeleteAfter($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $process = new FilterProcess();
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $item_code = FilterItem::checkItemInFilter(
                    $filter['id_filter'],
                    'other',
                    3
                );
                if ($item_code) {
                    $products_ids = $process->filter(
                        $filter['type'],
                        array('other_'.$item_code => $params['object']->id)
                    );
                    if ($products_ids) {
                        $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products_ids, false);
                    }
                }
            }
        }
    }

    public function hookActionProductAdd($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $indexer->rebuildEntry($filter['id_filter'], $filter['type'], array($params['id_product']), true);
            }
        }
    }

    public function hookActionProductDelete($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $indexer->removeEntry($filter['type'], $params['id_product']);
            }
        }
    }

    public function hookActionProductSave($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $indexer->rebuildEntry($filter['id_filter'], $filter['type'], array($params['id_product']), true);
            }
        }
    }

    public function hookActionProductUpdate($params)
    {
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if ($filters) {
            $indexer = new Indexer();
            foreach ($filters as $filter) {
                $indexer->rebuildEntry($filter['id_filter'], $filter['type'], array($params['id_product']), true);
            }
        }
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $cart = new Cart($params['cart']->id);
        $cart_products = $cart->getProducts();
        $filters = Filter::getAllShopFilters($this->context->shop->id, $this->context->language->id);
        if (!$filters || !$cart_products) {
            return;
        }
        $indexer = new Indexer();
        $products = array();
        foreach ($cart_products as $cart_product) {
            $products[] = $cart_product['id_product'];
        }
        foreach ($filters as $filter) {
            if (FilterItem::getAvailableFieldId($filter['id_filter'])) {
                $indexer->rebuildEntry($filter['id_filter'], $filter['type'], $products, false);
            }
        }
    }

    /**
     * System's hook that allow create own url for filter result page
     *
     * @param $params
     *
     * @return array
     */
    public function hookModuleRoutes($params)
    {
        $link = array(
            'tmadvancedfilter' => array(
                'controller' => 'filter',
                'rule'       => 'filter',
                'keywords'   => array(),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tmadvancedfilter',
                ),
            )
        );

        return $link;
    }

    /**
     * Build own module url
     * @return string
     */
    public static function getTMAdvancedFilterUrl()
    {
        $ssl_enable = Configuration::get('PS_SSL_ENABLED');
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $rewrite_set = (int)Configuration::get('PS_REWRITING_SETTINGS');
        $ssl = null;
        static $force_ssl = null;
        if ($ssl === null) {
            if ($force_ssl === null) {
                $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            }
            $ssl = $force_ssl;
        }
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop !== null) {
            $shop = new Shop($id_shop);
        } else {
            $shop = Context::getContext()->shop;
        }
        if ($ssl && $ssl_enable) {
            $base = 'https://'.$shop->domain_ssl;
        } else {
            $base = 'http://'.$shop->domain;
        }
        $langUrl = Language::getIsoById($id_lang).'/';
        if ((!$rewrite_set && in_array($id_shop, array((int)Context::getContext()->shop->id, null)))
            || !Language::isMultiLanguageActivated($id_shop)
            || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop)
        ) {
            $langUrl = '';
        }

        return $base.$shop->getBaseURI().$langUrl;
    }

    /**
     * Build own module link
     *
     * @param string $rewrite
     * @param null   $params
     * @param null   $id_lang
     *
     * @return string
     */
    public static function getTMAdvancedFilterLink($rewrite = 'tmadvancedfilter', $params = null, $id_lang = null)
    {
        $url = Tmadvancedfilter::getTMAdvancedFilterUrl();
        $dispatcher = Dispatcher::getInstance();
        if ($params != null) {
            return $url.$dispatcher->createUrl($rewrite, $id_lang, $params);
        }

        return $url.$dispatcher->createUrl($rewrite);
    }

    /**
     * Build filters by parameters
     *
     * @param bool $id_shop
     * @param bool $id_lang
     * @param      $type
     *
     * @return array|bool
     */
    public function buildFilter($type, $id_shop = false, $id_lang = false)
    {
        if (!$id_shop) {
            $id_shop = $this->context->shop->id;
        }
        if (!$id_lang) {
            $id_lang = $this->context->language->id;
        }
        $result = array();
        $output = '';
        if (!$filter_id = Filter::checkFilterExists($id_shop, $type)) {
            return false;
        }
        $filter = new Filter($filter_id, $id_lang);
        if (!$filter->indexed) {
            return false;
        }
        $result['id_filter'] = $filter->id;
        $result['type'] = $filter->type;
        $result['filter_name'] = $filter->name;
        $result['filter_description'] = $filter->description;
        $result['indexed'] = $filter->indexed;
        $result['id_layout'] = $filter->id_layout;
        if (!$filter_items = FilterItem::getFilterItems($filter_id, $id_lang, true)) {
            $result['filter'] = $output;

            return $result;
        }
        $blocks = $this->getFrontTree($filter_items);
        foreach ($blocks as $id => $block) {
            $this->renderFrontTree($block, $filter->id_layout, $id, $filter->type, $filter->counter_status);
        }
        $this->context->smarty->assign('filter_data', $this->frontData);
        $this->context->smarty->assign('type', $filter->type);
        $this->frontData = array();
        $result['filter'] = $this->display(
            $this->_path,
            '/views/templates/hook/_layouts/layout_'.$filter->id_layout.'.tpl'
        );

        return $result;
    }

    /**
     *  Build frontend parameters tree by related items
     *
     * @param $items
     *
     * @return array
     */
    public function getFrontTree($items)
    {
        if ($items) {
            $tree = $this->createFilterTree($items);
        }

        return $tree;
    }

    /**
     * Build response html from tree
     *
     * @param $tree
     * @param $id_layout
     * @param $block
     * @param $type
     */
    protected function renderFrontTree($tree, $id_layout, $block, $type, $possible_values)
    {
        foreach ($tree as $brunch) {
            $this->context->smarty->assign('filter_item', $brunch);
            $this->context->smarty->assign(
                'active_item',
                $this->checkAllParentsSelected($brunch['id_item'], false)
            );
            $this->context->smarty->assign('type', $type);
            switch ($brunch['type']) {
                case 'category':
                    $this->context->smarty->assign('items', $this->buildCategoryElement($brunch, $type, $possible_values));
                    break;
                case 'feature':
                    $this->context->smarty->assign('items', $this->buildFeatureElement($brunch, $type, $possible_values));
                    break;
                case 'attribute':
                    $this->context->smarty->assign('items', $this->buildAttributeElement($brunch, $type, $possible_values));
                    break;
                case 'other':
                    if ($brunch['id_item'] == FilterItem::getPriceFieldId($brunch['id_filter'])) {
                        $currency = new Currency($this->context->currency->id);
                        $this->context->smarty->assign('currency_sign', $currency->sign);
                        $this->context->smarty->assign('range_list', $this->buildOtherElement($brunch, $type, $possible_values));
                    } else {
                        $this->context->smarty->assign('items', $this->buildOtherElement($brunch, $type, $possible_values));
                    }
                    break;
            }
            $this->context->smarty->assign('id_filter_layout', $id_layout);
            $this->frontData[$block][] = $this->display(
                $this->_path,
                '/views/templates/hook/_elements/'.$brunch['field_type'].'.tpl'
            );
            if (isset($brunch['children']) && $brunch['children']) {
                $this->renderFrontTree($brunch['children'], $id_layout, $block, $type, $possible_values);
            }
        }
    }

    /**
     * Build html of category element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildCategoryElement($item, $type, $possible_values)
    {
        $categories = array();
        $categories_ids = FilterHelper::getCategoriesByDepth($item['position_inside'], $this->context->shop->id);
        foreach ($categories_ids as $key => $category) {
            $id_selected = $this->fieldInSelected($item['parent']);
            $cat = new Category($category['id_category'], $this->context->language->id);
            //$cat->all_values = $this->getAllProducts('category_'.$item['id_item'], $cat->id, $type);
            if ($possible_values) {
                $cat->possible_values = $this->getPossibleProducts('category_'.$item['id_item'], $cat->id, $type);
            }
            $cat->is_checked = Tmadvancedfilter::checkActive(
                $item['type'],
                $item['id_item'],
                $cat->id
            );
            if ($item['parent'] == 0) {
                $categories[$key]['element'] = $cat;
            } elseif ($id_selected) {
                foreach ($id_selected as $id) {
                    $parent = new FilterItem($item['parent']);
                    if ($parent->position_inside < $item['position_inside']) {
                        $parents = FilterHelper::getCategoryChildrenIds($cat->id);
                        if (in_array($id, $parents)) {
                            $categories[$key]['element'] = $cat;
                        }
                    } else {
                        $categories[$key]['element'] = $cat;
                    }
                }
            }
        }

        return $categories;
    }

    /**
     * Build html of feature element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildFeatureElement($item, $type, $possible_values)
    {
        $features = array();
        $feature_values = FeatureValue::getFeatureValues($item['position_inside']);
        foreach ($feature_values as $key => $feature_value) {
            $id_selected = $this->fieldInSelected($item['parent']);
            $feature = new FeatureValue($feature_value['id_feature_value'], $this->context->language->id);
            if ($possible_values) {
                $feature->possible_values = $this->getPossibleProducts(
                    $item['type'].'_'.$item['id_item'],
                    $feature->id,
                    $type
                );
            }
            $feature->is_checked = Tmadvancedfilter::checkActive(
                $item['type'],
                $item['id_item'],
                $feature->id
            );
            if ($item['parent'] == 0 || $id_selected) {
                $feature->name = $feature->value;
                $features[$key]['element'] = $feature;
            }
        }

        return $features;
    }

    /**
     * Build html of attribute element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildAttributeElement($item, $type, $possible_values)
    {
        $attributes = array();
        $attribute_values = AttributeGroup::getAttributes($this->context->language->id, $item['position_inside']);
        foreach ($attribute_values as $key => $attribute_value) {
            $id_selected = $this->fieldInSelected($item['parent']);
            $attribute = new Attribute($attribute_value['id_attribute'], $this->context->language->id);
            if ($possible_values) {
                $attribute->possible_values = $this->getPossibleProducts(
                    $item['type'].'_'.$item['id_item'],
                    $attribute->id,
                    $type
                );
            }
            $attribute->is_checked = Tmadvancedfilter::checkActive(
                $item['type'],
                $item['id_item'],
                $attribute->id
            );
            if ($item['parent'] == 0 || $id_selected) {
                $attributes[$key]['element'] = $attribute;
            }
        }

        return $attributes;
    }

    /**
     * Build html of other element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildOtherElement($item, $type, $possible_values)
    {
        $elements = array();
        $id_selected = $this->fieldInSelected($item['parent']);
        if ($item['parent'] == 0 || $id_selected) {
            switch ($item['position_inside']) {
                case '1': // prices
                    $prices = FilterProcess::getAllIndexPrices(
                        $type,
                        $this->context->shop->id,
                        $this->context->currency->id
                    );
                    $elements = $this->buildRangeSteps($prices);
                    break;
                case '2': // manufacturers
                    $elements = $this->buildManufacturerElement($item, $type, $possible_values);
                    break;
                case '3': // suppliers
                    $elements = $this->buildSupplierElement($item, $type, $possible_values);
                    break;
                case '4': // on sale
                    $element = new stdClass();
                    $element->id = 1;
                    $element->name = $this->l('On Sale');
                    if ($possible_values) {
                        $element->possible_values = $this->getPossibleProducts(
                            $item['type'].'_'.$item['id_item'],
                            1,
                            $type
                        );
                    }
                    $element->is_checked = Tmadvancedfilter::checkActive(
                        $item['type'],
                        $item['id_item'],
                        1
                    );
                    $elements[]['element'] = $element;
                    break;
                case '5': // condition
                    $conditions = array('New', 'Used', 'Refurbished');
                    foreach ($conditions as $i => $condition) {
                        $element = new stdClass();
                        $i = $i + 1;
                        $element->id = $i;
                        $element->name = $this->l($condition);
                        if ($possible_values) {
                            $element->possible_values = $this->getPossibleProducts(
                                $item['type'].'_'.$item['id_item'],
                                $i,
                                $type
                            );
                        }
                        $element->is_checked = Tmadvancedfilter::checkActive(
                            $item['type'],
                            $item['id_item'],
                            $i
                        );
                        $elements[$i]['element'] = $element;
                    }
                    break;
                case '6': // only with image
                    $element = new stdClass();
                    $element->id = 1;
                    $element->name = $this->l('With image only');
                    if ($possible_values) {
                        $element->possible_values = $this->getPossibleProducts(
                            $item['type'].'_'.$item['id_item'],
                            1,
                            $type
                        );
                    }
                    $element->is_checked = Tmadvancedfilter::checkActive(
                        $item['type'],
                        $item['id_item'],
                        1
                    );
                    $elements[]['element'] = $element;
                    break;
                case '7': // only available products
                    $element = new stdClass();
                    $element->id = 1;
                    $element->name = $this->l('Available only');
                    if ($possible_values) {
                        $element->possible_values = $this->getPossibleProducts(
                            $item['type'].'_'.$item['id_item'],
                            1,
                            $type
                        );
                    }
                    $element->is_checked = Tmadvancedfilter::checkActive(
                        $item['type'],
                        $item['id_item'],
                        1
                    );
                    $elements[]['element'] = $element;
                    break;
            }
        }

        return $elements;
    }

    protected function buildRangeSteps($prices)
    {
        sort($prices);
        $buckets = array(array());
        $result = array();
        $a = 0;
        $c = count($prices);
        for ($i = 0; $i !== $c; ++$i) {
            if (count($buckets[$a]) === 4) {
                ++$a;
                $buckets[$a] = array();
            }
            if (isset($buckets[$a][$prices[$i]])) {
                ++$buckets[$a][$prices[$i]];
            } elseif (isset($buckets[$a - 1][$prices[$i]])) {
                ++$buckets[$a - 1][$prices[$i]];
            } else {
                $buckets[$a][$prices[$i]] = 1;
            }
        }
        foreach ($buckets as $price_block) {
            $from = $this->startKey($price_block);
            $to = $this->endKey($price_block);
            if ($from > 100) {
                $from = round($from, -1);
            }
            if ($to > 100) {
                $to = round($to, -1);
            }
            $result[] = $from;
            $result[] = $to;
        }

        return array_unique($result);
    }

    public function startKey($array)
    {
        reset($array);

        return key($array);
    }

    public function endKey($array)
    {
        end($array);

        return key($array);
    }

    /**
     * Build html of manufacturer element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildManufacturerElement($item, $type, $possible_values)
    {
        $manufacturers = array();
        $manufacturers_values = FilterHelper::getManufacturers($this->context->shop->id);
        if ($manufacturers_values) {
            foreach ($manufacturers_values as $key => $value) {
                $id_selected = $this->fieldInSelected($item['parent']);
                $manufacturer = new Manufacturer($value['id_manufacturer'], $this->context->language->id);
                if ($possible_values) {
                    $manufacturer->possible_values = $this->getPossibleProducts(
                        $item['type'].'_'.$item['id_item'],
                        $manufacturer->id,
                        $type
                    );
                }
                $manufacturer->is_checked = Tmadvancedfilter::checkActive(
                    $item['type'],
                    $item['id_item'],
                    $manufacturer->id
                );
                if ($item['parent'] == 0 || $id_selected) {
                    $manufacturers[$key]['element'] = $manufacturer;
                }
            }
        }

        return $manufacturers;
    }

    /**
     * Build html of supplier element
     *
     * @param $item
     * @param $type
     *
     * @return array
     */
    protected function buildSupplierElement($item, $type, $possible_values)
    {
        $suppliers = array();
        $suppliers_values = FilterHelper::getSuppliers($this->context->shop->id);
        if ($suppliers_values) {
            foreach ($suppliers_values as $key => $value) {
                $id_selected = $this->fieldInSelected($item['parent']);
                $supplier = new Supplier($value['id_supplier'], $this->context->language->id);
                if ($possible_values) {
                    $supplier->possible_values = $this->getPossibleProducts(
                        $item['type'].'_'.$item['id_item'],
                        $supplier->id,
                        $type
                    );
                }
                $supplier->is_checked = Tmadvancedfilter::checkActive(
                    $item['type'],
                    $item['id_item'],
                    $supplier->id
                );
                if ($item['parent'] == 0 || $id_selected) {
                    $suppliers[$key]['element'] = $supplier;
                }
            }
        }

        return $suppliers;
    }

    /**
     * Check if required parent item was selected
     * and return id's of this level items available
     *
     * @param $id_item
     *
     * @return array
     */
    protected function fieldInSelected($id_item)
    {
        $item = new FilterItem($id_item);
        $item_rel = $item->type.'_'.$item->id;
        $result = array();
        if (Tools::getValue($item_rel) && !Tools::isEmpty(Tools::getValue($item_rel))) {
            $items_ids = Tools::getValue($item_rel);
            if (is_array($items_ids)) {
                foreach ($items_ids as $item_id) {
                    if (strpos($item_id, '|')) {
                        $ids = explode('|', $item_id);
                        foreach ($ids as $id) {
                            $result[] = $id;
                        }
                    }
                    $result[] = $item_id;
                }
            } elseif (strpos($items_ids, '|')) {
                $items_ids = explode('|', $items_ids);
                foreach ($items_ids as $item_id) {
                    $result[] = $item_id;
                }
            } else {
                $result[] = $items_ids;
            }

            return $result;
        }

        return false;
    }

    /**
     * Check if all required parents elements was checked
     *
     * @param      $id_item
     * @param bool $name
     *
     * @return bool
     */
    protected function checkAllParentsSelected($id_item, $name = false)
    {
        $result = true;
        if (!$id_item) {
            $data = explode('_', $name);
            $id_item = $data[count($data) - 1];
        }
        $item = new FilterItem($id_item);
        if (!$item->parent) {
            return $result;
        }
        $all_parents = $item->getAllParentId();
        if ($all_parents) {
            foreach ($all_parents as $parent) {
                $parent_item = new FilterItem($parent);
                if (!Tools::getValue($parent_item->type.'_'.$parent_item->id)) {
                    $result &= false;
                }
            }
        }

        return $result;
    }

    /**
     * Check is current element is selected
     *
     * @param $type
     * @param $id_item
     * @param $id_element
     *
     * @return bool
     */
    public static function checkActive($type, $id_item, $id_element)
    {
        $active = Tools::getValue($type.'_'.$id_item);
        if (is_array($active)) {
            $active = $active[0];
        }
        if (strpos($active, '|')) {
            $active = explode('|', $active);
        }
        if ($active) {
            if (is_array($active) && in_array($id_element, $active)) {
                return true;
            } elseif ($id_element == $active) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get price range that was selected
     *
     * @param $type
     * @param $id_item
     * @param $position
     *
     * @return bool
     */
    public static function getActiveRange($type, $id_item, $position)
    {
        $active = Tools::getValue($type.'_'.$id_item);
        if (!$active) {
            return false;
        }
        if (is_array($active)) {
            $active = $active[0];
        }
        $active = explode('|', $active);
        if ($active) {
            if ($position == 'from') {
                return $active[0];
            }
            if ($position == 'to') {
                return $active[1];
            }
        }

        return false;
    }

    protected function getAllProducts($parameter, $key, $type)
    {
        if (!$type && !$type = Tools::getValue('type')) {
            return false;
        }
        $process = new FilterProcess();
        $items = $process->filter($type, array($parameter => $key));
        if ($items) {
            return count($items);
        } else {
            return 0;
        }
    }
    /**
     * Get products that will be available for show if select this parameter
     *
     * @param $parameter
     * @param $key
     * @param $type
     *
     * @return bool|int
     */
    protected function getPossibleProducts($parameter, $key, $type)
    {
        if (!$type && !$type = Tools::getValue('type')) {
            return false;
        }
        $process = new FilterProcess();
        $all_parameters = Tmadvancedfilter::filterParameters($type, Tools::getAllValues());
        if (array_key_exists($parameter, $all_parameters)) {
            $all_parameters[$parameter] = $key;
        } else {
            $possible_parameter = array($parameter => $key);
            $all_parameters = array_merge($all_parameters, $possible_parameter);
        }
        $items = $process->filter($type, $all_parameters);
        if ($items) {
            return count($items);
        } else {
            return 0;
        }
    }

    /**
     * Get all available filters parameters(both "top|column")
     *
     * @return array
     */
    private function getAvailableParameters()
    {
        $result = array();
        foreach (array_keys($this->filter_types) as $type) {
            $id_filter = Filter::checkFilterExists($this->context->shop->id, $type);
            if ($id_filter) {
                $filter_items = FilterItem::getFilterItems($id_filter);
                if ($filter_items) {
                    foreach ($filter_items as $filter_item) {
                        $result[$type][] = $filter_item['type'].'_'.$filter_item['id_item'];
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Check all send parameters, to exclude non-filter elements
     *
     * @param $type       (string) filter type
     * @param $parameters (array) all parameters
     *
     * @return array
     */
    public function filterParameters($type, $parameters)
    {
        $this->available_parameters = $this->getAvailableParameters();
        $filtered = array();
        if ($parameters) {
            foreach ($parameters as $name => $parameter) {
                if (isset($this->available_parameters[$type])
                    && in_array($name, $this->available_parameters[$type])
                    && $parameter
                ) {
                    if ($this->checkAllParentsSelected(false, $name)) {
                        $filtered[$name] = $parameter;
                    }
                }
            }
        }

        return $filtered;
    }

    /**
     * Adapt filter parameters for good url
     *
     * @param $parameters
     *
     * @return array
     */
    protected function adaptFilterParameters($parameters)
    {
        $result = array();
        foreach ($parameters as $name => $parameter) {
            if (is_array($parameter)) {
                $values = array();
                foreach ($parameter as $value) {
                    $values[] = $value;
                }
                $result[$name] = implode('|', $values);
            } elseif ($parameter != 0) {
                $result[$name] = $parameter;
            }
        }

        return $result;
    }

    /**
     * Rebuild filter by ajax calling
     *
     * @param $type
     * @param $parameters
     *
     * @return mixed
     */
    public function buildFilterAjax($type, $parameters)
    {
        $id_filter = Filter::checkFilterExists($this->context->shop->id, $type);
        $filter = new Filter($id_filter);
        $this->context->smarty->assign(
            'content',
            $this->buildFilter($type, $this->context->shop->id, $this->context->language->id)
        );
        if ($filter->selected_filters) {
            $parameters = $this->adaptFilterParameters($parameters);
            $this->context->smarty->assign('filter_parameters', $this->buildFilterParameters($parameters, $id_filter));
        }

        return $this->display($this->_path, '/views/templates/hook/'.$type.'_filter.tpl');
    }

    /**
     * Build hash used in url to allow share customer filter link
     *
     * @param $type
     *
     * @return string
     */
    public function buildHash($type)
    {
        $hash = '';
        $parameters = $this->adaptFilterParameters(
            Tmadvancedfilter::filterParameters($type, Tools::getAllValues())
        );
        $page_view_parameters = $this->filterPageViewParameters(Tools::getAllValues());
        $type = array('type' => $type);
        $base_url = Tmadvancedfilter::getTMAdvancedFilterUrl();
        $url = Tmadvancedfilter::getTMAdvancedFilterLink(
            'tmadvancedfilter',
            array_merge($parameters, $type, $page_view_parameters),
            $this->context->language->id
        );
        $hash .= str_replace($base_url, '', $url);

        return $hash;
    }

    /**
     * Build list of currently selected filter parameters
     *
     * @param $parameters
     * @param $type
     *
     * @return array|bool
     */
    public function buildFilterParameters($parameters, $id_filter)
    {
        if (!$parameters) {
            return false;
        }

        $result = array();
        foreach ($parameters as $name => $parameter) {
            $item_type = explode('_', $name);
            $public_data = array();
            if (strpos($parameter, '|')) {
                if ($item_type[0] == 'other' && $item_type[1] == FilterItem::getPriceFieldId($id_filter)) {
                    $public_data[] = $this->getFilterParametersValues($parameter, $item_type[0], $item_type[1]);
                } else {
                    $params = explode('|', $parameter);
                    foreach ($params as $key => $param) {
                        $public_data[$key] = $this->getFilterParametersValues($param, $item_type[0], $item_type[1]);
                    }
                }
            } else {
                $public_data[] = $this->getFilterParametersValues($parameter, $item_type[0], $item_type[1]);
            }
            $result[$name] = $public_data;
            $public_data = array();
        }

        return $result;
    }

    /**
     * Decode filter parameters and get information about it
     *
     * @param $parameter
     * @param $type
     * @param $id_filter_item
     *
     * @return array
     */
    public function getFilterParametersValues($parameter, $type, $id_filter_item)
    {
        $result = array();
        $filter_item = new FilterItem($id_filter_item);
        switch ($type) {
            case 'category':
                $category = new Category($parameter, $this->context->language->id);
                $result['id'] = $category->id;
                $result['name'] = $category->name;
                $result['field_type'] = $filter_item->field_type;
                break;
            case 'feature':
                $feature = new FeatureValue($parameter, $this->context->language->id);
                $result['id'] = $feature->id;
                $result['name'] = $feature->value;
                $result['field_type'] = $filter_item->field_type;
                break;
            case 'attribute':
                $attribute = new Attribute($parameter, $this->context->language->id);
                $result['id'] = $attribute->id;
                $result['name'] = $attribute->name;
                $result['type'] = $attribute->name;
                $result['field_type'] = $filter_item->field_type;
                break;
            case 'other':
                $result = $this->getOtherFilterParametersValues(
                    $filter_item->position_inside,
                    $parameter,
                    $filter_item->field_type
                );
                break;
        }

        return $result;
    }

    /**
     * Get specific filter parameters info
     *
     * @param $type_other
     * @param $parameter
     * @param $field_type
     *
     * @return array
     */
    protected function getOtherFilterParametersValues($type_other, $parameter, $field_type)
    {
        $result = array();
        switch ($type_other) {
            case '1': // price
                $price_text = '';
                if (!$parameter) {
                    return false;
                }
                $prices = explode('|', $parameter);
                $from = $prices[0];
                $to = $prices[1];
                if ($from || $to) {
                    $price_text .= $this->l('Price ');
                } else {
                    return false;
                }
                if ($from) {
                    $price_text .= sprintf($this->l('from: %s%s '), $from, $this->context->currency->sign);
                }
                if ($to) {
                    $price_text .= sprintf($this->l('to: %s%s'), $to, $this->context->currency->sign);
                }
                $result['id'] = false;
                $result['name'] = $price_text;
                $result['field_type'] = $field_type;
                break;
            case '2': // manufacturer
                $manufacturer = new Manufacturer($parameter, $this->context->language->id);
                $result['id'] = $manufacturer->id;
                $result['name'] = $manufacturer->name;
                $result['field_type'] = $field_type;
                break;
            case '3': // suppliers
                $supplier = new Supplier($parameter, $this->context->language->id);
                $result['id'] = $supplier->id;
                $result['name'] = $supplier->name;
                $result['field_type'] = $field_type;
                break;
            case '4': // on sale
                $result['id'] = false;
                $result['name'] = $this->l('On Sale');
                $result['field_type'] = $field_type;
                break;
            case '5': // condition
                $conditions = array(1 => 'New', 2 => 'Used', 3 => 'Refurbished');
                $result['id'] = $parameter;
                $result['name'] = $conditions[$parameter];
                $result['field_type'] = $field_type;
                break;
            case '6': // only with image
                $result['id'] = false;
                $result['name'] = $this->l('Image required');
                $result['field_type'] = $field_type;
                break;
            case '7': // only available products
                $result['id'] = false;
                $result['name'] = $this->l('Available only');
                $result['field_type'] = $field_type;
                break;
        }

        return $result;
    }

    public function filterPageViewParameters($parameters)
    {
        $result = array();
        $page_view_parameters = array('p', 'n', 'orderby', 'orderway', 'orderbydefault', 'stock_management');
        foreach ($parameters as $name => $value) {
            if (in_array($name, $page_view_parameters)) {
                $result[$name] = $value;
            }
        }

        return $result;
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/tmadvancedfilter.js');
        $this->context->controller->addCSS($this->_path.'/views/css/tmadvancedfilter.css');
    }

    public function hookDisplayTopColumn()
    {
        $filterprocess = new FilterProcess();
        $id_filter = Filter::checkFilterExists($this->context->shop->id, 'top');
        $filter = new Filter($id_filter);
        $page_view_parameters = $this->filterPageViewParameters(Tools::getAllValues());
        $parameters = $this->adaptFilterParameters(
            Tmadvancedfilter::filterParameters('top', Tools::getAllValues())
        );
        if (Tools::getIsset('submitTopTmAdvancedFilter')) {
            $type = array('type' => 'top');
            Tools::redirect(
                Tmadvancedfilter::getTMAdvancedFilterLink(
                    'tmadvancedfilter',
                    array_merge($parameters, $type, $page_view_parameters),
                    $this->context->language->id
                )
            );
        }
        if ($parameters) {
            $this->context->smarty->assign('result', count($filterprocess->filter('top', $parameters)));
        }
        if ($filter->selected_filters) {
            $this->context->smarty->assign('filter_parameters', $this->buildFilterParameters($parameters, $id_filter));
        }
        $this->context->smarty->assign(
            'content',
            $this->buildFilter('top', $this->context->shop->id, $this->context->language->id)
        );

        return $this->display($this->_path, '/views/templates/hook/top_filter.tpl');
    }

    public function hookDisplayLeftColumn()
    {
        $filterprocess = new FilterProcess();
        $id_filter = Filter::checkFilterExists($this->context->shop->id, 'column');
        $filter = new Filter($id_filter);
        $page_view_parameters = $this->filterPageViewParameters(Tools::getAllValues());
        $parameters = $this->adaptFilterParameters(
            Tmadvancedfilter::filterParameters('column', Tools::getAllValues())
        );
        if (Tools::getIsset('submitColumnTmAdvancedFilter')) {
            $type = array('type' => 'column');
            Tools::redirect(
                Tmadvancedfilter::getTMAdvancedFilterLink(
                    'tmadvancedfilter',
                    array_merge($parameters, $type, $page_view_parameters),
                    $this->context->language->id
                )
            );
        }
        if ($parameters) {
            $this->context->smarty->assign('result', count($filterprocess->filter('column', $parameters)));
        }
        if ($filter->selected_filters) {
            $this->context->smarty->assign('filter_parameters', $this->buildFilterParameters($parameters, $id_filter));
        }
        $this->context->smarty->assign(
            'content',
            $this->buildFilter('column', $this->context->shop->id, $this->context->language->id)
        );

        return $this->display($this->_path, '/views/templates/hook/column_filter.tpl');
    }

    public function hookDisplayRightColumn()
    {
        return $this->hookDisplayLeftColumn();
    }

    /**
     * Get shop filter data by filter type
     * @see Tmadvancedfilter::getShopFilterByType(string)
     *
     * @param $type
     *
     * @return bool|Filter
     */
    public static function getShopFilterByType($type)
    {
        $id_filter = Filter::checkFilterExists(Context::getContext()->shop->id, $type);
        if (!$id_filter) {
            return false;
        }

        return new Filter($id_filter, Context::getContext()->language->id);
    }

    public static function getTemplateFile($file_name)
    {
        return _PS_THEME_DIR_.$file_name;
    }

    protected function getWarningMultishop()
    {
        if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL) {
            return $this->displayError(
                $this->l('You cannot manage filters
                    from "All Shops" or "Group Shop" context,
                    select the store you want to edit')
            );
        } else {
            return false;
        }
    }
}
