<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class TmAdvancedFilterFilterModuleFrontController extends ModuleFrontController
{
    public $php_self = 'tmadvancedfilter';

    public function initContent()
    {
        parent::initContent();
        $tmadvancedfilter = new Tmadvancedfilter();
        $process = new FilterProcess();
        $all_parameters = Tools::getAllValues();
        if (!$all_parameters || !isset($all_parameters['type'])) {
            $this->context->smarty->assign(
                array(
                    'products'            => false,
                    'add_prod_display'    => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                    'nbProducts'          => (int)0,
                    'homeSize'            => Image::getSize(ImageType::getFormatedName('home')),
                    'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM'),
                    'parameters'          => $all_parameters
                )
            );
        } else {
            $filtered_parameters = $tmadvancedfilter->filterParameters($all_parameters['type'], $all_parameters);
            $adapted_parameters = $filtered_parameters;
            $result_products = $process->filter($all_parameters['type'], $adapted_parameters);
            if ($result_products) {
                $this->productSort();
                $product_per_page = isset($this->context->cookie->nb_item_per_page) ? (int)$this->context->cookie->nb_item_per_page : Configuration::get(
                    'PS_PRODUCTS_PER_PAGE'
                );
                $n = abs((int)(Tools::getValue('n', $product_per_page)));
                $p = abs((int)(Tools::getValue('p', 1)));
                $order_by = Tools::getValue('orderby', 'name');
                $order_way = Tools::getValue('orderway', 'asc');
                $nbProducts = count($result_products);
                $this->pagination($nbProducts);
                $products = FilterHelper::getProducts(
                    $result_products,
                    Context::getContext()->language->id,
                    $p,
                    $n,
                    $order_by,
                    $order_way
                );
                $this->addColorsToProductList($products);
                $this->context->smarty->assign(
                    array(
                        'products'            => $products,
                        'add_prod_display'    => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                        'nbProducts'          => (int)$nbProducts,
                        'homeSize'            => Image::getSize(ImageType::getFormatedName('home')),
                        'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM'),
                        'parameters'          => $adapted_parameters
                    )
                );
            }
        }
        $this->setTemplate('filter.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_.'product_list.css');
    }
}
