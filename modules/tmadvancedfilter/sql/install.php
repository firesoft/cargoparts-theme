<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter` (
    `id_filter` int(11) NOT NULL AUTO_INCREMENT,
    `id_shop` int(11) NOT NULL,
    `type` VARCHAR(100),
    `counter_status` int(11) NOT NULL DEFAULT \'1\',
    `selected_filters` int(11) NOT NULL DEFAULT \'1\',
    `id_layout` int(11) NOT NULL DEFAULT \'1\',
    `indexed` int(11) NOT NULL DEFAULT \'1\',
    PRIMARY KEY  (`id_filter`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter_lang` (
    `id_filter` int(11) NOT NULL AUTO_INCREMENT,
    `id_lang` int(11) NOT NULL,
    `name` VARCHAR(100),
    `description` VARCHAR(10000),
    PRIMARY KEY  (`id_filter`, `id_lang`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter_items` (
    `id_item` int(11) NOT NULL AUTO_INCREMENT,
    `id_filter` int(11) NOT NULL,
    `type` VARCHAR(100),
    `position_inside` int(11) NOT NULL,
    `sort_order` int(11) NOT NULL,
    `parent` int(11),
    `column` int(11) NOT NULL DEFAULT \'1\',
    `field_type` VARCHAR(100),
    PRIMARY KEY  (`id_item`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter_items_lang` (
    `id_item` int(11) NOT NULL AUTO_INCREMENT,
    `id_lang` int(11) NOT NULL,
    `name` VARCHAR(100),
    `label` VARCHAR(100),
    `description` VARCHAR(10000),
    PRIMARY KEY  (`id_item`, `id_lang`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter_features_indexes` (
    `id_item` int(11) NOT NULL AUTO_INCREMENT,
    `id_filter` int(11) NOT NULL,
    `id_product` int(11) NOT NULL,
    `id_feature` int(11) NOT NULL,
    `id_feature_value` int(11) NOT NULL,
    PRIMARY KEY  (`id_item`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmadvancedfilter_attributes_indexes` (
    `id_item` int(11) NOT NULL AUTO_INCREMENT,
    `id_filter` int(11) NOT NULL,
    `id_product` int(11) NOT NULL,
    `id_attribute` int(11) NOT NULL,
    `id_attribute_value` int(11) NOT NULL,
    PRIMARY KEY  (`id_item`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
