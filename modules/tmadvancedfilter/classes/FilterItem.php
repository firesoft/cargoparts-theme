<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class FilterItem extends ObjectModel
{
    public $id_filter    = 1;
    public $type;
    public $position_inside;
    public $sort_order;
    public $parent;
    public $column;
    public $field_type;
    public $name;
    public $label;
    public $description;
    public $all_parents  = array();
    public $all_children = array();
    public static $definition   = array(
        'table'     => 'tmadvancedfilter_items',
        'primary'   => 'id_item',
        'multilang' => true,
        'fields'    => array(
            'id_filter'       => array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isunsignedInt'),
            'type'            => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 128),
            'position_inside' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'sort_order'      => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'parent'          => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'column'          => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'field_type'      => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 128),
            'name'            => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'label'           => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'description'     => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000)
        )
    );

    public static function getFilterItem($id_item, $id_lang)
    {
        $sql = 'SELECT tmi.*, tmil.`name`, tmil.`label`, tmil.`description`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items tmi
                LEFT JOIN '._DB_PREFIX_.'tmadvancedfilter_items_lang tmil
                ON (tmil.`id_item` = tmi.`id_item`)
                WHERE tmi.`id_item` = '.(int)$id_item.'
                AND tmil.`id_lang` = '.(int)$id_lang;

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get all filter items for current language
     *
     * @param $id_filter (int)
     * @param $id_lang   (int)
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getFilterItems($id_filter, $id_lang = false)
    {
        $inner_order = '';
        if ($inner_order) {
            $inner_order = 'ti.`parent`, ';
        }
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $sql = 'SELECT ti.*, til.*
                FROM '._DB_PREFIX_.'tmadvancedfilter_items ti
                LEFT JOIN '._DB_PREFIX_.'tmadvancedfilter_items_lang til
                ON(til.`id_item` = ti.`id_item`)
                WHERE ti.`id_filter` = '.(int)$id_filter.'
                AND til.`id_lang` = '.(int)$id_lang.'
                ORDER BY '.$inner_order.' ti.`sort_order`';

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get all filter items for current language(obj)
     *
     * @param $id_filter (int)
     * @param $id_lang   (int)
     *
     * @return array|bool
     */
    public static function getFilterItemsObj($id_filter, $id_lang, $inner_sort = false)
    {
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $filters = FilterItem::getFilterItems($id_filter, $id_lang, $inner_sort);
        if ($filters) {
            $result = array();
            foreach ($filters as $filter) {
                $result[] = new FilterItem($filter['id_item'], $id_lang);
            }

            return $result;
        }

        return false;
    }

    /**
     * Check if this parameter is already added for this filter
     *
     * @param $id_filter       (int)
     * @param $type            (string)
     * @param $position_inside (int)
     *
     * @return false|null|string
     */
    public static function checkItemInFilter($id_filter, $type, $position_inside)
    {
        $sql = 'SELECT `id_item`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `id_filter` = '.(int)$id_filter.'
                AND `type` = "'.pSql($type).'"
                AND `position_inside` = '.(int)$position_inside;

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Get price parameter field id for filter by filter id
     *
     * @param $id_filter (int)
     *
     * @return false|null|string
     */
    public static function getPriceFieldId($id_filter)
    {
        $sql = 'SELECT `id_item`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `type` = "other"
                AND `position_inside` = "1"
                AND `id_filter` = '.(int)$id_filter;

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Get field id that marked as the "Available only"
     *
     * @param $id_filter (int)
     *
     * @return false|null|string
     */
    public static function getAvailableFieldId($id_filter)
    {
        $sql = 'SELECT `id_item`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `type` = "other"
                AND `position_inside` = "7"
                AND `id_filter` = '.(int)$id_filter;

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Get all parents element's ids
     * @return array
     */
    public function getAllParentId()
    {
        $this->getParentId($this->id);

        return $this->all_parents;
    }

    /**
     * Get parent element id
     *
     * @param bool $id_item
     */
    public function getParentId($id_item = false)
    {
        $item = new FilterItem($id_item);
        if ($item->parent > 0) {
            array_push($this->all_parents, $item->parent);
            $this->getParentId($item->parent);
        }
    }

    /**
     * Get all children element's ids
     * @return array
     */
    public function getAllChildrenId()
    {
        $this->getChildId($this->id);

        return $this->all_children;
    }

    /**
     * Get child element id
     *
     * @param bool $id_item
     *
     * @throws PrestaShopDatabaseException
     */
    public function getChildId($id_item = false)
    {
        $sql = 'SELECT `id_item`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `parent` = '.(int)$id_item;
        if ($result = Db::getInstance()->executeS($sql)) {
            foreach ($result as $child) {
                array_push($this->all_children, $child['id_item']);
                $this->getChildId($child['id_item']);
            }
        }
    }

    /**
     * Get highest sort order from first column items
     *
     * @param $id_filter
     *
     * @return false|null|string
     */
    protected function getMaxSortOrderInFirstColumn($id_filter)
    {
        $sql = 'SELECT MAX(`sort_order`)
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `id_filter` = '.(int)$id_filter.'
                AND `column` = 1';

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Get all items that are not in first column and sort by column at first
     * and by sort order from last column at second
     *
     * @param $id_filter
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    protected function getSortedColumnsItems($id_filter)
    {
        $sql = 'SELECT `id_item`
                FROM '._DB_PREFIX_.'tmadvancedfilter_items
                WHERE `id_filter` = '.(int)$id_filter.'
                AND `column` != 1
                AND `parent` = 0
                ORDER by `column`, `sort_order`';

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Move all elements to first column after filter layout changing
     *
     * @param $id_filter
     *
     * @return bool
     * @throws PrestaShopException
     */
    public function moveAllItemsInFirstColumn($id_filter)
    {
        $other_items = $this->getSortedColumnsItems($id_filter);
        if ($other_items) {
            $order = $this->getMaxSortOrderInFirstColumn($id_filter) + 1;
            foreach ($other_items as $item) {
                $filter_item = new FilterItem($item['id_item']);
                $filter_item->column = 1;
                $filter_item->sort_order = $order++;
                if (!$filter_item->update()) {
                    return false;
                }
            }
        }

        return true;
    }
}
