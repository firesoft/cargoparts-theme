<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class AdminTMAdvancedFilterController extends ModuleAdminController
{
    /**
     *  Get filter creates form
     */
    public function ajaxProcessGetForm()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $id_filter = Tools::getValue('id_filter');
        $type = Tools::getValue('type');
        if ($id_filter) {
            $item = new Filter($id_filter);
        } else {
            $item = new Filter();
            $item->type = $type;
            $item->id_shop = $this->context->shop->id;
        }
        $output = $tmadvancedfilter->renderFilterForm($item);
        if ($output) {
            die(Tools::jsonEncode(array('status' => true, 'result' => $output)));
        }
        die(Tools::jsonEncode(array('status' => false, 'result' => $this->l('Oops...something went wrong!'))));
    }

    /**
     * Save/update filter creates form
     * @throws PrestaShopException
     */
    public function ajaxProcessUpdateFilterForm()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $output = '';
        $errors = array();
        $id_filter = (int)Tools::getValue('id_filter');
        if ($id_filter) {
            $filter = new Filter($id_filter);
            if ($filter->id_layout != 1 && $filter->id_layout != Tools::getValue('id_layout')) {
                $filteritems = new FilterItem();
                if (!$filteritems->moveAllItemsInFirstColumn($filter->id)) {
                    $errors[] = $this->l('Can\'t move filter items to default column!');
                }
            }
            $filter->id_layout = Tools::getValue('id_layout');
        } else {
            $filter = new Filter();
            $filter->id_layout = Tools::getValue('id_layout');
            $filter->indexed = 1;
        }
        $filter->type = Tools::getValue('type');
        $filter->id_shop = $this->context->shop->id;
        $filter->counter_status = Tools::getValue('counter_status');
        $filter->selected_filters = Tools::getValue('selected_filters');
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $filter->name[$lang['id_lang']] = Tools::getValue('name_'.$lang['id_lang']);
            $filter->description[$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang']);
        }
        if ($id_filter) {
            if (!$filter->update()) {
                $errors[] = $this->l('Can\'t update filter information!');
            }
        } else {
            if (!$filter->save()) {
                $errors[] = $this->l('Can\'t add filter information!');
            } else {

            }
        }
        if (!count($errors)) {
            $this->context->smarty->assign(
                'exist_filter',
                array(
                    'id_filter' => $filter->id,
                    'type'      => $filter->type,
                    'indexed'   => $filter->indexed,
                    'id_layout' => $filter->id_layout,
                    'name'      => $filter->name[$this->context->language->id]
                )
            );
            $this->context->smarty->assign('type', $filter->type);
            $output = $tmadvancedfilter->display(
                $tmadvancedfilter->module_path,
                'views/templates/admin/filter.tpl'
            );
            die(Tools::jsonEncode(
                array(
                    'status'         => true,
                    'id_filter'      => $filter->id,
                    'btn_index_text' => sprintf($this->l('Reindex %s filter'), $filter->type),
                    'result'         => $this->l(
                        'Item successfully saved.'
                    ),
                    'content'        => $output,
                )
            ));
        }
        die(Tools::jsonEncode(
            array('status' => false, 'response_msg' => $tmadvancedfilter->displayError(implode('<br />', $errors)))
        ));
    }

    /**
     * Remove filter with all entries
     */
    public function ajaxProcessRemoveFilter()
    {
        $id_filter = Tools::getValue('id_filter');
        $confirmed = Tools::getValue('confirmed');
        $filter = new Filter($id_filter);
        $tmadvancedfilter = new Tmadvancedfilter();
        $indexer = new Indexer();
        $errors = array();
        if (!$confirmed) {
            $this->context->smarty->assign('id_filter', $id_filter);
            $output = $tmadvancedfilter->display(
                $tmadvancedfilter->module_path,
                'views/templates/admin/_parts/filter_remove_confirmation.tpl'
            );
            die(Tools::jsonEncode(array('status' => true, 'result' => $output)));
        }
        if (!Validate::isLoadedObject($filter)) {
            die(Tools::jsonEncode(array('status' => false, 'response_msg' => $this->l('Can\'t load filter data!'))));
        }
        $filter_items = FilterItem::getFilterItems($id_filter);
        if ($filter_items) {
            foreach ($filter_items as $item) {
                $i = new FilterItem($item['id_item']);
                if (!$i->delete()) {
                    $errors[] = sprintf($this->l('Can\t remove item %s'), $i['name']);
                }
            }
        }
        if (!count($errors)) {
            if (!$indexer->dropIndexTable($filter->type)) {
                $errors[] = $this->l('Can\t remove indexed table');
            }
            if (!$indexer->dropIndexPriceTable($filter->type)) {
                $errors[] = $this->l('Can\t remove indexed price table');
            }
            if (!$indexer->deleteAllFilterAttributesIndexes($filter->id)) {
                $errors[] = $this->l('Can\t remove indexed filter attributes');
            }
            if (!$indexer->deleteAllFilterFeaturesIndexes($filter->id)) {
                $errors[] = $this->l('Can\t remove indexed filter features');
            }
            if (!$filter->delete()) {
                $errors[] = $this->l('Can\t remove item filter');
            }
        }
        if (count($errors)) {
            die(Tools::jsonEncode(
                array('status' => false, 'response_msg' => $tmadvancedfilter->displayError(implode('<br />', $errors)))
            ));
        }
        die(Tools::jsonEncode(
            array('status'          => true,
                  'type'            => $filter->type,
                  'btn_filter_text' => sprintf($this->l('Create %s filter'), Tools::ucfirst($filter->type)),
                  'response_msg'    => $this->l('Filter successfully removed.'))
        ));
    }

    /**
     * Get filter parameter creates form
     */
    public function ajaxProcessGetFilterForm()
    {
        $id_item = (int)Tools::getValue('id_item');
        $id_filter = Tools::getValue('id_filter');
        $type = Tools::getValue('type');
        $id = Tools::getValue('id');
        $id_parent = Tools::getValue('id_parent');
        $sort_order = Tools::getValue('sort_order');
        $column = Tools::getValue('column');
        $tmadvancedfilter = new Tmadvancedfilter();
        if ($id_item > 0) {
            $item = new FilterItem($id_item);
        } else {
            $item = new FilterItem();
            $item->id_filter = $id_filter;
            $item->type = $type;
            $item->position_inside = $id;
            $item->sort_order = $sort_order;
            $item->column = $column;
            $item->parent = $id_parent;
        }
        $output = $tmadvancedfilter->renderForm($item, $item->id_filter);
        if ($output) {
            die(Tools::jsonEncode(array('status' => true, 'result' => $output)));
        }
        die(Tools::jsonEncode(array('status' => 'false', 'response_msg' => $this->l('Oops...something went wrong!'))));
    }

    /**
     * Validate filter creates form before saving/updating
     */
    public function ajaxProcessValidateUpdateForm()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $errors = array();
        if (Tools::isEmpty(Tools::getValue('id_filter')) || !Validate::isInt(Tools::getValue('id_filter'))) {
            $errors[] = $this->l('No `id_filter` defined or it\'s invalid.');
        }
        if (Tools::isEmpty(Tools::getValue('position_inside'))
            || !Validate::isInt(Tools::getValue('position_inside'))) {
            $errors[] = $this->l('No `position_inside` defined or it\'s invalid.');
        }
        if (Tools::isEmpty(Tools::getValue('sort_order')) || !Validate::isInt(Tools::getValue('sort_order'))) {
            $errors[] = $this->l('No `sort_order` defined or it\'s invalid.');
        }
        if (Tools::isEmpty(Tools::getValue('type')) || !Validate::isGenericName(Tools::getValue('type'))) {
            $errors[] = $this->l('No `type` defined or it\'s invalid.');
        }
        if (!count($errors)) {
            die(Tools::jsonEncode(array('status' => true)));
        }
        die(Tools::jsonEncode(
            array('status' => false, 'errors' => $tmadvancedfilter->displayError(implode('<br />', $errors)))
        ));
    }

    /**
     * Save/update filter parameter creates form
     */
    public function ajaxProcessUpdateForm()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $errors = array();
        $id_item = (int)Tools::getValue('id_item');
        if ($id_item > 0) {
            $filter_item = new FilterItem($id_item);
        } else {
            $filter_item = new FilterItem();
        }
        $filter_item->id_filter = Tools::getValue('id_filter');
        $filter_item->type = Tools::getValue('type');
        $filter_item->position_inside = Tools::getValue('position_inside');
        $filter_item->sort_order = Tools::getValue('sort_order');
        $filter_item->column = Tools::getValue('column');
        $filter_item->parent = Tools::getValue('parent');
        $filter_item->field_type = Tools::getValue('field_type');
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            $filter_item->name[$lang['id_lang']] = Tools::getValue('name_'.$lang['id_lang']);
            $filter_item->label[$lang['id_lang']] = Tools::getValue('label_'.$lang['id_lang']);
            $filter_item->description[$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang']);
        }
        if ($id_item > 0) {
            if (!$filter_item->update()) {
                $errors[] = $this->l('Can\'t update filter settings!');
            }
        } else {
            if (!$filter_item->save()) {
                $errors[] = $this->l('Can\'t add filter settings!');
            }
        }
        if (!Filter::markIndexation($filter_item->id_filter, 0)) {
            $errors[] = $this->l('Can\'t mark indexation!');
        }
        if (!count($errors)) {
            die(Tools::jsonEncode(
                array(
                    'status'       => true,
                    'id_filter'    => $filter_item->id_filter,
                    'type'         => Filter::getFilterTypeById($filter_item->id_filter),
                    'response_msg' => $this->l(
                        'Item successfully saved.'
                    )
                )
            ));
        }
        die(Tools::jsonEncode(
            array('status' => false, 'response_msg' => $tmadvancedfilter->displayError(implode('<br />', $errors)))
        ));
    }

    /**
     * Remove filter parameter and all children parameters
     */
    public function ajaxProcessRemoveFilterItem()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $id_item = Tools::getValue('id_item');
        $confirmed = (int)Tools::getValue('confirmed');
        $children_items = Tools::getValue('children_items');
        if (!$confirmed) {
            $this->context->smarty->assign('id_item', $id_item);
            $message = $tmadvancedfilter->display(
                $tmadvancedfilter->module_path,
                'views/templates/admin/_parts/remove_confirmation.tpl'
            );
            if ($message) {
                die(Tools::jsonEncode(array('status' => true, 'response_msg' => $message)));
            } else {
                die(Tools::jsonEncode(
                    array('status' => false, 'response_msg' => $this->l(
                        'Something went wrong! Can\'t get remove confirmation form!'
                    ))
                ));
            }
        } else {
            if (!$id_item) {
                die(Tools::jsonEncode(
                    array('status' => false, 'response_msg' => $this->l('Something went wrong! Can\'t get item id!'))
                ));
            } else {
                if ($children_items) {
                    foreach ($children_items as $id_child) {
                        $child_item = new FilterItem($id_child);
                        if (!$child_item->delete()) {
                            die(Tools::jsonEncode(
                                array('status' => false, 'response_msg' => $this->l('Can\'t remove child!'))
                            ));
                        }
                    }
                }
                $item = new FilterItem($id_item);
                if (!$item->delete()) {
                    die(Tools::jsonEncode(
                        array('status' => false, 'response_msg' => $this->l(
                            'Something went wrong! Can\'t remove item!'
                        ))
                    ));
                } else {
                    if (!Filter::markIndexation($item->id_filter, 0)) {
                        die(Tools::jsonEncode(
                            array('status' => false, 'response_msg' => $this->l('Can\'t mark for indexation!'))
                        ));
                    }
                    die(Tools::jsonEncode(
                        array('status' => true, 'type' => Filter::getFilterTypeById(
                            $item->id_filter
                        ), 'id_filter' => $item->id_filter, 'response_msg' => $this->l(
                            'Item(s) successfully removed.'
                        ))
                    ));
                }
            }
        }
    }

    /**
     * Refresh list with rest filter items, after filter changing
     */
    public function ajaxProcessRefreshAllItemsList()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $id_filter = Tools::getValue('id_filter');
        $this->context->smarty->assign(
            'available_filters',
            $tmadvancedfilter->getAllFilters($id_filter, $this->context->shop->id, $this->context->language->id)
        );
        $content = $tmadvancedfilter->display(
            $tmadvancedfilter->module_path,
            'views/templates/admin/_parts/available_list.tpl'
        );
        if ($content) {
            die(Tools::jsonEncode(array('status' => true, 'content' => $content)));
        }
        die(Tools::jsonEncode(array('status' => false, 'response_msg' => $this->l('Unknown error!!!'))));
    }

    /**
     * Refresh list with selected filter items, after filter changing
     */
    public function ajaxProcessRefreshSelectedItemsList()
    {
        $tmadvancedfilter = new Tmadvancedfilter();
        $id_filter = Tools::getValue('id_filter');
        $id_layout = Tools::getValue('id_layout');
        $this->context->smarty->assign('exist_filter', array('id_filter' => $id_filter));
        $this->context->smarty->assign(
            'filters',
            $tmadvancedfilter::getBackTree($id_filter)
        );
        $content = $tmadvancedfilter->display(
            $tmadvancedfilter->module_path,
            'views/templates/admin/_layouts/layout_'.$id_layout.'.tpl'
        );
        if ($content || is_string($content)) {
            die(Tools::jsonEncode(array('status' => true, 'content' => $content)));
        }
        die(Tools::jsonEncode(array('status' => false, 'response_msg' => $this->l('Unknown error!!!'))));
    }

    /**
     * Update items sort order after filter changing
     * @throws PrestaShopException
     */
    public function ajaxProcessUpdateItemsSortOrders()
    {
        $errors = array();
        $order_action = Tools::getValue('sort_action');
        $sorted_data = Tools::getValue('data');
        $layout_column = Tools::getValue('layout_column');
        if ($sorted_data) {
            foreach ($sorted_data as $id => $sort_order) {
                if (Validate::isInt($id)) {
                    $item = new FilterItem($id);
                    $item->sort_order = $sort_order;
                    if ($layout_column) {
                        $item->column = $layout_column;
                    }
                    if (!$item->update()) {
                        $errors[] = $this->l('Can\'t update item(s) sort order');
                    }
                }
            }
        }
        if (count($errors)) {
            die(Tools::jsonEncode(
                array('status' => false, 'response_msg' => $this->l('Can\'t update item(s) sort order'))
            ));
        }
        if ($order_action == 'update') {
            die(Tools::jsonEncode(
                array('status' => true, 'response_msg' => $this->l('Sort orders is successfully updated'))
            ));
        }
        if ($order_action == 'add') {
            die(Tools::jsonEncode(array('status' => true, 'response_msg' => false)));
        }
        die(Tools::jsonEncode(array()));
    }

    /*
     * Get info about available products to calculate time for re-indexation
     */
    public function ajaxProcessGetSuitableProductsInfo()
    {
        $indexer = new Indexer();
        if (!$info = $indexer->splitSuitableProducts()) {
            die(Tools::jsonEncode(
                array('status' => false, 'response_msg' => $this->l('Can\'t get required information about products list'))
            ));
        }
        die(Tools::jsonEncode(array('status' => true, 'info' => $info)));
    }

    /**
     * Reindex filter tables
     */
    public function ajaxProcessReindexFilters()
    {
        $type = Tools::getValue('type');
        $part = Tools::getValue('part');
        $parts = Tools::getValue('parts');
        $products = Tools::getValue('products');
        $indexer = new Indexer();
        $result = $indexer->rebuildAllEntries($type, $part, $products);
        if ($part == $parts) {
            if ($result && Filter::markIndexation(Filter::checkFilterExists($this->context->shop->id, $type), 1)) {
                die(Tools::jsonEncode(
                    array('status' => true, 'type' => $type, 'response_msg' => $this->l(
                        'All products are successfully re-indexed.'
                    ))
                ));
            }
        }
        if (!$result) {
            die(Tools::jsonEncode(
                array('status' => false, 'response_msg' => $this->l('Can\'t reindex products!!! Unknown error'))
            ));
        }
        die(Tools::jsonEncode(
            array('status' => true)
        ));
    }
}
