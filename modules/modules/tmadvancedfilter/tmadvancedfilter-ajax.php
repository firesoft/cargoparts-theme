<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Newsletter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster (Alexander Grosul)
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/tmadvancedfilter.php');
$tmadvancedfilter = new Tmadvancedfilter();
if (Tools::getValue('action') == 'refreshFilter') {
    $id_filter = Tools::getValue('id_filter');
    $type = Tools::getValue('type');
    $filterprocess = new FilterProcess();
    $filtered_parameters = $tmadvancedfilter->filterParameters($type, Tools::getAllValues());
    $products = $filterprocess->filter($type, $filtered_parameters);
    $content = $tmadvancedfilter->buildFilterAjax($type, $filtered_parameters);
    if (!$filtered_parameters) {
        $products = false;
    }
    die(Tools::jsonEncode(
        array(
            'status'   => true,
            'products' => $products,
            'count'    => count($products),
            'message'  => $content,
            'type'     => Tools::ucfirst($type),
            'url'      => $tmadvancedfilter->buildHash($type, array_merge(array('type' => $type), $filtered_parameters))
        )
    ));
}

