<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Filter extends ObjectModel
{
    public $id_shop;
    public $type;
    public $name;
    public $description;
    public $counter_status;
    public $selected_filters;
    public $id_layout;
    public $indexed;
    public static $definition = array(
        'table'     => 'tmadvancedfilter',
        'primary'   => 'id_filter',
        'multilang' => true,
        'fields'    => array(
            'id_shop'          => array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isunsignedInt'),
            'type'             => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isGenericName', 'size' => 128),
            'name'             => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
            'description'      => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000),
            'counter_status'   => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'selected_filters' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'id_layout'        => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
            'indexed'          => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
        )
    );

    /**
     * Get all filters and filters info related to shop
     *
     * @param $id_shop (int) shop id
     * @param $id_lang (int) language id
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getAllShopFilters($id_shop, $id_lang)
    {
        $sql = 'SELECT taf.*, tafl.*
                FROM '._DB_PREFIX_.'tmadvancedfilter taf
                LEFT JOIN '._DB_PREFIX_.'tmadvancedfilter_lang tafl
                ON(tafl.`id_filter`=taf.`id_filter`)
                WHERE taf.`id_shop` = '.(int)$id_shop.'
                AND tafl.`id_lang` ='.(int)$id_lang;

        return Db::getInstance()->executeS($sql);
    }

    /**
     *  Check if this filter type exists for shop by id
     *
     * @param $id_shop (int) shop id
     * @param $type    (string) filter type
     *
     * @return false|null|string(id_filter)
     */
    public static function checkFilterExists($id_shop, $type)
    {
        $sql = 'SELECT `id_filter`
                FROM '._DB_PREFIX_.'tmadvancedfilter
                WHERE `type` = "'.pSql($type).'"
                AND `id_shop` = '.(int)$id_shop;

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Get filter type by id
     *
     * @param $id_filter (int)
     *
     * @return mixed
     */
    public static function getFilterTypeById($id_filter)
    {
        $filter = new Filter($id_filter);

        return $filter->type;
    }

    /**
     * Mark filter indexed after indexation process
     * or mark not indexed if indexation required
     *
     * @param $id_filter (int)
     * @param $status    (int)
     *
     * @return bool
     */
    public static function markIndexation($id_filter, $status)
    {
        return Db::getInstance()->update('tmadvancedfilter', array('indexed' => (int)$status), '`id_filter` = '.(int)$id_filter);
    }
}
