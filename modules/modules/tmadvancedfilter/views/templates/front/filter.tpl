{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{capture name=path}{l s='Advanced Filter' mod='tmadvancedfilter'}{/capture}

<h1 class="page-heading product-listing">{l s='Advanced Filter' mod='tmadvancedfilter'}</h1>

{if isset($products) && $products}
  <div class="content_sortPagiBar">
    <div class="sortPagiBar clearfix">
      {include file=Tmadvancedfilter::getTemplateFile('product-sort.tpl')}
      {include file=Tmadvancedfilter::getTemplateFile('nbr-product-page.tpl')}
    </div>
  </div>

  {include file=Tmadvancedfilter::getTemplateFile('product-list.tpl') products=$products}

  <div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix">
      {include file=Tmadvancedfilter::getTemplateFile('product-compare.tpl')}
      {include file=Tmadvancedfilter::getTemplateFile('./pagination.tpl') no_follow=1 paginationId='bottom'}
    </div>
  </div>
{else}
  <p class="alert alert-warning">{l s='No found products by this criteria.' mod='tmadvancedfilter'}</p>
{/if}
