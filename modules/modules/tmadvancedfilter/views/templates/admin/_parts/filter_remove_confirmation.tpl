{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="panel">
  <div class="panel-heading">
    <h4>{l s='Remove item?' mod='tmadvancedfilter'}</h4>
  </div>
  <div class="panel-content">
    <div class="alert alert-warning">
      {l s='Are you sure that you want to delete this item? All settings and related items will be removed too!' mod='tmadvancedfilter'}
    </div>
    <input type="hidden" name="id_filter" value="{$id_filter|escape:'htmlall':'UTF-8'}" />
  </div>
  <div class="panel-footer">
    <a href="#" class="btn btn-default btn-cancel">{l s='Cancel' mod='tmadvancedfilter'}</a>
    <a href="#" class="btn btn-danger confirmed-filter-remove">{l s='Remove' mod='tmadvancedfilter'}</a>
  </div>
</div>