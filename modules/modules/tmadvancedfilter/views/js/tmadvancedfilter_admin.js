/**
 * 2002-2017 TemplateMonster
 *
 * TM Advanced Filter
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */
if (typeof Object.create !== 'function') {
  Object.create = function(obj) {
    function F() {
    }

    F.prototype = obj;
    return new F();
  };
}
(function($, window, document, undefined) {
  var tmAdvancedFilterObj = {
    init                             : function(elem) {
      self           = this;
      submited_flag  = false; // check if add/edit form was submitted
      active_element = {}; // add active element object to know with which one we are working now
      element_moved  = false; // check if element was really moved
      list_from      = ''; // get where from element moving start
      validation     = self.validation;
      message        = self.message;
      timer          = false;
      remaining_time = false;
      self.initTabs();
      $('div.filter-items').each(function() {
        var id_filter = $(this).attr('data-id-filter');
        self.initSortable(id_filter);
        self.initChildrenSortable(id_filter);
      });
      $(document).on('click', 'a.reindex', function(e) {
        e.preventDefault();
        self.reindexFilters($(this).attr('data-filter-type'));
      });
      $(document).on('click', 'a.add-filter', function(e) {
        e.preventDefault();
        self.addFilter($(this), 0);
      });
      $(document).on('click', 'button[name="updateFilter"]', function(e) {
        e.preventDefault();
        self.addFilter($(this), 1);
        $.fancybox.close();
      });
      $(document).on('click', 'a.remove-filter', function(e) {
        e.preventDefault();
        self.removeFilter($(this).attr('data-id-filter'), 0);
      });
      $(document).on('click', 'a.confirmed-filter-remove', function(e) {
        e.preventDefault();
        self.removeFilter($(this).parents('.tmadvancedfilter').find('input[name="id_filter"]').val(), 1);
        $.fancybox.close();
      });
      $(document).on('click', 'a.edit-filter', function(e) {
        e.preventDefault();
        self.addFilter($(this), 0);
      });
      $(document).on('click', 'button[name="updateFilterItem"]', function(e) {
        e.preventDefault();
        message.clearMessages($(this)); // remove all messages before do something
        var errors = validation.validateUpdateForm($(this)); // validate form data before update filter (false = if no errors found and html if found)
        if (!errors) {
          $.fancybox.close();
          var result = self.updateFilterItem($(this)); // try to update filter data, false - if done and "errors" - if not
          if (result) {
            submited_flag  = true;  // set true to allow item move
            active_element = {}; // destroy active element
          }
        } else {
          message.displayError($(this), errors); // display all found errors
        }
      });
      $(document).on('click', 'a.remove-item', function(e) {
        e.preventDefault();
        self.removeFilterItem($(this), 0);
      });
      $(document).on('click', 'a.confirmed-remove', function(e) {
        e.preventDefault();
        $(this).addClass('.disabled');
        var result = self.removeFilterItem($(this), 1);
      });
      $(document).on('click', 'a.edit-item', function(e) {
        e.preventDefault();
        var id_filter = $(this).closest('.filter-items').find('input[name="id_filter"]').val();
        self.getAddForm($(this).attr('data-id-item'), id_filter, $(this).attr('data-sort-order'), $(this).attr('data-type'), $(this).attr('data-id'), $(this).parents('li').attr('data-id-item'), false, $(this).closest('ul.filter-column').attr('data-filter-column'));
        active_element = {};
      });
      $(document).on('click', 'a.btn-cancel', function(e) {
        e.preventDefault();
        if (element_moved) {
          active_element.sortable('cancel');
          element_moved = false;
        }
        active_element = {};
        $.fancybox.close();
      });
    },
    addFilter                        : function(data, confirm) {
      params = {};
      if (!confirm) {
        params['action'] = 'getForm';
        params['type']   = data.attr('data-filter-type');
        if (data.attr('data-id-filter')) {
          params['id_filter'] = data.attr('data-id-filter');
        }
      } else {
        params['action']    = 'updateFilterForm';
        params['id_filter'] = data.parents('form').find('input[name="id_filter"]').val();
        params['type']      = data.parents('form').find('input[name="type"]').val();
        data.parents('form').find('input').each(function() {
          params[$(this).attr('name')] = $(this).val();
        });
        data.parents('form').find('textarea').each(function() {
          params[$(this).attr('name')] = tinyMCE.get($(this).attr('name')).getContent();
        });
        if (!data.attr('data-id-filter')) {
          var id_layout = data.parents('form').find('input[name="id_layout"]:checked').val();
          if (!id_layout) {
            id_layout = 1;
          }
          params['id_layout'] = id_layout;
        }
        params['counter_status']   = data.parents('form').find('input[name="counter_status"]:checked').val();
        params['selected_filters'] = data.parents('form').find('input[name="selected_filters"]:checked').val();
      }
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : params,
          success  : function(response) {
            if (response.status) {
              if (!confirm) {
                self.openFancyBox(response.result, false);
              } else {
                showSuccessMessage(response.result);
                if (response.content) {
                  $('.tmadvancedfilter-panel a[data-filter-type="' + params['type'] + '"]').remove();
                  $('#' + params['type'] + '-advancedfilter').html(response.content);
                  self.initSortable(response.id_filter);
                }
              }
            } else {
              showErrorMessage(response.results);
            }
          }
        }
      )
    },
    removeFilter                     : function(id_filter, confirmed) {
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action    : 'removeFilter',
            id_filter : id_filter,
            confirmed : confirmed
          },
          success  : function(response) {
            if (response.status) {
              if (!confirmed) {
                self.openFancyBox(response.result, false);
              } else {
                showSuccessMessage(response.response_msg);
                $('#filter-items-' + id_filter).closest('.filter-container').fadeOut().remove();
                $('#' + response.type + '-advancedfilter').find('a[data-filter-type="' + response.type + '"]').remove();
                $('#' + response.type + '-advancedfilter > .tmadvancedfilter-panel').prepend('<a href="#" class="btn btn-default add-filter" data-filter-type="' + response.type + '">' + response.btn_filter_text + '</a>');
              }
            } else {
              showErrorMessage(response.response_msg);
            }
          }
        }
      )
    },
    initSortable                     : function(id_filter) {
      this.initSortableAll($('#available-filters-' + id_filter), '#used-filters-' + id_filter + ' .filter-column');
      this.initSortableSelected($('#used-filters-' + id_filter + ' .filter-column'), '#available-filters-' + id_filter);
    },
    initChildrenSortable             : function(id_filter) {
      $('#used-filters-' + id_filter).find('ul.inner-sort-list').each(function() {
        $(this).sortable({
          cursor : 'move',
          items  : '> li',
          update : function(event, ui) {
            self.updateItemsSortOrders(id_filter, self.getAllElementsPositionInSelected(ui.item.parent()), 'update', 0);
          }
        });
      });
    },
    initSortableAll                  : function(elem, connect) {
      elem.sortable({
        cursor      : 'move',
        items       : '> li',
        cancel      : 'li.no-drag',
        connectWith : connect + ', .inner-sort-list',
        start       : function(event, ui) {
          list_from = ui.item.context.parentElement.id;
          ui.item.parents('.filter-items').find('.used-filters').addClass('highlighted');
        },
        stop        : function() {
          list_from = '';
          $('.filter-items').find('.used-filters').removeClass('highlighted');
        },
        update      : function(event, ui) {
          var id_filter = ui.item.parents('.filter-items').find('input[name="id_filter"]').val();
          if (ui.item.context.parentElement.id == list_from) {
            $(this).sortable("cancel");
          } else {
            if (list_from == elem.attr('id')) {
              active_element = ui.item; // set this element active
              element_moved  = true;
              var sort_order = self.getPositionInSelected(ui);
              var id_parent  = ui.item.parents('li').attr('data-id-item');
              var column     = ui.item.closest('ul.filter-column').attr('data-filter-column');
              self.getAddForm(false, id_filter, sort_order, ui.item.attr('data-type'), ui.item.attr('data-id'), id_parent, $(this), column);
            }
          }
        }
      });
    },
    initSortableSelected             : function(elem, connect) {
      elem.sortable({
        cursor      : 'move',
        items       : '> li',
        connectWith : connect + ', .filter-column',
        update      : function(event, ui) {
          if (ui.item.context.parentElement.id.indexOf('available') != -1) {
            active_element = $(this);
            element_moved  = true;
            self.removeFilterItem(ui.item, 0);
          } else {
            var layout_column = ui.item.closest('.filter-column').attr('data-filter-column');
            self.updateItemsSortOrders(ui.item.parents('.filter-items').attr('data-id-filter'), self.getAllElementsPositionInSelected(ui.item.parent()), 'update', layout_column);
          }
        },
        start       : function(event, ui) {
          list_from = ui.item.context.parentElement.id;
        },
        stop        : function() {
          list_from = '';
        }
      });
    },
    initSortableSelectedColumns      : function() {
    },
    getPositionInSelected            : function(ui) {
      var sort_order = 0;
      ui.item.addClass('last-added'); // add class to know which one was added right now
      ui.item.parent().find('li').each(function(index) {
        if ($(this).hasClass('last-added')) {
          sort_order = index + 1;
          $(this).removeClass('last-added');
        }
      });
      return sort_order;
    },
    getAllElementsPositionInSelected : function(ui) {
      var data = {};
      ui.find('> li').each(function(index) {
        data[$(this).attr('data-id-item')] = index + 1;
      });
      return data;
    },
    getAddForm                       : function(id_item, id_filter, sort_order, type, id, id_parent, object, column) {
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action     : 'getFilterForm',
            id_item    : id_item,
            id_filter  : id_filter,
            type       : type,
            sort_order : sort_order,
            id         : id,
            id_parent  : id_parent,
            column     : column,
          },
          success  : function(response) {
            if (response.result) {
              self.openFancyBox(response.result, object);
            } else {
              showErrorMessage(response.response_msg);
            }
          }
        }
      )
    },
    updateFilterItem                 : function(data) {
      result              = '';
      query               = {};
      query['action']     = 'updateForm';
      query['parent']     = data.parents('form').find('select[name="parent"]').val();
      query['field_type'] = data.parents('form').find('select[name="field_type"]').val();
      content             = data.parents('form').find('input').each(function() {
        query[$(this).attr('name')] = $(this).val();
      });
      data.parents('form').find('textarea').each(function() {
        query[$(this).attr('name')] = tinyMCE.get($(this).attr('name')).getContent();
      });
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          async    : false,
          data     : query,
          success  : function(response) {
            if (response.status) {
              result = true;
              self.updateItemsSortOrders(response.id_filter, self.getAllElementsPositionInSelected($('#used-filters-' + response.id_filter)), 'add', 0);
              $('#tmadvancedfilter-tabs').find('a[data-filter-type="' + response.type + '"]').addClass('need-indexation');
              $('#' + response.type + '-advancedfilter').find('a[data-filter-type="' + response.type + '"]').addClass('need-indexation');
              showSuccessMessage(response.response_msg);
            } else {
              result = false;
              showErrorMessage(response.response_msg);
            }
          }
        }
      );
      return result;
    },
    removeFilterItem                 : function(data, confirmed) {
      if (!confirmed) {
        var id_item = data.parents('li').attr('data-id-item');
        if (!id_item) {
          var id_item = data.attr('data-id-item');
        }
      } else {
        var id_item        = data.parents('.tmadvancedfilter').find('input[name="id_item"]').val();
        var children_items = [];
        $('li[data-id-item="' + id_item + '"]').find('li').each(function() {
          children_items.push($(this).attr('data-id-item'));
        });
      }
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action         : 'removeFilterItem',
            id_item        : id_item,
            children_items : children_items,
            confirmed      : confirmed
          },
          success  : function(response) {
            if (response.status) {
              if (confirmed) {
                showSuccessMessage(response.response_msg);
                $('#tmadvancedfilter-tabs').find('a[data-filter-type="' + response.type + '"]').addClass('need-indexation');
                $('#' + response.type + '-advancedfilter').find('a[data-filter-type="' + response.type + '"]').addClass('need-indexation');
                self.updateItemsSortOrders(response.id_filter, self.getAllElementsPositionInSelected($('#used-filters-' + response.id_filter)), 'remove', 0);
                self.refreshAllItemsList(response.id_filter);
                self.refreshSelectedItemsList(response.id_filter);
                $.fancybox.close();
              } else {
                self.openFancyBox(response.response_msg, false);
              }
            } else {
              showErrorMessage(response.response_msg);
            }
          }
        }
      );
    },
    refreshAllItemsList              : function(id_filter) {
      var container = $('#available-filters-' + id_filter);
      container.addClass('loading');
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action    : 'refreshAllItemsList',
            id_filter : id_filter,
          },
          success  : function(response) {
            if (response.status) {
              container.html(response.content);
              container.removeClass('loading');
            } else {
              showErrorMessage(response.response_msg);
            }
          }
        }
      );
    },
    refreshSelectedItemsList         : function(id_filter) {
      var container = $('#used-filters-' + id_filter);
      var id_layout = container.attr('data-id-layout');
      if (!id_layout) {
        id_layout = 1;
      }
      container.addClass('loading');
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action    : 'refreshSelectedItemsList',
            id_filter : id_filter,
            id_layout : id_layout,
          },
          success  : function(response) {
            if (response.status) {
              container.html(response.content);
              self.initSortable(id_filter);
              self.initChildrenSortable(id_filter);
              container.removeClass('loading');
            } else {
              showErrorMessage(response.response_msg);
            }
          }
        }
      );
    },
    updateItemsSortOrders            : function(id_filter, data, sort_action, layout_column) {
      var result = false;
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          async    : true,
          data     : {
            action        : 'updateItemsSortOrders',
            data          : data,
            sort_action   : sort_action,
            layout_column : layout_column
          },
          success  : function(response) {
            if (response.status) {
              result = true;
              self.refreshSelectedItemsList(id_filter);
              if (response.response_msg) {
                showSuccessMessage(response.response_msg);
              }
            } else {
              if (response.response_msg) {
                showErrorMessage(response.response_msg);
              }
            }
          }
        }
      );
      return result;
    },
    openFancyBox                     : function(content, object) {
      $.fancybox.open({
        type       : 'inline',
        autoScale  : true,
        minHeight  : 30,
        minWidth   : 480,
        maxWidth   : 815,
        padding    : 0,
        content    : '<div class="bootstrap tmadvancedfilter">' + content + '</div>',
        helpers    : {
          overlay : {
            locked : false
          },
        },
        afterClose : function() {
          if (!submited_flag && object) {
            object.sortable('cancel');
            element_moved = false;
          } else if (!$.isEmptyObject(active_element) && element_moved) {
            active_element.sortable('cancel');
            element_moved = false;
            submited_flag = false;
          } else {
            submited_flag = false;
          }
        }
      });
    },
    reindexFilters                   : function(type) {
      timer_start    = false;
      timer_end      = false;
      remaining_time = false;
      $('#tmadvancedfilter').prepend('<div class="loading"><div class="warning-message">' + tmadv_warning_message + '</div><div class="time-remain">' + tmadv_remaining_time_text + ': ~</div><div class="progress-bar"><div class="progress"></div></div></div>');
      var info = false;
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          data     : {
            action : 'getSuitableProductsInfo'
          },
          success  : function(response) {
            if (response.status) {
              var info = response.info;
              for (i = 1; i <= info['parts']; i++) {
                self.reindexPart(type, info['parts'], i, info['parts_info'][i]);
              }
            } else {
              showErrorMessage(response.response_msg);
            }
            $('#tmadvancedfilter').find('div.loading').remove();
          }
        }
      )
    },
    reindexPart                      : function(type, parts, part, products) {
      $.ajax({
          type     : 'POST',
          url      : ajax_url + '&ajax',
          headers  : {"cache-control" : "no-cache"},
          dataType : 'json',
          async    : false,
          data     : {
            action   : 'reindexFilters',
            type     : type,
            products : products,
            part     : part,
            parts    : parts
          },
          success  : function(response) {
            if (response.status) {
              $('#tmadvancedfilter').find('div.loading div.progress').attr('style', 'width:' + part / parts * 100 + '%');
              if (timer_start === false) {
                timer_start = new Date().getTime();
              } else {
                timer_end   = new Date().getTime() - timer_start;
                timer_start = new Date().getTime();
              }
              time = self.getReminingTime(parts, part);
              if (remaining_time !== false) {
                $('#tmadvancedfilter').find('div.loading div.time-remain').html(tmadv_remaining_time_text + ': ' + time);
              }
              if (part == parts) {
                $('#tmadvancedfilter-tabs').find('a[data-filter-type="' + response.type + '"]').removeClass('need-indexation');
                $('#' + response.type + '-advancedfilter').find('a[data-filter-type="' + response.type + '"]').removeClass('need-indexation');
                $('#tmadvancedfilter').find('div.loading').remove();
                showSuccessMessage(response.response_msg);
              }
            } else {
              showErrorMessage(response.response_msg);
              $('#tmadvancedfilter').find('div.loading').remove();
            }
          }
        }
      )
      return true;
    },
    getReminingTime                  : function(parts, part) {
      if (parts && part && timer_end) {
        remaining_time = (parts - part) * timer_end;
        var tmadv_ms   = remaining_time % 1000;
        tmadv_s        = (remaining_time - tmadv_ms) / 1000;
        var tmadv_secs = tmadv_s % 60;
        tmadv_s        = (tmadv_s - tmadv_secs) / 60;
        var tmadv_mins = tmadv_s % 60;
        var tmadv_hrs  = (tmadv_s - tmadv_mins) / 60;
        return tmadv_hrs + '' + tmadv_hours_text + ' : ' + tmadv_mins + '' + tmadv_min_text + ' : ' + tmadv_secs + '' + tmadv_sec_text;
      }
    },
    initTabs                         : function() {
      $('#tmadvancedfilter-tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
      });
      $('#tmadvancedfilter-tabs a:first').tab('show');
    },
    message                          : {
      clearMessages : function(form) {
        form.parents('form').find('.alert-danger').remove();
      },
      displayError  : function(form, errors) {
        form.parents('form').find('.form-wrapper').before(errors);
      },
    },
    validation                       : {
      validateUpdateForm : function(data) {
        result          = '';
        query           = {};
        query['action'] = 'validateUpdateForm';
        content         = data.parents('form').find('input').each(function() {
          query[$(this).attr('name')] = $(this).val();
        });
        $.ajax({
            type     : 'POST',
            url      : ajax_url + '&ajax',
            headers  : {"cache-control" : "no-cache"},
            dataType : 'json',
            async    : false,
            data     : query,
            success  : function(response) {
              if (response.status) {
                result = false;
              } else {
                result = response.errors;
              }
            }
          }
        );
        return result;
      }
    }
  };
  $.fn.tmAdvancedFilter   = function() {
    return this.each(function() {
      var filter = Object.create(tmAdvancedFilterObj);
      filter.init(this);
      $.data(this, 'tmAdvancedFilter', filter);
    });
  };
})(jQuery, window, document);
$(document).ready(function() {
  $('#tmadvancedfilter').tmAdvancedFilter();
});
