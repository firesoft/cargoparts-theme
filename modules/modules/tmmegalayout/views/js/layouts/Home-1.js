var isCarousel = false;
$(document).ready(function() {
  if ($('#homepage-blog').length) {
    if ($(document).width() + compensante < 1200) {
      blogCarousel();
      isCarousel = true;
    }
    $(window).resize(function() {
      if (isCarousel) {
        blogResize()
      }
      if ($(document).width() + compensante < 1200 && !isCarousel) {
        blogCarousel();
        isCarousel = true;
      } else if ($(document).width() + compensante > 1199 && isCarousel) {
        blog_slider.destroySlider();
        $('#homepage-blog .block_content li').removeAttr('style');
        isCarousel = false;
      }
    });
  }
});
function blogCarousel() {
  blogCountItems();
  var blog = $('#homepage-blog').find('ul.row');
  if (blog.length && !!$.prototype.bxSlider) {
    blog_slider = blog.bxSlider({
      minSlides: blog_items_count,
      maxSlides: blog_items_count,
      slideWidth: 500,
      slideMargin: 30,
      pager: false,
      nextText: '',
      prevText: '',
      moveSlides: 1,
      infiniteLoop: false,
      hideControlOnEnd: true,
      responsive: false,
      useCSS: false,
      autoHover: false,
      speed: 500,
      pause: 3000,
      controls: true,
      autoControls: false
    });
  }
}
function blogResize() {
  blogCountItems();
  blog_slider.reloadSlider({
    minSlides: blog_items_count,
    maxSlides: blog_items_count,
    slideWidth: 500,
    slideMargin: 30,
    pager: false,
    nextText: '',
    prevText: '',
    moveSlides: 1,
    infiniteLoop: false,
    hideControlOnEnd: true,
    responsive: false,
    useCSS: false,
    autoHover: false,
    speed: 500,
    pause: 3000,
    controls: true,
    autoControls: false
  });
}
function blogCountItems() {
  var blog = $('#homepage-blog').width();
  if (blog < 500) {
    blog_items_count = 1;
  }
  if (blog >= 500) {
    blog_items_count = 2;
  }
  if (blog >= 900) {
    blog_items_count = 3;
  }
  if (blog > 1500) {
    blog_items_count = 4;
  }
}