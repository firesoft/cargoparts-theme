var isMobileLinks = false;
$(document).ready(function() {
  mobileLinks();
  $(window).resize(function() {
    mobileLinks();
  });
});

function mobileLinks() {
  var headLinks, headDropdown;
  if ($(document).width() + compensante < 768 && !isMobileLinks) {
    headLinks = $('#header_links');
    headDropdown = $('header .dropdown > .toogle_content');
    if (headLinks.length && headDropdown.length) {
      headLinks.parent().addClass('permalinks');
      headLinks.appendTo(headDropdown);
      isMobileLinks = true;
    }
  } else if ($(document).width() + compensante > 767 && isMobileLinks) {
    headLinks = $('#header_links');
    headDropdown = $('header .dropdown > .toogle_content');
    if (headLinks.length && headDropdown.length) {
      headLinks.appendTo($('header .permalinks'));
    }
    isMobileLinks = false;
  }
}