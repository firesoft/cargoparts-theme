<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_c4e3512b30e80dfbdcb9e09427bab7a6'] = 'TM Infinite Scroll';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_6e4ae73753c86edc73b6fa8636d3db87'] = 'Ajouter scroll infini au catalogue';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_96d4dadd096605d5813d2c6bdf82e82c'] = 'Paramètres enregistrés';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_b8f71505acb2618f7af0373807da9170'] = 'Ajouter un préchargement de votre ordinateur.N.B : Seule image gif est autorisée';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_453aceb005ceaf54a47da15fee8b2a26'] = 'Pages';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_eef3bd0525f4adada6ce2aa0e3a68151'] = 'Charge automatique';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_cbb81506a7fe3ef03f7a89c76c52131a'] = 'Pagination';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_d34d1b41068dc1c7fa8baa26f8f50a80'] = 'Affichage bouton `Afficher tout`';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_dfd0a82c4bf37b1e90b690a22a20692e'] = 'Décalage';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_dda847bef60742ff21e09d8f6e948b69'] = 'Précharge:';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_097ce4902863d229fbc384296508fbb4'] = 'Type ou taille d\'image non valide. Largeur et la hauteur maximale de l\'image 300px';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_df7859ac16e724c9b1fba0a364503d72'] = 'Une erreur est survenue lors de la tentative de télécharger le fichier.';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_064d78ad5a2d40b31b150d54f8008e08'] = 'Champ \'Décalage\' est vide.';
$_MODULE['<{tminfinitescroll}prestashop>tminfinitescroll_47d2e372d7915b2b7bb69f10d218a296'] = 'Valeur incorrecte de \'Décalage\'.';
