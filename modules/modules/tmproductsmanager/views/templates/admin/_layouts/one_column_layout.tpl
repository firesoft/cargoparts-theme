{**
* 2002-2017 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="three-column-layer bootstrap layer">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-basement">
        <div class="title">{l s='Image' mod='tmproductsmanager'}</div>
        <div class="box">
          {l s='Countdown' mod='tmproductsmanager'}
        </div>
        <div class="box">
          {l s='Page name' mod='tmproductsmanager'}
        </div>
        <div class="box">
          {l s='Discount code' mod='tmproductsmanager'}
        </div>
        <div class="box">
          {l s='Page description' mod='tmproductsmanager'}
        </div>
      </div>
    </div>
  </div>
</div>