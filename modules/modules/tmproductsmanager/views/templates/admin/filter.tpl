{**
* 2002-2017 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="botstrap">
  <div class="row">
    <div class="col-xs-12">
      <div class="panel">
        <form action="{$formUrl|escape:'html':'UTF-8'}" method="post" class="tmproductsmanager-form">
          <h3>{if isset($actionInfo) && $actionInfo}{$actionInfo.name|escape:'htmlall':'UTF-8'} -> {/if}{l s='Filter by:' mod='tmproductsmanager'}</h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
              <div class="row">
                <label class="col-xs-6">{l s='Categories' mod='tmproductsmanager'}</label>
                {include file='./_partials/tree.tpl' filter=$filters['categories']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Manufacturers' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='manufacturers' filter=$filters['manufacturers']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Suppliers' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='suppliers' filter=$filters['suppliers']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Conditions' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='conditions' filter=$filters['conditions']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='With image only' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='images' filter=$filters['images']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='On sale' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='onsale' filter=$filters['onsale']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Active' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='active' filter=$filters['active']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Visibility' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='visibility' filter=$filters['visibility']}
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
              <div class="row">
                <label class="col-xs-6">{l s='Available for order' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='order' filter=$filters['order']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Customizable' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='customizable' filter=$filters['customizable']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Online only' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='onlineonly' filter=$filters['onlineonly']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Show price' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='sprice' filter=$filters['sprice']}
              </div>
              <div class="row">
                <label class="col-xs-6">{l s='Virtual product' mod='tmproductsmanager'}</label>
                {include file='./_partials/simple-multiselect.tpl' name='virtual' filter=$filters['virtual']}
              </div>
            </div>
            {if isset($filters['features']) && $filters['features']}
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <h4>{l s='Available attributes' mod='tmproductsmanager'}</h4>
                {include file='./_partials/layered-multiselect.tpl' name='attributes' filter=$filters['attributes']}
              </div>
            {/if}
            {if isset($filters['features']) && $filters['features']}
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
                <h4>{l s='Available features' mod='tmproductsmanager'}</h4>
                {include file='./_partials/layered-multiselect.tpl' name='features' filter=$filters['features']}
              </div>
            {/if}
            {if isset($activeList) && $activeList}
                <div id="filter-active-list" class="col-xs-12">
                  {include file='./_partials/list-active-item.tpl' items=$activeList}
                </div>
            {/if}
          </div>
          <div class="panel-footer">
            <a href="{$back_url|escape:'htmlall':'UTF-8'}" class="btn btn-default"><i class="icon icon-arrow-left"></i> {l s='Back' mod='tmproductsmanager'}
            </a>
            <div class="pull-right">
              <button type="submit" class="btn btn-danger" name="submitResetFilter">{l s='Reset' mod='tmproductsmanager'}</button>
              <button type="submit" class="btn btn-success" name="submitCategoriesSelect">{l s='Select' mod='tmproductsmanager'}</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

{addJsDefL name='tmproductsmanagerSelectedCategoriesEmptyText'}{l s='empty' mod='tmproductsmanager'}{/addJsDefL}
{addJsDefL name='tmproductsmanagerSelectedCategories'}{l s='selected' mod='tmproductsmanager'}{/addJsDefL}
{addJsDefL name='tmproductsmanagerSelectAllText'}{l s='Select all' mod='tmproductsmanager'}{/addJsDefL}