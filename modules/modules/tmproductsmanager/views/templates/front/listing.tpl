{**
* 2002-2017 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{capture name=path}{$page->name|escape:'htmlall':'UTF-8'}{/capture}

{if isset($layout) && $layout}
  {$layout}
{/if}

{if isset($products) && $products}
  <div class="content_sortPagiBar">
    <div class="sortPagiBar clearfix">
      {include file=tmproductsmanager::getTemplateFile('product-sort.tpl')}
      {include file=tmproductsmanager::getTemplateFile('nbr-product-page.tpl')}
    </div>
  </div>

  {include file=Tmproductsmanager::getTemplateFile('product-list.tpl') products=$products}

  <div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix">
      {include file=Tmproductsmanager::getTemplateFile('product-compare.tpl')}
      {include file=Tmproductsmanager::getTemplateFile('./pagination.tpl') no_follow=1 paginationId='bottom'}
    </div>
  </div>
{else}
  <p class="alert alert-warning">{l s='No found products by this criteria.' mod='tmproductsmanager'}</p>
{/if}
