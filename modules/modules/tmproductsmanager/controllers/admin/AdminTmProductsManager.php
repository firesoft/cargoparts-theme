<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class AdminTmProductsManagerController extends ModuleAdminController
{
    public $module;
    public $filters;
    public $filterProcess;
    public $history;
    public $messages = array();
    public $allFilters = array();
    public $tabs = array();

    public function __construct()
    {
        $this->bootstrap = true;
        $this->meta_title = $this->l('TM Products Manager');
        parent::__construct();
        if (!$this->module->active) {
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminDashboard'));
        }
        $this->filters = new PMFilters(_DB_PREFIX_, $this->context->shop, $this->context->language);
        $this->history = new PMHistory(_DB_PREFIX_, $this->context);
        $this->tabs[] = array(
            'name' => $this->l('Information'),
            'tabs' => array(
                'enableProducts' => array(
                    'name' => $this->l('Enable/Disable products'),
                    'desc' => $this->l('Set the on/off status of your products.'),
                    'icon' => 'tab-icon-check'
                ),
                'availableOrder' => array(
                    'name' => $this->l('Available for order'),
                    'desc' => $this->l('Make some products available or unavailable for order.'),
                    'warning' => $this->l('NOTE: if "Available for order" is set to "Yes" for the products with hidden prices, their prices will automatically become visible again.'),
                    'icon' => 'tab-icon-toggle'
                ),
                'showPrice' => array(
                    'name' => $this->l('Show price'),
                    'desc' => $this->l('Show or hide product price.'),
                    'warning' => $this->l('NOTE: if you set "Show price" to "No" for the products, which are available for order, they will be automatically changed to unavailable.'),
                    'icon' => 'tab-icon-dollar'
                ),
                'onlineOnly' => array(
                    'name' => $this->l('Online only'),
                    'desc' => $this->l('Apply the "Online only" mark to the selected items.'),
                    'icon' => 'tab-icon-desktop'
                ),
                'condition' => array(
                    'name' => $this->l('Condition'),
                    'desc' => $this->l('Change the condition of multiple products.'),
                    'icon' => 'tab-icon-edit'
                ),
                'descriptionShort' => array(
                    'name' => $this->l('Short description'),
                    'desc' => $this->l('Add a short description which should be applied to all selected products.'),
                    'icon' => 'tab-icon-paste'
                ),
                'description' => array(
                    'name' => $this->l('Full description'),
                    'desc' => $this->l('Add a description to selected products with an ability to place it before the main description, after it, or replace the whole description with the new one.'),
                    'icon' => 'tab-icon-paste'
                ),
                'tags' => array(
                    'name' => $this->l('Tags'),
                    'desc' => $this->l('Add more tags to the chosen products.'),
                    'icon' => 'tab-icon-tags'
                ),
            )
        );
        $this->tabs[] = array(
            'name' => $this->l('Price'),
            'tabs' => array(
                'onSale' => array(
                    'name' => $this->l('Display "On sale"'),
                    'desc' => $this->l('Display the selected items with "On sale" mark.'),
                    'icon' => 'tab-icon-percent'
                ),
                'taxRules' => array(
                    'name' => $this->l('Change tax rules'),
                    'desc' => $this->l('Choose the tax rules which will be applied to several products.'),
                    'icon' => 'tab-icon-circle'
                ),
                'specificPrice' => array(
                    'name' => $this->l('Add new specific price'),
                    'desc' => $this->l('Set the special price for selected products with an ability to add the price activation period.'),
                    'icon' => 'tab-icon-money'
                ),
            )
        );
        $this->tabs[] = array(
            'name' => $this->l('Associations'),
            'tabs' => array(
                'brand' => array(
                    'name' => $this->l('Change brand'),
                    'desc' => $this->l('Change brand of selected items.'),
                    'icon' => 'tab-icon-registered'
                ),
                'defaultCategory' => array(
                    'name' => $this->l('Change default category'),
                    'desc' => $this->l('Choose the category which will be set as default for some products.'),
                    'warning' => $this->l('NOTE: You can choose only one default category. Keep in mind that your current default category will be replaced with the new one.'),
                    'icon' => 'tab-icon-list'
                ),
                'categories' => array(
                    'name' => $this->l('Change related categories'),
                    'desc' => $this->l('Set the related categories to multiple products with an ability to save or remove old relations.'),
                    'icon' => 'tab-icon-list'
                ),
                'accessories' => array(
                    'name' => $this->l('Add accessories'),
                    'desc' => $this->l('Add the accessories suitable for the chosen products.'),
                    'icon' => 'tab-icon-refresh'
                ),
            )
        );
        $this->tabs[] = array(
            'name' => $this->l('Quantity'),
            'tabs' => array(
                'quantity' => array(
                    'name' => $this->l('Change quantity'),
                    'desc' => $this->l('Set the required quantity of some products.'),
                    'icon' => 'tab-icon-quantity'
                )
            )
        );
        $this->tabs[] = array(
            'name' => $this->l('Extra Pages'),
            'tabs' => array(
                'newaction' => array(
                    'name' => $this->l('Add new page'),
                    'desc' => $this->l('Add new page.'),
                    'icon' => 'tab-icon-action'
                )
            )
        );
        $this->messages = array(
            1 => $this->l('Page successfully created/updated'),
            2 => $this->l('Banner successfully created/updated'),
            3 => $this->l('Banner successfully removed'),
            4 => $this->l('Banner status successfully changed'),
            5 => $this->l('Page status successfully changed'),
            6 => $this->l('Page successfully removed')
        );
    }

    public function initContent()
    {
        if ($message = $this->getWarningMultishop()) {
            $this->errors = $message;
        } else {
            $this->content .= $this->initFilters();
        }
        parent::initContent();
    }

    protected function initFilters()
    {
        $output = '';
        if (Tools::getIsset('msg') && $id_message = Tools::getValue('msg')) {
            $this->context->smarty->assign('msg',  $this->messages[$id_message]);
            $output .= $this->module->display($this->module->path, 'views/templates/admin/_forms/messages.tpl'); // show filter form
        }
        if (Tools::isSubmit('submitCategoriesSelect')) { // do if button search was pressed
            if ($filtered_query = $this->filterQuery()) { // get all filter parameters that was selected by a user
                $this->setCookie($filtered_query); // puts all parameters to memory
            }
        } elseif (Tools::isSubmit('submitResetFilter')) {
            $this->clearModuleCookies(); // clear memory
        } elseif (Tools::getIsset('removetmpm')) {
            if ($this->addToException(Tools::getValue('id_product'))) { // exclude product from result
                $output .= $this->module->displayConfirmation($this->l('Selected product removed from result.'));
            } else {
                $output .= $this->module->displayError($this->l('Selected product wasn\'t removed from result.'));
            }
        } elseif (Tools::getIsset('submitBulkremoveconfiguration')) {
            if ($this->addToException(Tools::getValue('tmpmBox'))) { // exclude multiple products from result
                $output .= $this->module->displayConfirmation($this->l('Selected product(s) removed from result.'));
            } else {
                $output .= $this->module->displayError($this->l('Selected product wasn\'t removed from result.'));
            }
        }
        $this->allFilters['categories'] = $this->filters->filterTreeCategories($this->history);
        $this->allFilters['manufacturers'] = $this->filters->filterManufacturers();
        $this->allFilters['suppliers'] = $this->filters->filterSuppliers();
        $this->allFilters['attributes'] = $this->filters->filterAttributes();
        $this->allFilters['features'] = $this->filters->filterFeatures();
        $this->allFilters['conditions'] = $this->filters->filterConditions();
        $this->allFilters['images'] = $this->filters->filterImages();
        $this->allFilters['onsale'] = $this->filters->filterSale();
        $this->allFilters['active'] = $this->filters->filterActive();
        $this->allFilters['customizable'] = $this->filters->filterCustomizable();
        $this->allFilters['onlineonly'] = $this->filters->filterOnlineonly();
        $this->allFilters['order'] = $this->filters->filterOrder();
        $this->allFilters['sprice'] = $this->filters->filterSPrice();
        $this->allFilters['virtual'] = $this->filters->filterVirtual();
        $this->allFilters['visibility'] = $this->filters->filterVisibility();
        $this->context->smarty->assign(array('filters' => $this->allFilters));
        if (Tools::getIsset('rmv') && Tools::getIsset('itm')) {
            if (!$this->removeParameter(Tools::getValue('rmv'), Tools::getValue('itm'))) {
                $output .= $this->module->displayError($this->l('Can\'t remove filter parameter.'));
            }
        }
        // remove an special offer
        if (Tools::getIsset('deletetmproductsmanager_pages') && $id_page = Tools::getValue('id_page')) {
            if ($errors = $this->removeAction((int)$id_page)) {
                $this->errors[] = $this->l('Can\'t remove a page');
            }
            $this->module->clearCache();
            Tools::redirectAdmin(
                AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminTmProductsManager').'&msg=6'
            );
        }
        if (!Tools::getIsset('action')) { // first page initialization (tabs)
            $this->context->smarty->assign('tmpmtabs', $this->tabs);
            $this->context->smarty->assign(
                'formUrl',
                AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminTmProductsManager')
            );
            if (Tools::getIsset('success')) { // display success message if some action was done
                $output .= $this->module->displayConfirmation($this->l('Changes saved successfully.'));
            }
            $this->clearModuleCookies(); // clear memory
            $output .= $this->renderTabFilter(); // display tabs
        } elseif (Tools::getIsset('action') && Tools::getIsset('filter')) { // second page initialization (filter)
            $this->context->smarty->assign(
                'formUrl',
                AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                    'AdminTmProductsManager'
                ).'&action='.Tools::getValue('action').'&filter'.(Tools::getValue(
                    'id_page'
                ) ? '&id_page='.Tools::getValue('id_page') : '')
            );
            $this->context->smarty->assign(
                'back_url',
                AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminTmProductsManager')
            );
            $this->context->smarty->assign('actionInfo', $this->getActionInfoFromType(Tools::getValue('action')));
            if ($selected_parameters = $this->history->get(
                'tmproductsmanager'
            )
            ) { // check if filter parameters exist in the memory, and mark needed fields on the form
                $parameters = Tools::jsonDecode($selected_parameters, true);
                $parameters_list = $this->activeParametersList($parameters);
                $this->context->smarty->assign('selected', $parameters);
                $this->context->smarty->assign('activeList', $parameters_list);
            }
            $output .= $this->module->display(
                $this->module->path,
                'views/templates/admin/filter.tpl'
            ); // show filter form
            if (isset($selected_parameters) && $selected_parameters) { // start displaying the result, if some result exists
                if (Tools::isSubmit('submitCategoriesSelect')
                    || Tools::isSubmit('submitResetFilter')
                    || Tools::getIsset('removetmpm')
                    || Tools::getIsset('submitBulkremoveconfiguration')
                    || Tools::getIsset('rmv')
                ) {
                    $output .= $this->renderFilterResult(true); // make new filtering if some action occurred
                } else {
                    $output .= $this->renderFilterResult(false); // display results from memory if no action occurred
                }
            }
        } elseif (Tools::getIsset('action') && Tools::getIsset('process')) { // third page initialization (action form)
            $info = $this->getActionInfoFromType(Tools::getValue('action'));
            $this->context->smarty->assign('actionInfo', $info);
            if (Tools::getIsset('updatetmproductsmanager_pages')
                && Tools::getIsset('id_page')
                && $id_page = Tools::getValue('id_page')
            ) {
                $page = new PMPage($id_page);
                $this->history->set('tmproductsmanager_result', $page->products);
                $this->history->set('tmproductsmanager', $page->filters);
                if (isset($page->tmexcludedproducts) && $page->tmexcludedproducts) {
                    $this->history->set('tmexcludedproducts', $page->tmexcludedproducts);
                }
            }
            if (!$result = $this->history->get('tmproductsmanager_result')) {
                $this->errors[] = Tools::displayError($this->l('No products selected'));
                $output = ''; // return if no products found
            } else {
                if (isset($info['desc']) && $info['desc']) {
                    $this->context->smarty->assign('content', $info['desc']);
                    $output .= $this->module->display(
                        $this->module->path,
                        'views/templates/admin/_forms/message_info.tpl'
                    ); // show filter form
                }
                $action = Tools::getValue('action');
                $form = new PMForms($this->context, $result, $action, $info);
                $output .= $form->$action();
            }
        }
        // processing of Save & Stay action. Working only for actions page
        if (Tools::isSubmit('saveNstay')) {
            if (!$result = $this->history->get('tmproductsmanager_result')
                || !Tools::getIsset('action')
                || Tools::isEmpty(Tools::getValue('action'))
            ) {
                $this->context->smarty->assign(
                    'formUrl',
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminTmProductsManager')
                );
                $this->clearModuleCookies();
                $this->errors[] = $this->l('Something went wrong');
                $output .= $this->renderTabFilter();
            } else {
                $process = new PMProcessing(
                    $this->context,
                    $this->history->get('tmproductsmanager_result'),
                    $this->history->get('tmproductsmanager'),
                    $this->history->get('tmexcludedproducts')
                );
                $result = $process->addPage(true);
                if (is_array($result)) {
                    // if array, displays gotten errors
                    $this->errors = $result;
                } else {
                    // else got id
                    $this->module->clearCache();
                    Tools::redirectAdmin(
                        AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=newaction&process&updatetmproductsmanager_pages&id_page='.(int)$result.'&msg=1'
                    );
                }
            }
        }
        // processing if action was confirmed
        if (Tools::isSubmit('submitTMPM') && !Tools::isSubmit('TMPMAddBanner') && !Tools::isSubmit('TMPMUpdateBanner')) {
            if (!$result = $this->history->get('tmproductsmanager_result')
                || !Tools::getIsset('action')
                || Tools::isEmpty(Tools::getValue('action'))
            ) {
                $this->context->smarty->assign(
                    'formUrl',
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminTmProductsManager')
                );
                $this->clearModuleCookies();
                $this->errors[] = $this->l('Something went wrong');
                $output .= $this->renderTabFilter();
            } else {
                $action = Tools::getValue('action');
                $process = new PMProcessing(
                    $this->context,
                    $this->history->get('tmproductsmanager_result'),
                    $this->history->get('tmproductsmanager'),
                    $this->history->get('tmexcludedproducts')
                );
                if (!$errors = $process->$action()) {
                    Tools::redirectAdmin(
                        AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&success'
                    );
                } else {
                    $this->errors = $errors;
                }
            }
        }

        // banners events
        // add or update banner
        if (Tools::isSubmit('TMPMAddBanner') || Tools::isSubmit('TMPMUpdateBanner')) {
            $id_page = Tools::getValue('id_page');
            $id_banner = Tools::getValue('id_banner');
            $process = new PMProcessing(
                $this->context,
                $this->history->get('tmproductsmanager_result'),
                $this->history->get('tmproductsmanager'),
                $this->history->get('tmexcludedproducts')
            );
            if (!$errors = $process->addBanner($id_page, $id_banner)) { // clear module cache after banner add/update and redirect to page form
                $this->module->clearCache();
                Tools::redirectAdmin(
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                        'AdminTmProductsManager'
                    ).'&action=newaction&process&updatetmproductsmanager_pages&id_page='.(int)$id_page.'&msg=2'
                );
            } else {
                $this->errors = $errors; // show errors if they occurred
            }
        }

        //remove banner if it exists and removed successful redirect to page form else show occurred errors
        if (Tools::getIsset('deleteupdatebanner') && $id_banner = Tools::getValue('id_banner')) {
            $banner = new PMPageBanner($id_banner);
            if (!$banner->delete()) {
                $this->errors = $this->l('Can\'t remove banner');
            } else {
                $this->module->clearCache();
                Tools::redirectAdmin(
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                        'AdminTmProductsManager'
                    ).'&action=newaction&process&updatetmproductsmanager_pages&id_page='.(int)$id_page.'&msg=3'
                );
            }
        }

        // update banner status if banner exists and redirect if successful else show errors
        if (Tools::getIsset('statusupdatebanner') && $id_banner = Tools::getValue('id_banner')) {
            $banner = new PMPageBanner($id_banner);
            if ($banner->banner_active) {
                $banner->banner_active = 0;
            } else {
                $banner->banner_active = 1;
            }
            if (!$banner->update()) {
                $this->errors = $this->l('Can\'t update banner');
            } else {
                $this->module->clearCache();
                Tools::redirectAdmin(
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                        'AdminTmProductsManager'
                    ).'&action=newaction&process&updatetmproductsmanager_pages&id_page='.(int)$id_page.'&msg=4'
                );
            }
        }

        // update special offers page status and voucher(Cart Rule) status if have related
        if (Tools::getIsset('statustmproductsmanager_pages') && $id_page = Tools::getValue('id_page')) {
            $page = new PMPage($id_page);
            if ($page->voucher) {
                $voucher = new CartRule($page->voucher);
            }
            if ($page->active) {
                $page->active = 0;
                $voucher->active = 0;
            } else {
                $page->active = 1;
                $voucher->active = 1;
            }
            if (!$page->update() || ($page->voucher && !$voucher->update())) {
                $this->errors = $this->l('Can\'t update page');
            } else {
                $this->module->clearCache();
                Tools::redirectAdmin(
                    AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                        'AdminTmProductsManager'
                    ).'&msg=5'
                );
            }
        }


        return $output;
    }

    /**
     * Display main filter tabs (first page)
     *
     * @return mixed
     */
    public function renderTabFilter()
    {
        $this->context->smarty->assign('tax_exclude_taxe_option', Tax::excludeTaxeOption());
        $this->context->smarty->assign('list', $this->getPagesList());

        return $this->module->display(
            $this->module->path,
            'views/templates/admin/filter-tab.tpl'
        );
    }

    /**
     * Include all necessary media files (js & css)
     */
    public function setMedia()
    {
        if (Tools::getIsset('controller') && Tools::getValue('controller') == 'AdminTmProductsManager') {
            $this->context->controller->addJquery();
            $this->addJqueryUI(
                array(
                    'ui.core',
                    'ui.widget'
                )
            );

            $this->context->controller->addJqueryPlugin(array('typewatch', 'fancybox', 'autocomplete', 'tagify'));
            $this->context->controller->addJS($this->module->path.'views/js/bootstrap-multiselect.js');
            $this->context->controller->addJS($this->module->path.'views/js/tmproductsmanager_admin.js');
            $this->context->controller->addCSS($this->module->path.'views/css/tmproductsmanager_admin.css');
            Media::addJsDef(
                array(
                    'currentToken' => Tools::getAdminTokenLite('AdminCartRules'),
                )
            );
        }
        parent::setMedia();
    }

    /**
     * Get raw array of all POST variables and filter it by filter prefix parameter(tmpm) and categories parameter
     *
     * @param array $filtered_query
     *
     * @return array
     */
    protected function filterQuery(array $filtered_query = array())
    {
        $raw_query = Tools::getAllValues();
        foreach ($raw_query as $key => $value) {
            if (strpos($key, 'tmpm_') === 0 || strpos($key, 'categoryBox') === 0) {
                $filtered_query[$key] = $value;
            }
        }

        return $filtered_query;
    }

    /**
     * Put a selected parameters to memory(from json encoded parameters array)
     *
     * @param $query
     */
    protected function setCookie($query)
    {
        $this->history->set('tmproductsmanager', Tools::jsonEncode($query));
    }

    /**
     * Put filtered products to memory
     *
     * @param $result
     */
    protected function setResultCookie($result)
    {
        if (Tools::isEmpty($result) || !$result) {
            $this->history->delete('tmproductsmanager_result');

            return;
        }
        $products = array();
        foreach ($result as $product) {
            $products[] = $product['id_product'];
        }
        $this->history->set('tmproductsmanager_result', Tools::jsonEncode($products));
    }

    /**
     * Clear memory
     */
    protected function clearModuleCookies()
    {
        $this->history->clear();
    }

    /**
     * Display filter result table
     *
     * @param bool  $not_cached get result from memory or make new filtering?
     * @param array $products
     *
     * @return mixed
     */
    protected function renderFilterResult($not_cached = true, array $products = array())
    {
        if ($not_cached) {
            $products = $this->getResult();
        } else {
            $products = $this->buildCachedResult($this->history->get('tmproductsmanager_result'));
        }
        $fields_list = array(
            'id_product' => array(
                'title' => $this->l('Id'),
                'type'  => 'text',
            ),
            'name'       => array(
                'title' => $this->l('Product name'),
                'type'  => 'text',
            )
        );
        $helper = new HelperList();
        $helper->title = sprintf($this->l('Products according to the criteria (%s)'), count($products));
        $helper->shopLinkType = '';
        $helper->identifier = 'id_product';
        $helper->list_id = 'tmpm';
        $helper->is_order_position = true;
        $helper->actions = array('remove');
        $helper->show_toolbar = true;
        $helper->module = new Tmproductsmanager();
        $helper->token = Tools::getAdminTokenLite('AdminTmProductsManager').(Tools::getIsset(
            'action'
        ) ? '&action='.Tools::getValue('action') : '').'&filter';
        $helper->simple_header = true;
        $helper->show_filters = false;
        $helper->currentIndex = AdminController::$currentIndex;
        $helper->no_link = true;
        if ($this->history->get('tmproductsmanager_result')) {
            $helper->toolbar_btn['save'] = array(
                'href' => self::$currentIndex.'&token='.$this->token.'&action='.Tools::getValue('action').'&process'.(Tools::getValue('id_page') ? '&id_page='.Tools::getValue('id_page') : ''),
                'desc' => $this->l('Process'),
                'class' => 'pull-right',
                'type' => 'cogs'
            );
        }

        $helper->toolbar_btn['back'] = array(
            'href' => self::$currentIndex.'&token='.$this->token,
            'desc' => $this->l('Back'),
            'type' => 'back'
        );
        $helper->bulk_actions = array(
            'remove' => array(
                'text' => 'Remove selected',
                'icon' => 'icon-remove'
            )
        );

        return $helper->generateList($products, $fields_list);
    }

    /**
     * Makes filtering by all parameters
     *
     * @param array $args
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    protected function getResult(array $args = array())
    {
        $this->filterProcess = new PMFilterProcess(
            _DB_PREFIX_,
            $this->context->shop,
            $this->context->language,
            $this->getExcludedProducts()
        );
        $raw_parameters = $this->getParameters();
        if ($raw_parameters) {
            $query = $this->filterProcess->start();
            foreach ($raw_parameters as $name => $value) {
                if (Tools::strpos($name, 'tmpm_') === 0) {
                    $name = 'join'.str_replace('tmpm_', '', $name);
                    $query .= $this->filterProcess->$name($value);
                } elseif (Tools::strpos($name, 'categoryBox') === 0) {
                    $query .= $this->filterProcess->joincategories($value);
                }
            }
            $query .= $this->filterProcess->end();
            $result = $this->filterProcess->process($query);
            $this->setResultCookie($result);

            return $this->filterProcess->process($query);
        }

        return $args;
    }

    protected function getParameters()
    {
        if ($parameters = $this->history->get('tmproductsmanager')) {
            return Tools::jsonDecode($parameters, true);
        }

        return false;
    }

    /**
     * Get a ids of products which was excluded
     * @return bool|false|null|string
     */
    protected function getExcludedProducts()
    {
        if ($excluded = $this->history->get('tmexcludedproducts')) {
            return $excluded;
        } else {
            return false;
        }
    }

    /**
     * Add products in exclude array(will not be displayed in current filtering)
     *
     * @param $ids list of products which will be excluded from results
     *
     * @return bool
     */
    protected function addToException($ids)
    {
        $list = array();
        if ($array = $this->history->get('tmexcludedproducts')) {
            $list = explode(',', $array);
        }
        if (is_array($ids)) {
            foreach ($ids as $id) {
                array_push($list, $id);
            }
        } else {
            array_push($list, $ids);
        }

        return $this->history->set('tmexcludedproducts', implode(',', $list));
    }

    /**
     * Make filtered products array from memory
     *
     * @param $array
     *
     * @return bool
     */
    protected function buildCachedResult($array)
    {
        $products = false;

        if ($array) {
            foreach (Tools::jsonDecode($array, true) as $key => $id_product) {
                $products[$key]['id_product'] = $id_product;
                $products[$key]['name'] = Product::getProductName($id_product);
            }
        }

        return $products;
    }

    protected function getWarningMultishop()
    {
        if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL) {
            return $this->l('You cannot manage items from "All Shops" or "Group Shop" context,
                                                    select the store you want to edit');
        } else {
            return false;
        }
    }

    public function getActionInfoFromType($type)
    {
        $result = array();
        foreach ($this->tabs as $tabs) {
            foreach ($tabs['tabs'] as $key => $tab) {
                if ($key == $type) {
                    $result = $tab;
                    break;
                }
            }
        }

        return $result;
    }

    protected function activeParametersList($parameters)
    {
        if (!$parameters) {
            return false;
        }

        $result = array();
        foreach ($parameters as $type => $value) {
            $type = str_replace('tmpm_', '', $type);
            if (isset($this->allFilters[$type])) {
                $activeFilter = $this->allFilters[$type];
            }
            if ($type != 'attributes' && $type != 'features' && $type != 'categoryBox') {
                foreach ($activeFilter as $current) {
                    if (in_array($current['id'], $value)) {
                        $result[$type]['value'][$current['id']] = $current['name'];
                    }
                }
                $result[$type]['values'] = false;
                switch ($type) {
                    case 'active':
                        $result[$type]['name'] = $this->l('Active');
                        break;
                    case 'manufacturers':
                        $result[$type]['name'] = $this->l('Manufacturers');
                        break;
                    case 'suppliers':
                        $result[$type]['name'] = $this->l('Suppliers');
                        break;
                    case 'conditions':
                        $result[$type]['name'] = $this->l('Conditions');
                        break;
                    case 'images':
                        $result[$type]['name'] = $this->l('With image only');
                        break;
                    case 'visibility':
                        $result[$type]['name'] = $this->l('Visibility');
                        break;
                    case 'onsale':
                        $result[$type]['name'] = $this->l('On sale');
                        break;
                    case 'order':
                        $result[$type]['name'] = $this->l('Availible for order');
                        break;
                    case 'customizable':
                        $result[$type]['name'] = $this->l('Customizable');
                        break;
                    case 'onlineonly':
                        $result[$type]['name'] = $this->l('Online only');
                        break;
                    case 'sprice':
                        $result[$type]['name'] = $this->l('Show price');
                        break;
                    case 'virtual':
                        break;
                }
            } else {
                switch ($type) {
                    case 'features':
                        $activeFilter = $this->allFilters[$type];
                        foreach ($activeFilter as $item) {
                            if (isset($item['items'])) {
                                foreach ($item['items'] as $current) {
                                    if (in_array($current['id_feature_value'], $value)) {
                                        $result[$type]['value'] = false;
                                        $result[$type]['name'] = false;
                                        $result[$type]['values'][$current['id_feature']]['value'][$current['id_feature_value']] = $current['name'];
                                        $result[$type]['values'][$current['id_feature']]['name'] = $item['info']['name'];
                                    }
                                }
                            }
                        }
                        break;
                    case 'attributes':
                        $activeFilter = $this->allFilters[$type];
                        foreach ($activeFilter as $item) {
                            if (isset($item['items'])) {
                                foreach ($item['items'] as $current) {
                                    if (in_array($current['id_attribute'], $value)) {
                                        $result[$type]['value'] = false;
                                        $result[$type]['name'] = false;
                                        $result[$type]['values'][$current['id_attribute_group']]['value'][$current['id_attribute']] = $current['name'];
                                        $result[$type]['values'][$current['id_attribute_group']]['name'] = $item['info']['name'];
                                    }
                                }
                            }
                        }
                        break;
                    case 'categoryBox':
                        $result[$type]['name'] = $this->l('Categories');
                        foreach ($value as $id_category) {
                            $category = new Category($id_category, $this->context->language->id);
                            $result[$type]['value'][$id_category] = $category->name;
                        }
                        break;
                }
            }
        }

        return $result;
    }

    protected function removeParameter($type, $item)
    {
        $parameters = Tools::jsonDecode($this->history->get('tmproductsmanager'), true);
        if (!$parameters) {
            return false;
        }
        $fieldType = 'tmpm_'.$type;
        if ($type == 'categoryBox') {
            $fieldType = $type;
        }
        if (isset($parameters[$fieldType]) && $parameters[$fieldType]) {
            foreach ($parameters[$fieldType] as $key => $id) {
                if ($id == $item) {
                    unset($parameters[$fieldType][$key]);
                }
            }
            if (!$parameters[$fieldType]) {
                unset($parameters[$fieldType]);
            }
        }

        $this->history->set('tmproductsmanager', Tools::jsonEncode($parameters));

        return true;
    }

    public function getPagesList()
    {
        $pages = PMPage::getPagesList($this->context->shop->id, $this->context->language->id);
        if (!$pages) {
            return false;
        }
        $fields_list = array(
            'id_page' => array(
                'title' => $this->l('Id'),
                'type'  => 'text',
            ),
            'name'    => array(
                'title' => $this->l('Page name'),
                'type'  => 'text',
            ),
            'type'    => array(
                'title' => $this->l('Page type'),
                'type'  => 'text',
            ),
            'code'    => array(
                'title' => $this->l('Code'),
                'type'  => 'text',
            ),
            'active'  => array(
                'title'  => $this->l('Active'),
                'type'   => 'bool',
                'align'  => 'center',
                'active' => 'status'
            ),
        );

        $helper = new HelperList();
        $helper->title = $this->l('Pages list');
        $helper->shopLinkType = '';
        $helper->identifier = 'id_page';
        $helper->table = 'tmproductsmanager_pages';
        $helper->actions = array('edit', 'delete');
        $helper->listTotal = count($pages);
        $helper->show_toolbar = true;
        $helper->simple_header = true;
        $helper->show_filters = false;
        $helper->token = Tools::getAdminTokenLite('AdminTmProductsManager');
        $helper->currentIndex = AdminController::$currentIndex.'&action=newaction&process';

        return $helper->generateList($pages, $fields_list);
    }

    public function removeAction($id_page)
    {
        $page = new PMPage($id_page);
        if (!$page->delete()) {
            return true;
        }

        return false;
    }
}
