<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMPageBanner extends ObjectModel
{
    public $banner_name;
    public $id_page;
    public $position;
    public $pages;
    public $banner_description;
    public $page_banner;
    public $specific_class;
    public $banner_active;
    public $banner_countdown;
    public static $definition = array(
        'table'     => 'tmproductsmanager_banners',
        'primary'   => 'id_banner',
        'multilang' => true,
        'fields'    => array(
            'banner_name'        => array('type' => self::TYPE_STRING, 'lang' => true, 'required' => true, 'validate' => 'isGenericName'),
            'id_page'            => array('type' => self::TYPE_INT, 'required' => true, 'validate' => 'isunsignedInt'),
            'position'           => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString'),
            'pages'              => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString'),
            'banner_description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
            'page_banner'        => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrl'),
            'specific_class'     => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'banner_active'      => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'banner_countdown'   => array('type' => self::TYPE_BOOL, 'validate' => 'isBool')
        )
    );

    /**
     * Get full banners list for current page
     *
     * @param      $id_page
     * @param bool $id_lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getPageBanners($id_page, $id_lang = false)
    {
        $sql = 'SELECT tb.*, tbl.*
                FROM '._DB_PREFIX_.'tmproductsmanager_banners tb
                LEFT JOIN '._DB_PREFIX_.'tmproductsmanager_banners_lang tbl
                ON (tb.`id_banner` = tbl.`id_banner`)
                WHERE tb.`id_page` = '.(int)$id_page;
        if ($id_lang) {
            $sql .= ' AND tbl.`id_lang` = '.(int)$id_lang;
        }

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get brief banners information
     *
     * @param $id_page
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getPageBannersLite($id_page)
    {
        $sql = 'SELECT *
                FROM '._DB_PREFIX_.'tmproductsmanager_banners
                WHERE `id_page` = '.(int)$id_page;

        return Db::getInstance()->executeS($sql);
    }

    /**
     * Get banners for current hook on current page and valid date
     *
     * @param $page
     * @param $position
     * @param $id_lang
     *
     * @return array|false|mysqli_result|null|PDOStatement|resource
     * @throws PrestaShopDatabaseException
     */
    public static function getPageHookBanners($page, $position, $id_lang)
    {
        $sql = 'SELECT tb.*, tbl.*
                FROM '._DB_PREFIX_.'tmproductsmanager_banners tb
                LEFT JOIN '._DB_PREFIX_.'tmproductsmanager_banners_lang tbl
                ON(tb.`id_banner` = tbl.`id_banner`)
                LEFT JOIN '._DB_PREFIX_.'tmproductsmanager_pages tp
                ON(tb.`id_page` = tp.`id_page`)
                WHERE tb.`position` LIKE \'%'.pSql($position).'%\'
                AND (tb.`pages` LIKE \'%'.pSql($page).'%\' OR tb.`pages` = "all")
                AND tbl.`id_lang` = '.(int)$id_lang.'
                AND tb.`banner_active` = 1
                AND tp.`active` = 1
                AND (tp.`date_from` < "'.date('Y-m-d H:i:s').'" OR tp.`date_from` = "0000-00-00 00:00:00")
                AND (tp.`date_to` > "'.date('Y-m-d H:i:s').'" OR tp.`date_to` = "0000-00-00 00:00:00")';

        return Db::getInstance()->executeS($sql);
    }
}
