<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMProcessing extends AdminController
{
    private $ids = array();
    private $filters = array();
    private $tmexcludedproducts = '';
    protected $context;
    public $languages;
    public $default_lang;

    public function __construct($context, $ids, $filters, $tmexcludedproducts)
    {
        $this->context = $context;
        $this->ids = Tools::jsonDecode($ids, true);
        $this->filters = Tools::jsonDecode($filters, true);
        if ($tmexcludedproducts) {
            $this->tmexcludedproducts = $tmexcludedproducts;
        }
        $this->languages = Language::getLanguages(true);
        $this->default_lang = Language::getLanguage(Configuration::get('PS_LANG_DEFAULT'));
    }

    /************************************************* Information tab ************************************************/
    public function enableProducts(array $errors = array())
    {
        $status = (int)Tools::getValue('status');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->active = $status;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function availableOrder(array $errors = array())
    {
        $available_for_order = (int)Tools::getValue('available_for_order');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->available_for_order = $available_for_order;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function showPrice(array $errors = array())
    {
        $show_price = (int)Tools::getValue('show_price');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->show_price = $show_price;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function onlineOnly(array $errors = array())
    {
        $online_only = (int)Tools::getValue('online_only');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->online_only = $online_only;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function condition(array $errors = array())
    {
        $condition = Tools::getValue('condition');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->condition = $condition;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function description(array $errors = array())
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                if (Tools::getValue('choose_place') == 'replace_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang']);
                    }
                } elseif (Tools::getValue('choose_place') == 'before_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = Tools::getValue(
                            'description_'.$lang['id_lang']
                        ).$product->description[$lang['id_lang']];
                    }
                } elseif (Tools::getValue('choose_place') == 'after_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = $product->description[$lang['id_lang']].Tools::getValue(
                            'description_'.$lang['id_lang']
                        );
                    }
                }
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function descriptionShort(array $errors = array())
    {
        if ($this->ids) {
            $limit = (int)Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
            if ($limit <= 0) {
                $limit = 400;
            }
            foreach (Language::getLanguages(false) as $lang) {
                if (Tools::strlen(strip_tags(Tools::getValue('description_short_'.$lang['id_lang']))) > $limit) {
                    $errors[] = sprintf($this->l('Description is too long in %s'), $lang['iso_code']);
                }
            }

            if (count($errors)) {
                return $errors;
            }

            foreach ($this->ids as $id) {
                $product = new Product($id);

                foreach (Language::getLanguages(false) as $lang) {
                    $product->description_short[$lang['id_lang']] = Tools::getValue(
                        'description_short_'.$lang['id_lang']
                    );
                }

                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function tags(array $errors = array())
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                foreach (Language::getLanguages(false) as $language) {
                    if ($value = Tools::getValue('tags_'.$language['id_lang'])) {
                        if (!Tag::addTags($language['id_lang'], (int)$id, $value)) {
                            $errors[] = $this->l('Can\'t delete associations');
                        }
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /********************************************* end of Information tab *********************************************/
    /********************************************* Price tab **********************************************************/
    public function onSale(array $errors = array())
    {
        $on_sale = Tools::getValue('on_sale');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->on_sale = $on_sale;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function taxRules(array $errors = array())
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_tax_rules_group = (int)Tools::getValue('tax_rules');
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function specificPrice(array $errors = array())
    {
        $discount_price = Tools::getValue('discount_price');
        $reduction_type = Tools::getValue('reduction_type');
        $reduction_tax = Tools::getValue('reduction_tax');
        $from = Tools::getValue('data_start');
        $to = Tools::getValue('data_end');

        if (!$from) {
            $from = '0000-00-00 00:00:00';
        }

        if (!$to) {
            $to = '0000-00-00 00:00:00';
        }

        if (!Validate::isPrice($discount_price)) {
            $errors[] = $this->l('Invalid price field');
        } elseif (strtotime($to) < strtotime($from)) {
            $errors[] = $this->l('Invalid date range');
        } elseif ($from && $to && (!Validate::isDateFormat($from) || !Validate::isDateFormat($to))) {
            $errors[] = $this->l('The from/to date is invalid.');
        } elseif ($reduction_type == 'percentage' && ((float)$discount_price <= 0 || (float)$discount_price > 100)) {
            $errors[] = $this->l('Submitted reduction value (0-100) is out-of-range');
        } else {
            if ($this->ids) {
                foreach ($this->ids as $id) {
                    if (SpecificPrice::exists($id, 0, $this->context->shop->id, 0, 0, 0, 0, 1, $from, $to, false)) {
                        $errors[] = $this->l('A specific price already exists for these parameters.');
                    }
                    if (count($errors)) {
                        return $errors;
                    } else {
                        $specificPrice = new SpecificPrice();
                        $specificPrice->id_product = (int)$id;
                        $specificPrice->id_shop = $this->context->shop->id;
                        $specificPrice->id_currency = 0;
                        $specificPrice->id_country = 0;
                        $specificPrice->id_group = 0;
                        $specificPrice->id_customer = 0;
                        $specificPrice->from_quantity = 1;
                        $specificPrice->price = -1;
                        $specificPrice->reduction_type = $reduction_type;
                        $specificPrice->reduction_tax = $reduction_tax;
                        $specificPrice->from = $from;
                        $specificPrice->to = $to;
                        if ($reduction_type != 'amount') {
                            $specificPrice->reduction = $discount_price / 100;
                        } else {
                            $specificPrice->reduction = $discount_price;
                        }

                        if (!$specificPrice->add()) {
                            return $this->displayError($this->l('The specific price could not be added.'));
                        }
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /********************************************* end of Price tab ***************************************************/
    /********************************************* Association tab ****************************************************/
    public function brand(array $errors = array())
    {
        $brand = (int)Tools::getValue('manufacturer');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_manufacturer = $brand;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function defaultCategory(array $errors = array())
    {
        $id_category_default = (int)Tools::getValue('id_parent');
        if (!$id_category_default) {
            return $this->l('No one category was selected');
        }
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_category_default = $id_category_default;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function categories(array $errors = array())
    {
        $method = Tools::getValue('method');
        $categories = Tools::getValue('id_parent');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                if ($method == 'replace') {
                    if (!$product->deleteCategories(true)) {
                        $errors[] = $this->l('Can\'t delete associations');
                    }
                }
                if (!$product->addToCategories($categories)) {
                    $errors[] = $this->l('Can\'t delete associations');
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function accessories()
    {
        $ids = array_filter(explode('-', Tools::getValue('inputAccessories')));
        if ($this->ids && $ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->changeAccessories($ids);
            }
        }

        return false;
    }
    /************************************************* end of Association tab *****************************************/
    /*************************************************** Quantity tab *************************************************/
    public function quantity(array $errors = array())
    {
        $quantity = Tools::getValue('quantity');
        $action = Tools::getValue('method');
        if (Tools::isEmpty($quantity) || !Validate::isInt($quantity)) {
            $errors[] = $this->l('Bad quantity value');
        } else {
            if ($this->ids) {
                foreach ($this->ids as $id) {
                    $instocknow = StockAvailable::getQuantityAvailableByProduct($id, null, $this->context->shop->id);
                    if ($action == 'increase') {
                        $newquantity = $instocknow + $quantity;
                    } elseif ($action == 'decrease') {
                        if ($instocknow - $quantity < 0) {
                            $newquantity = 0;
                        } else {
                            $newquantity = $instocknow - $quantity;
                        }
                    } else {
                        $newquantity = $quantity;
                    }
                    if (StockAvailable::setQuantity($id, 0, $newquantity, $this->context->shop->id) === false) {
                        $errors[] = sprintf($this->l('Error occurred during product(%s) saving'), $id);
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /**********************************************and of Quantity tab ************************************************/
    /**************************************** start New Actions *******************************************************/
    /**
     * Handle new page's information
     *
     * @return array|bool|int
     */
    public function newaction()
    {
        if ($errors = $this->addPage()) {
            return $errors;
        }

        return false;
    }

    /**
     * Validate data and create/update page
     *
     * @param bool $return_id
     *
     * @return array|bool|int
     * @throws PrestaShopException
     */
    public function addPage($return_id = false)
    {
        $errors = array();
        $id_page = Tools::getValue('id_page');
        $name = Tools::getValue('name_'.$this->default_lang['id_lang']);
        $type = Tools::getValue('type');
        $code = Tools::getValue($type.'_code');
        $friendly_url = Tools::getValue('friendly_url_'.$this->default_lang['id_lang']);
        if (Tools::isEmpty($name) || !Validate::isGenericName($name)) {
            $errors[] = sprintf($this->l('Field name must be filled at least on %s language'), $this->default_lang['name']);
        }
        if (!Tools::isEmpty($code) && !Validate::isCleanHtml($code)) {
            $errors[] = $this->l('Field "Code" is empty or invalid');
        }
        if (Tools::isEmpty($friendly_url) || !Validate::isLinkRewrite($friendly_url)) {
            $errors[] = sprintf($this->l('Field "Friendly URL" must be filled at least on %s language'), $this->default_lang['name']);
        }
        if ($id_page) {
            $page = new PMPage($id_page);
        } else {
            $page = new PMPage();
        }

        foreach ($this->languages as $language) {
            $image = '';
            $l = $language['id_lang'];
            $ln = $language['name'];
            // set page name, set default if no own language name or if own language name is invalid
            if (Tools::isEmpty(Tools::getValue('name_'.$l))) {
                $page->name[$l] = $name;
            } else {
                if (!Validate::isGenericName(Tools::getValue('name_'.$l))) {
                    $errors[] = sprintf($this->l('Field name is invalid on %s language'), $ln);
                } else {
                    $page->name[$l] = Tools::getValue('name_'.$l);
                }
            }
            // set page friendly url, set default if no own language friendly url or if own language friendly url is invalid
            if (Tools::isEmpty(Tools::getValue('friendly_url_'.$l))) {
                $page->friendly_url[$l] = $friendly_url;
            } else {
                if (!Validate::isLinkRewrite(Tools::getValue('friendly_url_'.$l))) {
                    $errors[] = sprintf($this->l('Field "Friendly URL" is invalid on %s language'), $ln);
                } else {
                    $page->friendly_url[$l] = Tools::getValue('friendly_url_'.$l);
                }
            }
            if (!Tools::isEmpty(Tools::getValue('description_'.$l)) && !Validate::isCleanHtml(Tools::getValue('description_'.$l))) {
                $errors[] = sprintf($this->l('Field description is invalid on %s language'), $ln);
            } else {
                $page->description[$l] = Tools::getValue('description_'.$l);
            }
            $image = Tools::getValue('page_image_'.$l);
            if ($image && !Validate::isString($image)) {
                $errors[] = sprintf($this->l('Field Action image is invalid on %s language'), $ln);
            } else {
                $page->page_image[$l] = $image;
            }
        }
        $page->id_shop = $this->context->shop->id;
        $page->filters = Tools::jsonEncode($this->filters); // encode array of filter parameters and write for the future manipulations
        // write information about excluded products to manage it in feature
        if ($this->tmexcludedproducts) {
            $page->tmexcludedproducts = $this->tmexcludedproducts;
        }
        $page->products = Tools::jsonEncode($this->ids); // encode array of suitable products for database writing
        $page->layout = Tools::getValue('layout');
        $page->active = Tools::getValue('active');
        $page->type = $type;
        $page->date_from = '0000-00-00 00:00:00'; // set default dates if no custom one
        $page->date_to = '0000-00-00 00:00:00'; // set default dates if no custom one
        $page->countdown = 0;
        $page->display_code = 0;
        $page->display_gift = 0;
        $page->gift_product = 0;
        $page->gift_product_attribute = 0;
        if ($code) { // set code if exists
            $page->code = $code;
        }
        if ($page->type == 'time') {
            $from = Tools::getValue('date_from');
            $to = Tools::getValue('date_to');
            if (Tools::isEmpty($from) || Tools::isEmpty($to)) {
                $errors[] = $this->l('Time period is required for this type of offer');
            }
            if (!Validate::isDateFormat($from) || !Validate::isDateFormat($to)) {
                $errors[] = $this->l('Time period is invalid');
            }
            $page->date_from = $from;
            $page->date_to = $to;
            $page->countdown = Tools::getValue('countdown');
            if ($page->voucher) {
                if (!$this->removeRelatedVoucher($page->voucher)) {
                    $errors[] = $this->l('Can\'t remove old voucher');
                }
            }
            $page->voucher = 0;
        } elseif ($page->type == 'voucher_amount' || $page->type == 'voucher_percent' || $page->type == 'free_shipping' || $page->type == 'free_gift') {
            $from = Tools::getValue($page->type.'_date_from');
            $to = Tools::getValue($page->type.'_date_to');
            if (!Validate::isDateFormat($from) || !Validate::isDateFormat($to)) {
                $errors[] = $this->l('Time period is invalid');
            }
            if (!Tools::isEmpty($from)) {
                $page->date_from = $from;
            }
            if (!Tools::isEmpty($to)) {
                $page->date_to = $to;
            }
            $page->countdown = Tools::getValue('countdown');
            $page->display_code = Tools::getValue('display_code');
            $page->display_gift = Tools::getValue('display_gift');
            if (!count($errors)) {
                // create/update related voucher
                $voucher = $this->createVoucher($page->voucher, $page->type);
                if (is_array($voucher)) {
                    foreach ($voucher as $error) {
                        $errors[] = $error;
                    }
                } else {
                    $page->voucher = $voucher;
                }
            }
            if ($page->type == 'free_gift') {
                $page->gift_product = Tools::getValue('gift_product');
                $page->gift_product_attribute =  Tools::getValue('ipa_'.$page->gift_product);
            }
        } else {
            $page->voucher = 0;
        }

        if (count($errors)) {
            return $errors;
        } else {
            if ($id_page) {
                if (!$page->update()) {
                    $errors[] = $this->l('Error occurred during page updating');
                }
            } else {
                if (!$page->add()) {
                    $errors[] = $this->l('Error occurred during page adding');
                }
            }
        }
        if ($page->type != 'none' && $page->type != 'time') {
            if (!$page->updatePageProducts($page->id, $this->ids, $this->context->shop->id)) {
                $errors[] = $this->l('Can\'t save related products');
            }
        }

        if (count($errors)) {
            return $errors;
        } else {
            if ($return_id) {
                return $page->id;
            }
            return false;
        }
    }

    /**
     * Add/update page's banner
     *
     * @param      $id_page
     * @param bool $id_banner if true banner exists else not
     *
     * @return array|bool
     * @throws PrestaShopException
     */
    public function addBanner($id_page, $id_banner = false)
    {
        $errors = array();
        $name = Tools::getValue('banner_name_'.$this->default_lang['id_lang']);
        if (Tools::isEmpty($name) || !Validate::isGenericName($name)) {
            $errors[] = sprintf($this->l('Field Banner name must be filled at least on %s language'), $this->default_lang['name']);
        }
        if ($id_banner) {
            $banner = new PMPageBanner($id_banner);
        } else {
            $banner = new PMPageBanner();
        }

        foreach ($this->languages as $language) {
            $l = $language['id_lang'];
            $ln = $language['name'];
            $banner_image = '';
            $banner_description = '';
            if (Tools::isEmpty(Tools::getValue('banner_name_'.$l))) {
                $banner->banner_name[$l] = $name;
            } else {
                if (!Validate::isGenericName(Tools::getValue('banner_name_'.$l))) {
                    $errors[] = sprintf($this->l('Field Banner name is invalid on %s language'), $ln);
                } else {
                    $banner->banner_name[$l] = Tools::getValue('banner_name_'.$l);
                }
            }
            $banner_description = Tools::getValue('banner_description_'.$l);
            if ($banner_description && !Validate::isCleanHtml($banner_description)) {
                $errors[] = sprintf($this->l('Field Banner description is invalid on %s language'), $ln);
            } else {
                $banner->banner_description[$l] = $banner_description;
            }
            $banner_image = Tools::getValue('page_banner_'.$l);
            if ($banner_image && !Validate::isString($banner_image)) {
                $errors[] = sprintf($this->l('Field Banner image is invalid on %s language'), $ln);
            } else {
                $banner->page_banner[$l] = $banner_image;
            }
        }

        $banner->id_page = $id_page;
        $banner->specific_class = Tools::getValue('specific_class');
        $banner->banner_active = Tools::getValue('banner_active');
        $banner->banner_countdown = Tools::getValue('banner_countdown');
        if (!Tools::getValue('position')) {
            $errors[] = $this->l('No positions selected');
        } else {
            $banner->position = implode(',', Tools::getValue('position'));
        }
        if (!Tools::getValue('pages')) {
            $errors[] = $this->l('No pages selected');
        } else {
            $banner->pages = implode(',', Tools::getValue('pages'));
        }
        if (count($errors)) {
            return $errors;
        } else {
            if ($id_banner) {
                if (!$banner->update()) {
                    $errors[] = $this->l('Error occurred during page updating');
                }
            } else {
                if (!$banner->add()) {
                    $errors[] = $this->l('Error occurred during page adding');
                }
            }
        }

        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    /**
     * Create/update new voucher(Cart Rule) for a page
     *
     * @param $id_voucher
     * @param $type
     *
     * @return array
     */
    protected function createVoucher($id_voucher, $type)
    {
        $errors = array();
        $name = Tools::getValue('name_'.$this->default_lang['id_lang']);
        $code = Tools::getValue($type.'_code');
        $priority = Tools::getValue($type.'_priority');
        $amount = Tools::getValue($type.'_amount');
        $percent = Tools::getValue($type.'_percent');
        $quantity = Tools::getValue($type.'_quantity');
        $quantity_per_user = Tools::getValue($type.'_quantity_per_user');
        if (Tools::isEmpty($code) || !Validate::isCleanHtml($code)) {
            $errors[] = $this->l('Field "Code" is empty or invalid');
        }
        if (Tools::isEmpty($priority) || !Validate::isInt($priority)) {
            $errors[] = $this->l('Field "Priority" is empty or invalid. Only integer numbers allowed.');
        }
        if ($type == 'voucher_amount' && (Tools::isEmpty($amount) || !Validate::isFloat($amount))) {
            $errors[] = $this->l('Field "Amount" is empty or invalid. Only integer and float numbers allowed.');
        }
        if ($type == 'voucher_percent' && (Tools::isEmpty($percent) || !Validate::isInt($percent))) {
            $errors[] = $this->l('Field "Value" is empty or invalid. Only integer numbers allowed.');
        } elseif ($type == 'voucher_percent' && ($percent < 0 || $percent > 100)) {
            $errors[] = $this->l('Reduction percentage must be between 0% and 100%');
        }
        if (Tools::isEmpty($quantity) || !Validate::isInt($quantity)) {
            $errors[] = $this->l('Field "Total available" is empty or invalid. Only integer numbers allowed.');
        }
        if (Tools::isEmpty($quantity_per_user) || !Validate::isInt($quantity_per_user)) {
            $errors[] = $this->l('Field "Total available for each user" is empty or invalid. Only integer numbers allowed.');
        }
        if (count($errors)) {
            return $errors;
        }
        if (!$id_voucher) {
            $voucher = new CartRule();
        } else {
            $voucher = new CartRule($id_voucher);
        }

        foreach ($this->languages as $language) {
            $l = $language['id_lang'];
            if (Tools::isEmpty(Tools::getValue('name_'.$l))) {
                $voucher->name[$l] = 'tmpm-'.$name;
            } else {
                $voucher->name[$l] = 'tmpm-'.Tools::getValue('name_'.$l);
            }
        }
        $voucher->date_from = '0000-00-00 00:00:00';
        $voucher->date_to = '0000-00-00 00:00:00';
        if (!Tools::isEmpty($type.'_date_from')) {
            $voucher->date_from = Tools::getValue($type.'_date_from');
        }
        if (!Tools::isEmpty($type.'_date_to')) {
            $voucher->date_to = Tools::getValue($type.'_date_to');
        }
        $voucher->quantity = $quantity;
        $voucher->quantity_per_user = $quantity_per_user;
        $voucher->priority = $priority;
        $voucher->code = $code;
        $voucher->active = Tools::getValue('active');
        $voucher->gift_product = 0;
        $voucher->gift_product_attribute = 0;
        $voucher->reduction_product = 0;
        $voucher->reduction_percent = 0;
        $voucher->free_shipping = 0;
        $voucher->reduction_amount = 0;
        if ($type == 'voucher_amount') {
            $voucher->reduction_amount = $amount;
            $voucher->reduction_tax = Tools::getValue($type.'_reduction_tax');
            $voucher->reduction_currency = Tools::getValue($type.'_reduction_currency');
        } elseif ($type == 'voucher_percent') {
            $voucher->reduction_percent = $percent;
            $voucher->reduction_product = -2;
        } elseif ($type == 'free_shipping') {
            $voucher->free_shipping = 1;
        } elseif ($type == 'free_gift') {
            $voucher->gift_product = Tools::getValue('gift_product');
            $voucher->gift_product_attribute = Tools::getValue('ipa_'.$voucher->gift_product);
        }
        $voucher->product_restriction = 1;

        if (count($errors)) {
            return $errors;
        }
        if (!$id_voucher) {
            if (!$voucher->add()) {
                $errors[] = $this->l('Error occurred during voucher creating.');
            }
        } else {
            if (!$voucher->update()) {
                $errors[] = $this->l('Error occurred during voucher updating.');
            }
        }
        if (count($errors)) {
            return $errors;
        }
        if ($result = $this->addCartRule($voucher->id)) {
            $errors[] = $result;
        }
        if (count($errors)) {
            return $errors;
        } else {
            return $voucher->id;
        }
    }

    /**
     * Write down products to voucher
     *
     * @param $id_voucher
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    private function addCartRule($id_voucher)
    {
        $products = $this->ids;
        if (!$products) {
            return $this->l('No products in this group');
        }
        if (!$this->clearRelatedDb($id_voucher)) {
            return $this->l('Can\'t remove old data');
        }
        $insert = Db::getInstance()->insert('cart_rule_product_rule_group', array('id_cart_rule' => (int)$id_voucher, 'quantity' => 1));
        if (!$insert) {
            return $this->l('Can\'t create voucher product rule group');
        }
        $id_product_rule_group = Db::getInstance()->Insert_ID();

        if ($id_product_rule_group) {
            $insert_rule = Db::getInstance()->insert('cart_rule_product_rule', array('id_product_rule_group' => (int)$id_product_rule_group, 'type' => 'products'));
            if (!$insert_rule) {
                return $this->l('Can\'t create cart rule');
            }
        }

        $id_product_rule = Db::getInstance()->Insert_ID();
        foreach ($products as $product) {
            Db::getInstance()->insert('cart_rule_product_rule_value', array('id_product_rule' => (int)$id_product_rule, 'id_item' => $product));
        }

        return false;
    }

    /**
     * Clear related CartRule products tables after each operation
     *
     * @param $id_voucher
     *
     * @return bool
     * @throws PrestaShopDatabaseException
     */
    private function clearRelatedDb($id_voucher)
    {
        $id_product_rule_group = Db::getInstance()->executeS(
            'SELECT `id_product_rule_group`
            FROM '._DB_PREFIX_.'cart_rule_product_rule_group
            WHERE `id_cart_rule` = '.(int)$id_voucher
        );
        Db::getInstance()->delete('cart_rule_product_rule_group', '`id_cart_rule` = '.(int)$id_voucher);
        if ($id_product_rule_group) {
            foreach ($id_product_rule_group as $id_rule) {
                $id_product_rule = Db::getInstance()->getValue(
                    'SELECT `id_product_rule`
                    FROM '._DB_PREFIX_.'cart_rule_product_rule
                    WHERE `id_product_rule_group` = '.(int)$id_rule['id_product_rule_group']
                );

                if (isset($id_product_rule) && $id_product_rule) {
                    Db::getInstance()->delete('cart_rule_product_rule_value', '`id_product_rule` = '.(int)$id_product_rule);
                }
                Db::getInstance()->delete('cart_rule_product_rule', '`id_product_rule_group` = '.(int)$id_rule['id_product_rule_group']);
            }
        }

        return true;
    }

    /**
     * Remove related page voucher
     *
     * @param $id_voucher
     *
     * @return bool
     */
    private function removeRelatedVoucher($id_voucher)
    {
        $voucher = new CartRule($id_voucher);
        if (!$voucher->delete()) {
            return false;
        }

        return true;
    }
    /**************************************** end of New Actions ******************************************************/
}
