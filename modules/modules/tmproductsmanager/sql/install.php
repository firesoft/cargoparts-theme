<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_history` (
    `id_tmproductsmanager` int(11) NOT NULL AUTO_INCREMENT,
    `id_shop` int(11) NOT NULL,
    `id_employee` int(11) NOT NULL,
    `key` VARCHAR(100),
    `value` VARCHAR(100000),
    `date` DATE,
    PRIMARY KEY (`id_tmproductsmanager`, `id_shop`, `id_employee`, `key`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_pages` (
    `id_page` int(11) NOT NULL AUTO_INCREMENT,
    `id_shop` int(11) NOT NULL,
    `filters` VARCHAR(1000),
    `tmexcludedproducts` VARCHAR(128) NOT NULL,
    `products` VARCHAR(10000),
    `type` VARCHAR(10000),
    `date_from` TIMESTAMP,
    `date_to` TIMESTAMP,
    `layout` VARCHAR(128) DEFAULT "default",
    `active` int(11),
    `countdown` int(11),
    `voucher` VARCHAR(128),
    `code` VARCHAR(128),
    `gift_product` int(11),
    `gift_product_attribute` int(11),
    `display_code` int(11),
    `display_gift` int(11),
    PRIMARY KEY (`id_page`, `id_shop`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_pages_lang` (
    `id_page` int(11) NOT NULL AUTO_INCREMENT,
    `id_lang` int(11) NOT NULL,
    `name` VARCHAR(100),
    `friendly_url` VARCHAR(128),
    `page_image` VARCHAR(100) NOT NULL,
    `description` VARCHAR(10000),
    PRIMARY KEY (`id_page`, `id_lang`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_pages_products` (
    `id_item` int(11) NOT NULL AUTO_INCREMENT,
    `id_shop` int(11),
    `id_page` int(11),
    `id_product` int(11),
    PRIMARY KEY (`id_item`, `id_page`, `id_product`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_banners` (
    `id_banner` int(11) NOT NULL AUTO_INCREMENT,
    `id_page` int(11),
    `position` VARCHAR(1000) NOT NULL,
    `pages` VARCHAR(1000) NOT NULL,
    `specific_class` VARCHAR(1000) NOT NULL,
    `banner_active` int(11),
    `banner_countdown` int(11),
    PRIMARY KEY (`id_banner`, `id_page`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'tmproductsmanager_banners_lang` (
    `id_banner` int(11) NOT NULL AUTO_INCREMENT,
    `id_lang` int(11),
    `banner_name` VARCHAR(128),
    `banner_description` VARCHAR(10000),
    `page_banner`  VARCHAR(100),
    PRIMARY KEY (`id_banner`, `id_lang`)
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
