<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2017 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once(dirname(__FILE__).'/classes/PMPage.php');
include_once(dirname(__FILE__).'/classes/PMPageBanner.php');
include_once(dirname(__FILE__).'/classes/PMFilters.php');
include_once(dirname(__FILE__).'/classes/PMFilterProcess.php');
include_once(dirname(__FILE__).'/classes/PMProcessing.php');
include_once(dirname(__FILE__).'/classes/PMForms.php');
include_once(dirname(__FILE__).'/classes/PMHistory.php');

class Tmproductsmanager extends Module
{
    public $path;

    public function __construct()
    {
        $this->name = 'tmproductsmanager';
        $this->tab = 'smart_shopping';
        $this->version = '0.1.2';
        $this->author = 'TemplateMonster';
        $this->need_instance = 1;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('TM Products Manager');
        $this->description = $this->l('Manage your products easy, by using needed parameters');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->languages = Language::getLanguages(false);
        $this->path = $this->local_path;
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
        $this->addTab() &&
        $this->registerHook('moduleRoutes') &&
        $this->registerHook('header') &&
        $this->registerHook('displayBanner') &&
        $this->registerHook('displayTop') &&
        $this->registerHook('displayTopColumn') &&
        $this->registerHook('displayHome') &&
        $this->registerHook('displayLeftColumn') &&
        $this->registerHook('displayRightColumn') &&
        $this->registerHook('displayFooter') &&
        $this->registerHook('displayProductPriceBlock') &&
        $this->registerHook('displayLeftColumnProduct');
    }

    public function uninstall()
    {
        if (!$this->removeRelatedVouchers()) {
            return false;
        }
        include(dirname(__FILE__).'/sql/uninstall.php');

        return $this->removeTab() &&
        parent::uninstall();
    }

    protected function addTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminTmProductsManager';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminCatalog');
        $tab->module = $this->name;
        foreach ($this->languages as $lang) {
            $tab->name[$lang['id_lang']] = 'TM Products Manager';
        }
        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    protected function removeTab()
    {
        if ($id_tab = (int)Tab::getIdFromClassName('AdminTmProductsManager')) {
            $tab = new Tab($id_tab);
            if (!$tab->delete()) {
                return false;
            }
        }

        return true;
    }

    protected function removeRelatedVouchers()
    {
        $result = true;
        $page = new PMPage();
        $vouchers = $page->getAllShopVouchers($this->context->shop->id);
        if ($vouchers) {
            foreach ($vouchers as $voucher) {
                $rule = new CartRule($voucher['voucher']);
                $result &= $rule->delete();
            }
        }

        return $result;
    }

    public function displayRemoveLink($token, $id)
    {
        $this->context->smarty->assign(
            array(
                'href'   => AdminController::$currentIndex.'&id_product='.$id.'&removetmpm&token='.($token != null ? $token : $this->token).(Tools::getValue('id_page') ? '&id_page='.Tools::getValue('id_page') : ''),
                'action' => $this->l('Remove from list'),
                'id'     => $id
            )
        );

        return $this->display($this->_path, 'views/templates/admin/_configure/helpers/list/list_action_remove.tpl');
    }

    public static function getTemplateFile($file_name)
    {
        return _PS_THEME_DIR_.$file_name;
    }

    public static function convertToInt($data)
    {
        return (int)$data;
    }

    public function hookModuleRoutes()
    {
        $link = array(
            'tmproductsmanager' => array(
                'controller' => 'listing',
                'rule'       => 'tmproductsmanager/listing/{id_page}-{title}.html',
                'keywords'   => array(
                    'id_page' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'id_page'),
                    'title'   => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'title')
                ),
                'params'     => array(
                    'fc'     => 'module',
                    'module' => 'tmproductsmanager',
                ),
            )
        );

        return $link;
    }

    /**
     * Build own module url
     * @return string
     */
    public static function getTMProductsmanagerUrl($id_lang = false)
    {
        $ssl_enable = Configuration::get('PS_SSL_ENABLED');
        if (!$id_lang) {
            $id_lang = (int)Context::getContext()->language->id;
        }
        $id_shop = (int)Context::getContext()->shop->id;
        $rewrite_set = (int)Configuration::get('PS_REWRITING_SETTINGS');
        $ssl = null;
        static $force_ssl = null;
        if ($ssl === null) {
            if ($force_ssl === null) {
                $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
            }
            $ssl = $force_ssl;
        }
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop !== null) {
            $shop = new Shop($id_shop);
        } else {
            $shop = Context::getContext()->shop;
        }
        if ($ssl && $ssl_enable) {
            $base = 'https://'.$shop->domain_ssl;
        } else {
            $base = 'http://'.$shop->domain;
        }
        $langUrl = Language::getIsoById($id_lang).'/';

        if ((!$rewrite_set && in_array($id_shop, array((int)Context::getContext()->shop->id, null)))
            || !Language::isMultiLanguageActivated($id_shop)
            || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop)
        ) {
            $langUrl = '';
        }

        return $base.$shop->getBaseURI().$langUrl;
    }

    /**
     * Build own module link
     *
     * @param string $rewrite
     * @param null   $params
     * @param null   $id_lang
     *
     * @return string
     */
    public static function getTMProductsmanagerLink($rewrite = 'tmproductsmanager', $params = null, $id_lang = null)
    {
        $url = Tmproductsmanager::getTMProductsmanagerUrl($id_lang);
        $dispatcher = Dispatcher::getInstance();

        if ($params != null) {
            return $url.$dispatcher->createUrl($rewrite, $id_lang, $params);
        }


        return $url.$dispatcher->createUrl($rewrite);
    }

    public static function getPageFriendlyURL($id_page, $id_lang)
    {
        $page = new PMPage($id_page, $id_lang);
        return $page->friendly_url;
    }

    /**
     * Get list of available page's layouts, support overrides for admin and front pages.
     * Not required .tpl instance in module root views directory.
     * Can only create one in the your_theme/modules/tmproductsmanager/views/templates/front/_layouts/
     * and your_theme/modules/tmproductsmanager/views/templates/admin/_layouts/
     * but correct makeup and file name are required. File names examples: default_layout.tpl, three_column_layout.tpl, *_layout.tpl
     * where _layout.tpl is required part and other part mast not contain symbols different from latin characters, numbers and signs "-" or "_"
     *
     * @return array
     */
    public function getAdminPageLayoutsName()
    {
        $layouts = array();
        $override_files = array();
        $files = Tools::scandir($this->local_path.'views/templates/admin/_layouts/', 'tpl');
        if (is_dir(_PS_ALL_THEMES_DIR_.$this->context->shop->theme_directory.'/modules/'.$this->name.'/views/templates/admin/_layouts/')) {
            $override_files = Tools::scandir(
                _PS_ALL_THEMES_DIR_.$this->context->shop->theme_directory.'/modules/'.$this->name.'/views/templates/admin/_layouts/',
                'tpl'
            );
        }
        if ($files) {
            foreach (array_unique(array_merge($files, $override_files)) as $file) {
                if (Tools::strpos($file, '_layout.tpl') !== false) {
                    $name = str_replace('_layout.tpl', '', $file);
                    if (!Tools::isEmpty($name) && Validate::isGenericName($name)) {
                        $layouts[] = $name;
                    }
                }
            }
        }
        return $layouts;
    }

    /**
     * Build admin page layouts for page
     *
     * @return array
     */
    public function getAdminPageLayouts()
    {
        $layouts = $this->getAdminPageLayoutsName();
        $map = array();
        if ($layouts) {
            foreach ($layouts as $layout) {
                $map[$layout] = $this->display($this->local_path, 'views/templates/admin/_layouts/'.$layout.'_layout.tpl');
            }
        }

        return $map;
    }

    /**
     * Get front layout template
     *
     * @param $layout
     * @param $page
     *
     * @return mixed
     */
    public function getFrontLayout($layout, $page)
    {
        $this->context->smarty->assign('page', $page);
        $this->context->smarty->assign('img_src', __PS_BASE_URI__);
        return $this->display($this->local_path, 'views/templates/front/_layouts/'.$layout.'_layout.tpl');
    }

    public function hookDisplayHeader()
    {
        Media::addJsDef(array(
            'tmdd_msg_days' => $this->l('days'),
            'tmdd_msg_hr' => $this->l('hrs'),
            'tmdd_msg_min' => $this->l('mins'),
            'tmdd_msg_sec' => $this->l('secs')
        ));
        $this->context->controller->addJS($this->_path.'views/js/jquery.countdown-modified.js');
        $this->context->controller->addJS($this->_path.'views/js/tmproductsmanager.js');
        $this->context->controller->addCss($this->_path.'views/css/tmproductsmanager.css');
        $this->context->smarty->assign('img_path', __PS_BASE_URI__);
        $this->context->smarty->assign('server_time', date('Y-m-d H:i:s'));
    }

    /**
     * Get banners and create front end template
     *
     * @param $hook
     *
     * @return bool
     */
    public function manageBanners($hook)
    {
        $page_name = $this->context->controller->php_self;
        if (!$page_name) {
            $page_name = '-';
        }
        $id_lang = $this->context->language->id;
        $cache_id = $this->getCacheId($this->name.'_'.$hook.'_'.$page_name);
        $banners = PMPageBanner::getPageHookBanners($page_name, $hook, $id_lang);
        if ($banners) {
            if (!$this->isCached('banner.tpl', $cache_id)) {
                $this->context->smarty->assign('banners', $banners);
            }

            return $this->display($this->local_path, 'views/templates/hook/banner.tpl', $cache_id);
        }

        return false;
    }

    public function clearCache()
    {
        $this->_clearCache('banner.tpl');
        $this->_clearCache('product_page_info.tpl');
    }

    public function hookDisplayTop()
    {
        return $this->manageBanners('displayTop');
    }

    public function hookDisplayBanner()
    {
        return $this->manageBanners('displayBanner');
    }

    public function hookDisplayTopColumn()
    {
        return $this->manageBanners('displayColumnTop');
    }

    public function hookDisplayHome()
    {
        return $this->manageBanners('displayHome');
    }

    public function hookDisplayLeftColumn()
    {
        return $this->manageBanners('displayLeftColumn');
    }

    public function hookDisplayRightColumn()
    {
        return $this->manageBanners('displayRightColumn');
    }

    public function hookDisplayFooter()
    {
        return $this->manageBanners('displayFooter');
    }

    public function hookDisplayProductPriceBlock($product)
    {
        if ($product['type'] != 'before_price') {
            return false;
        }
        $id_product = $product['product']['id_product'];
        $current_page = explode('-', Tools::getValue('id_page'))[0];
        $id_shop = $this->context->shop->id;
        $id_lang = $this->context->language->id;
        $result = array();
        $pm_page = new PMPage();
        if (!$pages = $pm_page->checkProductAction($id_product, $id_shop)) {
            return false;
        }
        foreach ($pages as $id_page) {
            $page = new PMPage($id_page['id_page']);
            if ($page->date_from) {
                if ($page->date_from > date('Y-m-d H:i:s') || $page->date_to < date('Y-m-d H:i:s')) {
                    continue;
                }
            }
            $result[$page->id]['id_page'] = $page->id;
            if ($page->display_code) {
                if (in_array($page->display_code, array(2, 5))) {
                    $result[$page->id]['code'] = $page->code;
                } elseif (in_array($page->display_code, array(1, 4)) && $current_page == $page->id) {
                    $result[$page->id]['code'] = $page->code;
                } elseif (in_array($page->display_code, array(3, 6)) && $current_page != $page->id) {
                    $result[$page->id]['code'] = $page->code;
                }
            }
            if ($page->display_gift) {
                $gift_attribute = false;
                $gift_attribute_image = false;
                $gift_product = PMPage::getProducts(array($page->gift_product), $id_lang, 1, 1);
                if ($page->gift_product_attribute) {
                    $gift_attribute_image = Product::getCombinationImageById($page->gift_product_attribute, $id_lang);
                    $p = new Product($page->gift_product, false, $id_lang);
                    $gift_attribute = array();
                    $gift_attributes_available = $p->getAttributesGroups($id_lang);
                    foreach ($gift_attributes_available as $attr) {
                        if ($attr['id_product_attribute'] == $page->gift_product_attribute) {
                            $gift_attribute[] = $attr;
                        }
                    }
                }
                if (in_array($page->display_code, array(2, 5))) {
                    $result[$page->id]['gift'] = $gift_product[0];
                    $result[$page->id]['gift_attributes'] = $gift_attribute;
                    $result[$page->id]['gift_attribute_image'] = $gift_attribute_image['id_image'];
                } elseif (in_array($page->display_code, array(1, 4)) && $current_page == $page->id) {
                    $result[$page->id]['gift'] = $gift_product[0];
                    $result[$page->id]['gift_attributes'] = $gift_attribute;
                    $result[$page->id]['gift_attribute_image'] = $gift_attribute_image['id_image'];
                } elseif (in_array($page->display_code, array(3, 6)) && $current_page != $page->id) {
                    $result[$page->id]['gift'] = $gift_product[0];
                    $result[$page->id]['gift_attributes'] = $gift_attribute;
                    $result[$page->id]['gift_attribute_image'] = $gift_attribute_image['id_image'];
                }
            }
        }
        $this->context->smarty->assign('offers', $result);
        $this->context->smarty->assign('id_lang', $id_lang);

        return $this->display($this->local_path, 'views/templates/hook/info.tpl', 'tmpm_'.$this->getCacheId($id_product));
    }

    public function hookDisplayLeftColumnProduct()
    {
        if ($this->context->controller->php_self != 'product' || !$id_product = Tools::getValue('id_product')) {
            return false;
        }
        if (!$this->isCached('product_page_info.tpl', 'tmpm_'.$this->getCacheId($id_product))) {
            $id_shop = $this->context->shop->id;
            $id_lang = $this->context->language->id;
            $result = array();
            $pm_page = new PMPage();
            if (!$pages = $pm_page->checkProductAction($id_product, $id_shop)) {
                return false;
            }
            foreach ($pages as $id_page) {
                $page = new PMPage($id_page['id_page']);
                if ($page->date_from) {
                    if ($page->date_from > date('Y-m-d H:i:s') || $page->date_to < date('Y-m-d H:i:s')) {
                        continue;
                    }
                }
                $result[$page->id]['id_page'] = $page->id;
                if (in_array($page->display_code, array(2,4, 5, 6, 7))) {
                    $result[$page->id]['code'] = $page->code;
                }
                if ($page->display_gift) {
                    $gift_attribute = false;
                    $gift_attribute_image = false;
                    $gift_product = PMPage::getProducts(array($page->gift_product), $id_lang, 1, 1);
                    if ($page->gift_product_attribute) {
                        $gift_attribute_image = Product::getCombinationImageById($page->gift_product_attribute, $id_lang);
                        $p = new Product($page->gift_product, false, $id_lang);
                        $gift_attribute = array();
                        $gift_attributes_available = $p->getAttributesGroups($id_lang);
                        foreach ($gift_attributes_available as $attr) {
                            if ($attr['id_product_attribute'] == $page->gift_product_attribute) {
                                $gift_attribute[] = $attr;
                            }
                        }
                    }
                    if (in_array($page->display_gift, array(2, 4, 5, 6, 7))) {
                        $result[$page->id]['gift'] = $gift_product[0];
                        $result[$page->id]['gift_attributes'] = $gift_attribute;
                        $result[$page->id]['gift_attribute_image'] = $gift_attribute_image['id_image'];
                    }
                }
            }
            $this->context->smarty->assign('offers', $result);
            $this->context->smarty->assign('id_lang', $id_lang);
        }

        return $this->display($this->local_path, 'views/templates/hook/product_page_info.tpl', 'tmpm_'.$this->getCacheId($id_product));
    }
}
