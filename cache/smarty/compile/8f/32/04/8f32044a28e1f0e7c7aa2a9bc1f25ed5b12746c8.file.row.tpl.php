<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:34
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\layouts\row.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2031959b7f7fa99a5f8-56599120%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f32044a28e1f0e7c7aa2a9bc1f25ed5b12746c8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\layouts\\row.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2031959b7f7fa99a5f8-56599120',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'elem' => 0,
    'preview' => 0,
    'position' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fa9cf976_80297410',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fa9cf976_80297410')) {function content_59b7f7fa9cf976_80297410($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.replace.php';
?>

<div class="row sortable <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>data-type="row" data-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_item'], ENT_QUOTES, 'UTF-8', true);?>
" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_parent'], ENT_QUOTES, 'UTF-8', true);?>
" data-sort-order="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
" data-specific-class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
" data-id-unique="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
  <article class="row-inner clearfix inner">
    <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>
      <div class="button-container clearfix">
        <span class="element-name"><?php echo smartyTranslate(array('s'=>'Row','mod'=>'tmmegalayout'),$_smarty_tpl);?>

          <span class="sort-order"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span> <span class="identificator"><?php if ($_smarty_tpl->tpl_vars['elem']->value['specific_class']) {?>(<?php echo smarty_modifier_replace(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'),' ',' | ');?>
)<?php }?></span></span>
        <div class="dropdown button-container pull-right">
          <a href="#" id="dropdownMenu-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" class="dropdown-toggle" aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" type="button"></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
">
            <li><a href="#" class="add-column">+ <?php echo smartyTranslate(array('s'=>'Add col','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li class="divider" role="separator"></li>
            <li><a href="#" class="edit-row"><?php echo smartyTranslate(array('s'=>'Edit row','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li><a href="#" class="edit-styles"><?php echo smartyTranslate(array('s'=>'Stylize','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li><a href="#" class="remove-item"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
          </ul>
        </div>
      </div>
    <?php }?>
    <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['position']->value);?>

  </article>
</div><?php }} ?>
