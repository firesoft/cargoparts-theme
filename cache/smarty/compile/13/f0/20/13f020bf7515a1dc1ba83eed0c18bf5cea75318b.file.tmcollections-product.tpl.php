<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 14:00:58
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmcollections\views\templates\hook\tmcollections-product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1884259b9725a0ad036-09764615%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13f020bf7515a1dc1ba83eed0c18bf5cea75318b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmcollections\\views\\templates\\hook\\tmcollections-product.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1884259b9725a0ad036-09764615',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'collections' => 0,
    'id_product' => 0,
    'PS_CATALOG_MODE' => 0,
    'collection' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b9725a174dc8_01767524',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b9725a174dc8_01767524')) {function content_59b9725a174dc8_01767524($_smarty_tpl) {?>

<p class="buttons_bottom_block no-print">
  <?php if (count($_smarty_tpl->tpl_vars['collections']->value)==1||count($_smarty_tpl->tpl_vars['collections']->value)==0) {?>
    <a href="#" id="collection_button_nopop" onclick="AddProductToCollection('action_add', '<?php echo intval($_smarty_tpl->tpl_vars['id_product']->value);?>
', $('#idCombination').val(), <?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value)) {?>document.getElementById('quantity_wanted').value<?php } else { ?>1<?php }?>); return false;" rel="nofollow"
        title="<?php echo smartyTranslate(array('s'=>'Add to my collection','mod'=>'tmcollections'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add to collection','mod'=>'tmcollections'),$_smarty_tpl);?>
</a>
  <?php } else { ?>
    <a id="collection_button" tabindex="0" data-toggle="popover" data-trigger="focus" title="<?php echo smartyTranslate(array('s'=>'Collection','mod'=>'tmcollections'),$_smarty_tpl);?>
" data-placement="top"><?php echo smartyTranslate(array('s'=>'Add to collection','mod'=>'tmcollections'),$_smarty_tpl);?>
</a>
    <div hidden id="popover-content-collection">
      <ul>
        <?php  $_smarty_tpl->tpl_vars['collection'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['collection']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['collections']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['collection']->key => $_smarty_tpl->tpl_vars['collection']->value) {
$_smarty_tpl->tpl_vars['collection']->_loop = true;
?>
          <li title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['collection']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['collection']->value['id_collection'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
              onclick="AddProductToCollection('action_add', '<?php echo intval($_smarty_tpl->tpl_vars['id_product']->value);?>
', $('#idCombination').val(), <?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value)) {?>document.getElementById('quantity_wanted').value<?php } else { ?>1<?php }?>, '<?php echo intval($_smarty_tpl->tpl_vars['collection']->value['id_collection']);?>
');">
            <?php echo smartyTranslate(array('s'=>'Add to %s','sprintf'=>array($_smarty_tpl->tpl_vars['collection']->value['name']),'mod'=>'tmcollections'),$_smarty_tpl);?>

          </li>
        <?php } ?>
      </ul>
    </div>
  <?php }?>
</p><?php }} ?>
