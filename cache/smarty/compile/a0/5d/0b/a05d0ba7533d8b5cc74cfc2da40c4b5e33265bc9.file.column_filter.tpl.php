<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:16:24
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmadvancedfilter\\views\templates\hook\column_filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2575859b7fa48d2db62-49105910%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a05d0ba7533d8b5cc74cfc2da40c4b5e33265bc9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmadvancedfilter\\\\views\\templates\\hook\\column_filter.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2575859b7fa48d2db62-49105910',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
    'filter_parameters' => 0,
    'data' => 0,
    'parameter' => 0,
    'field' => 0,
    'result' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7fa48dff4a0_84776289',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7fa48dff4a0_84776289')) {function content_59b7fa48dff4a0_84776289($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['content']->value['indexed']) {?>
  <form action="#" method="post">
    <div id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter" class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter tmadvancedfilter block">
      <?php if ($_smarty_tpl->tpl_vars['content']->value['filter_name']) {?><h2 class="title_block"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['filter_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h2><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['content']->value['filter_description']) {?><div class="filter-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['content']->value['filter_description']);?>
</div><?php }?>
      <?php if (isset($_smarty_tpl->tpl_vars['filter_parameters']->value)&&$_smarty_tpl->tpl_vars['filter_parameters']->value) {?>
        <div class="filter-parameters-row clearfix">
          <ul id="filter-selected-parameters">
            <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['data']->_loop = false;
 $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['filter_parameters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
 $_smarty_tpl->tpl_vars['field']->value = $_smarty_tpl->tpl_vars['data']->key;
?>
              <?php if ($_smarty_tpl->tpl_vars['data']->value) {?>
                <?php  $_smarty_tpl->tpl_vars['parameter'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['parameter']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['parameter']->key => $_smarty_tpl->tpl_vars['parameter']->value) {
$_smarty_tpl->tpl_vars['parameter']->_loop = true;
?>
                  <?php if ($_smarty_tpl->tpl_vars['parameter']->value['field_type']) {?>
                    <li data-filter-field="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-filter-field-type="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['field_type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['parameter']->value['id'])&&$_smarty_tpl->tpl_vars['parameter']->value['id']) {?>data-filter-field-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['id'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<i class="icon icon-remove"></i></li>
                  <?php }?>
                <?php } ?>
              <?php }?>
            <?php } ?>
          </ul>
        </div>
      <?php }?>
      <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['content']->value['filter']);?>

      <button name="submitColumnTmAdvancedFilter" type="submit" class="btn btn-success btn-lg result"><?php echo smartyTranslate(array('s'=>'Show','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
 <span class="count"><?php if (isset($_smarty_tpl->tpl_vars['result']->value)&&$_smarty_tpl->tpl_vars['result']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['result']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?></span></button>
      <input type="hidden" name="id_filter" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_filter'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
      <input type="hidden" name="type" value="column" />
    </div>
  </form>
<?php }?>
<?php }} ?>
