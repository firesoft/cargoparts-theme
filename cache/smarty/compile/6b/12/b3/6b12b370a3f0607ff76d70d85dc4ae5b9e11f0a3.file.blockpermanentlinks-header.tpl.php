<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:23
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\blockpermanentlinks\blockpermanentlinks-header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2439559b7e5e37b3253-27182836%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b12b370a3f0607ff76d70d85dc4ae5b9e11f0a3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\blockpermanentlinks\\blockpermanentlinks-header.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2439559b7e5e37b3253-27182836',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_name' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e388ff84_28657429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e388ff84_28657429')) {function content_59b7e5e388ff84_28657429($_smarty_tpl) {?><!-- Block permanent links module HEADER -->
<ul id="header_links">
  <li id="header_link_home">
    <a <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>class="active"<?php }?> href="index.php" title="<?php echo smartyTranslate(array('s'=>'home','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'home','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a>
  </li>
  <li id="header_link_contact">
    <a <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='contact') {?>class="active"<?php }?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'contact','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a>
  </li>
  <?php if (class_exists('smartblog')) {?>
    <li id="header_link_blog">
      <a <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='module-smartblog-category') {?>class="active"<?php }?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('smartblog'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'blog','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'blog','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a>
    </li>
  <?php }?>
  <li id="header_link_sitemap">
    <a <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='sitemap') {?>class="active"<?php }?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('sitemap'), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'sitemap','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'sitemap','mod'=>'blockpermanentlinks'),$_smarty_tpl);?>
</a>
  </li>
</ul>
<!-- /Block permanent links module HEADER --><?php }} ?>
