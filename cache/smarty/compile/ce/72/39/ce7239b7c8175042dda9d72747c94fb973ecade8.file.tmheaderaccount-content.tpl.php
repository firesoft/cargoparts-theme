<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:40
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmheaderaccount\views\templates\hook\tmheaderaccount-content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2555059b7e5f4373a45-37027791%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce7239b7c8175042dda9d72747c94fb973ecade8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmheaderaccount\\views\\templates\\hook\\tmheaderaccount-content.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2555059b7e5f4373a45-37027791',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'is_logged' => 0,
    'hook' => 0,
    'configs' => 0,
    'avatar' => 0,
    'firstname' => 0,
    'lastname' => 0,
    'link' => 0,
    'returnAllowed' => 0,
    'voucherAllowed' => 0,
    'HOOK_BLOCK_MY_ACCOUNT' => 0,
    'shop_name' => 0,
    'HOOK_CREATE_ACCOUNT_TOP' => 0,
    'genders' => 0,
    'gender' => 0,
    'days' => 0,
    'day' => 0,
    'sl_day' => 0,
    'months' => 0,
    'k' => 0,
    'sl_month' => 0,
    'month' => 0,
    'years' => 0,
    'year' => 0,
    'sl_year' => 0,
    'newsletter' => 0,
    'field_required' => 0,
    'optin' => 0,
    'b2b_enable' => 0,
    'PS_REGISTRATION_PROCESS_TYPE' => 0,
    'dlv_all_fields' => 0,
    'field_name' => 0,
    'required_fields' => 0,
    'countries' => 0,
    'v' => 0,
    'sl_country' => 0,
    'postCodeExist' => 0,
    'stateExist' => 0,
    'one_phone_at_least' => 0,
    'HOOK_CREATE_ACCOUNT_FORM' => 0,
    'request_uri' => 0,
    'address' => 0,
    'vatnumber_ajax_call' => 0,
    'email_create' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f45d75c5_80466933',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f45d75c5_80466933')) {function content_59b7e5f45d75c5_80466933($_smarty_tpl) {?>

<ul class="header-login-content toogle_content<?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?> is-logged<?php }?>">
  <?php if ($_smarty_tpl->tpl_vars['is_logged']->value) {?>
    <li <?php if ($_smarty_tpl->tpl_vars['hook']->value!='left'&&$_smarty_tpl->tpl_vars['hook']->value!='right') {?>class="<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_DISPLAY_STYLE']);?>
"<?php }?>>
      <ul>
        <li class="user-data">
          <?php if ($_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_USE_AVATAR']) {?>
            <img src="<?php echo $_smarty_tpl->tpl_vars['avatar']->value;?>
" alt=""<?php if ($_smarty_tpl->tpl_vars['hook']->value!='left'&&$_smarty_tpl->tpl_vars['hook']->value!='right') {?> width="<?php if ($_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_DISPLAY_STYLE']=="twocolumns") {?>150<?php } else { ?>90<?php }?>"<?php }?>>
          <?php }?>
          <p><span><?php echo $_smarty_tpl->tpl_vars['firstname']->value;?>
</span> <span><?php echo $_smarty_tpl->tpl_vars['lastname']->value;?>
</span></p>
        </li>
        <li>
          <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('history',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My orders','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My orders','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['returnAllowed']->value) {?>
          <li>
            <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-follow',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My returns','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
              <?php echo smartyTranslate(array('s'=>'My merchandise returns','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </a>
          </li>
        <?php }?>
        <li>
          <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
        <li>
          <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
        <li>
          <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('identity',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'Manage my personal information','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My personal info','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
        <?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value) {?>
          <li>
            <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('discount',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
              <?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </a>
          </li>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['HOOK_BLOCK_MY_ACCOUNT']->value)&&$_smarty_tpl->tpl_vars['HOOK_BLOCK_MY_ACCOUNT']->value!='') {?>
          <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['HOOK_BLOCK_MY_ACCOUNT']->value);?>

        <?php }?>
      </ul>
      <p class="logout">
        <a class="btn btn-primary btn-md" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'), ENT_QUOTES, 'UTF-8', true);?>
?mylogout" title="<?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
          <?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

        </a>
      </p>
    </li>
  <?php } else { ?>
    <li class="login-content">
      <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true), ENT_QUOTES, 'UTF-8', true);?>
" method="post">
        <h3 class="login-heading"><?php echo smartyTranslate(array('s'=>'Login','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</h3>
        <div class="alert alert-danger" style="display:none;"></div>
        <div class="form_content clearfix">
          <div class="form-group">
            <input class="is_required validate account_input form-control email" data-validate="isEmail" type="text" name="header-email" id="header-email" placeholder="<?php echo smartyTranslate(array('s'=>'Email address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['email'])) {?><?php echo mb_convert_encoding(htmlspecialchars(stripslashes($_POST['email']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <span>
              <input class="is_required validate account_input form-control password" type="password" data-validate="isPasswd" name="header-passwd" id="header-passwd" placeholder="<?php echo smartyTranslate(array('s'=>'Password','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['passwd'])) {?><?php echo mb_convert_encoding(htmlspecialchars(stripslashes($_POST['passwd']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>" autocomplete="off"/>
            </span>
          </div>
          <p class="submit">
            <button type="submit" name="HeaderSubmitLogin" class="btn btn-primary btn-md">
              <?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </button>
          </p>
          <p>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('password','true'), ENT_QUOTES, 'UTF-8', true);?>
" class="forgot-password">
              <?php echo smartyTranslate(array('s'=>'Forgot your password?','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </a>
          </p>
          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayHeaderLoginButtons"),$_smarty_tpl);?>

          <div class="create-content">
            <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="create">
              <?php echo smartyTranslate(array('s'=>'Create an account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </a>
            <p><?php echo smartyTranslate(array('s'=>'Personalize your experience at','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_name']->value, ENT_QUOTES, 'UTF-8', true);?>
</p>
          </div>
        </div>
      </form>
    </li>
    <li class="create-account-content hidden">
      <div class="alert alert-danger" style="display:none;"></div>
      <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true), ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
        <?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_TOP']->value;?>

        <div class="account_creation">
          <div class="clearfix">
            <?php  $_smarty_tpl->tpl_vars['gender'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gender']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['genders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gender']->key => $_smarty_tpl->tpl_vars['gender']->value) {
$_smarty_tpl->tpl_vars['gender']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['gender']->key;
?>
              <div class="radio-inline">
                <input type="radio" name="id_gender" id="id-gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" value="<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" <?php if (isset($_POST['id_gender'])&&$_POST['id_gender']==$_smarty_tpl->tpl_vars['gender']->value->id) {?>checked="checked"<?php }?> />
                <label for="id-gender<?php echo $_smarty_tpl->tpl_vars['gender']->value->id;?>
" class="top"><?php echo $_smarty_tpl->tpl_vars['gender']->value->name;?>
</label>
              </div>
            <?php } ?>
          </div>
          <div class="required form-group">
            <input onkeyup="$('#firstname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" name="firstname" id="customer-firstname" placeholder="<?php echo smartyTranslate(array('s'=>'First name','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['customer_firstname'])) {?><?php echo $_POST['customer_firstname'];?>
<?php }?>"/>
          </div>
          <div class="required form-group">
            <input onkeyup="$('#lastname').val(this.value);" type="text" class="is_required validate form-control" data-validate="isName" name="lastname" id="customer-lastname" placeholder="<?php echo smartyTranslate(array('s'=>'Last name','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['customer_lastname'])) {?><?php echo $_POST['customer_lastname'];?>
<?php }?>"/>
          </div>
          <div class="required form-group">
            <input type="email" class="is_required validate form-control" data-validate="isEmail" name="email" id="email-create" placeholder="<?php echo smartyTranslate(array('s'=>'Email','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['email'])) {?><?php echo $_POST['email'];?>
<?php }?>"/>
          </div>
          <div class="required password form-group">
            <input type="password" class="is_required validate form-control" data-validate="isPasswd" name="passwd" placeholder="<?php echo smartyTranslate(array('s'=>'Password','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" id="passwd-create"/>
            <span class="form_info"><?php echo smartyTranslate(array('s'=>'(Five characters minimum)','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'Date of Birth','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
            <div class="date-birth row">
              <div class="col-xs-4">
                <select name="days" class="form-control">
                  <option value=""><?php echo smartyTranslate(array('s'=>'Day','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</option>
                  <?php  $_smarty_tpl->tpl_vars['day'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['day']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['days']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['day']->key => $_smarty_tpl->tpl_vars['day']->value) {
$_smarty_tpl->tpl_vars['day']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['day']->value;?>
" <?php if (($_smarty_tpl->tpl_vars['sl_day']->value==$_smarty_tpl->tpl_vars['day']->value)) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['day']->value;?>
&nbsp;&nbsp;</option>
                  <?php } ?>
                </select>
                
              </div>
              <div class="col-xs-4">
                <select name="months" class="form-control">
                  <option value=""><?php echo smartyTranslate(array('s'=>'Month','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</option>
                  <?php  $_smarty_tpl->tpl_vars['month'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['month']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['months']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['month']->key => $_smarty_tpl->tpl_vars['month']->value) {
$_smarty_tpl->tpl_vars['month']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['month']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if (($_smarty_tpl->tpl_vars['sl_month']->value==$_smarty_tpl->tpl_vars['k']->value)) {?> selected="selected"<?php }?>><?php echo smartyTranslate(array('s'=>$_smarty_tpl->tpl_vars['month']->value,'mod'=>'tmheaderaccount'),$_smarty_tpl);?>
&nbsp;</option>
                  <?php } ?>
                </select>
              </div>
              <div class="col-xs-4">
                <select name="years" class="form-control">
                  <option value=""><?php echo smartyTranslate(array('s'=>'Year','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</option>
                  <?php  $_smarty_tpl->tpl_vars['year'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['year']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['year']->key => $_smarty_tpl->tpl_vars['year']->value) {
$_smarty_tpl->tpl_vars['year']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
" <?php if (($_smarty_tpl->tpl_vars['sl_year']->value==$_smarty_tpl->tpl_vars['year']->value)) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
&nbsp;&nbsp;</option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <?php if (isset($_smarty_tpl->tpl_vars['newsletter']->value)&&$_smarty_tpl->tpl_vars['newsletter']->value) {?>
            <div class="checkbox">
              <input type="checkbox" name="newsletter" id="newsletter-tmha" value="1" <?php if (isset($_POST['newsletter'])&&$_POST['newsletter']==1) {?> checked="checked"<?php }?> />
              <label for="newsletter-tmha"><?php echo smartyTranslate(array('s'=>'Sign up for our newsletter!','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <?php if (array_key_exists('newsletter',$_smarty_tpl->tpl_vars['field_required']->value)) {?>
                <sup> *</sup>
              <?php }?>
            </div>
          <?php }?>
          <?php if (isset($_smarty_tpl->tpl_vars['optin']->value)&&$_smarty_tpl->tpl_vars['optin']->value) {?>
            <div class="checkbox">
              <input type="checkbox" name="optin" id="optin-tmha" value="1" <?php if (isset($_POST['optin'])&&$_POST['optin']==1) {?> checked="checked"<?php }?> />
              <label for="optin-tmha"><?php echo smartyTranslate(array('s'=>'Receive special offers from our partners!','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <?php if (array_key_exists('optin',$_smarty_tpl->tpl_vars['field_required']->value)) {?>
                <sup> *</sup>
              <?php }?>
            </div>
          <?php }?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['b2b_enable']->value) {?>
          <div class="account_creation">
            <h3 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Your company information','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</h3>
            <p class="form-group">
              <label for="company-tmha"><?php echo smartyTranslate(array('s'=>'Company','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <input type="text" class="form-control" name="company" id="company-tmha" value="<?php if (isset($_POST['company'])) {?><?php echo $_POST['company'];?>
<?php }?>"/>
            </p>
            <p class="form-group">
              <label for="siret-tmha"><?php echo smartyTranslate(array('s'=>'SIRET','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <input type="text" class="form-control" name="siret" id="siret-tmha" value="<?php if (isset($_POST['siret'])) {?><?php echo $_POST['siret'];?>
<?php }?>"/>
            </p>
            <p class="form-group">
              <label for="ape-tmha"><?php echo smartyTranslate(array('s'=>'APE','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <input type="text" class="form-control" name="ape" id="ape-tmha" value="<?php if (isset($_POST['ape'])) {?><?php echo $_POST['ape'];?>
<?php }?>"/>
            </p>
            <p class="form-group">
              <label for="website-tmha"><?php echo smartyTranslate(array('s'=>'Website','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <input type="text" class="form-control" name="website" for="website-tmha" value="<?php if (isset($_POST['website'])) {?><?php echo $_POST['website'];?>
<?php }?>"/>
            </p>
          </div>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['PS_REGISTRATION_PROCESS_TYPE']->value)&&$_smarty_tpl->tpl_vars['PS_REGISTRATION_PROCESS_TYPE']->value) {?>
          <div class="account_creation">
            <h3 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Your address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</h3>
            <?php  $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field_name']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dlv_all_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field_name']->key => $_smarty_tpl->tpl_vars['field_name']->value) {
$_smarty_tpl->tpl_vars['field_name']->_loop = true;
?>
              <?php if ($_smarty_tpl->tpl_vars['field_name']->value=="company") {?>
                <?php if (!$_smarty_tpl->tpl_vars['b2b_enable']->value) {?>
                  <p class="form-group">
                    <label for="company2-tmha"><?php echo smartyTranslate(array('s'=>'Company','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php if (in_array($_smarty_tpl->tpl_vars['field_name']->value,$_smarty_tpl->tpl_vars['required_fields']->value)) {?><sup>*</sup><?php }?>
                    </label>
                    <input type="text" class="form-control" name="company" id="company2-tmha" value="<?php if (isset($_POST['company'])) {?><?php echo $_POST['company'];?>
<?php }?>"/>
                  </p>
                <?php }?>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="vat_number") {?>
                <div style="display:none;">
                  <p class="form-group">
                    <label for="vat_number"><?php echo smartyTranslate(array('s'=>'VAT number','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php if (in_array($_smarty_tpl->tpl_vars['field_name']->value,$_smarty_tpl->tpl_vars['required_fields']->value)) {?>
                        <sup>*</sup>
                      <?php }?></label>
                    <input type="text" class="form-control" name="vat_number" id="vat_number-tmha" value="<?php if (isset($_POST['vat_number'])) {?><?php echo $_POST['vat_number'];?>
<?php }?>"/>
                  </p>
                </div>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="firstname") {?>
                <p class="required form-group">
                  <label for="firstname-tmha"><?php echo smartyTranslate(array('s'=>'First name','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <input type="text" class="form-control" name="firstname" id="firstname-tmha" value="<?php if (isset($_POST['firstname'])) {?><?php echo $_POST['firstname'];?>
<?php }?>"/>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="lastname") {?>
                <p class="required form-group">
                  <label for="lastname-tmha"><?php echo smartyTranslate(array('s'=>'Last name','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <input type="text" class="form-control" name="lastname" id="lastname-tmha" value="<?php if (isset($_POST['lastname'])) {?><?php echo $_POST['lastname'];?>
<?php }?>"/>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="address1") {?>
                <p class="required form-group">
                  <label for="address1-tmha"><?php echo smartyTranslate(array('s'=>'Address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <input type="text" class="form-control" name="address1" id="address1-tmha" value="<?php if (isset($_POST['address1'])) {?><?php echo $_POST['address1'];?>
<?php }?>"/>
                  <span class="inline-infos"><?php echo smartyTranslate(array('s'=>'Street address, P.O. Box, Company name, etc.','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="address2") {?>
                <p class="form-group is_customer_param">
                  <label for="address2-tmha"><?php echo smartyTranslate(array('s'=>'Address (Line 2)','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php if (in_array($_smarty_tpl->tpl_vars['field_name']->value,$_smarty_tpl->tpl_vars['required_fields']->value)) {?>
                      <sup>*</sup>
                    <?php }?></label>
                  <input type="text" class="form-control" name="address2" id="address2-tmha" value="<?php if (isset($_POST['address2'])) {?><?php echo $_POST['address2'];?>
<?php }?>"/>
                  <span class="inline-infos"><?php echo smartyTranslate(array('s'=>'Apartment, suite, unit, building, floor, etc...','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="postcode") {?>
                <?php $_smarty_tpl->tpl_vars['postCodeExist'] = new Smarty_variable(true, null, 0);?>
                <p class="required postcode form-group">
                  <label for="postcode-tmha"><?php echo smartyTranslate(array('s'=>'Zip/Postal Code','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <input type="text" class="form-control" name="postcode" data-validate="isPostCode" id="postcode-tmha" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>"/>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="city") {?>
                <p class="required form-group">
                  <label for="city-tmha"><?php echo smartyTranslate(array('s'=>'City','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <input type="text" class="form-control" name="city" id="city-tmha" value="<?php if (isset($_POST['city'])) {?><?php echo $_POST['city'];?>
<?php }?>"/>
                </p>
                <!-- if customer hasn't update his layout address, country has to be verified but it's deprecated -->
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="Country:name"||$_smarty_tpl->tpl_vars['field_name']->value=="country") {?>
                <p class="required select form-group">
                  <label><?php echo smartyTranslate(array('s'=>'Country','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <select name="id_country" class="form-control">
                    <option value="">-</option>
                    <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                      <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id_country'];?>
"<?php if ((isset($_POST['id_country'])&&$_POST['id_country']==$_smarty_tpl->tpl_vars['v']->value['id_country'])||(!isset($_POST['id_country'])&&$_smarty_tpl->tpl_vars['sl_country']->value==$_smarty_tpl->tpl_vars['v']->value['id_country'])) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</option>
                    <?php } ?>
                  </select>
                </p>
              <?php } elseif ($_smarty_tpl->tpl_vars['field_name']->value=="State:name"||$_smarty_tpl->tpl_vars['field_name']->value=='state') {?>
                <?php $_smarty_tpl->tpl_vars['stateExist'] = new Smarty_variable(true, null, 0);?>
                <p class="required id_state select form-group">
                  <label><?php echo smartyTranslate(array('s'=>'State','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                  <select name="id_state" class="form-control">
                    <option value="">-</option>
                  </select>
                </p>
              <?php }?>
            <?php } ?>
            <?php if ($_smarty_tpl->tpl_vars['postCodeExist']->value==false) {?>
              <p class="required postcode form-group unvisible">
                <label for="postcode2-tmha"><?php echo smartyTranslate(array('s'=>'Zip/Postal Code','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                <input type="text" class="form-control" name="postcode" id="postcode2-tmha" value="<?php if (isset($_POST['postcode'])) {?><?php echo $_POST['postcode'];?>
<?php }?>"/>
              </p>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['stateExist']->value==false) {?>
              <p class="required id_state select unvisible form-group">
                <label for="id_state"><?php echo smartyTranslate(array('s'=>'State','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
                <select name="id_state" class="form-control">
                  <option value="">-</option>
                </select>
              </p>
            <?php }?>
            <p class="textarea form-group">
              <label for="other-tmha"><?php echo smartyTranslate(array('s'=>'Additional information','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
              <textarea class="form-control" name="other" id="other-tmha" cols="26" rows="3"><?php if (isset($_POST['other'])) {?><?php echo $_POST['other'];?>
<?php }?></textarea>
            </p>
            <p class="form-group">
              <label for="phone-tmha"><?php echo smartyTranslate(array('s'=>'Home phone','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value) {?>
                  <sup>**</sup>
                <?php }?></label>
              <input type="text" class="form-control" name="phone" id="phone-tmha" value="<?php if (isset($_POST['phone'])) {?><?php echo $_POST['phone'];?>
<?php }?>"/>
            </p>
            <p class="<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value) {?>required <?php }?>form-group">
              <label for="phone_mobile-tmha"><?php echo smartyTranslate(array('s'=>'Mobile phone','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value) {?>
                  <sup>**</sup>
                <?php }?></label>
              <input type="text" class="form-control" name="phone_mobile" id="phone_mobile-tmha" value="<?php if (isset($_POST['phone_mobile'])) {?><?php echo $_POST['phone_mobile'];?>
<?php }?>"/>
            </p>
            <?php if (isset($_smarty_tpl->tpl_vars['one_phone_at_least']->value)&&$_smarty_tpl->tpl_vars['one_phone_at_least']->value) {?>
              <?php $_smarty_tpl->tpl_vars["atLeastOneExists"] = new Smarty_variable(true, null, 0);?>
              <p class="inline-infos required">** <?php echo smartyTranslate(array('s'=>'You must register at least one phone number.','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</p>
            <?php }?>
            <p class="required form-group">
              <label for="alias-tmha"><?php echo smartyTranslate(array('s'=>'Assign an address alias for future reference.','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
              <input type="text" class="form-control" name="alias" id="alias-tmha" value="<?php if (isset($_POST['alias'])) {?><?php echo $_POST['alias'];?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'My address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>"/>
            </p>
          </div>
          <div class="account_creation dni">
            <h3 class="page-subheading"><?php echo smartyTranslate(array('s'=>'Tax identification','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</h3>
            <p class="required form-group">
              <label for="dni-tmha"><?php echo smartyTranslate(array('s'=>'Identification number','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
 <sup>*</sup></label>
              <input type="text" class="form-control" name="dni" id="dni-tmha" value="<?php if (isset($_POST['dni'])) {?><?php echo $_POST['dni'];?>
<?php }?>"/>
              <span class="form_info"><?php echo smartyTranslate(array('s'=>'DNI / NIF / NIE','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
            </p>
          </div>
        <?php }?>
        <?php echo $_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value;?>

        <div class="submit clearfix">
          <input type="hidden" name="email_create" value="1"/>
          <input type="hidden" name="is_new_customer" value="1"/>
          <input type="hidden" class="hidden" name="back" value="my-account"/>
          <p class="submit">
            <button type="submit" name="submitAccount" class="btn btn-primary btn-md">
              <span>
                <?php echo smartyTranslate(array('s'=>'Register','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

              </span>
            </button>
          </p>
          <p class="text-center">
            <a href="#" class="signin"><span><?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span></a>
          </p>
        </div>
      </form>
    </li>
    <li class="forgot-password-content hidden">
      <p><?php echo smartyTranslate(array('s'=>'Please enter the email address you used to register. We will then send you a new password. ','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</p>
      <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['request_uri']->value, ENT_QUOTES, 'UTF-8', true);?>
" method="post" class="std">
        <fieldset>
          <div class="form-group">
            <div class="alert alert-success" style="display:none;"></div>
            <div class="alert alert-danger" style="display:none;"></div>
            <input class="form-control" type="email" name="email" id="email-forgot" placeholder="<?php echo smartyTranslate(array('s'=>'Email address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" value="<?php if (isset($_POST['email'])) {?><?php echo stripslashes(htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8', true));?>
<?php }?>"/>
          </div>
          <p class="submit">
            <button type="submit" class="btn btn-primary btn-md">
              <span>
                <?php echo smartyTranslate(array('s'=>'Retrieve Password','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

              </span>
            </button>
          </p>
          <p class="text-center">
            <a href="#" class="signin"><span><?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span></a>
          </p>
        </fieldset>
      </form>
    </li>
    <?php if (isset($_POST['id_state'])&&$_POST['id_state']) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedState'=>intval($_POST['id_state'])),$_smarty_tpl);?>
<?php } elseif (isset($_smarty_tpl->tpl_vars['address']->value->id_state)&&$_smarty_tpl->tpl_vars['address']->value->id_state) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedState'=>intval($_smarty_tpl->tpl_vars['address']->value->id_state)),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedState'=>false),$_smarty_tpl);?>
<?php }?><?php if (isset($_POST['id_state_invoice'])&&isset($_POST['id_state_invoice'])&&$_POST['id_state_invoice']) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedStateInvoice'=>intval($_POST['id_state_invoice'])),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedStateInvoice'=>false),$_smarty_tpl);?>
<?php }?><?php if (isset($_POST['id_country'])&&$_POST['id_country']) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedCountry'=>intval($_POST['id_country'])),$_smarty_tpl);?>
<?php } elseif (isset($_smarty_tpl->tpl_vars['address']->value->id_country)&&$_smarty_tpl->tpl_vars['address']->value->id_country) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedCountry'=>intval($_smarty_tpl->tpl_vars['address']->value->id_country)),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedCountry'=>false),$_smarty_tpl);?>
<?php }?><?php if (isset($_POST['id_country_invoice'])&&isset($_POST['id_country_invoice'])&&$_POST['id_country_invoice']) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedCountryInvoice'=>intval($_POST['id_country_invoice'])),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('idSelectedCountryInvoice'=>false),$_smarty_tpl);?>
<?php }?><?php if (isset($_smarty_tpl->tpl_vars['countries']->value)) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('countries'=>$_smarty_tpl->tpl_vars['countries']->value),$_smarty_tpl);?>
<?php }?><?php if (isset($_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value)&&$_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('vatnumber_ajax_call'=>$_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value),$_smarty_tpl);?>
<?php }?><?php if (isset($_smarty_tpl->tpl_vars['email_create']->value)&&$_smarty_tpl->tpl_vars['email_create']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('email_create'=>$_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['boolval'][0][0]->boolval($_smarty_tpl->tpl_vars['email_create']->value)),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('email_create'=>false),$_smarty_tpl);?>
<?php }?>
  <?php }?>
</ul><?php }} ?>
