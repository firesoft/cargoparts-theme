<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 14:00:56
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\socialsharing\views\templates\hook\socialsharing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2321559b97258d62315-98218119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '748ea8674bd0c2bdfd0de6f90115e0bca82c8237' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\socialsharing\\views\\templates\\hook\\socialsharing.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2321559b97258d62315-98218119',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'PS_SC_TWITTER' => 0,
    'PS_SC_FACEBOOK' => 0,
    'PS_SC_GOOGLE' => 0,
    'PS_SC_PINTEREST' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b97258df1632_63389740',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b97258df1632_63389740')) {function content_59b97258df1632_63389740($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value||$_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value||$_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value||$_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value) {?>
  <p class="socialsharing_product no-print">
    <?php if ($_smarty_tpl->tpl_vars['PS_SC_TWITTER']->value) {?>
      <button data-type="twitter" type="button" class="btn btn-twitter social-sharing">
        <i class="fa fa-twitter"></i>
      </button>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['PS_SC_FACEBOOK']->value) {?>
      <button data-type="facebook" type="button" class="btn btn-facebook social-sharing">
        <i class="fa fa-facebook"></i>
      </button>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['PS_SC_GOOGLE']->value) {?>
      <button data-type="google-plus" type="button" class="btn btn-google-plus social-sharing">
        <i class="fa fa-google-plus"></i>
      </button>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['PS_SC_PINTEREST']->value) {?>
      <button data-type="pinterest" type="button" class="btn btn-pinterest social-sharing">
        <i class="fa fa-pinterest"></i>
      </button>
    <?php }?>
  </p>
<?php }?><?php }} ?>
