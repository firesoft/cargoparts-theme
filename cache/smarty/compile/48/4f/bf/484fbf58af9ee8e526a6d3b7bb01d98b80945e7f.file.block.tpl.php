<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:37
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmegalayout\\views\templates\hook\layouts\block.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1093659b7e5f1bd6813-53854346%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '484fbf58af9ee8e526a6d3b7bb01d98b80945e7f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmegalayout\\\\views\\templates\\hook\\layouts\\block.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1093659b7e5f1bd6813-53854346',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'force_ssl' => 0,
    'base_dir_ssl' => 0,
    'base_dir' => 0,
    'shop_name' => 0,
    'logo_url' => 0,
    'logo_image_width' => 0,
    'logo_image_height' => 0,
    'HOOK_HOME_TAB_CONTENT' => 0,
    'HOOK_HOME_TAB' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f1c97973_33572187',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f1c97973_33572187')) {function content_59b7e5f1c97973_33572187($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="logo") {?>
  <div class="header_logo">
    <a href="<?php if (isset($_smarty_tpl->tpl_vars['force_ssl']->value)&&$_smarty_tpl->tpl_vars['force_ssl']->value) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir_ssl']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_name']->value, ENT_QUOTES, 'UTF-8', true);?>
">
      <img class="logo img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo_url']->value, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_name']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php if (isset($_smarty_tpl->tpl_vars['logo_image_width']->value)&&$_smarty_tpl->tpl_vars['logo_image_width']->value) {?> width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo_image_width']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php }?><?php if (isset($_smarty_tpl->tpl_vars['logo_image_height']->value)&&$_smarty_tpl->tpl_vars['logo_image_height']->value) {?> height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo_image_height']->value, ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>/>
    </a>
  </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="copyright") {?>
  <?php if (Configuration::get('FOOTER_POWEREDBY')) {?>
    <div class="copyright">
      <?php echo smartyTranslate(array('s'=>'[1] %3$s %2$s - Ecommerce software by %1$s [/1]','mod'=>'tmmegalayout','sprintf'=>array('PrestaShop™',date('Y'),'©'),'tags'=>array('<a class="_blank" href="http://www.prestashop.com">')),$_smarty_tpl);?>

    </div>
  <?php }?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="tabs") {?>
  <?php if (isset($_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value)&&trim($_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value)) {?>
    <?php if (isset($_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value)&&trim($_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value)) {?>
      <ul id="home-page-tabs" class="nav nav-tabs clearfix">
        <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value;?>

      </ul>
    <?php }?>
    <div class="tab-content"><?php echo $_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value;?>
</div>
  <?php }?>
<?php }?><?php }} ?>
