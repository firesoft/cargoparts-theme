<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:36
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\blockcontactinfos\blockcontactinfos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1212159b7e5f057bd84-48614800%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '24dc907b9dc5d1ba1f8a5fdf8cd6feba0493deba' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\blockcontactinfos\\blockcontactinfos.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1212159b7e5f057bd84-48614800',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'blockcontactinfos_company' => 0,
    'blockcontactinfos_address' => 0,
    'blockcontactinfos_phone' => 0,
    'blockcontactinfos_email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f05c6af2_78325022',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f05c6af2_78325022')) {function content_59b7e5f05c6af2_78325022($_smarty_tpl) {?><?php if (!is_callable('smarty_function_mailto')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\function.mailto.php';
?><!-- MODULE Block contact infos -->
<section id="block_contact_infos" class="footer-block">
  <div>
    <h4><?php echo smartyTranslate(array('s'=>'Store Information','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
</h4>
    <ul class="toggle-footer">
      <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_company']->value!='') {?>
        <li>
          <i class="fa fa-map-marker"></i>
          <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_company']->value, ENT_QUOTES, 'UTF-8', true);?>

            <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_address']->value!='') {?>
              , <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_address']->value, ENT_QUOTES, 'UTF-8', true);?>

            <?php }?>
        </li>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value!='') {?>
        <li>
          <i class="fa fa-phone"></i>
          <span>
            <a href="tel:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value, ENT_QUOTES, 'UTF-8', true);?>
">
              <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value, ENT_QUOTES, 'UTF-8', true);?>

            </a>
          </span>
        </li>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value!='') {?>
        <li>
          <i class="fa fa-envelope"></i>
          <span><?php echo smarty_function_mailto(array('address'=>htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value, ENT_QUOTES, 'UTF-8', true),'encode'=>"hex"),$_smarty_tpl);?>
</span>
        </li>
      <?php }?>
    </ul>
  </div>
</section>
<!-- /MODULE Block contact infos --><?php }} ?>
