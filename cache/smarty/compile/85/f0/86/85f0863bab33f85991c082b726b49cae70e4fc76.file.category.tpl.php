<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:52:37
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1122459b7e6a5d5fce1-30723466%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '85f0863bab33f85991c082b726b49cae70e4fc76' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\category.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1122459b7e6a5d5fce1-30723466',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'products' => 0,
    'module_blocklayered' => 0,
    'nbr_filterBlocks' => 0,
    'p' => 0,
    'n' => 0,
    'nb_products' => 0,
    'productShowingStart' => 0,
    'productShowing' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e6a5de3a80_29017417',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e6a5de3a80_29017417')) {function content_59b7e6a5de3a80_29017417($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php if (isset($_smarty_tpl->tpl_vars['category']->value)) {?>
  <?php if ($_smarty_tpl->tpl_vars['category']->value->id&&$_smarty_tpl->tpl_vars['category']->value->active) {?>
    <?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
      <?php $_smarty_tpl->tpl_vars["module_blocklayered"] = new Smarty_variable(Module::getInstanceByName('blocklayered'), null, 0);?>
      <?php if (isset($_smarty_tpl->tpl_vars['module_blocklayered']->value)&&$_smarty_tpl->tpl_vars['module_blocklayered']->value&&$_smarty_tpl->tpl_vars['nbr_filterBlocks']->value!=0) {?>
        <button id="filter-button" class="btn btn-md btn-default"><?php echo smartyTranslate(array('s'=>'Filter'),$_smarty_tpl);?>
</button>
        <div id="filter-overlay"></div>
      <?php }?>
      <div class="content_sortPagiBar clearfix">
        <div class="sortPagiBar clearfix">
          <?php if (isset($_smarty_tpl->tpl_vars['p']->value)&&$_smarty_tpl->tpl_vars['p']->value) {?>
            <div class="product-count">
              <?php if (($_smarty_tpl->tpl_vars['n']->value*$_smarty_tpl->tpl_vars['p']->value)<$_smarty_tpl->tpl_vars['nb_products']->value) {?>
                <?php $_smarty_tpl->tpl_vars['productShowing'] = new Smarty_variable($_smarty_tpl->tpl_vars['n']->value*$_smarty_tpl->tpl_vars['p']->value, null, 0);?>
              <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars['productShowing'] = new Smarty_variable(($_smarty_tpl->tpl_vars['n']->value*$_smarty_tpl->tpl_vars['p']->value-$_smarty_tpl->tpl_vars['nb_products']->value-$_smarty_tpl->tpl_vars['n']->value*$_smarty_tpl->tpl_vars['p']->value)*-1, null, 0);?>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['p']->value==1) {?>
                <?php $_smarty_tpl->tpl_vars['productShowingStart'] = new Smarty_variable(1, null, 0);?>
              <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars['productShowingStart'] = new Smarty_variable($_smarty_tpl->tpl_vars['n']->value*$_smarty_tpl->tpl_vars['p']->value-$_smarty_tpl->tpl_vars['n']->value+1, null, 0);?>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['nb_products']->value>1) {?>
                <?php echo smartyTranslate(array('s'=>'%1$d - %2$d of %3$d items','sprintf'=>array($_smarty_tpl->tpl_vars['productShowingStart']->value,$_smarty_tpl->tpl_vars['productShowing']->value,$_smarty_tpl->tpl_vars['nb_products']->value)),$_smarty_tpl);?>

              <?php } else { ?>
                <?php echo smartyTranslate(array('s'=>'%1$d - %2$d of 1 item','sprintf'=>array($_smarty_tpl->tpl_vars['productShowingStart']->value,$_smarty_tpl->tpl_vars['productShowing']->value)),$_smarty_tpl);?>

              <?php }?>
            </div>
          <?php }?>
          <?php echo $_smarty_tpl->getSubTemplate ("./nbr-product-page.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

          <?php echo $_smarty_tpl->getSubTemplate ("./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

          <?php echo $_smarty_tpl->getSubTemplate ("./product-compare.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        </div>
      </div>
      <?php echo $_smarty_tpl->getSubTemplate ("./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>

      <div class="content_sortPagiBar">
        <div class="bottom-pagination-content clearfix">
          <?php echo $_smarty_tpl->getSubTemplate ("./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('paginationId'=>'bottom'), 0);?>

        </div>
      </div>
    <?php }?>
  <?php }?>
<?php }?><?php }} ?>
