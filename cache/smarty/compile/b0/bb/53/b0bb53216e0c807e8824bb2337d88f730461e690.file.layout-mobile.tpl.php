<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 14:40:17
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmediaparallax\views\templates\hook\layout-mobile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:991859b97b91353ef3-60505916%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b0bb53216e0c807e8824bb2337d88f730461e690' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmediaparallax\\views\\templates\\hook\\layout-mobile.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '991859b97b91353ef3-60505916',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'base_url' => 0,
    'popText' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b97b91e49d54_93802693',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b97b91e49d54_93802693')) {function content_59b97b91e49d54_93802693($_smarty_tpl) {?>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['item_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_info']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item_key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_info']['iteration']++;
?>
  <?php $_smarty_tpl->_capture_stack[0][] = array('some_content', 'popText', null); ob_start(); ?>
    <section class="rd-parallax rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" style="position: relative; <?php if (isset($_smarty_tpl->tpl_vars['item']->value['image'])) {?>background-image: url('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['item']->value['image']);?>
');<?php }?> background-size: cover; overflow:hidden;">
      <?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?>
        <div class="container">
      <?php }?>
        <div class="parallax-main-layout"></div>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['content'])&&$_smarty_tpl->tpl_vars['item']->value['content']) {?>
          <div class="item-content"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['item']->value['content']);?>
</div>
        <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?>
        </div>
      <?php }?>
    </section>
  <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
  <script>
    $(document).ready(function() {
      var elem = $('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['selector'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
      if (elem.length) {
        $('body').append(`<?php echo strtr($_smarty_tpl->tpl_vars['popText']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
`);
        var wrapper = $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
        elem.before(wrapper);
        $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 .parallax-main-layout').replaceWith(elem);
        <?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?>
        win = $(window);
        win.on('load resize', function() {
          wrapper.css('width', win.width()).css('margin-left', Math.floor(win.width() * -0.5)).css('left', '50%');
        });
        <?php }?>
      }
    });
  </script>
<?php } ?><?php }} ?>
