<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:49:18
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\tools\options.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1991859b801fecef488-73043858%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b010e4a1f8a36a90ed5d06d89662104257dd10fc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\tools\\options.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1991859b801fecef488-73043858',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b801fed8bc54_37327777',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b801fed8bc54_37327777')) {function content_59b801fed8bc54_37327777($_smarty_tpl) {?>

<div class="tmmegalayout-lsettins">
  <div class="form-wrapper">
    <div class="form-group">
      <label class="control-label pull-left"><?php echo smartyTranslate(array('s'=>'Optimization (test mode)','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
      <div class="tmlist-layout-btns" data-layout-id="4">
        <a class="layout-btn" id="optionShowMessages" href="#" data-layout-id="4">
          <i class="process-icon-toggle-on <?php if (!Configuration::get('TMMEGALAYOUT_SHOW_MESSAGES')) {?>hidden<?php }?>"></i>
          <i class="process-icon-toggle-off <?php if (Configuration::get('TMMEGALAYOUT_SHOW_MESSAGES')) {?>hidden<?php }?>"></i>
        </a>
      </div>
      <p class="desc"><small><?php echo smartyTranslate(array('s'=>'This option allow you optimize files includes. If you will optimize it, only usable files will be included in that or other pages(pages are automatically checked), in other way all files will be included on all pages. You must reoptimize, after do any changes on one of preset.','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</small>
      </p>
    </div>
    <div class="form-group">
      <label class="control-label pull-left"><?php echo smartyTranslate(array('s'=>'Reset to default','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
      <a href="#" class="btn btn-default btn-sm reset-layouts"><?php echo smartyTranslate(array('s'=>'Reset','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
      <p class="desc"><small><?php echo smartyTranslate(array('s'=>'Remove all presets that you have and install default presets','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</small>
      </p>
    </div>
  </div>
</div><?php }} ?>
