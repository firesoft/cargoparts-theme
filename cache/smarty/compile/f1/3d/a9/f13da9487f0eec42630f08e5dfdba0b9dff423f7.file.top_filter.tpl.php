<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:43
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmadvancedfilter\\views\templates\hook\top_filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:819359b7e5f74fe4c9-36385428%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f13da9487f0eec42630f08e5dfdba0b9dff423f7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmadvancedfilter\\\\views\\templates\\hook\\top_filter.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '819359b7e5f74fe4c9-36385428',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
    'page_name' => 0,
    'result' => 0,
    'filter_parameters' => 0,
    'data' => 0,
    'parameter' => 0,
    'field' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f75bf9d0_52045022',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f75bf9d0_52045022')) {function content_59b7e5f75bf9d0_52045022($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['content']->value['indexed']) {?>
  <form action="#" method="post" class="clearfix">
    <div id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter" class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter tmadvancedfilter">
      <div class="container">
        <div class="filter-body">
          <?php if ($_smarty_tpl->tpl_vars['content']->value['filter_name']||$_smarty_tpl->tpl_vars['content']->value['filter_description']) {?>
            <div class="filter-info">
              <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>
                <?php if ($_smarty_tpl->tpl_vars['content']->value['filter_name']) {?><h3><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['filter_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h3><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['content']->value['filter_description']) {?><div class="filter-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['content']->value['filter_description']);?>
</div><?php }?>
              <?php } else { ?>
                <h3><?php echo smartyTranslate(array('s'=>'[1]Advanced[/1] filter','mod'=>'tmadvancedfilter','tags'=>array('<strong>')),$_smarty_tpl);?>
</h3>
              <?php }?>
            </div>
          <?php }?>
          <div class="clearfix">
            <div class="filter-content">
              <div class="content">
                <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['content']->value['filter']);?>

              </div>
              <div class="filter-btn">
                <button name="submitTopTmAdvancedFilter" type="submit" class="btn btn-primary btn-md result"><?php echo smartyTranslate(array('s'=>'Show','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
</button>
                <p><span class="count"><?php if (isset($_smarty_tpl->tpl_vars['result']->value)&&$_smarty_tpl->tpl_vars['result']->value) {?> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['result']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?></span> <?php echo smartyTranslate(array('s'=>'Products','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
</p>
              </div>
            </div>
            <div class="filter-navigation">
              <?php if (isset($_smarty_tpl->tpl_vars['filter_parameters']->value)&&$_smarty_tpl->tpl_vars['filter_parameters']->value) {?>
                <ul id="filter-selected-parameters" class="clearfix">
                  <?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['data']->_loop = false;
 $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['filter_parameters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
 $_smarty_tpl->tpl_vars['field']->value = $_smarty_tpl->tpl_vars['data']->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['data']->value) {?>
                      <?php  $_smarty_tpl->tpl_vars['parameter'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['parameter']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['parameter']->key => $_smarty_tpl->tpl_vars['parameter']->value) {
$_smarty_tpl->tpl_vars['parameter']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['parameter']->value['field_type']) {?>
                          <li data-filter-field="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-filter-field-type="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['field_type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['parameter']->value['id'])&&$_smarty_tpl->tpl_vars['parameter']->value['id']) {?>data-filter-field-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['id'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?>><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['parameter']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<i></i></li>
                        <?php }?>
                      <?php } ?>
                    <?php }?>
                  <?php } ?>
                </ul>
              <?php }?>
              <?php if ($_smarty_tpl->tpl_vars['page_name']->value!='index') {?>
                <span id="adv-filter" class="btn btn-md btn-primary">Advanced Search</span>
              <?php }?>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="id_filter" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_filter'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
      <input type="hidden" name="type" value="top" />
    </div>
  </form>
<?php }?><?php }} ?>
