<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:42
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmadvancedfilter\\views\templates\hook\_elements\range-input.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2023559b7e5f6efda13-79102452%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1830fca4637ed514a95e3329f5b6f82b59e0e319' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmadvancedfilter\\\\views\\templates\\hook\\_elements\\range-input.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2023559b7e5f6efda13-79102452',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'type' => 0,
    'filter_item' => 0,
    'from' => 0,
    'currency_sign' => 0,
    'to' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f70244e5_30298119',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f70244e5_30298119')) {function content_59b7e5f70244e5_30298119($_smarty_tpl) {?>


<div class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter-row">
  <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['name']) {?>
    <label class="parameter-name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

      <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['label']) {?><small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['label'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small><?php }?>
    </label><?php }?>
  <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['description']) {?><div class="parameter-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['filter_item']->value['description']);?>
</div><?php }?>
  <div class="select-range">
    <?php $_smarty_tpl->tpl_vars['from'] = new Smarty_variable(mb_convert_encoding(htmlspecialchars(Tmadvancedfilter::getActiveRange($_smarty_tpl->tpl_vars['filter_item']->value['type'],$_smarty_tpl->tpl_vars['filter_item']->value['id_item'],'from'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['to'] = new Smarty_variable(mb_convert_encoding(htmlspecialchars(Tmadvancedfilter::getActiveRange($_smarty_tpl->tpl_vars['filter_item']->value['type'],$_smarty_tpl->tpl_vars['filter_item']->value['id_item'],'to'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'), null, 0);?>
    <div class="form-group">
      <input
              class="form-control"
              data-type="price"
              data-element-type="input"
              data-type-value="from"
              name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
[]"
              value="<?php if ($_smarty_tpl->tpl_vars['from']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['from']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"
              autocomplete="off"
              placeholder="<?php echo smartyTranslate(array('s'=>'from','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
 (<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency_sign']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
)"/>
    </div>
    <div class="form-group">
      <input
              class="form-control"
              data-type="price"
              data-element-type="input"
              data-type-value="to"
              name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
[]"
              value="<?php if ($_smarty_tpl->tpl_vars['to']->value) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['to']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"
              autocomplete="off"
              placeholder="<?php echo smartyTranslate(array('s'=>'to','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
 (<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['currency_sign']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
)"/>
    </div>
  </div>
</div><?php }} ?>
