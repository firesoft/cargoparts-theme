<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:34
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\layouts\col.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1126259b7f7fa444063-19395012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '95f0c6afad164a4c2a5c2bad40d35af49f4af6e7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\layouts\\col.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1126259b7f7fa444063-19395012',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'class' => 0,
    'elem' => 0,
    'preview' => 0,
    'position' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fa47b2a4_95098268',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fa47b2a4_95098268')) {function content_59b7f7fa47b2a4_95098268($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.replace.php';
?>

<div class="col sortable <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['class']->value, ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>data-type="col" data-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_item'], ENT_QUOTES, 'UTF-8', true);?>
" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_parent'], ENT_QUOTES, 'UTF-8', true);?>
" data-sort-order="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
" data-specific-class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
" data-col-lg="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['col_lg'], ENT_QUOTES, 'UTF-8', true);?>
" data-col-md="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['col_md'], ENT_QUOTES, 'UTF-8', true);?>
" data-col-sm="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['col_sm'], ENT_QUOTES, 'UTF-8', true);?>
" data-col-xs="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['col_xs'], ENT_QUOTES, 'UTF-8', true);?>
" data-id-unique="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
  <article class="col-inner clearfix inner">
    <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>
      <div class="button-container clearfix">
        <span class="element-name"><?php echo smartyTranslate(array('s'=>'Column','mod'=>'tmmegalayout'),$_smarty_tpl);?>

          <span class="sort-order"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
</span> <span class="identificator"><?php if ($_smarty_tpl->tpl_vars['elem']->value['specific_class']) {?>(<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['elem']->value['specific_class'],' ',' | ');?>
)<?php }?></span></span>
        <div class="dropdown button-container pull-right">
          <a href="#" id="dropdownMenu-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" class="dropdown-toggle" aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" type="button"></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
">
            <li><a href="#" class="add-row">+ <?php echo smartyTranslate(array('s'=>'Add row','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li><a href="#" class="add-module">+ <?php echo smartyTranslate(array('s'=>'Add Module','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li class="divider" role="separator"></li>
            <li><a href="#" class="edit-column"><?php echo smartyTranslate(array('s'=>'Edit column','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li><a href="#" class="edit-styles"><?php echo smartyTranslate(array('s'=>'Stylize','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <li><a href="#" class="remove-item"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
          </ul>
        </div>
      </div>
    <?php }?>
    <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['position']->value);?>

  </article>
</div><?php }} ?>
