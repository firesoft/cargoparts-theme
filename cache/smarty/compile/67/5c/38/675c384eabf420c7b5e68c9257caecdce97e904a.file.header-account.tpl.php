<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:41
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmheaderaccount\views\templates\hook\header-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2942359b7e5f58dcd67-11518272%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '675c384eabf420c7b5e68c9257caecdce97e904a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmheaderaccount\\views\\templates\\hook\\header-account.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2942359b7e5f58dcd67-11518272',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'f_status' => 0,
    'g_status' => 0,
    'vk_status' => 0,
    'back' => 0,
    'back_page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f5960572_49855836',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f5960572_49855836')) {function content_59b7e5f5960572_49855836($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['back_page'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'), ENT_QUOTES, 'UTF-8', true), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>
  <div class="social-login-buttons">
    <?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-facebook" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array(),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your Facebook Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Facebook','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['f_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-google" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your Google Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Google','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['f_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['f_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-vk" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your VK Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'VK','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
  </div>
<?php }?><?php }} ?>
