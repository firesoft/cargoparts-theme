<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:04:03
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmanufacturerblock\views\templates\hook\tmmanufacturerblock.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1728459b7f763b00a33-16489107%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fcde05d820522229b13dc5727b9f8943498e10ab' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmanufacturerblock\\views\\templates\\hook\\tmmanufacturerblock.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1728459b7f763b00a33-16489107',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'manufacturers' => 0,
    'display_caroucel' => 0,
    'nb_display' => 0,
    'display_name' => 0,
    'manufacturer' => 0,
    'link' => 0,
    'display_image' => 0,
    'img_manu_dir' => 0,
    'image_type' => 0,
    'caroucel_nb' => 0,
    'slide_width' => 0,
    'slide_margin' => 0,
    'caroucel_item_scroll' => 0,
    'caroucel_auto' => 0,
    'caroucel_speed' => 0,
    'caroucel_auto_pause' => 0,
    'caroucel_random' => 0,
    'caroucel_loop' => 0,
    'caroucel_hide_controll' => 0,
    'caroucel_pager' => 0,
    'caroucel_control' => 0,
    'caroucel_auto_control' => 0,
    'caroucel_auto_hover' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f763b42e24_49263605',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f763b42e24_49263605')) {function content_59b7f763b42e24_49263605($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['manufacturers']->value) {?>
  <div id="tm_manufacturers_block" class="clearfix">
    <div class="head clearfix">
      <h3 class="head-block pull-left"><?php echo smartyTranslate(array('s'=>'[1]Top[/1] brands','mod'=>'tmmanufacturerblock','tags'=>array('<strong>')),$_smarty_tpl);?>
</h3>
      <a href="index.php?controller=manufacturer" class="link pull-right hidden-xs"><?php echo smartyTranslate(array('s'=>'see all brands','mod'=>'tmmanufacturerblock'),$_smarty_tpl);?>
</a>
    </div>
    <ul class="manufacturers_items<?php if ($_smarty_tpl->tpl_vars['display_caroucel']->value) {?> clearfix<?php }?>">
      <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['manufacturers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['manufacturers']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value) {
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['manufacturers']['iteration']++;
?>
        <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['manufacturers']['iteration']<=$_smarty_tpl->tpl_vars['nb_display']->value) {?>
          <li class="manufacturer_item<?php if (!$_smarty_tpl->tpl_vars['display_caroucel']->value) {?> col-xs-4 col-sm-3 col-md-2<?php } else { ?> caroucel_item<?php }?>">
            <?php if (isset($_smarty_tpl->tpl_vars['display_name']->value)&&$_smarty_tpl->tpl_vars['display_name']->value) {?>
              <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getmanufacturerLink($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'],$_smarty_tpl->tpl_vars['manufacturer']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'More about %s','sprintf'=>array($_smarty_tpl->tpl_vars['manufacturer']->value['name']),'mod'=>'tmmanufacturerblock'),$_smarty_tpl);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</a>
            <?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['display_image']->value)&&$_smarty_tpl->tpl_vars['display_image']->value) {?>
              <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getmanufacturerLink($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'],$_smarty_tpl->tpl_vars['manufacturer']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo smartyTranslate(array('s'=>'More about %s','sprintf'=>array($_smarty_tpl->tpl_vars['manufacturer']->value['name']),'mod'=>'tmmanufacturerblock'),$_smarty_tpl);?>
">
                <img class="img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_manu_dir']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['image'], ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image_type']->value, ENT_QUOTES, 'UTF-8', true);?>
.jpg" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['image'], ENT_QUOTES, 'UTF-8', true);?>
"/>
              </a>
            <?php }?>
          </li>
        <?php }?>
      <?php } ?>
    </ul>
  </div>
  <script type="text/javascript">
    var m_display_caroucel = <?php echo intval($_smarty_tpl->tpl_vars['display_caroucel']->value);?>
;
  </script>
  <?php if ($_smarty_tpl->tpl_vars['display_caroucel']->value) {?>
    <script type="text/javascript">
      var m_caroucel_nb = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_nb']->value);?>
;
      var m_caroucel_slide_width = <?php echo intval($_smarty_tpl->tpl_vars['slide_width']->value);?>
;
      var m_caroucel_slide_margin = <?php echo intval($_smarty_tpl->tpl_vars['slide_margin']->value);?>
;
      var m_caroucel_item_scroll = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_item_scroll']->value);?>
;
      var m_caroucel_auto = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_auto']->value);?>
;
      var m_caroucel_speed = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_speed']->value);?>
;
      var m_caroucel_auto_pause = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_auto_pause']->value);?>
;
      var m_caroucel_random = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_random']->value);?>
;
      var m_caroucel_loop = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_loop']->value);?>
;
      var m_caroucel_hide_controll = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_hide_controll']->value);?>
;
      var m_caroucel_pager = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_pager']->value);?>
;
      var m_caroucel_control = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_control']->value);?>
;
      var m_caroucel_auto_control = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_auto_control']->value);?>
;
      var m_caroucel_auto_hover = <?php echo intval($_smarty_tpl->tpl_vars['caroucel_auto_hover']->value);?>
;
    </script>
  <?php }?>
<?php }?><?php }} ?>
