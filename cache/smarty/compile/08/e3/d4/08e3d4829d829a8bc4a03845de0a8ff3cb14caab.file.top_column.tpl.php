<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:48:15
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmoneclickorder\views\templates\admin\top_column.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1428959b7e59f54ce69-96887413%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08e3d4829d829a8bc4a03845de0a8ff3cb14caab' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmoneclickorder\\views\\templates\\admin\\top_column.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1428959b7e59f54ce69-96887413',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orders' => 0,
    'module_tab_link' => 0,
    'order' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e59f5973f0_38587138',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e59f5973f0_38587138')) {function content_59b7e59f5973f0_38587138($_smarty_tpl) {?>
<li id="one_click_order" class="dropdown" data-type="order">
  <a href="javascript:void(0);" class="dropdown-toggle notifs" data-toggle="dropdown">
    <i class="icon-book"></i>
    <?php if (count($_smarty_tpl->tpl_vars['orders']->value)!=0) {?>
      <span class="notifs_badge">
								<span><?php echo mb_convert_encoding(htmlspecialchars(count($_smarty_tpl->tpl_vars['orders']->value), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
							</span>
    <?php }?>
  </a>
  <div class="dropdown-menu notifs_dropdown">
    <section class="notifs_panel">
      <div class="notifs_panel_header">
        <h3><?php echo smartyTranslate(array('s'=>'Latest Quick Orders','mod'=>'tmoneclickorder'),$_smarty_tpl);?>
</h3>
      </div>
      <div class="list_notif">
        <?php if ($_smarty_tpl->tpl_vars['orders']->value) {?>
          <?php  $_smarty_tpl->tpl_vars['order'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['order']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['orders']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->key => $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['orders']['iteration']++;
?>
            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['orders']['iteration']<=3) {?>
              <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_tab_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&id_order=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['id_order'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&status=new">
                <p><?php echo smartyTranslate(array('s'=>'Order number','mod'=>'tmoneclickorder'),$_smarty_tpl);?>

                  <strong>#<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['id_order'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</strong></p>
                <p class="pull-right"><?php echo smartyTranslate(array('s'=>'Total','mod'=>'tmoneclickorder'),$_smarty_tpl);?>

                  <span class="total badge badge-success"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>Cart::getTotalCart(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['id_cart'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'))),$_smarty_tpl);?>
</span>
                </p>
                  <?php if ($_smarty_tpl->tpl_vars['order']->value['customer']['name']) {?>
                    <p><?php echo smartyTranslate(array('s'=>'From','mod'=>'tmoneclickorder'),$_smarty_tpl);?>
 <strong><?php echo $_smarty_tpl->tpl_vars['order']->value['customer']['name'];?>
</strong></p>
                  <?php }?>
                <small class="text-muted"><i class="icon-time"></i> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['date_add'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
              </a>
            <?php }?>
          <?php } ?>
        <?php } else { ?>
          <span class="no_notifs">
                    <?php echo smartyTranslate(array('s'=>'No new orders have been placed on your shop.','mod'=>'tmoneclickorder'),$_smarty_tpl);?>

                </span>
        <?php }?>
      </div>
      <div class="notifs_panel_footer">
        <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_tab_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'Show all orders','mod'=>'tmoneclickorder'),$_smarty_tpl);?>
</a>
      </div>
    </section>
  </div>

</li>

<script>
  $(document).ready(function() {
    $('#one_click_order').insertAfter($('#orders_notif'));
  });
</script>

<?php }} ?>
