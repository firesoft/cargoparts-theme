<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:27
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmdaydeal\views\templates\hook\tmdaydeal-home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2289259b7e5e7c6b996-50845773%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '412825c03905a0b58f5fc299ee60a2d192713b88' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmdaydeal\\views\\templates\\hook\\tmdaydeal-home.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2289259b7e5e7c6b996-50845773',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'daydeal_products' => 0,
    'product' => 0,
    'link' => 0,
    'daydeal_products_extra' => 0,
    'product_link' => 0,
    'id_image' => 0,
    'label' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'product_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e7db6ab1_88156239',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e7db6ab1_88156239')) {function content_59b7e5e7db6ab1_88156239($_smarty_tpl) {?>
<div class="clearfix"></div>
<div id="daydeal-products" class="block products_block">
	<h4 class="title_block"><?php echo smartyTranslate(array('s'=>'Deal of the day','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</h4>
	<div class="block_content">
        <?php if (isset($_smarty_tpl->tpl_vars['daydeal_products']->value)&&$_smarty_tpl->tpl_vars['daydeal_products']->value) {?>
		<ul class="products clearfix row">
        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['daydeal_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
        
            <?php $_smarty_tpl->tpl_vars['id_image'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value->getCover($_smarty_tpl->tpl_vars['product']->value->id), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['product_link'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['product']->value), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['product_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value->id, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['label'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value[$_smarty_tpl->tpl_vars['product']->value->id]["label"], null, 0);?>
            <?php $_smarty_tpl->tpl_vars['data_start'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value[$_smarty_tpl->tpl_vars['product']->value->id]["data_start"], null, 0);?>
            <?php $_smarty_tpl->tpl_vars['data_end'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value[$_smarty_tpl->tpl_vars['product']->value->id]["data_end"], null, 0);?>
            <?php $_smarty_tpl->tpl_vars['reduction_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value[$_smarty_tpl->tpl_vars['product']->value->id]["reduction_type"], null, 0);?>
            <?php $_smarty_tpl->tpl_vars['discount_price'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value[$_smarty_tpl->tpl_vars['product']->value->id]["discount_price"], null, 0);?>

            <li class="col-xs-12 col-sm-4 col-md-3">
                <a class="product_img_link" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                	<img class="img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite,$_smarty_tpl->tpl_vars['id_image']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
                    <?php if (isset($_smarty_tpl->tpl_vars['label']->value)&&$_smarty_tpl->tpl_vars['label']->value) {?>
                        <span class="label-daydeal"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['label']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
                    <?php }?>
                </a>
                <h5>
               		<a class="product-name" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</a>
                </h5>
                <?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&$_smarty_tpl->tpl_vars['product']->value->show_price&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value))) {?>
                   <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl);?>

                    <span class="price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value->price),$_smarty_tpl);?>
</span>
     
					<?php if ($_smarty_tpl->tpl_vars['product']->value->specificPrice['reduction']>0&&$_smarty_tpl->tpl_vars['product']->value->specificPrice['reduction']) {?>
                    	<span class="old-price product-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value->getPriceWithoutReduct(false,@constant('NULL'))),$_smarty_tpl);?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value->specificPrice['reduction_type']=='percentage') {?>
                            <span class="price-percent-reduction">-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->specificPrice['reduction'], ENT_QUOTES, 'UTF-8', true)*100;?>
%</span>
                        <?php }?>
                    <?php }?>
                <?php }?>
                <p class="product-description"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['product']->value->description_short),75,'...');?>
</p>
                <div class="clearfix">
                    <?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
                        <?php if ($_smarty_tpl->tpl_vars['product']->value->quantity>0) {?>
                            <a class="button ajax_add_to_cart_button btn btn-default" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,''), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['product_id']->value);?>
" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmdaydeal'),$_smarty_tpl);?>
">
                                <span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</span>
                            </a>
                        <?php } else { ?>
                        <span class="button ajax_add_to_cart_button btn btn-default disabled">
                            <span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</span>
                        </span>
                        <?php }?>
                    <?php }?>
                    <a class="button lnk_view btn btn-default" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product_link']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'View','mod'=>'tmdaydeal'),$_smarty_tpl);?>
">
                        <span><?php echo smartyTranslate(array('s'=>'View','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</span>
                    </a>
                </div>
            </li>
        <?php } ?>
        </ul>
        <?php } else { ?>
                <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'No special products at this time.','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</p>
        <?php }?>
	</div>
</div>

<?php }} ?>
