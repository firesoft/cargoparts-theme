<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:36
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\views\templates\admin\tmmegalayout-layout-buttons.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1448859b7f7fcc1cc12-61651459%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e71f685e0d765bacde787eaa113935c86e694b96' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\views\\templates\\admin\\tmmegalayout-layout-buttons.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1448859b7f7fcc1cc12-61651459',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
    'item_status' => 0,
    'page_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fcc425a8_25456078',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fcc425a8_25456078')) {function content_59b7f7fcc425a8_25456078($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['content']->value['layout'])) {?>
  <p <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layout'])) {?>data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?> class="tmlist-layout-btns pull-left">
    <a data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" href="#" class="<?php if ($_smarty_tpl->tpl_vars['content']->value['status']||$_smarty_tpl->tpl_vars['content']->value['partly_use']) {?>hidden<?php }?> layout-btn use-layout"><i class="process-icon-toggle-off"></i> <?php echo smartyTranslate(array('s'=>'Use as default','mod'=>'tmmegalayout'),$_smarty_tpl);?>

    </a>
    <a data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" href="#" class="<?php if (!$_smarty_tpl->tpl_vars['content']->value['status']&&!$_smarty_tpl->tpl_vars['content']->value['partly_use']) {?>hidden<?php }?> layout-btn disable-layout"><i class="process-icon-toggle-on"></i> <?php echo smartyTranslate(array('s'=>'Use as default','mod'=>'tmmegalayout'),$_smarty_tpl);?>

    </a>
  </p>
  <?php if (Tmmegalayout::displayAllPagesHook($_smarty_tpl->tpl_vars['content']->value['hook_name'])) {?>
    <select class="tmmegalayout-availible-pages" multiple="multiple" name="tmmegalayout-availible-pages">
      <?php  $_smarty_tpl->tpl_vars['item_status'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_status']->_loop = false;
 $_smarty_tpl->tpl_vars['page_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['content']->value['pages_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_status']->key => $_smarty_tpl->tpl_vars['item_status']->value) {
$_smarty_tpl->tpl_vars['item_status']->_loop = true;
 $_smarty_tpl->tpl_vars['page_name']->value = $_smarty_tpl->tpl_vars['item_status']->key;
?>
        <option <?php if ($_smarty_tpl->tpl_vars['item_status']->value) {?>selected="selected"<?php }?> value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['page_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['page_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</option>
      <?php } ?>
    </select>
  <?php }?>
<?php }?><?php }} ?>
