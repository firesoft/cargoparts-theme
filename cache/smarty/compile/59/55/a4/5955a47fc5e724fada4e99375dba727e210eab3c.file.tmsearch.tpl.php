<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:24
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmsearch\views\templates\hook\tmsearch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:714259b7e5e4c68e05-28802013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5955a47fc5e724fada4e99375dba727e210eab3c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmsearch\\views\\templates\\hook\\tmsearch.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '714259b7e5e4c68e05-28802013',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search_categories' => 0,
    'active_category' => 0,
    'category' => 0,
    'search_query' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e4cef018_67103228',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e4cef018_67103228')) {function content_59b7e5e4cef018_67103228($_smarty_tpl) {?><div id="tmsearch">
  <span id="search-toggle"></span>
  <form id="tmsearchbox" method="get" action="<?php echo mb_convert_encoding(htmlspecialchars(Tmsearch::getTMSearchLink('tmsearch'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
    <?php if (!Configuration::get('PS_REWRITING_SETTINGS')) {?>
      <input type="hidden" name="fc" value="module"/>
      <input type="hidden" name="controller" value="tmsearch"/>
      <input type="hidden" name="module" value="tmsearch"/>
    <?php }?>
    <input type="hidden" name="orderby" value="position"/>
    <input type="hidden" name="orderway" value="desc"/>
    <select name="search_categories" class="form-control">
      <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['search_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
        <option <?php if ($_smarty_tpl->tpl_vars['active_category']->value==$_smarty_tpl->tpl_vars['category']->value['id']) {?>selected="selected"<?php }?> value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['id'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['category']->value['id']==2) {?><?php echo smartyTranslate(array('s'=>'All Categories','mod'=>'tmsearch'),$_smarty_tpl);?>
<?php } else { ?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?></option>
      <?php } ?>
    </select>
    <input class="tm_search_query form-control" type="text" id="tm_search_query" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'tmsearch'),$_smarty_tpl);?>
" value="<?php echo stripslashes(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['search_query']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'));?>
"/>
    <button type="submit" name="tm_submit_search" class="button-search"></button>
  </form>
</div><?php }} ?>
