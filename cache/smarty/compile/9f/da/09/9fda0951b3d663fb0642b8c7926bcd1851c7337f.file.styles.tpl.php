<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:28:07
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\views\templates\admin\tools\styles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2644459b7fd0739bcd6-63224716%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fda0951b3d663fb0642b8c7926bcd1851c7337f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\views\\templates\\admin\\tools\\styles.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2644459b7fd0739bcd6-63224716',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'styles' => 0,
    'id_unique' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7fd07503811_25911746',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7fd07503811_25911746')) {function content_59b7fd07503811_25911746($_smarty_tpl) {?>

<div class="form-wrapper tmmegalayout-styles">
  <h2><?php echo smartyTranslate(array('s'=>'Style','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</h2>
  <ul class="nav nav-tabs">
    <li id="s-tab-1" class="active">
      <a class="layouts-tab" data-toggle="tab" href="#sitems-1"><?php echo smartyTranslate(array('s'=>'Background styles','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
    <li id="s-tab-2">
      <a class="layouts-tab" data-toggle="tab" href="#sitems-2"><?php echo smartyTranslate(array('s'=>'Border styles','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
    <li id="s-tab-3">
      <a class="layouts-tab" data-toggle="tab" href="#sitems-3"><?php echo smartyTranslate(array('s'=>'Other styles','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
  </ul>
  <div class="tab-content tmmegalayout-styles-content">
    <div id="sitems-1" class="tab-pane active">
      <div class="clearfix">
        <div class="col-sm-12 col-md-3 background-color-col">
          <div class="background-color-container">
            <label class="uppercase"><?php echo smartyTranslate(array('s'=>'color','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <div class="input-group">
              <input data-hex="true" data-type="clr" class="form-control color tmml_color_input" name="background-color" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_color'])&&$_smarty_tpl->tpl_vars['styles']->value['background_color']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['background_color'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-9">
          <div class="background-settings-col">
            <div class="row">
              <div class="col-xs-12 background-image-container col-md-8">
                <label class="uppercase"><?php echo smartyTranslate(array('s'=>'Image','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
                <div class="form-group">
                  <div class="input-group">
                    <input disabled="disabled" class="form-control" name="background-image" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_image'])&&$_smarty_tpl->tpl_vars['styles']->value['background_image']) {?><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['styles']->value['background_image']);?>
<?php }?>"/>
                    <span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                    <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                    <span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="flmbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="uppercase"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
                  <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="radio">
                        <label>
                          <input <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['styles']->value['background_repeat']=='no-repeat') {?>checked="checked"<?php }?> type="radio" name="background-repeat" value="no-repeat"/>
                          no-repeat
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="radio">
                        <label>
                          <input <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['styles']->value['background_repeat']=='repeat-x') {?>checked="checked"<?php }?> type="radio" name="background-repeat" value="repeat-x"/>
                          repeat-x
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="radio">
                        <label>
                          <input <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['styles']->value['background_repeat']=='repeat-y') {?>checked="checked"<?php }?> type="radio" name="background-repeat" value="repeat-y"/>
                          repeat-y
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-3">
                      <div class="radio">
                        <label>
                          <input <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['styles']->value['background_repeat']=='repeat') {?>checked="checked"<?php }?> type="radio" name="background-repeat" value="repeat"/>
                          repeat
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 background-settings-container col-md-4">
                <div class="form-group">
                  <label class="uppercase"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
                  <select name="background-position">
                    <option value=""></option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center">
                      center center
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top">
                      center top
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom">
                      center bottom
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top">
                      left top
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center">
                      left center
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom">
                      left bottom
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top">
                      right top
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center">
                      right center
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_position'])&&$_smarty_tpl->tpl_vars['styles']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom">
                      right bottom
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="uppercase"><?php echo smartyTranslate(array('s'=>'size','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
                  <select name="background-size">
                    <option value=""></option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_size'])&&$_smarty_tpl->tpl_vars['styles']->value['background_size']=='contain') {?>selected="selected"<?php }?> value="contain">
                      contain
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_size'])&&$_smarty_tpl->tpl_vars['styles']->value['background_size']=='cover') {?>selected="selected"<?php }?> value="cover">
                      cover
                    </option>
                    <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_size'])&&$_smarty_tpl->tpl_vars['styles']->value['background_size']=='auto') {?>selected="selected"<?php }?> value="auto">
                      auto
                    </option>
                  </select>
                </div>
                <label class="uppercase"><?php echo smartyTranslate(array('s'=>'origin','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
                <select name="background-origin">
                  <option value=""></option>
                  <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_origin'])&&$_smarty_tpl->tpl_vars['styles']->value['background_origin']=='border-box') {?>selected="selected"<?php }?> value="border-box">
                    border-box
                  </option>
                  <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_origin'])&&$_smarty_tpl->tpl_vars['styles']->value['background_origin']=='content-box') {?>selected="selected"<?php }?> value="content-box">
                    content-box
                  </option>
                  <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['background_origin'])&&$_smarty_tpl->tpl_vars['styles']->value['background_origin']=='padding-box') {?>selected="selected"<?php }?> value="padding-box">
                    padding-box
                  </option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="sitems-2" class="tab-pane">
      <div class="clearfix border-settings">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <label class="uppercase"><?php echo smartyTranslate(array('s'=>'type','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <select name="border-top-style">
              <option value=""></option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_style']=='none') {?>selected="selected"<?php }?> value="none">
                none
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed">
                dashed
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted">
                dotted
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double">
                double
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid">
                solid
              </option>
            </select>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <select name="border-right-style">
              <option value=""></option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_style']=='none') {?>selected="selected"<?php }?> value="none">
                none
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed">
                dashed
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted">
                dotted
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double">
                double
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid">
                solid
              </option>
            </select>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <select name="border-bottom-style">
              <option value=""></option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_style']=='none') {?>selected="selected"<?php }?> value="none">
                none
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed">
                dashed
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted">
                dotted
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double">
                double
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid">
                solid
              </option>
            </select>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <select name="border-left-style">
              <option value=""></option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_style']=='none') {?>selected="selected"<?php }?> value="none">
                none
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed">
                dashed
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted">
                dotted
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double">
                double
              </option>
              <option <?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid">
                solid
              </option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <label class="uppercase"><?php echo smartyTranslate(array('s'=>'width','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_width']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_width']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_width']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_width']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <label class="uppercase"><?php echo smartyTranslate(array('s'=>'radius','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_right_radius']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_right_radius']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_left_radius']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegalayout'),$_smarty_tpl);?>

              <small><i>(px, em)</i></small>
            </label>
            <input class="form-control" data-type="dmns" type="text" name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_left_radius']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
          <label class="uppercase"><?php echo smartyTranslate(array('s'=>'color','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <div class="input-group">
              <input data-hex="true" data-type="clr" class="form-control color tmml_color_input" name="border-top-color" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['styles']->value['border_top_color']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <div class="input-group">
              <input data-hex="true" data-type="clr" class="form-control color tmml_color_input" name="border-right-color" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['styles']->value['border_right_color']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <div class="input-group">
              <input data-hex="true" data-type="clr" class="form-control color tmml_color_input" name="border-bottom-color" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['styles']->value['border_bottom_color']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
            <div class="input-group">
              <input data-hex="true" data-type="clr" class="form-control color tmml_color_input" name="border-left-color" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['styles']->value['border_left_color']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="sitems-3" class="tab-pane">
      <div class="other-settings clearfix">
        <label class="uppercase"><?php echo smartyTranslate(array('s'=>'other','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
        <div class="row">
          <label class="col-xs-12 col-md-2 lh-fix"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</label>
          <div class="col-xs-12 col-md-6">
            <input class="form-control" data-type="shdw" type="text" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['styles']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['styles']->value['box_shadow']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['styles']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?>"/>
            <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example: 0px 0px 0px 0px rgba(0,0,0,0.75)','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="id_unique" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['id_unique']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
  <div class="tmmegalayout-style-btns">
    <a href="#" class="save-styles btn btn-success"><?php echo smartyTranslate(array('s'=>'Save','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
    <a href="#" class="clear-styles btn btn-link"><?php echo smartyTranslate(array('s'=>'Clear styles','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
  </div>
</div><?php }} ?>
