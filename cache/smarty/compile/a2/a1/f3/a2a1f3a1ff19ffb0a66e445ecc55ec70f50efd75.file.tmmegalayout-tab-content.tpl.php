<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:36
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\views\templates\admin\tmmegalayout-tab-content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:63559b7f7fc396676-75993263%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2a1f3a1ff19ffb0a66e445ecc55ec70f50efd75' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\views\\templates\\admin\\tmmegalayout-tab-content.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '63559b7f7fc396676-75993263',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
    'layout' => 0,
    'current_name' => 0,
    'block' => 0,
    'templates_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fc408aa9_56166533',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fc408aa9_56166533')) {function content_59b7f7fc408aa9_56166533($_smarty_tpl) {?>

<div class="tmmegalayout-lsettins panel clearfix">
  <div class="button-container">
    <a href="#" class="btn btn-success add_layout" data-hook-name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['hook_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">+ <?php echo smartyTranslate(array('s'=>'Add a Preset','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
  </div>
  <div class="tmlist-group-container dropdown">
    <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layouts_list'])&&$_smarty_tpl->tpl_vars['content']->value['layouts_list']) {?>
      <?php $_smarty_tpl->tpl_vars['current_name'] = new Smarty_variable('', null, 0);?>
      <?php  $_smarty_tpl->tpl_vars['layout'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['layout']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['content']->value['layouts_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['layout']->key => $_smarty_tpl->tpl_vars['layout']->value) {
$_smarty_tpl->tpl_vars['layout']->_loop = true;
?>
        <?php if ($_smarty_tpl->tpl_vars['layout']->value['status']==1) {?>
          <?php $_smarty_tpl->tpl_vars['current_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['layout']->value['layout_name'], null, 0);?>
        <?php }?>
      <?php } ?>
      <?php if (!$_smarty_tpl->tpl_vars['current_name']->value) {?>
        <?php $_smarty_tpl->tpl_vars['current_name'] = new Smarty_variable('--', null, 0);?>
      <?php }?>
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['current_name']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</button>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['block']->value)&&$_smarty_tpl->tpl_vars['block']->value) {?><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['block']->value);?>
<?php }?>
    <ul data-list-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['hook_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="tmlist-group tmml-layouts-list dropdown-menu" aria-labelledby="dropdownMenu">
      <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layouts_list'])&&$_smarty_tpl->tpl_vars['content']->value['layouts_list']) {?>
        <?php  $_smarty_tpl->tpl_vars['layout'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['layout']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['content']->value['layouts_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['layout']->key => $_smarty_tpl->tpl_vars['layout']->value) {
$_smarty_tpl->tpl_vars['layout']->_loop = true;
?>
          <li data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="tmlist-group-item <?php if ($_smarty_tpl->tpl_vars['layout']->value['status']==1) {?>active<?php }?>">
            <a href="#">
              <i class="icon-star <?php if (!$_smarty_tpl->tpl_vars['layout']->value['status']) {?> hidden<?php } else { ?> visible<?php }?>"></i>
              <i class="icon-star-half-empty <?php if (!Tmmegalayout::hasAssignedPages(mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8'))) {?>hidden<?php } else { ?>visible<?php }?>"></i>
              <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['layout_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

            </a>
          </li>
        <?php } ?>
      <?php }?>
    </ul>
  </div>
  <div class="tmlist-layout-buttons clearfix">
    <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layout'])) {?>
      <?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['templates_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp7=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp7."tmmegalayout-layout-buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>
  </div>
</div>
<div class="layout-container">
  <?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['templates_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp8=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ($_tmp8."tmmegalayout-layout-content.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>$_smarty_tpl->tpl_vars['content']->value), 0);?>

</div>
<input type="hidden" data-name="bgimg" id="flmbgimg" value=""/>
<input type="hidden" name="tmml_hook_name" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['hook_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/><?php }} ?>
