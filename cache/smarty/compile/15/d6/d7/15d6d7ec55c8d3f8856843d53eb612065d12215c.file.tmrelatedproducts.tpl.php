<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 14:00:55
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmrelatedproducts\views\templates\hook\tmrelatedproducts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:475059b9725735bac7-86983361%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15d6d7ec55c8d3f8856843d53eb612065d12215c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmrelatedproducts\\views\\templates\\hook\\tmrelatedproducts.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '475059b9725735bac7-86983361',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'related_products' => 0,
    'related' => 0,
    'restricted_country_mode' => 0,
    'link' => 0,
    'relatedLink' => 0,
    'PS_CATALOG_MODE' => 0,
    'static_token' => 0,
    'display_related_price' => 0,
    'priceDisplay' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b972576ac332_58052787',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b972576ac332_58052787')) {function content_59b972576ac332_58052787($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['related_products']->value)&&$_smarty_tpl->tpl_vars['related_products']->value) {?>
  <section class="page-product-box">
    <h3 class="head-block"><?php echo smartyTranslate(array('s'=>'[1]Related[/1] products','mod'=>'tmrelatedproducts','tags'=>array('<strong>')),$_smarty_tpl);?>
</h3>
    <div class="block products_block related-block clearfix">
      <div class="block_content">
        <ul id="tmrelatedproducts" class="clearfix">
          <?php  $_smarty_tpl->tpl_vars['related'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['related']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['related_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['related']->key => $_smarty_tpl->tpl_vars['related']->value) {
$_smarty_tpl->tpl_vars['related']->_loop = true;
?>
            <?php if (($_smarty_tpl->tpl_vars['related']->value['allow_oosp']||$_smarty_tpl->tpl_vars['related']->value['quantity_all_versions']>0||$_smarty_tpl->tpl_vars['related']->value['quantity']>0)&&$_smarty_tpl->tpl_vars['related']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
              <?php $_smarty_tpl->tpl_vars['relatedLink'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['related']->value['id_product'],$_smarty_tpl->tpl_vars['related']->value['link_rewrite'],$_smarty_tpl->tpl_vars['related']->value['category']), null, 0);?>
              <li itemscope itemtype="https://schema.org/Product" class="item product-box">
                <div class="product">
                  <div class="image-block">
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relatedLink']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related']->value['legend'], ENT_QUOTES, 'UTF-8', true);?>
" class="lnk_img product-image">
                      <img itemprop="image" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['related']->value['link_rewrite'],$_smarty_tpl->tpl_vars['related']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related']->value['legend'], ENT_QUOTES, 'UTF-8', true);?>
" />
                    </a>
                    <?php if (!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&($_smarty_tpl->tpl_vars['related']->value['allow_oosp']||$_smarty_tpl->tpl_vars['related']->value['quantity']>0)) {?>
                      <div class="buttons">
                        <a class="btn btn-md btn-primary ajax_add_to_cart_button" href="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['related']->value['id_product']);?>
<?php $_tmp3=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,"qty=1&amp;id_product=".$_tmp3."&amp;token=".((string)$_smarty_tpl->tpl_vars['static_token']->value)."&amp;add"), ENT_QUOTES, 'UTF-8', true);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['related']->value['id_product']);?>
" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmrelatedproducts'),$_smarty_tpl);?>
">
                          <span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmrelatedproducts'),$_smarty_tpl);?>
</span>
                        </a>
                      </div>
                    <?php }?>
                  </div>
                  <h5 itemprop="name" class="product-name">
                    <a itemprop="url" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['relatedLink']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['related']->value['name'], ENT_QUOTES, 'UTF-8', true);?>

                    </a>
                  </h5>
                  <?php if ($_smarty_tpl->tpl_vars['related']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&$_smarty_tpl->tpl_vars['display_related_price']->value) {?>
                    <p class="price_display">
                      <span class="price">
                        <?php if ($_smarty_tpl->tpl_vars['priceDisplay']->value!=1) {?>
                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['related']->value['price']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['related']->value['price_tax_exc']),$_smarty_tpl);?>

                        <?php }?>
                      </span>
                    </p>
                  <?php }?>
                </div>
              </li>
            <?php }?>
          <?php } ?>
        </ul>
      </div>
    </div>
  </section>
<?php }?><?php }} ?>
