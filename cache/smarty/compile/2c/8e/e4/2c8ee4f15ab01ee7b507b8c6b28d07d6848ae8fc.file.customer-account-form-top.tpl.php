<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:39
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmheaderaccount\views\templates\hook\customer-account-form-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:122959b7e5f36160c1-80234229%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c8ee4f15ab01ee7b507b8c6b28d07d6848ae8fc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmheaderaccount\\views\\templates\\hook\\customer-account-form-top.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '122959b7e5f36160c1-80234229',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'f_status' => 0,
    'g_status' => 0,
    'vk_status' => 0,
    'link' => 0,
    'back' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f36ccce9_81006267',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f36ccce9_81006267')) {function content_59b7e5f36ccce9_81006267($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>
  <div class="clearfix social-login-buttons">
    <?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-facebook" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array(),true);?>
" title="<?php echo smartyTranslate(array('s'=>'Register with Your Facebook Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Facebook','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['f_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-google" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array(),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Register with Your Google Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Google','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['f_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['f_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn-login-vk" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array(),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Register with Your VK Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'VK','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
  </div>
<?php }?>
<?php }} ?>
