<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 14:00:58
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmproductvideos\views\templates\hooks\tmproductvideos_tab_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1888159b9725ad0d427-35117859%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0b3b5109e7bb542143b8f12a03f0eb0d0e044e9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmproductvideos\\views\\templates\\hooks\\tmproductvideos_tab_content.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1888159b9725ad0d427-35117859',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'videos' => 0,
    'video' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b9725b284b77_80483996',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b9725b284b77_80483996')) {function content_59b9725b284b77_80483996($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['videos']->value)&&$_smarty_tpl->tpl_vars['videos']->value) {?>
  <h3 class="page-product-heading"><?php if (count($_smarty_tpl->tpl_vars['videos']->value)>1) {?><?php echo smartyTranslate(array('s'=>'Videos','mod'=>'tmproductvideos'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Video','mod'=>'tmproductvideos'),$_smarty_tpl);?>
<?php }?></h3>
  <div id="product-video-tab-content" class="product-video-tab-content tab-pane">
    <?php  $_smarty_tpl->tpl_vars['video'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['video']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['videos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['video']->key => $_smarty_tpl->tpl_vars['video']->value) {
$_smarty_tpl->tpl_vars['video']->_loop = true;
?>
      <?php if ($_smarty_tpl->tpl_vars['video']->value['type']=='youtube') {?>
        <div class="videowrapper">
          <iframe
            src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
?enablejsapi=1&version=3&html5=1&wmode=transparent&controls=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_controls'], ENT_QUOTES, 'UTF-8', true);?>
&autoplay=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_autoplay'], ENT_QUOTES, 'UTF-8', true);?>
&autohide=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_autohide'], ENT_QUOTES, 'UTF-8', true);?>
&disablekb=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_disablekb'], ENT_QUOTES, 'UTF-8', true);?>
&fs=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_fs'], ENT_QUOTES, 'UTF-8', true);?>
&iv_load_policy=<?php if ($_smarty_tpl->tpl_vars['settings']->value['yt_ilp']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_ilp'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>3<?php }?>&loop=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_loop'], ENT_QUOTES, 'UTF-8', true);?>
&showinfo=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['yt_info'], ENT_QUOTES, 'UTF-8', true);?>
&theme=<?php if ($_smarty_tpl->tpl_vars['settings']->value['yt_theme']==0) {?>dark<?php } else { ?>light<?php }?>"
            frameborder="0"
            wmode="Opaque">
          </iframe>
        </div>
      <?php } elseif ($_smarty_tpl->tpl_vars['video']->value['type']=='vimeo') {?>
        <div class='embed-container'>
          <iframe
            src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
?autoplay=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_autoplay'], ENT_QUOTES, 'UTF-8', true);?>
&autopause=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_autopause'], ENT_QUOTES, 'UTF-8', true);?>
&badge=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_badge'], ENT_QUOTES, 'UTF-8', true);?>
&byline=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_byline'], ENT_QUOTES, 'UTF-8', true);?>
&loop=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_loop'], ENT_QUOTES, 'UTF-8', true);?>
&portrait=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_portrait'], ENT_QUOTES, 'UTF-8', true);?>
&title=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['v_title'], ENT_QUOTES, 'UTF-8', true);?>
"
            frameborder="0"
            webkitAllowFullScreen
            mozallowfullscreen
            allowFullScreen>
          </iframe>
        </div>
      <?php } elseif ($_smarty_tpl->tpl_vars['video']->value['type']=='custom') {?>
        <video id="product_tab_video_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['id_video'], ENT_QUOTES, 'UTF-8', true);?>
" class="video-js vjs-default-skin" <?php if ($_smarty_tpl->tpl_vars['settings']->value['cv_controls']) {?>controls<?php }?><?php if ($_smarty_tpl->tpl_vars['settings']->value['cv_autoplay']) {?> autoplay<?php }?><?php if ($_smarty_tpl->tpl_vars['settings']->value['cv_loop']) {?> loop<?php }?> <?php if ($_smarty_tpl->tpl_vars['settings']->value['cv_preload']) {?> preload="auto"<?php }?> <?php if ($_smarty_tpl->tpl_vars['video']->value['cover_image']) {?>poster="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['cover_image'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?> data-setup="{}">
          <?php if ($_smarty_tpl->tpl_vars['video']->value['format']=='mp4'||$_smarty_tpl->tpl_vars['video']->value['format']=='webm'||$_smarty_tpl->tpl_vars['video']->value['format']=='ogg') {?>
            <source src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" type='video/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['format'], ENT_QUOTES, 'UTF-8', true);?>
' />
          <?php } else { ?>
            <source src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" type='video/mp4' />
          <?php }?>
          <p class="vjs-no-js"><?php echo smartyTranslate(array('s'=>'To view this video please enable JavaScript, and consider upgrading to a web browser that','mod'=>'tmproductvideos'),$_smarty_tpl);?>
 <a href="//videojs.com/html5-video-support/" target="_blank"><?php echo smartyTranslate(array('s'=>'supports HTML5 video','mod'=>'tmproductvideos'),$_smarty_tpl);?>
</a></p>
        </video>
        <script>
          // Once the video is ready
          _V_("product_tab_video_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['id_video'], ENT_QUOTES, 'UTF-8', true);?>
").ready(function(){
            // Store the video object
            var myPlayer = this;
            // Make up an aspect ratio
            var aspectRatio = 9/16;
            function resizeVideoJS(){
              var element = $(".product-information .tab-content");
              var width = element.width();
              myPlayer.width(width).height(width * aspectRatio);
            }
            // Initialize resizeVideoJS()
            resizeVideoJS();
            // Then on resize call resizeVideoJS()
            $(window).resize(resizeVideoJS);
          });
        </script>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['video']->value['name']) {?>
        <h4 class="video-name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</h4>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['video']->value['description']) {?>
        <p class="video-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['video']->value['description']);?>
</p>
      <?php }?>
    <?php } ?>
  </div>
<?php }?><?php }} ?>
