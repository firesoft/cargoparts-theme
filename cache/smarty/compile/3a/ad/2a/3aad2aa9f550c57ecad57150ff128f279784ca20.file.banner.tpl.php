<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:32
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmosaicproducts\views\templates\hook\layouts\_partials\banner.tpl" */ ?>
<?php /*%%SmartyHeaderCode:305359b7e5ec826350-48100383%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3aad2aa9f550c57ecad57150ff128f279784ca20' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmosaicproducts\\views\\templates\\hook\\layouts\\_partials\\banner.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '305359b7e5ec826350-48100383',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
    'banner' => 0,
    'tmmp_image_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5ec85e1e5_43748106',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5ec85e1e5_43748106')) {function content_59b7e5ec85e1e5_43748106($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['banner'] = new Smarty_variable($_smarty_tpl->tpl_vars['data']->value, null, 0);?>
<div class="tmmp-frontend-banner tmmp-frontend-banner-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->id, ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['banner']->value->specific_class) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->specific_class, ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
  <?php if ($_smarty_tpl->tpl_vars['banner']->value->url) {?>
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->url, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->title, ENT_QUOTES, 'UTF-8', true);?>
">
  <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['banner']->value->image_path) {?>
      <img class="img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tmmp_image_path']->value, ENT_QUOTES, 'UTF-8', true);?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->image_path, ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->title, ENT_QUOTES, 'UTF-8', true);?>
" />
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['banner']->value->description) {?>
      <div class="tmmp-banner-description">
        <?php echo $_smarty_tpl->tpl_vars['banner']->value->description;?>

      </div>
    <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['banner']->value->url) {?>
    </a>
  <?php }?>
</div><?php }} ?>
