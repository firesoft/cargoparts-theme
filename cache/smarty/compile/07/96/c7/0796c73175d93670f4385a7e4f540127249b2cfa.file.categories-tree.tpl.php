<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:25
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegamenu\views\templates\hook\items\categories-tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1881359b7e5e5539031-63558496%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0796c73175d93670f4385a7e4f540127249b2cfa' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegamenu\\views\\templates\\hook\\items\\categories-tree.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1881359b7e5e5539031-63558496',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tree' => 0,
    'page_name' => 0,
    'id_selected' => 0,
    'branch' => 0,
    'link' => 0,
    'url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e55aac62_23653776',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e55aac62_23653776')) {function content_59b7e5e55aac62_23653776($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.escape.php';
?>

<?php if (isset($_smarty_tpl->tpl_vars['tree']->value)&&$_smarty_tpl->tpl_vars['tree']->value) {?>
  <?php  $_smarty_tpl->tpl_vars['branch'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['branch']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tree']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['branch']->key => $_smarty_tpl->tpl_vars['branch']->value) {
$_smarty_tpl->tpl_vars['branch']->_loop = true;
?>
    <li class="category<?php if ($_smarty_tpl->tpl_vars['page_name']->value=='category'&&$_smarty_tpl->tpl_vars['id_selected']->value==$_smarty_tpl->tpl_vars['branch']->value['id_category']) {?> sfHoverForce<?php }?>">
      <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'), null, 0);?>
      <?php if ($_smarty_tpl->tpl_vars['branch']->value['level_depth']>1) {?>
        <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['branch']->value['id_category']), null, 0);?>
      <?php }?>
      <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['url']->value, 'quote', 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['branch']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['branch']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</a>
      <?php if (isset($_smarty_tpl->tpl_vars['branch']->value['children'])&&$_smarty_tpl->tpl_vars['branch']->value['children']) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('./categories-tree-branch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['branch']->value['children'],'id_selected'=>$_smarty_tpl->tpl_vars['id_selected']->value), 0);?>

      <?php }?>
    </li>
  <?php } ?>
<?php }?><?php }} ?>
