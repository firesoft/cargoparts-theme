<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:42
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmadvancedfilter\\views\templates\hook\_elements\select.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2837859b7e5f66db918-81725055%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6de3756341c9738807c4b12cddbf2a7c8404a697' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmadvancedfilter\\\\views\\templates\\hook\\_elements\\select.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2837859b7e5f66db918-81725055',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'type' => 0,
    'filter_item' => 0,
    'active_item' => 0,
    'items' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f6732e53_68468704',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f6732e53_68468704')) {function content_59b7e5f6732e53_68468704($_smarty_tpl) {?>


<div class="form-group <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter-row">
  <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['name']) {?>
    <label class="parameter-name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

      <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['label']) {?>
        <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['label'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
      <?php }?>
    </label>
  <?php }?>
  <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['description']) {?>
    <div class="parameter-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['filter_item']->value['description']);?>
</div>
  <?php }?>
  <select
          id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
          <?php if (!$_smarty_tpl->tpl_vars['active_item']->value) {?>disabled="disabled"<?php }?>
          class="form-control"
          name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
          autocomplete="off">
    <option value="0">--</option>
    <?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <option
          <?php if ($_smarty_tpl->tpl_vars['active_item']->value&&$_smarty_tpl->tpl_vars['item']->value['element']->is_checked) {?>selected="selected"<?php }?>
          value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
          <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

          <?php if (isset($_smarty_tpl->tpl_vars['item']->value['element']->possible_values)) {?>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['element']->possible_values) {?>
              (<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->possible_values, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
)
            <?php } else { ?>
              (0)
            <?php }?>
          <?php }?>
        </option>
      <?php } ?>
    <?php }?>
  </select>
</div><?php }} ?>
