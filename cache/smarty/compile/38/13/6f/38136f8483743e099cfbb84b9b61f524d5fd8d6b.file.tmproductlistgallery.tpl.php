<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:30
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmproductlistgallery\views\templates\hook\tmproductlistgallery.tpl" */ ?>
<?php /*%%SmartyHeaderCode:828059b7e5ea65f159-25434494%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '38136f8483743e099cfbb84b9b61f524d5fd8d6b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmproductlistgallery\\views\\templates\\hook\\tmproductlistgallery.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '828059b7e5ea65f159-25434494',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_images' => 0,
    'smarty_settings' => 0,
    'homeSize' => 0,
    'product' => 0,
    'image' => 0,
    'imageId' => 0,
    'link' => 0,
    'imageTitle' => 0,
    'imageVisible' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5ea89a102_63390812',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5ea89a102_63390812')) {function content_59b7e5ea89a102_63390812($_smarty_tpl) {?>


<?php if (count($_smarty_tpl->tpl_vars['product_images']->value)>1) {?>
<?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow') {?>
  <div class="slideshow-wrap" style="max-width: <?php echo $_smarty_tpl->tpl_vars['homeSize']->value['width'];?>
px; position: relative; margin: 0 auto;">
  <span style="width: 100%; display: block; padding-top:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['homeSize']->value['height'];?>
<?php $_tmp10=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['homeSize']->value['width'];?>
<?php $_tmp11=ob_get_clean();?><?php echo $_tmp10/$_tmp11*100/2;?>
%; padding-bottom:<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['homeSize']->value['height'];?>
<?php $_tmp12=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['homeSize']->value['width'];?>
<?php $_tmp13=ob_get_clean();?><?php echo $_tmp12/$_tmp13*100/2;?>
%;"></span>
<?php }?>
  <div class="tmproductlistgallery <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow'&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails']&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_position']=='vertical') {?> vertical-thumbnails<?php }?><?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow'&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_pager']) {?> slideshow-dots<?php }?>">
    <div class="tmproductlistgallery-images <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['smarty_settings']->value['st_roll_type'], ENT_QUOTES, 'UTF-8', true);?>
">
      <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']++;
?>
        <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
        <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
          <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['legend'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
        <?php } else { ?>
          <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']==1) {?>
          <a class="product_img_link cover-image" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
            <img class="img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
          </a>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='rollover') {?>
          <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']!=1) {?>
            <a class="product_img_link rollover-hover" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
              <img class="img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
            </a>
            <?php break 1?>
          <?php }?>
        <?php } else { ?>
          <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']!=1) {?>
            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration']<$_smarty_tpl->tpl_vars['smarty_settings']->value['st_nb_items']+1) {?>
              <a class="product_img_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
                <img class="img-responsive" data-lazy="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
              </a>
            <?php }?>
          <?php }?>
        <?php }?>
      <?php } ?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow'&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails']) {?>
      <div class="slider tmproductlistgallery-thumbnails<?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_carousel']) {?> use-carousel<?php }?><?php if (count($_smarty_tpl->tpl_vars['product_images']->value)<=$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_count']) {?> less-visible<?php }?>">
        <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']++;
?>
          <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
          <?php $_smarty_tpl->tpl_vars['imageVisible'] = new Smarty_variable(100/$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_visible'], null, 0);?>
          <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['legend'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
          <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
          <?php }?>
          <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration']<$_smarty_tpl->tpl_vars['smarty_settings']->value['st_nb_items']+1) {?>
            <div id="thumb-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['id_image'], ENT_QUOTES, 'UTF-8', true);?>
" class="gallery-image-thumb<?php if ($_smarty_tpl->tpl_vars['image']->value['cover']==1) {?> active<?php }?>"<?php if (!$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_carousel']&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_position']=='horizontal') {?> style="max-width: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageVisible']->value, ENT_QUOTES, 'UTF-8', true);?>
%"<?php }?>>
              <span data-href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
">
                <img class="img-responsive" id="thumb-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['id_image'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_position']=='horizontal'&&$_smarty_tpl->tpl_vars['smarty_settings']->value['st_thumbnails_carousel']) {?>data-lazy<?php } else { ?>src<?php }?>="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" itemprop="image" />
              </span>
            </div>
          <?php }?>
        <?php } ?>
      </div>
    <?php }?>
  </div>
  <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow') {?>
  </div>
  <?php }?>
<?php } else { ?>
  <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
    <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
    <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
      <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['image']->value['legend'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
    <?php } else { ?>
      <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), null, 0);?>
    <?php }?>
    <a class="product_img_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
      <img class="replace-2x img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageTitle']->value, ENT_QUOTES, 'UTF-8', true);?>
" />
    </a>
  <?php } ?>
<?php }?>

<?php }} ?>
