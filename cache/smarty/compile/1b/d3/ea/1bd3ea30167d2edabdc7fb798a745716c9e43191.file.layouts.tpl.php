<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:22
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmediaparallax\views\templates\hook\layouts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1055659b7e5e26db881-16958470%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1bd3ea30167d2edabdc7fb798a745716c9e43191' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmediaparallax\\views\\templates\\hook\\layouts.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1055659b7e5e26db881-16958470',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'item' => 0,
    'layout' => 0,
    'base_url' => 0,
    'popText' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e289ae02_44285119',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e289ae02_44285119')) {function content_59b7e5e289ae02_44285119($_smarty_tpl) {?>
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['item_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_info']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item_key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['item_info']['iteration']++;
?>
  <?php $_smarty_tpl->_capture_stack[0][] = array('some_content', 'popText', null); ob_start(); ?>
    <div class="rd-parallax rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
      <?php if ($_smarty_tpl->tpl_vars['item']->value['inverse']=='1') {?><?php $_smarty_tpl->createLocalArrayVariable('item', null, 0);
$_smarty_tpl->tpl_vars['item']->value['inverse'] = 'inverse';?><?php } else { ?><?php $_smarty_tpl->createLocalArrayVariable('item', null, 0);
$_smarty_tpl->tpl_vars['item']->value['inverse'] = 'normal';?><?php }?>
      <?php if ($_smarty_tpl->tpl_vars['item']->value['fade']=='1') {?><?php $_smarty_tpl->createLocalArrayVariable('item', null, 0);
$_smarty_tpl->tpl_vars['item']->value['fade'] = 'true';?><?php } else { ?><?php $_smarty_tpl->createLocalArrayVariable('item', null, 0);
$_smarty_tpl->tpl_vars['item']->value['fade'] = 'false';?><?php }?>
      <?php  $_smarty_tpl->tpl_vars['layout'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['layout']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item']->value['child']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['layout_info']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['layout']->key => $_smarty_tpl->tpl_vars['layout']->value) {
$_smarty_tpl->tpl_vars['layout']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['layout_info']['iteration']++;
?>
        <?php if ($_smarty_tpl->tpl_vars['layout']->value['inverse']=='1') {?><?php $_smarty_tpl->createLocalArrayVariable('layout', null, 0);
$_smarty_tpl->tpl_vars['layout']->value['inverse'] = 'inverse';?><?php } else { ?><?php $_smarty_tpl->createLocalArrayVariable('layout', null, 0);
$_smarty_tpl->tpl_vars['layout']->value['inverse'] = 'normal';?><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['layout']->value['fade']=='1') {?><?php $_smarty_tpl->createLocalArrayVariable('layout', null, 0);
$_smarty_tpl->tpl_vars['layout']->value['fade'] = 'true';?><?php } else { ?><?php $_smarty_tpl->createLocalArrayVariable('layout', null, 0);
$_smarty_tpl->tpl_vars['layout']->value['fade'] = 'false';?><?php }?>
        <?php if ($_smarty_tpl->tpl_vars['layout']->value['type']==1) {?>
          <div class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="media" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-url="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
               data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"></div>
        <?php } elseif ($_smarty_tpl->tpl_vars['layout']->value['type']==2) {?>
          <div style="position: relative" class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="media" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <div style="position: absolute; z-index: 999999; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; " class="vide"></div>
          </div>
          <script>
            $(document).ready(function() {
              $(window).on('load', function() {
                var wrapper = $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
                wrapper.find('.vide').vide({
                  mp4: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['video_mp4'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
                  webm: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['video_webm'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
                  poster: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
                  posterType: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['image_type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
',
                  volume: '0'
                });
              });
            });
          </script>
        <?php } elseif ($_smarty_tpl->tpl_vars['layout']->value['type']==3) {?>
          <div class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="media" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <div class="text-layout <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['layout']->value['content']);?>
</div>
          </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['layout']->value['type']==4) {?>
          <div class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="media" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <div class="youtube-video-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['layout_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"></div>
            <script>
              <?php if (!isset($_smarty_tpl->tpl_vars['layout']) || !is_array($_smarty_tpl->tpl_vars['layout']->value)) $_smarty_tpl->createLocalArrayVariable('layout');
if ($_smarty_tpl->tpl_vars['layout']->value['type'] = '4') {?>
              $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 .youtube-video-<?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->getVariable('smarty')->value['foreach']['layout_info']['iteration']);?>
').YTPlayer({
                fitToBackground: false,
                videoId: '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['video_link'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'
              });
              <?php }?>
            </script>
          </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['layout']->value['type']==5) {?>
          <div class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="media" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <img class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 parallax-image" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['base_url']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
          </div>
        <?php }?>
      <?php } ?>
      <div class="rd-parallax-layer" data-offset="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['offset'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-speed="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-type="html" data-fade="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['fade'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" data-direction="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['inverse'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?>
        <div class="container"><?php }?>
          <div class="parallax-main-layout"></div><?php if (isset($_smarty_tpl->tpl_vars['item']->value['content'])&&$_smarty_tpl->tpl_vars['item']->value['content']) {?>
          <div class="item-content"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['item']->value['content']);?>
</div><?php }?><?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?></div><?php }?></div>
    </div>
  <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
  <script>
    $(document).ready(function() {
      var elem = $('<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['selector'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
      if (elem.length) {
        $('body').append('<?php echo strtr($_smarty_tpl->tpl_vars['popText']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
');
        var wrapper = $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
');
        elem.before(wrapper);
        $('.rd-parallax-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['item_info']['iteration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 .parallax-main-layout').replaceWith(elem);
        win = $(window);
        <?php if ($_smarty_tpl->tpl_vars['item']->value['full_width']==1) {?>
        win.on('load resize', function() {
          wrapper.css('width', win.width()).css('margin-left', Math.floor(win.width() * -0.5)).css('left', '50%');
        });
        <?php }?>
      }
    });
  </script>
<?php } ?>
<?php ob_start();?><?php echo count($_smarty_tpl->tpl_vars['items']->value);?>
<?php $_tmp1=ob_get_clean();?><?php if (isset($_smarty_tpl->tpl_vars['items']->value)&&$_tmp1>0) {?>
  <script>
    $(document).ready(function() {
      $(window).on('load', function() {
        $.RDParallax();
        $('.rd-parallax-layer video').each(function() {
          $(this)[0].play();
        });
      });
    });
  </script>
<?php }?>
<?php }} ?>
