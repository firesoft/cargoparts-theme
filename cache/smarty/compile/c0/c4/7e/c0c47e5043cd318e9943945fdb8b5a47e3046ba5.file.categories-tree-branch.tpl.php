<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:26
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegamenu\views\templates\hook\items\categories-tree-branch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2694459b7e5e6a0b001-31531533%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0c47e5043cd318e9943945fdb8b5a47e3046ba5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegamenu\\views\\templates\\hook\\items\\categories-tree-branch.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2694459b7e5e6a0b001-31531533',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'page_name' => 0,
    'id_selected' => 0,
    'item' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e6b14e95_23391429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e6b14e95_23391429')) {function content_59b7e5e6b14e95_23391429($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['items']->value)&&$_smarty_tpl->tpl_vars['items']->value) {?>
  <ul>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
      <li class="category<?php if ($_smarty_tpl->tpl_vars['page_name']->value=='category'&&$_smarty_tpl->tpl_vars['id_selected']->value==$_smarty_tpl->tpl_vars['item']->value['id_category']) {?> sfHoverForce<?php }?>">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['item']->value['id_category']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</a>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['children'])&&$_smarty_tpl->tpl_vars['item']->value['children']) {?>
          <?php echo $_smarty_tpl->getSubTemplate ('./categories-tree-branch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['item']->value['children'],'id_selected'=>$_smarty_tpl->tpl_vars['id_selected']->value), 0);?>

        <?php }?>
      </li>
    <?php } ?>
  </ul>
<?php }?><?php }} ?>
