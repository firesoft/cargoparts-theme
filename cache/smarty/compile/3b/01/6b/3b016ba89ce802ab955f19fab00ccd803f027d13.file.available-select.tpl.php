<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:36
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegamenu\views\templates\admin\_partials\available-select.tpl" */ ?>
<?php /*%%SmartyHeaderCode:580359b7f7fc52de16-75694820%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b016ba89ce802ab955f19fab00ccd803f027d13' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegamenu\\views\\templates\\admin\\_partials\\available-select.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '580359b7f7fc52de16-75694820',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'groups' => 0,
    'group' => 0,
    'code' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fc5938c3_73691029',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fc5938c3_73691029')) {function content_59b7f7fc5938c3_73691029($_smarty_tpl) {?>

<select multiple="multiple" id="availableItems" class="availible_items" autocomplete="off">
  <?php  $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['group']->key => $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
?>
    <optgroup label="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['group']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
      <?php if (isset($_smarty_tpl->tpl_vars['group']->value['items'])&&$_smarty_tpl->tpl_vars['group']->value['items']) {?>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['group']->value['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
          <?php echo $_smarty_tpl->getSubTemplate ('./option.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('key'=>$_smarty_tpl->tpl_vars['code']->value,'item'=>$_smarty_tpl->tpl_vars['item']->value), 0);?>

        <?php } ?>
      <?php }?>
    </optgroup>
  <?php } ?>
</select><?php }} ?>
