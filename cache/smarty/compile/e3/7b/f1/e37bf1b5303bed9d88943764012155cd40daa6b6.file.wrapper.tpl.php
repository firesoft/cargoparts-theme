<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:34
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\layouts\wrapper.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2807659b7f7fae9e782-05184853%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e37bf1b5303bed9d88943764012155cd40daa6b6' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\layouts\\wrapper.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2807659b7f7fae9e782-05184853',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'elem' => 0,
    'preview' => 0,
    'position' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7faef8dc3_96525357',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7faef8dc3_96525357')) {function content_59b7f7faef8dc3_96525357($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.replace.php';
?>

<div class="wrapper sortable <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>data-type="wrapper" data-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_item'], ENT_QUOTES, 'UTF-8', true);?>
" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_parent'], ENT_QUOTES, 'UTF-8', true);?>
" data-sort-order="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
" data-specific-class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
" data-id-unique="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
  <article class="wrapper-inner clearfix inner">
    <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>
      <div class="button-container clearfix">
        <span class="element-name"><?php echo smartyTranslate(array('s'=>'Wrapper','mod'=>'tmmegalayout'),$_smarty_tpl);?>

          <span class="sort-order"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
</span> <span class="identificator"><?php if ($_smarty_tpl->tpl_vars['elem']->value['specific_class']) {?>(<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['elem']->value['specific_class'],' ',' | ');?>
)<?php }?></span></span>
        <ul class="wrapper-menu">
          <li><a href="#" class="add-row btn btn-default">+ <?php echo smartyTranslate(array('s'=>'Add row','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
          <li><a href="#" class="remove-item"></a></li>
          <li><a href="#" class="edit-wrapper"></a></li>
          <li><a href="#" class="edit-styles"></a></li>
          <li><span></span></li>
        </ul>
      </div>
    <?php }?>
    <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['position']->value);?>

  </article>
</div><?php }} ?>
