<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:34
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\layouts\module.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1119359b7f7fa01e5b3-58240074%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f0e77e19e4ba3c4dfc18a900b5c34144e9ef2858' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\layouts\\module.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1119359b7f7fa01e5b3-58240074',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'elem' => 0,
    'preview' => 0,
    'module_icon' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fa0bc924_25156636',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fa0bc924_25156636')) {function content_59b7f7fa0bc924_25156636($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.escape.php';
?>

<div class="module sortable <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
 <?php if (isset($_smarty_tpl->tpl_vars['elem']->value['warning'])) {?>not-active<?php }?>" <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>data-type="module" data-id="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['elem']->value['id_item'], 'intval', 'UTF-8');?>
" data-parent-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_parent'], ENT_QUOTES, 'UTF-8', true);?>
" data-module="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true);?>
" data-sort-order="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['sort_order'], ENT_QUOTES, 'UTF-8', true);?>
" data-specific-class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
" data-id-unique="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
  <?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars(Tmmegalayout::getModuleIcon($_tmp1), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['module_icon'] = new Smarty_variable($_tmp2, null, 0);?>
  <article class="module-inner clearfix inner">
    <?php if (isset($_smarty_tpl->tpl_vars['elem']->value['warning'])) {?><p class="alert alert-warning"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['elem']->value['warning']);?>
</p><?php }?>
    <div class="button-container clearfix">
      <span <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>data-toggle="tooltip" data-placement="right" title="<?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp3=ob_get_clean();?><?php echo mb_convert_encoding(htmlspecialchars(Module::getModuleName($_tmp3), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="module-name"<?php }?>>
        <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>
          <span class="module-sign">
            <?php if ($_smarty_tpl->tpl_vars['module_icon']->value) {?>
              <img class="img-responsive" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_icon']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
            <?php } elseif ($_smarty_tpl->tpl_vars['elem']->value['module_name']=="logo") {?>L<?php } elseif ($_smarty_tpl->tpl_vars['elem']->value['module_name']=="tabs") {?>T<?php } elseif ($_smarty_tpl->tpl_vars['elem']->value['module_name']=="copyright") {?>©<?php } else { ?>M<?php }?>
          </span>
        <?php }?>
        <span class="module-text">
          <?php if ($_smarty_tpl->tpl_vars['elem']->value['module_name']=="logo"||$_smarty_tpl->tpl_vars['elem']->value['module_name']=="copyright"||$_smarty_tpl->tpl_vars['elem']->value['module_name']=="tabs") {?>
            <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

              <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

            <?php } else { ?>
              <?php ob_start();?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['module_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php $_tmp4=ob_get_clean();?><?php echo mb_convert_encoding(htmlspecialchars(Module::getModuleName($_tmp4), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

            <?php }?>
        </span>
      </span>
      <?php if ($_smarty_tpl->tpl_vars['preview']->value==false) {?>
        <div class="dropdown button-container pull-right">
          <a href="#" id="dropdownMenu-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true);?>
" class="dropdown-toggle" aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" type="button"></a>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['elem']->value['id_unique'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <?php if (!isset($_smarty_tpl->tpl_vars['elem']->value['warning'])) {?>
              <li><a href="#" class="edit-module"><?php echo smartyTranslate(array('s'=>'Edit settings','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
            <?php }?>
            <li><a href="#" class="remove-item"><?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a></li>
          </ul>
        </div>
      <?php }?>
    </div>
  </article>
</div><?php }} ?>
