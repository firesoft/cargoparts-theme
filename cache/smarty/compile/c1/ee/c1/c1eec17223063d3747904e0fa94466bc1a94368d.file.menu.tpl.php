<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:26
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmmegamenu\views\templates\hook\menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12159b7e5e6dcbf61-67563567%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1eec17223063d3747904e0fa94466bc1a94368d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmmegamenu\\views\\templates\\hook\\menu.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12159b7e5e6dcbf61-67563567',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'menu' => 0,
    'hook' => 0,
    'item' => 0,
    'id' => 0,
    'id_row' => 0,
    'row' => 0,
    'col' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e6ea4eb5_20939063',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e6ea4eb5_20939063')) {function content_59b7e5e6ea4eb5_20939063($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'C:\\xampp\\htdocs\\prestashop_1.6\\tools\\smarty\\plugins\\modifier.escape.php';
?>

<?php if (isset($_smarty_tpl->tpl_vars['menu']->value)&&$_smarty_tpl->tpl_vars['menu']->value) {?>
  <?php if ($_smarty_tpl->tpl_vars['hook']->value=='left_column'||$_smarty_tpl->tpl_vars['hook']->value=='right_column') {?>
    <section class="block">
    <h4 class="title_block"><span><?php echo smartyTranslate(array('s'=>'Menu','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></h4>
    <div class="block_content <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['hook']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_menu column_menu top-level tmmegamenu_item">
  <?php } else { ?>
    <div class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['hook']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_menu top-level tmmegamenu_item">
    <div class="menu-title tmmegamenu_item"><span><?php echo smartyTranslate(array('s'=>'Menu','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></div>
  <?php }?>
  <ul class="menu clearfix top-level-menu tmmegamenu_item">
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
      <li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['specific_class'], ENT_QUOTES, 'UTF-8', true);?>
<?php if ($_smarty_tpl->tpl_vars['item']->value['is_simple']) {?> simple<?php }?> top-level-menu-li tmmegamenu_item <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
">
        <?php if ($_smarty_tpl->tpl_vars['item']->value['url']) {?>
          <a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
 top-level-menu-li-a tmmegamenu_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
">
        <?php } else { ?>
          <span class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
 top-level-menu-li-span tmmegamenu_item">
        <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['item']->value['title']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['badge']) {?>
              <span class="menu_badge <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
 top-level-badge tmmegamenu_item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['badge'], ENT_QUOTES, 'UTF-8', true);?>
</span>
            <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value['url']) {?>
          </a>
        <?php } else { ?>
          </span>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value['is_simple']) {?>
          <ul class="is-simplemenu tmmegamenu_item first-level-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
">
            <?php echo $_smarty_tpl->tpl_vars['item']->value['submenu'];?>

          </ul>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['item']->value['is_mega']) {?>
          <div class="is-megamenu tmmegamenu_item first-level-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8', true);?>
">
            <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['id_row'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['submenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['id_row']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
              <div id="megamenu-row-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8', true);?>
" class="megamenu-row row megamenu-row-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8', true);?>
">
                <?php if (isset($_smarty_tpl->tpl_vars['row']->value)) {?>
                  <?php  $_smarty_tpl->tpl_vars['col'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['col']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['row']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['col']->key => $_smarty_tpl->tpl_vars['col']->value) {
$_smarty_tpl->tpl_vars['col']->_loop = true;
?>
                    <div id="column-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['col'], ENT_QUOTES, 'UTF-8', true);?>
"
                         class="megamenu-col megamenu-col-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8', true);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['col'], ENT_QUOTES, 'UTF-8', true);?>
 col-sm-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['width'], ENT_QUOTES, 'UTF-8', true);?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['class'], ENT_QUOTES, 'UTF-8', true);?>
">
                      <ul class="content">
                        <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['col']->value['content'], 'qoutes', 'UTF-8');?>

                      </ul>
                    </div>
                  <?php } ?>
                <?php }?>
              </div>
            <?php } ?>
          </div>
        <?php }?>
      </li>
    <?php } ?>
  </ul>
  <?php if ($_smarty_tpl->tpl_vars['hook']->value=='left_column'||$_smarty_tpl->tpl_vars['hook']->value=='right_column') {?>
    </div>
  </section>
  <?php } else { ?>
    </div>
  <?php }?>
<?php }?><?php }} ?>
