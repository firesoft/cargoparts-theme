<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:31
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmproductsslider\views\templates\hook\tmproductsslider_fullwidth.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1909459b7e5eb74fcd5-55072735%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4de5468c625e6b2114aa1cc276f01ccb9dd768d4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmproductsslider\\views\\templates\\hook\\tmproductsslider_fullwidth.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1909459b7e5eb74fcd5-55072735',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'settings' => 0,
    'slides' => 0,
    'product' => 0,
    'slide' => 0,
    'link' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'img' => 0,
    'groups' => 0,
    'id_lang' => 0,
    'features' => 0,
    'feature' => 0,
    'priceDisplay' => 0,
    'PS_STOCK_MANAGEMENT' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5eba661b6_85038826',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5eba661b6_85038826')) {function content_59b7e5eba661b6_85038826($_smarty_tpl) {?>



<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['online_only'] = false;?>                 
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['reference'] = false;?>                   
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['new_sale_labels'] = false;?>             
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['condition'] = false;?>                   
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['product_name'] = true;?>                 
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['description_short'] = true;?>           
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['description'] = false;?>                  
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['manufacturer'] = false;?>                 
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['supplier'] = false;?>                     
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['features'] = false;?>                    
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['prices'] = true;?>                       
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['quantity'] = false;?>                    
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['cart_button'] = true;?>                  
<?php $_smarty_tpl->createLocalArrayVariable('settings', null, 0);
$_smarty_tpl->tpl_vars['settings']->value['more_button'] = false;?>                  




<script type="text/javascript">
  jQuery(document).ready(function($) {
    var options = {
      $AutoPlay: <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_autoplay'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>true<?php } else { ?>false<?php }?>,                //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
      $AutoPlaySteps: 1,                                              //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
      $AutoPlayInterval: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_interval'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,       //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
      $PauseOnHover: 1,                                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
      $Loop: <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_loop'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>true<?php } else { ?>false<?php }?>,
      $ArrowKeyNavigation: true,                                      //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
      $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
      $SlideDuration: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_duration'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,          //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
      $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
      $SlideWidth: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,                //[Optional] Width of every slide in pixels, default value is width of 'slides' container
      $SlideSpacing: 0,                                               //[Optional] Space between each slide in pixels, default value is 0
      $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
      $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
      $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
      $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
      $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
      $AutoCenter: 1,
      $ArrowNavigatorOptions: {                                       //[Optional] Options to specify and enable arrow navigator or not
        $Class: $JssorArrowNavigator$,                              //[Requried] Class to create arrow navigator instance
        $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
        $Steps: 1                                                   //[Optional] Steps to go for each navigation request, default value is 1
      },
      $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
        $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
        $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
        $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
        $SpacingX: 12,                                              //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
        $Cols: 5,                                                   //[Optional] Number of pieces to display, default value is 1
        $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
        $Orientation: 1,
        $Loop: <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_loop'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>true<?php } else { ?>false<?php }?>,
        $AutoCenter: 1
      },
      $BulletNavigatorOptions: {                                      //[Optional] Options to specify and enable navigator or not
        $Class: $JssorBulletNavigator$,                             //[Required] Class to create navigator instance
        $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
        $AutoCenter: 0,                                             //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
        $Steps: 1,                                                  //[Optional] Steps to go for each navigation request, default value is 1
        $Lanes: 1,                                                  //[Optional] Specify lanes to arrange items, default value is 1
        $SpacingX: 5,                                               //[Optional] Horizontal space between each item in pixel, default value is 0
        $SpacingY: 0,                                               //[Optional] Vertical space between each item in pixel, default value is 0
        $Orientation: 1                                             //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
      }
    };
    var nestedSliders = [];
    var nestedSlidersOptions = {
      $AutoPlay: false,                                               //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
      $ArrowKeyNavigation: true,                                      //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
      $SlideEasing: $JssorEasing$.$EaseOutQuint,                      //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
      $SlideDuration: 300,                                            //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
      $MinDragOffsetToSlide: 20,                                      //[Optional] Minimum drag offset to trigger slide , default value is 20
      $SlideSpacing: 0,                                               //[Optional] Space between each slide in pixels, default value is 0
      $DisplayPieces: 1,                                              //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
      $ParkingPosition: 0,                                            //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
      $UISearchMode: 0,                                               //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
      $PlayOrientation: 1,                                            //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
      $DragOrientation: 3,                                            //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
      $ThumbnailNavigatorOptions: {                                   //[Optional] Options to specify and enable thumbnail navigator or not
        $Class: $JssorThumbnailNavigator$,                          //[Required] Class to create thumbnail navigator instance
        $ChanceToShow: 2,                                           //[Required] 0 Never, 1 Mouse Over, 2 Always
        $ActionMode: 1,                                             //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
        $SpacingY: 29,                                              //[Optional] Vertical space between each thumbnail in pixel, default value is 0
        $Cols: 3,                                                   //[Optional] Number of pieces to display, default value is 1
        $ParkingPosition: 0,                                        //[Optional] The offset position to park thumbnail
        $AutoCenter: 2,
        $Orientation: 2
      }
    };
    var jssor_slider = new $JssorSlider$("slider_container", options);
    <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_images_gallery'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>
      <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
        <?php if (isset($_smarty_tpl->tpl_vars['product']->value['image'])&&$_smarty_tpl->tpl_vars['product']->value['image']) {?>
          var jssor_slider<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 = new $JssorSlider$("inner-slider-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
", nestedSlidersOptions);
          nestedSliders.push(jssor_slider<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
);
        <?php }?>
      <?php } ?>
    <?php }?>
    function ScaleSlider() {
      var parentWidth = jssor_slider.$Elmt.parentNode.clientWidth;
      if (parentWidth)
        jssor_slider.$ScaleWidth(Math.max(Math.min(parentWidth, <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
), 30));
      else
        window.setTimeout(ScaleSlider, 30);
    }

    ScaleSlider();
    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", ScaleSlider);
    //responsive code end
  });
</script>

<?php if (isset($_smarty_tpl->tpl_vars['slides']->value)&&$_smarty_tpl->tpl_vars['slides']->value) {?>
  <div id="tm-products-slider" class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['slider_type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
    <div id="slider_container" style="width: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px;">
      <div data-u="slides" class="main-slides" style="width: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px; height: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_height'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
px;">
        <?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
          <div class="slide-inner">
            <div data-u="thumb">
              <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['image'])&&$_smarty_tpl->tpl_vars['slide']->value['image']) {?>
                <img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['slide']->value['info']->name,$_smarty_tpl->tpl_vars['slide']->value['image']['id_image'],'medium'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt=""/>
              <?php }?>
              <div class="thumb-info<?php if (!$_smarty_tpl->tpl_vars['slide']->value['image']) {?> without-image<?php }?>">
                <p class="thumb-name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->price&&$_smarty_tpl->tpl_vars['slide']->value['info']->show_price&&$_smarty_tpl->tpl_vars['settings']->value['prices']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
                  <span class="thumb-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->price),$_smarty_tpl);?>
</span>
                <?php }?>
              </div>
            </div>
            <div class="sliders-wrap">
              <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['image'])&&$_smarty_tpl->tpl_vars['slide']->value['image']) {?>
                <div class="slide-image col-xs-6">
                  <div class="slide-image-wrap">
                    <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->new&&$_smarty_tpl->tpl_vars['settings']->value['new_sale_labels']) {?>
                      <span class="new-box">
                        <span class="new-label"><?php echo smartyTranslate(array('s'=>'New','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                      </span>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->on_sale&&$_smarty_tpl->tpl_vars['settings']->value['new_sale_labels']) {?>
                      <span class="sale-box no-print">
                        <span class="sale-label"><?php echo smartyTranslate(array('s'=>'Sale!','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                      </span>
                    <?php }?>
                    <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_images_gallery'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>
                      <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['images'])&&$_smarty_tpl->tpl_vars['slide']->value['images']) {?>
                        <div id="inner-slider-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="sliders-inner">
                          <div data-u="slides" class="inner-slides">
                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slide']->value['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
?>
                              <div>
                                <img data-u="image" class="img-responsive" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['slide']->value['info']->name,$_smarty_tpl->tpl_vars['img']->value['id_image'],'large_default'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
                                <img data-u="thumb" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['slide']->value['info']->name,$_smarty_tpl->tpl_vars['img']->value['id_image'],'small_default'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
                              </div>
                            <?php } ?>
                          </div>
                          <div data-u="thumbnavigator" class="inner-thumbnail-buttons">
                            <div data-u="slides">
                              <div data-u="prototype" class="p">
                                <div class=w>
                                  <div data-u="thumbnailtemplate" class="t"></div>
                                </div>
                                <div class=c></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php }?>
                    <?php } else { ?>
                      <a class="slide-image" href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->getLink(), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                        <img class="img-responsive" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['slide']->value['info']->name,$_smarty_tpl->tpl_vars['slide']->value['image']['id_image'],'large_default'), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
                      </a>
                    <?php }?>
                  </div>
                </div>
              <?php }?>
              <div class="slide-info <?php if ($_smarty_tpl->tpl_vars['slide']->value['image']) {?>col-xs-6<?php } else { ?>col-xs-12<?php }?>">
                <h3 class="head-block"><?php echo smartyTranslate(array('s'=>"[1]THIS WEEK'S[/1] HOT DEALS",'mod'=>'tmproductsslider','tags'=>array('<strong>')),$_smarty_tpl);?>
</h3>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->online_only&&$_smarty_tpl->tpl_vars['settings']->value['online_only']) {?>
                  <p class="online_only"><?php echo smartyTranslate(array('s'=>'Online only','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</p>
                <?php }?>
                <?php if ((!empty($_smarty_tpl->tpl_vars['slide']->value['info']->reference)||$_smarty_tpl->tpl_vars['slide']->value['info']->reference)&&$_smarty_tpl->tpl_vars['settings']->value['reference']) {?>
                  <p id="product_reference">
                    <label><?php echo smartyTranslate(array('s'=>'Reference:','mod'=>'tmproductsslider'),$_smarty_tpl);?>
 </label>
                    <span class="editable" itemprop="sku"<?php if (!empty($_smarty_tpl->tpl_vars['slide']->value['info']->reference)&&$_smarty_tpl->tpl_vars['slide']->value['info']->reference) {?> content="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->reference, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?>><?php if (!isset($_smarty_tpl->tpl_vars['groups']->value)) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->reference, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php }?></span>
                  </p>
                <?php }?>
                <?php if (!$_smarty_tpl->tpl_vars['slide']->value['info']->is_virtual&&$_smarty_tpl->tpl_vars['slide']->value['info']->condition&&$_smarty_tpl->tpl_vars['settings']->value['condition']) {?>
                  <p id="product_condition">
                    <label><?php echo smartyTranslate(array('s'=>'Condition:','mod'=>'tmproductsslider'),$_smarty_tpl);?>
 </label>
                    <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->condition=='new') {?>
                      <span class="editable"><?php echo smartyTranslate(array('s'=>'New product','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                    <?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['info']->condition=='used') {?>
                      <span class="editable"><?php echo smartyTranslate(array('s'=>'Used','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                    <?php } elseif ($_smarty_tpl->tpl_vars['slide']->value['info']->condition=='refurbished') {?>
                      <span class="editable"><?php echo smartyTranslate(array('s'=>'Refurbished','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                    <?php }?>
                  </p>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['settings']->value['product_name']) {?><h4><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->getLink(), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></h4><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->description_short&&$_smarty_tpl->tpl_vars['settings']->value['description_short']) {?>
                  <p class="slide-description des-short"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['slide']->value['info']->description_short),200,'...',false), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->description&&$_smarty_tpl->tpl_vars['settings']->value['description']) {?>
                  <p class="slide-description"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['slide']->value['info']->description),230,'...',false), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->manufacturer_name&&$_smarty_tpl->tpl_vars['settings']->value['manufacturer']) {?>
                  <div class="slide-manufacturer">
                    <span><?php echo smartyTranslate(array('s'=>'Brand:','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                    <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->manufacturer_name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                  </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->supplier_name&&$_smarty_tpl->tpl_vars['settings']->value['supplier']) {?>
                  <div class="slide-supplier">
                    <span><?php echo smartyTranslate(array('s'=>'Supplier:','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                    <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->supplier_name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

                  </div>
                <?php }?>
                <?php $_smarty_tpl->tpl_vars["features"] = new Smarty_variable($_smarty_tpl->tpl_vars['slide']->value['info']->getFrontFeatures($_smarty_tpl->tpl_vars['id_lang']->value), null, 0);?>
                <?php if (isset($_smarty_tpl->tpl_vars['features']->value)&&$_smarty_tpl->tpl_vars['features']->value&&$_smarty_tpl->tpl_vars['settings']->value['features']) {?>
                  <div class="product-features">
                    <?php  $_smarty_tpl->tpl_vars['feature'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feature']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feature']->key => $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->_loop = true;
?>
                      <div><span><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
:</span> <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['value'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div>
                    <?php } ?>
                  </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->price&&$_smarty_tpl->tpl_vars['slide']->value['info']->show_price&&$_smarty_tpl->tpl_vars['settings']->value['prices']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
                  <div class="product-price">
                    <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->base_price&&$_smarty_tpl->tpl_vars['slide']->value['info']->specificPrice) {?>
                      <span class="product-price product-price-new"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->price),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->price_tax_exc),$_smarty_tpl);?>
<?php }?></span>
                      <span class="product-price product-price-reduction">
                        <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->specificPrice['reduction_type']=='percentage') {?>
                          -<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->specificPrice['reduction'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')*100;?>
%
                        <?php } else { ?>
                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->specificPrice['reduction']*-1),$_smarty_tpl);?>

                        <?php }?>
                      </span>
                      <span class="product-price product-price-old"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->base_price),$_smarty_tpl);?>
</span>
                    <?php } else { ?>
                      <span class="product-price"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->price),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['slide']->value['info']->price_tax_exc),$_smarty_tpl);?>
<?php }?></span>
                    <?php }?>
                    <?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&$_smarty_tpl->tpl_vars['PS_STOCK_MANAGEMENT']->value&&$_smarty_tpl->tpl_vars['slide']->value['info']->available_for_order&&!$_smarty_tpl->tpl_vars['slide']->value['info']->quantity<=0&&$_smarty_tpl->tpl_vars['settings']->value['quantity'])) {?>
                      <!-- number of item in stock -->
                      <p id="product-quantity">
                        <span><?php echo intval($_smarty_tpl->tpl_vars['slide']->value['info']->quantity);?>
</span>
                        <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->quantity==1) {?>
                          <span><?php echo smartyTranslate(array('s'=>'Item','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                        <?php } else { ?>
                          <span><?php echo smartyTranslate(array('s'=>'Items','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                        <?php }?>
                      </p>
                    <?php }?>
                  </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['settings']->value['more_button']||$_smarty_tpl->tpl_vars['settings']->value['cart_button']) {?>
                  <div class="buttons-container">
                    <?php if ($_smarty_tpl->tpl_vars['slide']->value['info']->available_for_order&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&$_smarty_tpl->tpl_vars['settings']->value['cart_button']) {?>
                      <?php if ((!isset($_smarty_tpl->tpl_vars['slide']->value['info']->customization_required)||!$_smarty_tpl->tpl_vars['slide']->value['info']->customization_required)&&$_smarty_tpl->tpl_vars['slide']->value['info']->quantity>0) {?>
                        <a class="ajax_add_to_cart_button btn btn-md btn-primary cart-button" href="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['slide']->value['info']->id);?>
<?php $_tmp14=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,"qty=1&amp;id_product=".$_tmp14."&amp;token=".((string)$_smarty_tpl->tpl_vars['static_token']->value)."&amp;add",false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmproductsslider'),$_smarty_tpl);?>
"
                           data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['slide']->value['info']->id);?>
" data-minimal_quantity="<?php echo intval($_smarty_tpl->tpl_vars['slide']->value['info']->minimal_quantity);?>
">
                          <span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                        </a>
                      <?php } else { ?>
                        <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->getLink(), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="btn btn-md btn-primary cart-button">
                          <span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                        </a>
                      <?php }?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['more_button']) {?>
                      <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['info']->getLink(), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="btn lnk_view btn btn-default">
                        <span><?php echo smartyTranslate(array('s'=>'Read More','mod'=>'tmproductsslider'),$_smarty_tpl);?>
</span>
                      </a>
                    <?php }?>
                  </div>
                <?php }?>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <?php if (($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_navigation'])||!$_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']) {?>
        <span data-u="arrowleft" class="prev-btn"></span>
        <span data-u="arrowright" class="next-btn"></span>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_pagination']) {?>
        <div data-u="navigator" class="pagers">
          <div data-u="prototype">
            <div data-u="numbertemplate"></div>
          </div>
        </div>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['settings']->value['fullwidth_extended_settings']&&$_smarty_tpl->tpl_vars['settings']->value['fullwidth_slider_thumbnails']) {?>
        <div data-u="thumbnavigator" class="thumbnail-buttons">
          <div data-u="slides">
            <div data-u="prototype" class="p">
              <div class=w>
                <div data-u="thumbnailtemplate" class="t"></div>
              </div>
              <div class=c></div>
            </div>
          </div>
        </div>
      <?php }?>
    </div>
  </div>
<?php }?>

<?php }} ?>
