<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 16:28:53
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmadvancedfilter\views\templates\front\filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:699659b843856c8400-83872432%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e609a8c5f2149b62834fb0a7323335e7982d692e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmadvancedfilter\\views\\templates\\front\\filter.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '699659b843856c8400-83872432',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b84385751277_81884550',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b84385751277_81884550')) {function content_59b84385751277_81884550($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'Advanced Filter','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<h1 class="page-heading product-listing"><?php echo smartyTranslate(array('s'=>'Advanced Filter','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
</h1>

<?php if (isset($_smarty_tpl->tpl_vars['products']->value)&&$_smarty_tpl->tpl_vars['products']->value) {?>
  <div class="content_sortPagiBar">
    <div class="sortPagiBar clearfix">
      <?php echo $_smarty_tpl->getSubTemplate (Tmadvancedfilter::getTemplateFile('product-sort.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <?php echo $_smarty_tpl->getSubTemplate (Tmadvancedfilter::getTemplateFile('nbr-product-page.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
  </div>

  <?php echo $_smarty_tpl->getSubTemplate (Tmadvancedfilter::getTemplateFile('product-list.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>


  <div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix">
      <?php echo $_smarty_tpl->getSubTemplate (Tmadvancedfilter::getTemplateFile('product-compare.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

      <?php echo $_smarty_tpl->getSubTemplate (Tmadvancedfilter::getTemplateFile('./pagination.tpl'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_follow'=>1,'paginationId'=>'bottom'), 0);?>

    </div>
  </div>
<?php } else { ?>
  <p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'No found products by this criteria.','mod'=>'tmadvancedfilter'),$_smarty_tpl);?>
</p>
<?php }?>
<?php }} ?>
