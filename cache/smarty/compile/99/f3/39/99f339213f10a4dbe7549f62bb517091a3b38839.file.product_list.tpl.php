<?php /* Smarty version Smarty-3.1.19, created on 2017-09-13 11:29:59
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmcategoryproducts\\views\templates\admin\product_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:458659b94ef75c7dc6-07449462%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99f339213f10a4dbe7549f62bb517091a3b38839' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmcategoryproducts\\\\views\\templates\\admin\\product_list.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '458659b94ef75c7dc6-07449462',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'products' => 0,
    'product' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b94ef76608a5_02161537',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b94ef76608a5_02161537')) {function content_59b94ef76608a5_02161537($_smarty_tpl) {?>

<div class="bootstrap">
  <div class="search">
    <input type="search" name="product_search" placeholder="Search">
    <a href="#" class="clear_serach btn btn-default">
      <i class="icon-remove"></i>
    </a>
  </div>
  <div class="content">
  <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
    <div data-product-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="product">
      <img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="">
      <p><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</p>
      <a href="#" class="remove-product">
        <i class="icon-remove"></i>
      </a>
    </div>
  <?php } ?>
  </div>
  <div class="footer-buttons">
    <button id="select_all_products"  class="btn btn-sm btn-default pull-left"><?php echo smartyTranslate(array('s'=>'Select all','mod'=>'tmcategoryproducts'),$_smarty_tpl);?>
</button>
    <button id="deselect_all_products"  class="btn btn-sm btn-default pull-left"><?php echo smartyTranslate(array('s'=>'Deselect all','mod'=>'tmcategoryproducts'),$_smarty_tpl);?>
</button>
    <button id="add_products"  class="btn btn-sm btn-default pull-right"><?php echo smartyTranslate(array('s'=>'Add','mod'=>'tmcategoryproducts'),$_smarty_tpl);?>
</button>
    <input class="hidden" type="text" value="[]" name="products">
  </div>
</div><?php }} ?>
