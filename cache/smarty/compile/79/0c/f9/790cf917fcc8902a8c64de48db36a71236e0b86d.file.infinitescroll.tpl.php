<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:52:37
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tminfinitescroll\views\templates\hook\infinitescroll.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1503559b7e6a5ada730-55261633%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '790cf917fcc8902a8c64de48db36a71236e0b86d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tminfinitescroll\\views\\templates\\hook\\infinitescroll.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1503559b7e6a5ada730-55261633',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'TMINFINITESCROLL_PAGINATION' => 0,
    'TMINFINITESCROLL_AUTO_LOAD' => 0,
    'TMINFINITESCROLL_SHOW_ALL' => 0,
    'TMINFINITESCROLL_OFFSET' => 0,
    'TMINFINITESCROLL_PRELOADER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e6a5b3df49_87035035',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e6a5b3df49_87035035')) {function content_59b7e6a5b3df49_87035035($_smarty_tpl) {?>


<script>
  function getMaxPages() {
    return $('#pagination_bottom ul.pagination>li').length - 2;
  }
  function getStartPage() {
      return $('#pagination_bottom ul.pagination>li.current').index() + 1;
  }

  function afterPageCreate() {
    var elem = $('#pagination_bottom ul.pagination>li.current').last().next(),
            child = elem.find('>a'),
            childHtml = child.html();

    if (elem.length) {
      elem.addClass('current');
      child.replaceWith($('<span>' + childHtml + '</span>'));
    }
  }

  function beforeAnimate(items) {
    items.css({'opacity':'0'});
  }

  function animateElems(items) {
    items.each(function(index){
      var item = $(this);
      setTimeout(function(){
        item.animate({'opacity':'1'},500);
      },index*500);
    });
  }

  function afterCreate() {
    
      <?php if (!$_smarty_tpl->tpl_vars['TMINFINITESCROLL_PAGINATION']->value||$_smarty_tpl->tpl_vars['TMINFINITESCROLL_AUTO_LOAD']->value) {?>
        
        setTimeout(function(){
          $('#pagination_bottom').hide();
        }, 500);

        
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['TMINFINITESCROLL_SHOW_ALL']->value&&!$_smarty_tpl->tpl_vars['TMINFINITESCROLL_AUTO_LOAD']->value) {?>
        
        
      <?php }?>
    
  }

  $(document).ready(function(){
    var product_list = $('.product_list');
    if (product_list.length) {
      product_list.ajaxInfiniteScroll({
        maxPages: getMaxPages,
        startPage: getStartPage,
        offset: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['TMINFINITESCROLL_OFFSET']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
        ajax: {
          url: request
        },
        loader: {
          image : '<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['TMINFINITESCROLL_PRELOADER']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
'
        },
        nav: {
          more: <?php if ($_smarty_tpl->tpl_vars['TMINFINITESCROLL_AUTO_LOAD']->value) {?>false<?php } else { ?>true<?php }?>,
          moreEnabledText: '<?php echo smartyTranslate(array('s'=>'Show next'),$_smarty_tpl);?>
',
        },
        afterPageCreate: afterPageCreate,
        afterCreate: afterCreate,
        beforeAnimate: beforeAnimate,
        animate: animateElems
      });
    }
  });
</script>

<?php }} ?>
