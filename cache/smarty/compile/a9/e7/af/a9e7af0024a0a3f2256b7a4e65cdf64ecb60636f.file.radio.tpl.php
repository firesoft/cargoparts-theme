<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:42
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmadvancedfilter\\views\templates\hook\_elements\radio.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13359b7e5f63af7a2-06780843%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9e7af0024a0a3f2256b7a4e65cdf64ecb60636f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmadvancedfilter\\\\views\\templates\\hook\\_elements\\radio.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13359b7e5f63af7a2-06780843',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'active_item' => 0,
    'type' => 0,
    'filter_item' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f641d2c5_78125222',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f641d2c5_78125222')) {function content_59b7e5f641d2c5_78125222($_smarty_tpl) {?>


<?php if ($_smarty_tpl->tpl_vars['items']->value&&$_smarty_tpl->tpl_vars['active_item']->value) {?>
  <div class="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
-filter-row">
    <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['name']) {?>
      <label class="parameter-name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>

        <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['label']) {?>
          <small><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['label'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</small>
        <?php }?>
      </label>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['filter_item']->value['description']) {?>
      <div class="parameter-description"><?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['filter_item']->value['description']);?>
</div>
    <?php }?>
    <div class="values-list">
      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <div class="radio-inline">
          <input id="category_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" <?php if (isset($_smarty_tpl->tpl_vars['item']->value['element']->possible_values)&&!$_smarty_tpl->tpl_vars['item']->value['element']->possible_values) {?>disabled="disabled"<?php }?> <?php if ($_smarty_tpl->tpl_vars['active_item']->value&&$_smarty_tpl->tpl_vars['item']->value['element']->is_checked) {?>checked="checked"<?php }?> type="radio"
                 name="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['type'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['filter_item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
          <label for="category_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->id, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->name, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 <?php if (isset($_smarty_tpl->tpl_vars['item']->value['element']->possible_values)&&$_smarty_tpl->tpl_vars['item']->value['element']->possible_values&&!$_smarty_tpl->tpl_vars['item']->value['element']->is_checked) {?> (<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['element']->possible_values, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
)<?php }?>
          </label>
        </div>
      <?php } ?>
    </div>
  </div>
<?php }?><?php }} ?>
