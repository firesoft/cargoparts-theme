<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:28
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmdaydeal\views\templates\hook\tmdaydeal-price-block.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1106159b7e5e82ceb81-04300037%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60be653561e6017bda2fc3c3de11380c4d2239a3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmdaydeal\\views\\templates\\hook\\tmdaydeal-price-block.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1106159b7e5e82ceb81-04300037',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'daydeal_products_extra' => 0,
    'data_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e8370845_74137830',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e8370845_74137830')) {function content_59b7e5e8370845_74137830($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"])&&$_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"]) {?>
  <?php $_smarty_tpl->tpl_vars['data_end'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"], null, 0);?>
  <div class="block products_block daydeal-box">
    <h3><?php echo smartyTranslate(array('s'=>'Time left to buy','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</h3>
    <div data-countdown="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['data_end']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"></div>
  </div>
  <script type="text/javascript">
    var tmdd_msg_days = "<?php echo smartyTranslate(array('s'=>'days','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_hr = "<?php echo smartyTranslate(array('s'=>'hrs','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_min = "<?php echo smartyTranslate(array('s'=>'mins','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_sec = "<?php echo smartyTranslate(array('s'=>'secs','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
      $("[data-countdown]").each(function() {
        var $this = $(this), finalDate = $(this).data("countdown");
        $this.countdown(finalDate, function(event) {
          $this.html(event.strftime('<span><span>%D</span>'+tmdd_msg_days+'</span><span><span>%H</span>'+tmdd_msg_hr+'</span><span><span>%M</span>'+tmdd_msg_min+'</span><span><span>%S</span>'+tmdd_msg_sec+'</span>'));
        });
      });
  </script>
<?php }?><?php }} ?>
