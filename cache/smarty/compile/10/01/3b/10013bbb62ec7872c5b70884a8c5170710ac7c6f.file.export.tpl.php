<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:49:13
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\\views\templates\admin\tools\export.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2938559b801f915f622-84718011%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10013bbb62ec7872c5b70884a8c5170710ac7c6f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\\\views\\templates\\admin\\tools\\export.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2938559b801f915f622-84718011',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'hooks' => 0,
    'hook' => 0,
    'layout' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b801f99e0405_16575255',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b801f99e0405_16575255')) {function content_59b801f99e0405_16575255($_smarty_tpl) {?>

<div class="tmpanel-content cleafix" id="export_layout">
  <?php  $_smarty_tpl->tpl_vars['hook'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['hook']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['hooks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['hook']->key => $_smarty_tpl->tpl_vars['hook']->value) {
$_smarty_tpl->tpl_vars['hook']->_loop = true;
?>
    <div class="block">
      <div class="hook-title clearfix <?php if ($_smarty_tpl->tpl_vars['hook']->value['status']=='off') {?>disabled<?php }?>">
        <h4 class="pull-left">Hook "<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['hook']->value['hook_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"</h4>
      </div>
      <?php if ($_smarty_tpl->tpl_vars['hook']->value['layouts']) {?>
        <ul class="tree">
          <?php  $_smarty_tpl->tpl_vars['layout'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['layout']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['hook']->value['layouts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['layout']->key => $_smarty_tpl->tpl_vars['layout']->value) {
$_smarty_tpl->tpl_vars['layout']->_loop = true;
?>
            <li class="tree-item">
              <span class="tree-item-name">
                <i class="icon-image"></i>
                <label class="tree-toggler"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['layout_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</label>
                ( <a href="#" class="layout-preview" data-id-layout="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'Layout preview','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a> |
                <a href="#" class="layout-export" data-id-layout="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo smartyTranslate(array('s'=>'Export layout','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a> )
              </span>
            </li>
          <?php } ?>
        </ul>
      <?php } else { ?>
        <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'No layout for export','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</p>
      <?php }?>
    </div>
  <?php } ?>
</div><?php }} ?>
