<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:29
         compiled from "C:\xampp\htdocs\prestashop_1.6\themes\theme1442\modules\tmcategoryproducts\\views\templates\hook\tmcaregoryproducts-home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1260359b7e5e99a8ea1-38538440%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5230c3803ea361318c16a6e8ef882cd5e0e6f0c0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\themes\\theme1442\\modules\\tmcategoryproducts\\\\views\\templates\\hook\\tmcaregoryproducts-home.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1260359b7e5e99a8ea1-38538440',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'blocks' => 0,
    'block' => 0,
    'block_identificator' => 0,
    'img_cat_dir' => 0,
    'cat_img' => 0,
    'link' => 0,
    'products' => 0,
    'product' => 0,
    'homeSize' => 0,
    'add_prod_display' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'static_token' => 0,
    'quick_view' => 0,
    'comparator_max_item' => 0,
    'priceDisplay' => 0,
    'compared_products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5e9ca94d1_02945640',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5e9ca94d1_02945640')) {function content_59b7e5e9ca94d1_02945640($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['blocks']->value)&&$_smarty_tpl->tpl_vars['blocks']->value) {?>
  <?php  $_smarty_tpl->tpl_vars['block'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['block']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['blocks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['block']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['block']->key => $_smarty_tpl->tpl_vars['block']->value) {
$_smarty_tpl->tpl_vars['block']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['block']['iteration']++;
?>
    <?php $_smarty_tpl->tpl_vars["block_identificator"] = new Smarty_variable(((string)$_smarty_tpl->getVariable('smarty')->value['foreach']['block']['iteration'])."_".((string)$_smarty_tpl->tpl_vars['block']->value['id']), null, 0);?>
    <section id="block-category-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" class="category-block row">
      <div class="col-xs-12 col-lg-3">
        <?php $_smarty_tpl->tpl_vars["cat_img"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['img_cat_dir']->value).((string)$_smarty_tpl->tpl_vars['block']->value['id'])."-medium_default.jpg", null, 0);?>
        <div class="category" <?php if ($_smarty_tpl->tpl_vars['block']->value['id']!=2) {?> style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['cat_img']->value;?>
)" <?php }?>>
          <?php if (isset($_smarty_tpl->tpl_vars['block']->value['name'])) {?>
            <h4 class="title"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</h4>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['block']->value['id']!=2) {?>
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['block']->value['id']), ENT_QUOTES, 'UTF-8', true);?>
" class="link">
              <?php echo smartyTranslate(array('s'=>'view all','mod'=>'tmcategoryproductsslider'),$_smarty_tpl);?>

            </a>
          <?php }?>
        </div>
      </div>
      <div class="col-xs-12 col-lg-9">
        <?php if (isset($_smarty_tpl->tpl_vars['block']->value['products'])&&$_smarty_tpl->tpl_vars['block']->value['products']) {?>
          <?php $_smarty_tpl->tpl_vars['products'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['products'], null, 0);?>
          <ul class="product_list grid tab-pane">
            <?php if ($_smarty_tpl->tpl_vars['block']->value['use_carousel']) {?>
              <li>
            <?php }?>
              <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['products']['index']++;
?>
                <?php if (!$_smarty_tpl->tpl_vars['block']->value['use_carousel']) {?>
                  <li>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['block']->value['use_carousel']&&!(1 & $_smarty_tpl->getVariable('smarty')->value['foreach']['products']['index'])&&$_smarty_tpl->getVariable('smarty')->value['foreach']['products']['index']!=0) {?>
                  </li>
                  <li>
                <?php }?>
                <div class="product-container" itemscope itemtype="https://schema.org/Product">
                  <div class="product-image-container">
                    <?php $_smarty_tpl->_capture_stack[0][] = array('displayProductListGallery', null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListGallery','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                    <?php if (Smarty::$_smarty_vars['capture']['displayProductListGallery']) {?>
                      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListGallery','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>

                    <?php } else { ?>
                      <a class="product_img_link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url">
                        <img class="replace-2x img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['id_image'],'home_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['legend'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['legend'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>"title="<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['legend'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['legend'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>"<?php if (isset($_smarty_tpl->tpl_vars['homeSize']->value)) {?> width="<?php echo $_smarty_tpl->tpl_vars['homeSize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['homeSize']->value['height'];?>
"<?php }?> itemprop="image" />
                      </a>
                    <?php }?>
                    <div class="functional-buttons">
                      <?php if (($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['product']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['product']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
                        <?php if ((!isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])||!$_smarty_tpl->tpl_vars['product']->value['customization_required'])&&($_smarty_tpl->tpl_vars['product']->value['allow_oosp']||$_smarty_tpl->tpl_vars['product']->value['quantity']>0)) {?>
                          <?php $_smarty_tpl->_capture_stack[0][] = array('default', null, null); ob_start(); ?>add=1&amp;id_product=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
<?php if (isset($_smarty_tpl->tpl_vars['product']->value['id_product_attribute'])&&$_smarty_tpl->tpl_vars['product']->value['id_product_attribute']) {?>&amp;ipa=<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
<?php }?><?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
<?php }?><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                          <div>
                            <a class="ajax_add_to_cart_button" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',true,null,Smarty::$_smarty_vars['capture']['default'],false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart'),$_smarty_tpl);?>
" data-id-product-attribute="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product_attribute']);?>
"
                               data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['product']->value['id_product']);?>
" data-minimal_quantity="<?php if (isset($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity'])&&$_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']>=1) {?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['product_attribute_minimal_quantity']);?>
<?php } else { ?><?php echo intval($_smarty_tpl->tpl_vars['product']->value['minimal_quantity']);?>
<?php }?>">
                            </a>
                          </div>
                        <?php }?>
                      <?php }?>
                      <?php if ((isset($_smarty_tpl->tpl_vars['product']->value['customization_required'])&&$_smarty_tpl->tpl_vars['product']->value['customization_required'])) {?>
                        <div><a itemprop="url" class="customize" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
"></a></div>
                      <?php }?>
                      <?php if (isset($_smarty_tpl->tpl_vars['quick_view']->value)&&$_smarty_tpl->tpl_vars['quick_view']->value) {?>
                        <div class="qv-wrap">
                          <a class="quick-view" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" data-href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
"></a>
                        </div>
                      <?php }?>
                      <?php if (isset($_smarty_tpl->tpl_vars['comparator_max_item']->value)&&$_smarty_tpl->tpl_vars['comparator_max_item']->value) {?>
                        <div class="compare">
                          <a class="add_to_compare" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" data-id-product="<?php echo $_smarty_tpl->tpl_vars['product']->value['id_product'];?>
" title="<?php echo smartyTranslate(array('s'=>'Add to Compare'),$_smarty_tpl);?>
"></a>
                        </div>
                      <?php }?>
                      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListFunctionalButtons','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>

                    </div>
                  </div>
                  <div class="product-info">
                    <h5 itemprop="name">
                      <?php if (isset($_smarty_tpl->tpl_vars['product']->value['pack_quantity'])&&$_smarty_tpl->tpl_vars['product']->value['pack_quantity']) {?><?php echo (intval($_smarty_tpl->tpl_vars['product']->value['pack_quantity'])).(' x ');?>
<?php }?>
                      <a class="product-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" itemprop="url" >
                        <span class="list-name"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['name'],100,'...'), ENT_QUOTES, 'UTF-8', true);?>
</span>
                        <span class="grid-name"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['name'],45,'...'), ENT_QUOTES, 'UTF-8', true);?>
</span>
                      </a>
                    </h5>
                    <?php if (isset($_smarty_tpl->tpl_vars['product']->value['is_virtual'])&&!$_smarty_tpl->tpl_vars['product']->value['is_virtual']) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductDeliveryTime",'product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl);?>
<?php }?>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"weight"),$_smarty_tpl);?>

                    <?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&((isset($_smarty_tpl->tpl_vars['product']->value['show_price'])&&$_smarty_tpl->tpl_vars['product']->value['show_price'])||(isset($_smarty_tpl->tpl_vars['product']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['product']->value['available_for_order'])))) {?>
                      <div class="content_price">
                        <?php if (isset($_smarty_tpl->tpl_vars['product']->value['show_price'])&&$_smarty_tpl->tpl_vars['product']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>'before_price'),$_smarty_tpl);?>

                          <span class="price product-price<?php if (isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?> product-price-new<?php }?>">
                            <?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['product']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?>
                          </span>
                          <?php if ($_smarty_tpl->tpl_vars['product']->value['price_without_reduction']>0&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']>0) {?>
                            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"old_price"),$_smarty_tpl);?>

                            <span class="old-price product-price">
                              <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['product']->value['price_without_reduction']),$_smarty_tpl);?>

                            </span>
                            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'id_product'=>$_smarty_tpl->tpl_vars['product']->value['id_product'],'type'=>"old_price"),$_smarty_tpl);?>

                            <?php if ($_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction_type']=='percentage') {?>
                              <span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['product']->value['specific_prices']['reduction']*100;?>
%</span>
                            <?php }?>
                          <?php }?>
                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"price"),$_smarty_tpl);?>

                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>"unit_price"),$_smarty_tpl);?>

                          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayProductPriceBlock",'product'=>$_smarty_tpl->tpl_vars['product']->value,'type'=>'after_price'),$_smarty_tpl);?>

                        <?php }?>
                      </div>
                    <?php }?>
                  </div>
                </div><!-- .product-container> -->
                <?php if (!$_smarty_tpl->tpl_vars['block']->value['use_carousel']) {?>
                  </li>
                <?php }?>
              <?php } ?>
            <?php if ($_smarty_tpl->tpl_vars['block']->value['use_carousel']) {?>
              </li>
            <?php }?>
          </ul>
          <?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'min_item')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'min_item'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'Please select at least one product','js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'min_item'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php $_smarty_tpl->smarty->_tag_stack[] = array('addJsDefL', array('name'=>'max_item')); $_block_repeat=true; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'max_item'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo smartyTranslate(array('s'=>'You cannot add more than %d product(s) to the product comparison','sprintf'=>$_smarty_tpl->tpl_vars['comparator_max_item']->value,'js'=>1),$_smarty_tpl);?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo $_smarty_tpl->smarty->registered_plugins['block']['addJsDefL'][0][0]->addJsDefL(array('name'=>'max_item'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('comparator_max_item'=>$_smarty_tpl->tpl_vars['comparator_max_item']->value),$_smarty_tpl);?>

          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['addJsDef'][0][0]->addJsDef(array('comparedProductsIds'=>$_smarty_tpl->tpl_vars['compared_products']->value),$_smarty_tpl);?>

        <?php } else { ?>
          <p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'No products in this category.','mod'=>'tmcategoryproducts'),$_smarty_tpl);?>
</p>
        <?php }?>
      </div>
    </section>
    <?php if ($_smarty_tpl->tpl_vars['block']->value['use_carousel']) {?>
    
      <script type="text/javascript">
        $(document).ready(function() {
          setNbCatItems(<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_nb'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
);
          tmCategoryCarousel<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
 = $('#block-category-<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
  ul.product_list').bxSlider({
            responsive: true,
            useCSS: false,
            minSlides: carousel_nb_new,
            maxSlides: carousel_nb_new,
            slideWidth: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_slide_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            slideMargin: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_slide_margin'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            infiniteLoop: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_loop'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            hideControlOnEnd: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_hide_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            randomStart: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_random'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            moveSlides: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_item_scroll'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            pager: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_pager'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            autoHover: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_hover'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            auto: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            speed: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            pause: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_pause'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            controls: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            autoControls: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            startText: '',
            stopText: '',
          });
          var tm_cps_doit;
          $(window).resize(function() {
            clearTimeout(tm_cps_doit);
            tm_cps_doit = setTimeout(function() {
              resizedwtm_cps<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
();
            }, 201);
          });
        });
        function resizedwtm_cps<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
(){
          setNbCatItems(<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_nb'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
);
          tmCategoryCarousel<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block_identificator']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
.reloadSlider({
            responsive: true,
            useCSS: false,
            minSlides: carousel_nb_new,
            maxSlides: carousel_nb_new,
            slideWidth: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_slide_width'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            slideMargin: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_slide_margin'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            infiniteLoop: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_loop'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            hideControlOnEnd: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_hide_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            randomStart: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_random'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            moveSlides: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_item_scroll'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            pager: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_pager'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            autoHover: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_hover'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            auto: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            speed: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_speed'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            pause: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_pause'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            controls: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            autoControls: <?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['carousel_settings']['carousel_auto_control'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
,
            startText: '',
            stopText: '',
          });
        }
        function setNbCatItems(countItems) {
          var catBlock = $('.category-block').width();
          if (catBlock < 400)
            carousel_nb_new = 1;
          if (catBlock >= 400)
            carousel_nb_new = 2;
          if (catBlock >= 580)
            carousel_nb_new = 3;
          if (catBlock > 850)
            carousel_nb_new = countItems;
        }
      </script>
    
    <?php }?>
  <?php } ?>
<?php }?><?php }} ?>
