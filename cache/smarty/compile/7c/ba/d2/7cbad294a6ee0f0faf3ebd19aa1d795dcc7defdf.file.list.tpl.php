<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:37
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegamenu\views\templates\admin\list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1709459b7f7fd5705c7-55771785%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cbad294a6ee0f0faf3ebd19aa1d795dcc7defdf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegamenu\\views\\templates\\admin\\list.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1709459b7f7fd5705c7-55771785',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'url_enable' => 0,
    'tabs' => 0,
    'tab' => 0,
    'html_items' => 0,
    'item' => 0,
    'links' => 0,
    'banners' => 0,
    'image_baseurl' => 0,
    'videos' => 0,
    'maps' => 0,
    'top_level_menu' => 0,
    'top_level_menu_li' => 0,
    'top_level_menu_li_hover' => 0,
    'top_level_badge' => 0,
    'top_level_menu_li_a' => 0,
    'top_level_menu_li_a_hover' => 0,
    'first_level_menu' => 0,
    'first_level_menu_li' => 0,
    'first_level_menu_li_a' => 0,
    'first_level_menu_li_a_hover' => 0,
    'next_level_menu' => 0,
    'next_level_menu_li' => 0,
    'next_level_menu_li_a' => 0,
    'next_level_menu_li_a_hover' => 0,
    'theme_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fee89786_65613921',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fee89786_65613921')) {function content_59b7f7fee89786_65613921($_smarty_tpl) {?>

<div id="tmmegamenu-configurations-buttons" class="panel clearfix">
    <button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#myModal"><i class="process-icon-edit"></i> <?php echo smartyTranslate(array('s'=>'Edit styles','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</button>
    <a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addItem">
    	<i class="process-icon-new"></i> <?php echo smartyTranslate(array('s'=>'Add item','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
    <a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addHtml">
    	<i class="icon-html5"></i> <?php echo smartyTranslate(array('s'=>'Add HTML','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
    <a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addLink">
    	<i class="icon-link"></i> <?php echo smartyTranslate(array('s'=>'Add link','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
	<a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addBanner">
    	<i class="icon-image"></i> <?php echo smartyTranslate(array('s'=>'Add banner','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
	<a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addVideo">
    	<i class="icon-youtube"></i> <?php echo smartyTranslate(array('s'=>'Add video','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
	<a class="btn btn-sm btn-success" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addMap">
    	<i class="icon-map-marker"></i> <?php echo smartyTranslate(array('s'=>'Add map','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    </a>
</div>
<div class="panel tmmegamenu">
    <h3>
        <?php echo smartyTranslate(array('s'=>'Items list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['tabs']->value)&&count($_smarty_tpl->tpl_vars['tabs']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['tabs']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
            <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addItem">
                <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                    <i class="process-icon-new"></i>
                </span>
            </a>
        </span>
    </h3>
    <?php if (isset($_smarty_tpl->tpl_vars['tabs']->value)&&$_smarty_tpl->tpl_vars['tabs']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table tablist table-striped">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'Item id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Item name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Item code','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Sort order','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Specific class','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Badge','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Status','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['tab'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tab']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tab']->key => $_smarty_tpl->tpl_vars['tab']->value) {
$_smarty_tpl->tpl_vars['tab']->_loop = true;
?>
                        <tr id="item_<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td class="pointer dragHandle center"><div class="dragGroup"><div class="positions"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['sort_order'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</div></div></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['tab']->value['specific_class']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>-<?php }?></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['tab']->value['badge']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['badge'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>-<?php }?></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['tab']->value['is_mega']) {?><?php echo smartyTranslate(array('s'=>'Is mega','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<?php } elseif ($_smarty_tpl->tpl_vars['tab']->value['is_simple']) {?><?php echo smartyTranslate(array('s'=>'Is simple','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<?php } else { ?>-<?php }?></td>
                            <td>
                                <a class="list-action-enable<?php if ($_smarty_tpl->tpl_vars['tab']->value['active']) {?> action-enabled<?php } else { ?> action-disabled<?php }?>" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&updateItemStatus&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
&itemstatus=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['active'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php if ($_smarty_tpl->tpl_vars['tab']->value['active']) {?><?php echo smartyTranslate(array('s'=>'Enabled','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Disabled','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<?php }?>">
                                    <i class="icon-check<?php if (!$_smarty_tpl->tpl_vars['tab']->value['active']) {?> hidden<?php }?>"></i>
                                    <i class="icon-remove<?php if ($_smarty_tpl->tpl_vars['tab']->value['active']) {?> hidden<?php }?>"></i>
                                </a>
                            </td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editItem&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteItem&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tab']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="panel tmmegamenu-html">
	<h3>
        <?php echo smartyTranslate(array('s'=>'HTML list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['html_items']->value)&&count($_smarty_tpl->tpl_vars['html_items']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['html_items']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
        <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addHtml">
            <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                <i class="process-icon-new"></i>
            </span>
        </a>
    </span>
    </h3>
	<?php if (isset($_smarty_tpl->tpl_vars['html_items']->value)&&$_smarty_tpl->tpl_vars['html_items']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'HTML id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'HTML name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Specific class','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['html_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['html']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['html']['iteration']++;
?>
                        <tr <?php if (!($_smarty_tpl->getVariable('smarty')->value['foreach']['html']['iteration'] % 2)) {?>class = "odd"<?php }?>>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php if ($_smarty_tpl->tpl_vars['item']->value['specific_class']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>-<?php }?></td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editHtml&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteHtml&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="panel tmmegamenu-link">
	<h3>
        <?php echo smartyTranslate(array('s'=>'Links list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['links']->value)&&count($_smarty_tpl->tpl_vars['links']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['links']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
        <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addLink">
            <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                <i class="process-icon-new"></i>
            </span>
        </a>
    </span>
    </h3>
	<?php if (isset($_smarty_tpl->tpl_vars['links']->value)&&$_smarty_tpl->tpl_vars['links']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'Link id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Link name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Specific class','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'URL','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Target blank','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['links']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']++;
?>
                        <tr <?php if (!($_smarty_tpl->getVariable('smarty')->value['foreach']['link']['iteration'] % 2)) {?>class = "odd"<?php }?>>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php if ($_smarty_tpl->tpl_vars['item']->value['specific_class']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>-<?php }?></td>
                            <td><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['item']->value['blank']) {?>true<?php } else { ?>false<?php }?></td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editLink&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteLink&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="panel tmmegamenu-link">
	<h3>
        <?php echo smartyTranslate(array('s'=>'Banners list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['banners']->value)&&count($_smarty_tpl->tpl_vars['banners']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['banners']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
        <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addBanner">
            <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                <i class="process-icon-new"></i>
            </span>
        </a>
    </span>
    </h3>
	<?php if (isset($_smarty_tpl->tpl_vars['banners']->value)&&$_smarty_tpl->tpl_vars['banners']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'Banner id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Banner name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Specific class','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'URL','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Target blank','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banners']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']++;
?>
                        <tr <?php if (!($_smarty_tpl->getVariable('smarty')->value['foreach']['link']['iteration'] % 2)) {?>class = "odd"<?php }?>>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><img class="banner-thumbnail" src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['image_baseurl']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" /></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['item']->value['specific_class']) {?><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['specific_class'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
<?php } else { ?>-<?php }?></td>
                            <td><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></td>
                            <td><?php if ($_smarty_tpl->tpl_vars['item']->value['blank']) {?>true<?php } else { ?>false<?php }?></td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editBanner&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteBanner&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="panel tmmegamenu-video">
	<h3>
        <?php echo smartyTranslate(array('s'=>'Videos list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['videos']->value)&&count($_smarty_tpl->tpl_vars['videos']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['videos']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
        <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addVideo">
            <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                <i class="process-icon-new"></i>
            </span>
        </a>
    </span>
    </h3>
	<?php if (isset($_smarty_tpl->tpl_vars['videos']->value)&&$_smarty_tpl->tpl_vars['videos']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'Video id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Video name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Video','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'URL','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['videos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']++;
?>
                        <tr <?php if (!($_smarty_tpl->getVariable('smarty')->value['foreach']['link']['iteration'] % 2)) {?>class = "odd"<?php }?>>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td>
                            	<?php if ($_smarty_tpl->tpl_vars['item']->value['type']=='youtube') {?>
                                    <iframe type="text/html" 
                                        src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
?enablejsapi=1&version=3&html5=1&wmode=transparent"
                                        frameborder="0"
                                        wmode="Opaque"></iframe>
                                <?php } else { ?>
                                    <iframe 
                                        src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"
                                        frameborder="0"
                                        webkitAllowFullScreen
                                        mozallowfullscreen
                                        allowFullScreen>
                                    </iframe>
                                <?php }?>
                            </td>
                            <td><a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</a></td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editVideo&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteVideo&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="panel tmmegamenu-map">
	<h3>
        <?php echo smartyTranslate(array('s'=>'Maps list','mod'=>'tmmegamenu'),$_smarty_tpl);?>

        <span class="badge"><?php if (isset($_smarty_tpl->tpl_vars['maps']->value)&&count($_smarty_tpl->tpl_vars['maps']->value)) {?><?php echo count($_smarty_tpl->tpl_vars['maps']->value);?>
<?php } else { ?>0<?php }?></span>
        <span class="panel-heading-action">
        <a class="list-toolbar-btn" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&addMap">
            <span class="label-tooltip" data-placement="top" data-html="true" data-original-title="Add new" data-toggle="tooltip" title="">
                <i class="process-icon-new"></i>
            </span>
        </a>
    </span>
    </h3>
	<?php if (isset($_smarty_tpl->tpl_vars['maps']->value)&&$_smarty_tpl->tpl_vars['maps']->value) {?>
        <div class="table-responsive-row clearfix">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo smartyTranslate(array('s'=>'Map id','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Map name','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Latitude','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th><?php echo smartyTranslate(array('s'=>'Longitude','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['maps']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['link']['iteration']++;
?>
                        <tr <?php if (!($_smarty_tpl->getVariable('smarty')->value['foreach']['link']['iteration'] % 2)) {?>class = "odd"<?php }?>>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['latitude'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['longitude'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</td>
                            <td>
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="edit btn btn-default" title="<?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&editMap&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-pencil"></i> <?php echo smartyTranslate(array('s'=>'Edit','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-caret-down"></i>&nbsp;
                                    </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="delete" title="<?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_enable']->value, ENT_QUOTES, 'UTF-8', true);?>
&deleteMap&id_item=<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id_item'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"><i class="icon-trash"></i> <?php echo smartyTranslate(array('s'=>'Delete','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
    	<?php echo smartyTranslate(array('s'=>'There is no item yet.','mod'=>'tmmegamenu'),$_smarty_tpl);?>

    <?php }?>
</div>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo smartyTranslate(array('s'=>'Menu\'s general styles','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
            </div>
            <div class="modal-body">
                <div class="form-wrapper">
                	<form id="tmmegamenu-style" class="form-horizontal" action="" method="post">
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper opened">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                    	                            </div>
                        	                    </div>
                            	            </div>
                                	    </div>
                               		</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                       <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                </select>
                                	            <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	    </div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                 	               <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                          	                      <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                	                <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                           		<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['top_level_menu']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="top-level-menu" />
                        	</div>
                    	</fieldset>
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level element','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                       		<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                               		<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlmlbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlmlbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                       		</div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                               		<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                               		</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                               		<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-right-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                       		</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                       		</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="top-level-menu-li" />
                        	</div>
                    	</fieldset>
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level element','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<span><?php echo smartyTranslate(array('s'=>'(hover & active)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                   		<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                   		<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlmlhbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlmlhbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                <label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                               		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                   		<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-right-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                               		<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                           		<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                           		<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                       		<div class="col-lg-3">
                                            	<input class="form-control" name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_hover']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="top-level-menu-li:hover" />
                            	<input type="hidden" class="classes" value="top-level-menu-li.sfHover, .tmmegamenu_item.top-level-menu-li.sfHoverForce" />
                        	</div>
                    	</fieldset>
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level badge','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                       		<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlbbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlbbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                               		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                               		<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                           		<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                               		<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                           		<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                               		<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                           		</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                           		<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-right-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                               		</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_badge']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['top_level_badge']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_badge']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="top-level-badge" />
                        	</div>
                    	</fieldset>
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                            		<div class="col-lg-10">
                                		<div class="form-group">
                                    		<div class="col-lg-4">
                                        		<div class="row">
                                            		<div class="input-group">
                                                		<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            		</div>
                                       			</div>
                                    		</div>
                                		</div>
                            		</div>
                        		</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                            		<div class="col-lg-10">
                                		<div class="form-group">
                                    		<div class="col-lg-4">
                                        		<div class="row">
                                            		<div class="input-group">
                                                		<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            		</div>
                                        		</div>
                                    		</div>
                                		</div>
                            		</div>
                        		</div>
                        		<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlmlabgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlmlabgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                            		<div class="col-lg-10">
                                		<div class="form-group no-indent">
                                    		<div class="col-lg-3">
                                        		<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    		</div>
                                    		<div class="col-lg-3">
                                        	<div class="input-group">
                                           		<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                        	</div>
                                        	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	</div>
                                    	<div class="col-lg-3">
                                        	<div class="input-group">
                                            	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                        	</div>
                                        	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	</div>
                                    	<div class="col-lg-3">
                                        	<div class="input-group">
                                            	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                        	</div>
                                        	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	</div>
                                	</div>
                            	</div>
                        	</div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                    <div class="col-lg-10">
                                        <div class="form-group no-indent">
                                            <div class="col-lg-3">
                                                <select name="border-top-style">
                                                    <option></option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                </select>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <select name="border-right-style">
                                                    <option></option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                </select>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <select name="border-bottom-style">
                                                    <option></option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                </select>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <select name="border-left-style">
                                                    <option></option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                    <option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                </select>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                    <div class="col-lg-10">
                                        <div class="form-group no-indent">
                                            <div class="col-lg-3">
                                                <input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                    <div class="col-lg-10">
                                        <div class="form-group no-indent">
                                            <div class="col-lg-3">
                                                <input class="form-control" name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                            <div class="col-lg-3">
                                                <input class="form-control" name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="mainclass" value="top-level-menu-li-a" />
                                <input type="hidden" class="classes" value="menu-title" />
                            </div>
                        </fieldset>
                    	<fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Top level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<span><?php echo smartyTranslate(array('s'=>'(hover & active)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                               		</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="tlmlahbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_image'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="tlmlahbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                               		<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                               		<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                               		<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                       		<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-right-style">
                                               		<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                               		<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                   		</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['top_level_menu_li_a_hover']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="top-level-menu-li-a:hover" />
                            	<input type="hidden" class="classes" value="top-level-menu-li.sfHover > a, .tmmegamenu_item.top-level-menu-li.sfHoverForce > a, .tmmegamenu_item.menu-title.active" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'First level','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="flmbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_image'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="flmbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                    	                            </div>
                        	                    </div>
                            	            </div>
                                	    </div>
                               		</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                       <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                </select>
                                	            <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	    </div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                 	               <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                          	                      <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                	                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                           		<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['first_level_menu']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="first-level-menu" />
                                <input type="hidden" class="classes" value="top_menu > ul > li ul.is-simplemenu, .tmmegamenu_item.top_menu > ul > li ul.is-simplemenu ul, .tmmegamenu_item.column_menu > ul > li ul.is-simplemenu, .tmmegamenu_item.column_menu > ul > li ul.is-simplemenu ul" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'First level element','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['background_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="first-level-menu-li" />
                                <input type="hidden" class="classes" value="top_menu li > li, .tmmegamenu_item.column_menu li > li" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'First level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['background_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="first-level-menu-li-a" />
                                <input type="hidden" class="classes" value="top_menu li li a, .tmmegamenu_item.column_menu li li a" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'First level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<span><?php echo smartyTranslate(array('s'=>'(hover & active)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['background_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['first_level_menu_li_a_hover']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="first-level-menu-li-a:hover" />
                                <input type="hidden" class="classes" value="top_menu li li.sfHover > a, .tmmegamenu_item.top_menu li li:hover > a, .tmmegamenu_item.top_menu li li.sfHoverForce > a, .tmmegamenu_item.column_menu li li.sfHover > a, .tmmegamenu_item.column_menu li li:hover > a, .tmmegamenu_item.column_menu li li.sfHoverForce > a" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Next level','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background Image','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-10">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input disabled="disabled" data-name="bgimg" class="form-control" name="background-image" id="nlmbgimg" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_image'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_image']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['background_image'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                    	<span class="input-group-addon"><a href="#" class="clear-image"><span class="icon-remove"></span></a></span>
                                                        <span class="input-group-addon"><a href="#" class="clear-image-none">none</a></span>
                                    					<span class="input-group-addon"><a href="filemanager/dialog.php?type=1&field_id=tlbgimg" data-input-name="nlmbgimg" type="button" class="iframe-btn"><span class="icon-file"></span></a></span>
                    	                            </div>
                        	                    </div>
                            	            </div>
                                	    </div>
                               		</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background settings','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="background-repeat">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat']=='no-repeat') {?>selected="selected"<?php }?> value="no-repeat"><?php echo smartyTranslate(array('s'=>'no-repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat']=='repeat-x') {?>selected="selected"<?php }?> value="repeat-x"><?php echo smartyTranslate(array('s'=>'repeat-x','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat']=='repeat-y') {?>selected="selected"<?php }?> value="repeat-y"><?php echo smartyTranslate(array('s'=>'repeat-y','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                       <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_repeat']=='repeat') {?>selected="selected"<?php }?> value="repeat"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                </select>
                                	            <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'repeat','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                    	    </div>
                                        	<div class="col-lg-3">
                                            	<select name="background-position">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='center center') {?>selected="selected"<?php }?> value="center center"><?php echo smartyTranslate(array('s'=>'center center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                 	               <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='center top') {?>selected="selected"<?php }?> value="center top"><?php echo smartyTranslate(array('s'=>'center top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='center bottom') {?>selected="selected"<?php }?> value="center bottom"><?php echo smartyTranslate(array('s'=>'center bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='left top') {?>selected="selected"<?php }?> value="left top"><?php echo smartyTranslate(array('s'=>'left top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                          	                      <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='left center') {?>selected="selected"<?php }?> value="left center"><?php echo smartyTranslate(array('s'=>'left center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            	                    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='left bottom') {?>selected="selected"<?php }?> value="left bottom"><?php echo smartyTranslate(array('s'=>'left bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                	                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='right top') {?>selected="selected"<?php }?> value="right top"><?php echo smartyTranslate(array('s'=>'right top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                    	            <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='right center') {?>selected="selected"<?php }?> value="right center"><?php echo smartyTranslate(array('s'=>'right center','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                        	        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['background_position'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['background_position']=='right bottom') {?>selected="selected"<?php }?> value="right bottom"><?php echo smartyTranslate(array('s'=>'right bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'position','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border radius (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_right_radius'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-right-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_right_radius'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_right_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                           		<input class="form-control" data-name='px' name="border-bottom-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_left_radius'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-left-radius" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_left_radius'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_left_radius']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['border_top_left_radius'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Box shadow','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-6">
                                            	<input data-name="shdw" class="form-control" name="box-shadow" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu']->value['box_shadow'])&&$_smarty_tpl->tpl_vars['next_level_menu']->value['box_shadow']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu']->value['box_shadow'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'example:','mod'=>'tmmegamenu'),$_smarty_tpl);?>
 0px 0px 0px 0px rgba(0,0,0,0.75)</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="next-level-menu" />
                                <input type="hidden" class="classes" value="top_menu > ul > li ul.is-simplemenu ul, .tmmegamenu_item.column_menu > ul > li ul.is-simplemenu ul" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Next level element','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['background_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="next-level-menu-li" />
                                <input type="hidden" class="classes" value="top_menu li li > li, .tmmegamenu_item.column_menu li li > li" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Next level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['background_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="next-level-menu-li-a" />
                                <input type="hidden" class="classes" value="top_menu li li li a, .tmmegamenu_item.column_menu li li li a" />
                        	</div>
                    	</fieldset>
                        <fieldset>
                        	<h4><?php echo smartyTranslate(array('s'=>'Next level element link','mod'=>'tmmegamenu'),$_smarty_tpl);?>
<span><?php echo smartyTranslate(array('s'=>'(hover & active)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</span></h4>
                        	<div class="fieldset-content-wrapper closed">
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Background color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group">
                                        	<div class="col-lg-4">
                                            	<div class="row">
                                                	<div class="input-group">
                                                    	<input type="color" data-hex="true" name="background-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['background_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['background_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['background_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                	</div>
                                            	</div>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border color','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                    <input type="color" data-hex="true" name="border-top-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                                </div>
                                                <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-right-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-bottom-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<div class="input-group">
                                                	<input type="color" data-hex="true" name="border-left-color" class="form-control color mColorPickerInput" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_color'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_color']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_color'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	</div>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border type','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<select name="border-top-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
      	    	                                    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
        	                                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                	                                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                    	                        </select>
                        	                    <p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                            	            </div>
                                	        <div class="col-lg-3">
                                    	        <select name="border-right-style">
                                        	        <option></option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                      		        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	    <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-bottom-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                         	                        <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                            		                <option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                             	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<select name="border-left-style">
                                                	<option></option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style']=='solid') {?>selected="selected"<?php }?> value="solid"><?php echo smartyTranslate(array('s'=>'solid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style']=='dotted') {?>selected="selected"<?php }?> value="dotted"><?php echo smartyTranslate(array('s'=>'dotted','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style']=='dashed') {?>selected="selected"<?php }?> value="dashed"><?php echo smartyTranslate(array('s'=>'dashed','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                                	<option <?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_style']=='double') {?>selected="selected"<?php }?> value="double"><?php echo smartyTranslate(array('s'=>'double','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</option>
                                            	</select>
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<div class="form-group">
                                	<label class="control-label col-lg-2"><?php echo smartyTranslate(array('s'=>'Border width (px, em)','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</label>
                                	<div class="col-lg-10">
                                    	<div class="form-group no-indent">
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-top-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_top_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'top','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-right-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_right_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'right','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-bottom-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_bottom_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'bottom','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                        	<div class="col-lg-3">
                                            	<input class="form-control" data-name='px' name="border-left-width" value="<?php if (isset($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_width'])&&$_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_width']!='') {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_level_menu_li_a_hover']->value['border_left_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>" />
                                            	<p class="help-block no-indent"><?php echo smartyTranslate(array('s'=>'left','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</p>
                                        	</div>
                                    	</div>
                                	</div>
                            	</div>
                            	<input type="hidden" class="mainclass" value="next-level-menu-li-a:hover" />
                                <input type="hidden" class="classes" value="top_menu li li li.sfHover > a, .tmmegamenu_item.top_menu li li li:hover > a, .tmmegamenu_item.top_menu li li li.sfHoverForce > a, .tmmegamenu_item.column_menu li li li.sfHover > a, .tmmegamenu_item.column_menu li li li:hover > a, .tmmegamenu_item.column_menu li li li.sfHoverForce > a" />
                        	</div>
                    	</fieldset>
                    	<input type="hidden" name="cssname" value="megamenu_custom_styles" />
                	</form>
            	</div>
            </div>
            <div class="modal-footer clearfix">
                <button id="generate-styles" class="btn btn-sm btn-success" ><?php echo smartyTranslate(array('s'=>'Generate styles','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</button>
                <button id="reset-styles" class="btn btn-sm btn-danger pull-left" ><?php echo smartyTranslate(array('s'=>'Reset styles','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo smartyTranslate(array('s'=>'Close','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</button>
            </div>
        </div>
        <div class="modal-loader"><span class="loader-gif"></span></div>
    </div>
</div>
<script type="text/javascript">
	var warning_class_text = '<?php echo smartyTranslate(array('s'=>'Not all fields are valid','mod'=>'tmmegamenu'),$_smarty_tpl);?>
';
    var theme_url = '<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
';
</script><?php }} ?>
