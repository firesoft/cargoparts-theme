<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 11:06:37
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmmegalayout\views\templates\admin\tmmegalayout-layout-content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:638559b7f7fd031772-56332737%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fb4f244acf1393db372ea08f67cd63895c898386' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmmegalayout\\views\\templates\\admin\\tmmegalayout-layout-content.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '638559b7f7fd031772-56332737',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7f7fd057205_97461583',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7f7fd057205_97461583')) {function content_59b7f7fd057205_97461583($_smarty_tpl) {?>

<div <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layout'])) {?>data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"<?php }?> class="tmmegalayout-admin container">
  <?php if (isset($_smarty_tpl->tpl_vars['content']->value['layout'])) {?>
    <div class="tmlayout-row">
      <span class="tmmlmegalayout-layout-name"><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['layout_name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</span>
      <a data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" href="#" class="edit-layout"></a>
      <a data-layout-id="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" href="#" class="remove-layout"></a>
    </div>
    <article class="inner">
      <?php echo preg_replace("%(?<!\\\\)'%", "\'",$_smarty_tpl->tpl_vars['content']->value['layout']);?>

      <p class="add-buttons">
        <span class="col-xs-12 col-sm-6 add-but">
          <a href="#" class="btn add-wrapper min-level">+ <?php echo smartyTranslate(array('s'=>'Add wrapper','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
        </span>
        <span class="col-xs-12 col-sm-6 add-but">
          <a href="#" class="btn add-row  min-level">+ <?php echo smartyTranslate(array('s'=>'Add row','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</a>
        </span>
      </p>
    </article>
    <input type="hidden" name="tmml_id_layout" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['content']->value['id_layout'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
  <?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['content']->value['layouts_list']) {?>
      <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'Select a layout','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</p>
    <?php } else { ?>
      <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'Add a layout','mod'=>'tmmegalayout'),$_smarty_tpl);?>
</p>
    <?php }?>
  <?php }?>
</div><?php }} ?>
