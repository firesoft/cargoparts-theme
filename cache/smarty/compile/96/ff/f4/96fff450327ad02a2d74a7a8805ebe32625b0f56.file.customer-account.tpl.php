<?php /* Smarty version Smarty-3.1.19, created on 2017-09-12 09:49:39
         compiled from "C:\xampp\htdocs\prestashop_1.6\modules\tmheaderaccount\views\templates\hook\customer-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:635259b7e5f3cb43b8-87108284%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '96fff450327ad02a2d74a7a8805ebe32625b0f56' => 
    array (
      0 => 'C:\\xampp\\htdocs\\prestashop_1.6\\modules\\tmheaderaccount\\views\\templates\\hook\\customer-account.tpl',
      1 => 1504802236,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '635259b7e5f3cb43b8-87108284',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'f_status' => 0,
    'link' => 0,
    'facebook_id' => 0,
    'g_status' => 0,
    'back' => 0,
    'google_id' => 0,
    'vk_status' => 0,
    'vkcom_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59b7e5f3e5aa91_13950336',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59b7e5f3e5aa91_13950336')) {function content_59b7e5f3e5aa91_13950336($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
  <li>
    <a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklink',array(),true);?>
" title="<?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
      <i class="fa fa-facebook"></i>
      <span><?php if (!$_smarty_tpl->tpl_vars['facebook_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Facebook','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?></span>
    </a>
  </li>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
  <li>
    <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array(),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
      <i class="fa fa-google"></i>
      <span><?php if (!$_smarty_tpl->tpl_vars['google_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Google','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?></span>
    </a>
  </li>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
  <li>
    <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true);?>
" <?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array(),true);?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
      <i class="fa fa-vk"></i>
      <span><?php if (!$_smarty_tpl->tpl_vars['vkcom_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With VK','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?></span>
    </a>
  </li>
<?php }?>
<?php }} ?>
