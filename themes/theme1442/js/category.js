$(document).on('click', '.lnk_more', function(e) {
  e.preventDefault();
  $('#category_description_short').hide();
  $('#category_description_full').show();
  $(this).hide();
});
$(document).ready(function() {
  $('#subcategories').find('ul').slick({
    // dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 8,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });
});