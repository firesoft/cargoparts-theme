/**
 * 2002-2016 TemplateMonster
 *
 * TM Mega Layout
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster (Alexander Grosul & Alexander Pervakov)
 *  @copyright 2002-2016 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

var isMobile = false;
$(document).ready(function() {
  mobileDropdown();
  if ($(document).width() + compensante < 1200) {
    isMobile = true;
  } else {
    stickUp(true);
  }
  $(window).resize(function() {
    if ($(document).width() + compensante < 1200 && !isMobile) {
      isMobile = true;
      stickUp(false);
    } else if ($(document).width() + compensante > 1199 && isMobile) {
      isMobile = false;
      stickUp(true);
    }
  });
  featuredCarousel();
  smartBlogCarousel();
});
function stickUp(isStuck) {
  var stickUp = $(".stick-up");
  if (stickUp.length) {
    if (isStuck) {
      stickUp.wrap('<div class="stickUpTop"><div class="stickUpHolder">');
      $('.stickUpTop').tmStickUp();
    } else if ($('.stickUpTop').length) {
      var pseudoBlock = $('.pseudoStickyBlock');
      if (pseudoBlock.length) {
        pseudoBlock.remove();
      }
      stickUp.unwrap().unwrap();
    }
  }
}
/* Stik Up menu script */
(function($) {
  $.fn.tmStickUp = function(options) {
    var getOptions = {
      correctionSelector: $('.correctionSelector')
    };
    $.extend(getOptions, options);
    var
      _this = $(this)
      , _window = $(window)
      , _document = $(document)
      , thisOffsetTop = 0
      , thisOuterHeight = 0
      , thisMarginTop = 0
      , thisPaddingTop = 0
      , documentScroll = 0
      , pseudoBlock
      , lastScrollValue = 0
      , scrollDir = ''
      , tmpScrolled
    ;
    init();
    function init() {
      thisOffsetTop = parseInt(_this.offset().top);
      thisMarginTop = parseInt(_this.css('margin-top'));
      thisOuterHeight = parseInt(_this.outerHeight(true));
      $('<div class="pseudoStickyBlock"></div>').insertAfter(_this);
      pseudoBlock = $('.pseudoStickyBlock');
      pseudoBlock.css({'position': 'relative', 'display': 'block'});
      addEventsFunction();
    }//end init
    function addEventsFunction() {
      var isS = false;
      var stuck;
      _document.on('scroll', function() {
        tmpScrolled = $(this).scrollTop();
        if (tmpScrolled > lastScrollValue) {
          scrollDir = 'down';
        } else {
          scrollDir = 'up';
        }
        lastScrollValue = tmpScrolled;
        correctionValue = getOptions.correctionSelector.outerHeight(true);
        documentScroll = parseInt(_window.scrollTop());
        stuck = thisOffsetTop - correctionValue + 250 < documentScroll;
        if (stuck && !isS) {
          _this.addClass('isStuck');
          _this.css({position: 'fixed', top: correctionValue});
          pseudoBlock.css({'height': thisOuterHeight});
          isS = true;
        } else if (!stuck && isS) {
          _this.css({top: -70});
          setTimeout(function() {
            _this.removeClass('isStuck');
            _this.css({position: 'relative', top: 0});
            pseudoBlock.css({'height': 0});
          }, 100);
          isS = false;
        }
      }).trigger('scroll');
    }
  };//end tmStickUp function
})(jQuery);
function mobileDropdown() {
  var elementClick = $('.dropdown > .toggle'),
    elementSlide = $('.dropdown > .toogle_content');
  elementClick.on("click", function() {
    elementClick.toggleClass("active");
    if (elementClick.hasClass('active')) {
      elementSlide.slideDown();
    } else {
      elementSlide.slideUp();
    }
  });
  $(document).mouseup(function(e) {
    if (isMobile) {
      var container = $('.dropdown');
      if (container.has(e.target).length === 0) {
        elementSlide.slideUp();
        elementClick.removeClass("active");
      }
    }
  });
}
function featuredCarousel() {
  countItemsFeatured();
  var featuredCarousel = $('.featured-carousel #homefeatured');
  if (featuredCarousel.length && !!$.prototype.bxSlider) {
    featured_slider = featuredCarousel.bxSlider({
      minSlides: featured_carousel_items,
      maxSlides: featured_carousel_items,
      slideWidth: 500,
      slideMargin: 0,
      pager: false,
      nextText: '',
      prevText: '',
      moveSlides: 1,
      infiniteLoop: true,
      hideControlOnEnd: true,
      responsive: true,
      useCSS: false,
      autoHover: false,
      speed: 500,
      pause: 3000,
      controls: true,
      autoControls: false
    });
  }
  $(window).resize(function() {
    if (featuredCarousel.length) {
      resizeFeatured()
    }
  });
}
function resizeFeatured() {
  countItemsFeatured();
  featured_slider.reloadSlider({
    minSlides: featured_carousel_items,
    maxSlides: featured_carousel_items,
    slideWidth: 500,
    slideMargin: 0,
    pager: false,
    nextText: '',
    prevText: '',
    moveSlides: 1,
    infiniteLoop: true,
    hideControlOnEnd: true,
    responsive: true,
    useCSS: false,
    autoHover: false,
    speed: 500,
    pause: 3000,
    controls: true,
    autoControls: false
  });
}
function countItemsFeatured() {
  var featured = $('.featured-carousel').width();
  if (featured < 450) {
    featured_carousel_items = 1;
  }
  if (featured >= 450) {
    featured_carousel_items = 2;
  }
  if (featured >= 900) {
    featured_carousel_items = 3;
  }
  if (featured > 1500) {
    featured_carousel_items = 4;
  }
}
function smartBlogCarousel() {
  countItemsBlog();
  var blogCarousel = $('.blog-carousel #homepage-blog ul.row');
  if (blogCarousel.length && !!$.prototype.bxSlider) {
    blog_slider = blogCarousel.bxSlider({
      minSlides: blog_carousel_items,
      maxSlides: blog_carousel_items,
      slideWidth: 500,
      slideMargin: 30,
      pager: false,
      nextText: '',
      prevText: '',
      moveSlides: 1,
      infiniteLoop: false,
      hideControlOnEnd: true,
      responsive: true,
      useCSS: false,
      autoHover: false,
      speed: 500,
      pause: 3000,
      controls: true,
      autoControls: false
    });
  }
  $(window).resize(function() {
    if (blogCarousel.length) {
      resizeBlog()
    }
  });
}
function resizeBlog() {
  countItemsBlog();
  blog_slider.reloadSlider({
    minSlides: blog_carousel_items,
    maxSlides: blog_carousel_items,
    slideWidth: 500,
    slideMargin: 30,
    pager: false,
    nextText: '',
    prevText: '',
    moveSlides: 1,
    infiniteLoop: false,
    hideControlOnEnd: true,
    responsive: true,
    useCSS: false,
    autoHover: false,
    speed: 500,
    pause: 3000,
    controls: true,
    autoControls: false
  });
}
function countItemsBlog() {
  var blog = $('#homepage-blog').width();
  if (blog < 500) {
    blog_carousel_items = 1;
  }
  if (blog >= 500) {
    blog_carousel_items = 2;
  }
  if (blog >= 900) {
    blog_carousel_items = 3;
  }
  if (blog > 1500) {
    blog_carousel_items = 4;
  }
}