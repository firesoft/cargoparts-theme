//global variables
var responsiveflag = false;
var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
var isiPad = /iPad/i.test(navigator.userAgent);
$(document).ready(function() {
  controller = new ScrollMagic();
  highdpiInit();
  responsiveResize();
  $(window).resize(responsiveResize);
  $(window).resize(adaptiveSizeGrid);
  if (navigator.userAgent.match(/Android/i)) {
    var viewport = document.querySelector('meta[name="viewport"]');
    viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
    window.scrollTo(0, 1);
  }
  blockHover();
  if (typeof quickView !== 'undefined' && quickView) {
    quick_view();
  }
  dropDown();
  sitemapAccordion();
  counter();
  testimonialsSlider();
  toTop();
  if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product'])) {
    bindGrid();
    $(document).on('change', '.selectProductSort', function(e) {
      if (typeof request != 'undefined' && request) {
        var requestSortProducts = request;
      }
      var splitData = $(this).val().split(':');
      var url = '';
      if (typeof requestSortProducts != 'undefined' && requestSortProducts) {
        url += requestSortProducts;
        if (typeof splitData[0] !== 'undefined' && splitData[0]) {
          url += ( requestSortProducts.indexOf('?') < 0 ? '?' : '&') + 'orderby=' + splitData[0];
          if (typeof splitData[1] !== 'undefined' && splitData[1]) {
            url += '&orderway=' + splitData[1];
          }
        }
        document.location.href = url;
      }
    });
    $(document).on('change', 'select[name="n"]', function() {
      $(this.form).submit();
    });
    $(document).on('change', 'select[name="currency_payment"]', function() {
      setCurrency($(this).val());
    });
  }
  $(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function() {
    if (this.value != '') {
      location.href = this.value;
    }
  });
  $(document).on('click', '.back', function(e) {
    e.preventDefault();
    history.back();
  });
  jQuery.curCSS = jQuery.css;
  if (!!$.prototype.cluetip) {
    $('a.cluetip').cluetip({
      local: true,
      cursor: 'pointer',
      dropShadow: false,
      dropShadowSteps: 0,
      showTitle: false,
      tracking: true,
      sticky: false,
      mouseOutClose: true,
      fx: {
        open: 'fadeIn',
        openSpeed: 'fast'
      }
    }).css('opacity', 0.8);
  }
  if (typeof(FancyboxI18nClose) !== 'undefined' && typeof(FancyboxI18nNext) !== 'undefined' && typeof(FancyboxI18nPrev) !== 'undefined' && !!$.prototype.fancybox) {
    $.extend($.fancybox.defaults.tpl, {
      closeBtn: '<a title="' + FancyboxI18nClose + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
      next: '<a title="' + FancyboxI18nNext + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
      prev: '<a title="' + FancyboxI18nPrev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
    });
  }
  // Close Alert messages
  $('.alert.alert-danger').on('click', this, function(e) {
    if (e.offsetX >= 16 && e.offsetX <= 39 && e.offsetY >= 16 && e.offsetY <= 34) {
      $(this).fadeOut();
    }
  });
});
function highdpiInit() {
  if (typeof highDPI === 'undefined') {
    return;
  }
  if (highDPI && $('.replace-2x').css('font-size') == '1px') {
    var els = $('img.replace-2x').get();
    for (var i = 0; i < els.length; i++) {
      src = els[i].src;
      extension = src.substr((src.lastIndexOf('.') + 1));
      src = src.replace('.' + extension, '2x.' + extension);
      var img = new Image();
      img.src = src;
      img.height != 0 ? els[i].src = src : els[i].src = els[i].src;
    }
  }
}
// Used to compensante Chrome/Safari bug (they don't care about scroll bar for width)
function scrollCompensate() {
  var inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';
  var outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);
  document.body.appendChild(outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;
  if (w1 == w2) {
    w2 = outer.clientWidth;
  }
  document.body.removeChild(outer);
  return (w1 - w2);
}
function responsiveResize() {
  compensante = scrollCompensate();
  if (($(window).width() + scrollCompensate()) <= 767 && responsiveflag == false) {
    accordion('enable');
    accordionFooter('enable');
    responsiveflag = true;
    if (typeof bindUniform !== 'undefined') {
      bindUniform();
    }
  } else if (($(window).width() + scrollCompensate()) >= 768) {
    accordion('disable');
    accordionFooter('disable');
    responsiveflag = false;
    if (typeof bindUniform !== 'undefined') {
      bindUniform();
    }
  }
}
function blockHover(status) {
  $(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e) {
    if ('ontouchstart' in document.documentElement) {
      return;
    }
    if ($('body').find('.container').width() >= 1170) {
      $(this).parent().addClass('hovered');
    }
  });
  $(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e) {
    if ($('body').find('.container').width() >= 1170) {
      $(this).parent().removeClass('hovered');
    }
  });
}
function quick_view() {
  $(document).on('click', '.quick-view:visible, .quick-view-mobile:visible', function(e) {
    e.preventDefault();
    var url = $(this).attr('data-href');
    if (!url && url == 'undefined') {
      var url = this.rel;
    }
    var anchor = '';
    if (url.indexOf('#') != -1) {
      anchor = url.substring(url.indexOf('#'), url.length);
      url = url.substring(0, url.indexOf('#'));
    }
    if (url.indexOf('?') != -1) {
      url += '&';
    } else {
      url += '?';
    }
    if (!!$.prototype.fancybox) {
      $.fancybox({
        'padding': 0,
        'width': 1021,
        'height': 500,
        'type': 'iframe',
        'tpl': {
          wrap: '<div class="fancybox-wrap fancybox-quick-view" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
        },
        'href': url + 'content_only=1' + anchor
      });
    }
  });
}
function bindGrid() {
  var storage = false;
  if (typeof(getStorageAvailable) !== 'undefined') {
    storage = getStorageAvailable();
  }
  if (!storage) {
    return;
  }
  var view = $.totalStorage('display');
  if (!view && (typeof displayList != 'undefined') && displayList) {
    view = 'list';
  }
  if (view && view != 'grid') {
    display(view);
  } else {
    $('.display').find('li#grid').addClass('selected');
  }
  $(document).on('click', '#grid, #list', function(e) {
    e.preventDefault();
    if (!$(this).hasClass('selected')) {
      display($(this).attr('id'));
    }
  });
}
function display(view) {
  if (view == 'list') {
    $('ul.product_list').removeClass('grid').addClass('list');
    $('.product_list > li:visible')
      .removeAttr('class')
      .removeAttr('style')
      .addClass('ajax_block_product');
    $('.product_list > li:visible').each(function(index, element) {
      var html = '';
      html = '<div class="product-container"><div class="row">';
      html += '<div class="left-block col-xs-4"><div class="product-image-container">';
      html += $(element).find('.product-image-container').html();
      html += '</div></div>';
      html += '<div class="center-block col-xs-4">';
      html += '<h5 itemprop="name">' + $(element).find('h5').html() + '</h5>';
      html += '<div class="product-flags">' + $(element).find('.product-flags').html() + '</div>';
      var availability = $(element).find('.availability').html();  // check : catalog mode is enabled
      if (availability != null) {
        html += '<span class="availability">' + availability + '</span>';
      }
      var reviews = $(element).find('.hook-reviews');
      if (reviews.length) {
        html += '<div class="hook-reviews">' + reviews.html() + '</div>';
      }
      html += '<p class="product-desc">' + $(element).find('.product-desc').html() + '</p>';
      var colorList = $(element).find('.color-list-container').html();
      if (colorList !== null) {
        html += '<div class="color-list-container">' + colorList + '</div>';
      }
      html += '</div>';
      html += '<div class="right-block col-xs-4">';
      var price = $(element).find('.content_price').html();  // check : catalog mode is enabled
      if (price !== null) {
        html += '<div class="content_price">' + price + '</div>';
      }
      html += '<div class="button-container">' + $(element).find('.button-container').html() + '</div>';
      var compare = $(element).find('.compare');
      if (compare.length) {
        html += '<div class="compare">' + compare.html() + '</div>';
      }
      var wishlist = $(element).find('.wishlist');
      if (wishlist.length) {
        html += '<div class="wishlist">' + wishlist.html() + '</div>';
      }
      html += '</div>';
      html += '</div></div>';
      $(element).html(html);
      // var reviews = $(element).find('.hook-reviews');
      if (reviews.length) {
        $(element).find('.button-container .hook-reviews').remove();
      }
      if (compare.length) {
        $(element).find('.functional-buttons .compare').remove();
      }
      if (wishlist.length) {
        $(element).find('.functional-buttons .wishlist').remove();
      }
    });
    $('.display').find('li#list').addClass('selected');
    $('.display').find('li#grid').removeAttr('class');
    $.totalStorage('display', 'list');
    if ($('.product_list li div.wishlist').length) {
      WishlistButton();
    }
    adaptiveSizeGrid();
  } else {
    $('ul.product_list').removeClass('list').addClass('grid');
    $('.product_list > li:visible').each(function(index, element) {
      var reviews = $(element).find('.hook-reviews'),
        compare = $(element).find('.compare'),
        wishlist = $(element).find('.wishlist');
      if (reviews) {
        reviews.appendTo($(element).find('.button-container'));
      }
      if (compare) {
        compare.appendTo($(element).find('.functional-buttons'));
      }
      if (wishlist) {
        wishlist.appendTo($(element).find('.functional-buttons'));
      }
      var html = '';
      html += '<div class="product-container"><div class="left-block"><div class="product-image-container">';
      html += $(element).find('.product-image-container').html() + '</div>';
      html += '<div class="button-container">' + $(element).find('.button-container').html() + '</div></div>';
      html += '<div class="right-block">';
      html += '<h5 itemprop="name">' + $(element).find('h5').html() + '</h5>';
      html += '<p itemprop="description" class="product-desc">' + $(element).find('.product-desc').html() + '</p>';
      var price = $(element).find('.content_price').html(); // check : catalog mode is enabled
      if (price !== null) {
        html += '<div class="content_price">' + price + '</div>';
      }
      var colorList = $(element).find('.color-list-container').html();
      if (colorList !== null) {
        html += '<div class="color-list-container">' + colorList + '</div>';
      }
      html += '<div class="product-flags">' + $(element).find('.product-flags').html() + '</div>';
      html += '</div></div>';
      $(element).html(html);
    });
    $('.display').find('li#grid').addClass('selected');
    $('.display').find('li#list').removeAttr('class');
    $.totalStorage('display', 'grid');
    if ($('.product_list li div.wishlist').length) {
      WishlistButton();
    }
    adaptiveSizeGrid();
  }
}
function dropDown() {
  elementClick = '#header .current';
  elementSlide = 'ul.toogle_content';
  activeClass = 'active';
  $(elementClick).on('click', function(e) {
    e.stopPropagation();
    var subUl = $(this).next(elementSlide);
    if (subUl.is(':hidden')) {
      subUl.slideDown();
      $(this).addClass(activeClass);
    } else {
      subUl.slideUp();
      $(this).removeClass(activeClass);
    }
    $(elementClick).not(this).next(elementSlide).slideUp();
    $(elementClick).not(this).removeClass(activeClass);
    e.preventDefault();
  });
  $(elementSlide).on('click', function(e) {
    e.stopPropagation();
  });
  $(document).on('click', function(e) {
    e.stopPropagation();
    if (e.which != 3) {
      var elementHide = $(elementClick).next(elementSlide);
      $(elementHide).slideUp();
      $(elementClick).removeClass('active');
    }
  });
}
function accordionFooter(status) {
  if (status == 'enable') {
    $('#footer .footer-block h4').on('click', function(e) {
      $(this)
        .toggleClass('active')
        .parent()
        .find('.toggle-footer')
        .stop()
        .slideToggle('medium');
      e.preventDefault();
    });
    $('#footer')
      .addClass('accordion')
      .find('.toggle-footer')
      .slideUp('fast');
  } else {
    $('.footer-block h4').removeClass('active').off().parent().find('.toggle-footer').removeAttr('style').slideDown('fast');
    $('#footer').removeClass('accordion');
  }
}
//  TOGGLE COLUMNS
function accordion(status) {
  if (status == 'enable') {
    $('#product .product-information .tab-content > h3, #right_column .block:not(#layered_block_left) .title_block, #left_column .block:not(#layered_block_left) .title_block, #left_column #newsletter_block_left h4').on('click', function() {
      $(this)
        .toggleClass('active')
        .parent()
        .find('.block_content')
        .stop()
        .slideToggle('medium');
      $(this)
        .next('.tab-pane')
        .stop()
        .slideToggle('medium');
    });
    $('#right_column, #left_column')
      .addClass('accordion')
      .find('.block:not(#layered_block_left) .block_content')
      .slideUp('fast');
    $('#product .product-information .tab-content > h3:first').addClass('active');
    if (typeof(ajaxCart) !== 'undefined') {
      ajaxCart.collapse();
    }
  } else {
    $('#product .product-information .tab-content > h3, #right_column .block:not(#layered_block_left) .title_block, #left_column .block:not(#layered_block_left) .title_block, #left_column #newsletter_block_left h4')
      .removeClass('active')
      .off()
      .parent()
      .find('.block_content, .tab-pane')
      .removeAttr('style')
      .not('.tab-pane')
      .slideDown('fast');
    $('#left_column, #right_column').removeClass('accordion');
    $('#product .product-information .tab-content > h3:first').addClass('active');
  }
}
function bindUniform() {
  if (!!$.prototype.uniform) {
    $('select.form-control').not('.not_uniform').uniform();
  }
}
$(window).load(function() {
  adaptiveSizeGrid();
});
//  TOGGLE SITEMAP
function sitemapAccordion() {
  $('#sitemap #center_column ul.tree > li > ul')
    .addClass('accordion_content')
    .parent()
    .find('> a')
    .wrap('<p class="page-subheading accordion_current"></p>');
  $('#center_column .accordion_current').on('click', function() {
    $(this)
      .toggleClass('active')
      .parent()
      .find('.accordion_content')
      .stop()
      .slideToggle('medium');
  });
  $('#center_column')
    .addClass('accordionBox')
    .find('.accordion_content')
    .slideUp('fast');
  if (typeof(ajaxCart) !== 'undefined') {
    ajaxCart.collapse();
  }
}
function counter() {
  $('.count').each(function() {
    $(this).prop('Counter', 0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function(now) {
        $(this).text(Math.ceil(now));
      }
    });
  });
}
function adaptiveSizeGrid() {
  $('*:not(.bx-viewport) > ul.product_list.grid:not(.tab-pane)').each(function() {
    $(this).children().removeAttr("style");
    var maxWidth = $(this).children().first().width();
    $(this).children().css("max-width", maxWidth);
  });
};
function testimonialsSlider() {
  var testimonials_slider = $('#testimonials');
  testimonials_slider.bxSlider({
    responsive: true,
    useCSS: false,
    minSlides: 1,
    maxSlides: 1,
    slideWidth: 1200,
    slideMargin: 0,
    moveSlides: 1,
    pager: false,
    autoHover: false,
    speed: 500,
    pause: 3000,
    controls: true,
    autoControls: true,
    startText: '',
    stopText: '',
    prevText: '',
    nextText: ''
  });
}
function toTop() {
  var o = $('html');
  if (o.hasClass('desktop')) {
    $().UItoTop({
      easingType: 'easeOutQuart',
      containerClass: 'ui-to-top fa fa-angle-up'
    });
  }
}