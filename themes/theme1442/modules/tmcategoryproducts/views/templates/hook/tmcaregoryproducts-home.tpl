{**
* 2002-2016 TemplateMonster
*
* TM Category Products
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if isset($blocks) && $blocks}
  {foreach from=$blocks item='block' name='block'}
    {assign var="block_identificator" value="{$smarty.foreach.block.iteration}_{$block.id}"}
    <section id="block-category-{$block_identificator|escape:'htmlall':'UTF-8'}" class="category-block row">
      <div class="col-xs-12 col-lg-3">
        {assign var="cat_img" value="{$img_cat_dir}{$block.id}-medium_default.jpg"}
        <div class="category" {if $block.id != 2} style="background-image: url({$cat_img})" {/if}>
          {if isset($block.name)}
            <h4 class="title">{$block.name|escape:'htmlall':'UTF-8'}</h4>
          {/if}
          {if $block.id != 2}
            <a href="{$link->getCategoryLink($block.id)|escape:'html':'UTF-8'}" class="link">
              {l s='view all' mod='tmcategoryproductsslider'}
            </a>
          {/if}
        </div>
      </div>
      <div class="col-xs-12 col-lg-9">
        {if isset($block.products) && $block.products}
          {assign var='products' value=$block.products}
          <ul class="product_list grid tab-pane">
            {if $block.use_carousel}
              <li>
            {/if}
              {foreach from=$products item=product name=products}
                {if !$block.use_carousel}
                  <li>
                {/if}
                {if $block.use_carousel && $smarty.foreach.products.index is even && $smarty.foreach.products.index != 0}
                  </li>
                  <li>
                {/if}
                <div class="product-container" itemscope itemtype="https://schema.org/Product">
                  <div class="product-image-container">
                    {capture name='displayProductListGallery'}{hook h='displayProductListGallery' product=$product}{/capture}
                    {if $smarty.capture.displayProductListGallery}
                      {hook h='displayProductListGallery' product=$product}
                    {else}
                      <a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
                        <img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}"{if isset($homeSize)} width="{$homeSize.width}" height="{$homeSize.height}"{/if} itemprop="image" />
                      </a>
                    {/if}
                    <div class="functional-buttons">
                      {if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
                        {if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
                          {capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
                          <div>
                            <a class="ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}"
                               data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
                            </a>
                          </div>
                        {/if}
                      {/if}
                      {if (isset($product.customization_required) && $product.customization_required)}
                        <div><a itemprop="url" class="customize" href="{$product.link|escape:'html':'UTF-8'}"></a></div>
                      {/if}
                      {if isset($quick_view) && $quick_view}
                        <div class="qv-wrap">
                          <a class="quick-view" href="{$product.link|escape:'html':'UTF-8'}" data-href="{$product.link|escape:'html':'UTF-8'}"></a>
                        </div>
                      {/if}
                      {if isset($comparator_max_item) && $comparator_max_item}
                        <div class="compare">
                          <a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to Compare'}"></a>
                        </div>
                      {/if}
                      {hook h='displayProductListFunctionalButtons' product=$product}
                    </div>
                  </div>
                  <div class="product-info">
                    <h5 itemprop="name">
                      {if isset($product.pack_quantity) && $product.pack_quantity}{$product.pack_quantity|intval|cat:' x '}{/if}
                      <a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url" >
                        <span class="list-name">{$product.name|truncate:100:'...'|escape:'html':'UTF-8'}</span>
                        <span class="grid-name">{$product.name|truncate:45:'...'|escape:'html':'UTF-8'}</span>
                      </a>
                    </h5>
                    {if isset($product.is_virtual) && !$product.is_virtual}{hook h="displayProductDeliveryTime" product=$product}{/if}
                    {hook h="displayProductPriceBlock" product=$product type="weight"}
                    {if (!$PS_CATALOG_MODE && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                      <div class="content_price">
                        {if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
                          {hook h="displayProductPriceBlock" product=$product type='before_price'}
                          <span class="price product-price{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0} product-price-new{/if}">
                            {if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
                          </span>
                          {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                            {hook h="displayProductPriceBlock" product=$product type="old_price"}
                            <span class="old-price product-price">
                              {displayWtPrice p=$product.price_without_reduction}
                            </span>
                            {hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
                            {if $product.specific_prices.reduction_type == 'percentage'}
                              <span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
                            {/if}
                          {/if}
                          {hook h="displayProductPriceBlock" product=$product type="price"}
                          {hook h="displayProductPriceBlock" product=$product type="unit_price"}
                          {hook h="displayProductPriceBlock" product=$product type='after_price'}
                        {/if}
                      </div>
                    {/if}
                  </div>
                </div><!-- .product-container> -->
                {if !$block.use_carousel}
                  </li>
                {/if}
              {/foreach}
            {if $block.use_carousel}
              </li>
            {/if}
          </ul>
          {addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
          {addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
          {addJsDef comparator_max_item=$comparator_max_item}
          {addJsDef comparedProductsIds=$compared_products}
        {else}
          <p class="alert alert-warning">{l s='No products in this category.' mod='tmcategoryproducts'}</p>
        {/if}
      </div>
    </section>
    {if $block.use_carousel}
    {literal}
      <script type="text/javascript">
        $(document).ready(function() {
          setNbCatItems({/literal}{$block.carousel_settings.carousel_nb|escape:'htmlall':'UTF-8'}{literal});
          tmCategoryCarousel{/literal}{$block_identificator|escape:'htmlall':'UTF-8'}{literal} = $('#block-category-{/literal}{$block_identificator|escape:'htmlall':'UTF-8'}{literal}  ul.product_list').bxSlider({
            responsive: true,
            useCSS: false,
            minSlides: carousel_nb_new,
            maxSlides: carousel_nb_new,
            slideWidth: {/literal}{$block.carousel_settings.carousel_slide_width|escape:'htmlall':'UTF-8'}{literal},
            slideMargin: {/literal}{$block.carousel_settings.carousel_slide_margin|escape:'htmlall':'UTF-8'}{literal},
            infiniteLoop: {/literal}{$block.carousel_settings.carousel_loop|escape:'htmlall':'UTF-8'}{literal},
            hideControlOnEnd: {/literal}{$block.carousel_settings.carousel_hide_control|escape:'htmlall':'UTF-8'}{literal},
            randomStart: {/literal}{$block.carousel_settings.carousel_random|escape:'htmlall':'UTF-8'}{literal},
            moveSlides: {/literal}{$block.carousel_settings.carousel_item_scroll|escape:'htmlall':'UTF-8'}{literal},
            pager: {/literal}{$block.carousel_settings.carousel_pager|escape:'htmlall':'UTF-8'}{literal},
            autoHover: {/literal}{$block.carousel_settings.carousel_auto_hover|escape:'htmlall':'UTF-8'}{literal},
            auto: {/literal}{$block.carousel_settings.carousel_auto|escape:'htmlall':'UTF-8'}{literal},
            speed: {/literal}{$block.carousel_settings.carousel_speed|escape:'htmlall':'UTF-8'}{literal},
            pause: {/literal}{$block.carousel_settings.carousel_auto_pause|escape:'htmlall':'UTF-8'}{literal},
            controls: {/literal}{$block.carousel_settings.carousel_control|escape:'htmlall':'UTF-8'}{literal},
            autoControls: {/literal}{$block.carousel_settings.carousel_auto_control|escape:'htmlall':'UTF-8'}{literal},
            startText: '',
            stopText: '',
          });
          var tm_cps_doit;
          $(window).resize(function() {
            clearTimeout(tm_cps_doit);
            tm_cps_doit = setTimeout(function() {
              resizedwtm_cps{/literal}{$block_identificator|escape:'htmlall':'UTF-8'}{literal}();
            }, 201);
          });
        });
        function resizedwtm_cps{/literal}{$block_identificator|escape:'htmlall':'UTF-8'}{literal}(){
          setNbCatItems({/literal}{$block.carousel_settings.carousel_nb|escape:'htmlall':'UTF-8'}{literal});
          tmCategoryCarousel{/literal}{$block_identificator|escape:'htmlall':'UTF-8'}{literal}.reloadSlider({
            responsive: true,
            useCSS: false,
            minSlides: carousel_nb_new,
            maxSlides: carousel_nb_new,
            slideWidth: {/literal}{$block.carousel_settings.carousel_slide_width|escape:'htmlall':'UTF-8'}{literal},
            slideMargin: {/literal}{$block.carousel_settings.carousel_slide_margin|escape:'htmlall':'UTF-8'}{literal},
            infiniteLoop: {/literal}{$block.carousel_settings.carousel_loop|escape:'htmlall':'UTF-8'}{literal},
            hideControlOnEnd: {/literal}{$block.carousel_settings.carousel_hide_control|escape:'htmlall':'UTF-8'}{literal},
            randomStart: {/literal}{$block.carousel_settings.carousel_random|escape:'htmlall':'UTF-8'}{literal},
            moveSlides: {/literal}{$block.carousel_settings.carousel_item_scroll|escape:'htmlall':'UTF-8'}{literal},
            pager: {/literal}{$block.carousel_settings.carousel_pager|escape:'htmlall':'UTF-8'}{literal},
            autoHover: {/literal}{$block.carousel_settings.carousel_auto_hover|escape:'htmlall':'UTF-8'}{literal},
            auto: {/literal}{$block.carousel_settings.carousel_auto|escape:'htmlall':'UTF-8'}{literal},
            speed: {/literal}{$block.carousel_settings.carousel_speed|escape:'htmlall':'UTF-8'}{literal},
            pause: {/literal}{$block.carousel_settings.carousel_auto_pause|escape:'htmlall':'UTF-8'}{literal},
            controls: {/literal}{$block.carousel_settings.carousel_control|escape:'htmlall':'UTF-8'}{literal},
            autoControls: {/literal}{$block.carousel_settings.carousel_auto_control|escape:'htmlall':'UTF-8'}{literal},
            startText: '',
            stopText: '',
          });
        }
        function setNbCatItems(countItems) {
          var catBlock = $('.category-block').width();
          if (catBlock < 400)
            carousel_nb_new = 1;
          if (catBlock >= 400)
            carousel_nb_new = 2;
          if (catBlock >= 580)
            carousel_nb_new = 3;
          if (catBlock > 850)
            carousel_nb_new = countItems;
        }
      </script>
    {/literal}
    {/if}
  {/foreach}
{/if}