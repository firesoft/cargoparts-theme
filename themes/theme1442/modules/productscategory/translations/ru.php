<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productscategory}prestashop>productscategory_8a4f5a66d0fcc9d13614516db6e3d47a'] = 'Товары в той же категории';
$_MODULE['<{productscategory}prestashop>productscategory_1d269d7f013c3d9d891a146f4379eb02'] = 'Добавляет блок на страницу товара, отображающий товары из той же категории.';
$_MODULE['<{productscategory}prestashop>productscategory_8dd2f915acf4ec98006d11c9a4b0945b'] = 'Настройки обновлены.';
$_MODULE['<{productscategory}prestashop>productscategory_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{productscategory}prestashop>productscategory_e06ba84b50810a88438ae0537405f65a'] = 'Отображать цены товаров';
$_MODULE['<{productscategory}prestashop>productscategory_1d986024f548d57b1d743ec7ea9b09d9'] = 'Показывать цены товаров, появляющихся в этом блоке.';
$_MODULE['<{productscategory}prestashop>productscategory_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включено';
$_MODULE['<{productscategory}prestashop>productscategory_b9f5c797ebbf55adccdd8539a65a0241'] = 'Отключено';
$_MODULE['<{productscategory}prestashop>productscategory_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{productscategory}prestashop>productscategory_d668ca7ee8e633e13d109f094ce984ed'] = '[1]%s другой товар[/1] в этой категории:';
$_MODULE['<{productscategory}prestashop>productscategory_f55e0a28b86c2ab66ac632ab9ddf1833'] = 'В этой категории %s товаров:';
$_MODULE['<{productscategory}prestashop>productscategory_c60b74a0465a16a734ac328f8cfe0d5f'] = '[1]%s других товаров[/1] в этой категории:';
$_MODULE['<{productscategory}prestashop>productscategory_bebb44f38b03407098d48198c1d0aaa5'] = 'В этой категории %s товаров:';
