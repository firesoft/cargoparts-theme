{if $comment.id_smart_blog_comment != ''}
  <ul class="commentList">
    <li id="comment-{$comment.id_smart_blog_comment}" class="even">
      <div class="well comment">
        <div class="comment_author">
          <span class="icon fa fa-user"></span>
          <div class="name">{$childcommnets.name}</div>
          <div class="created">
            <span itemprop="commentTime">{$childcommnets.created|date_format}</span>
          </div>
        </div>
        <div class="comment_details">
          <p>{$childcommnets.content}</p>
          {if Configuration::get('smartenablecomment') == 1}
            {if $comment_status == 1}
              <div class="reply">
                <a onclick="return addComment.moveForm('comment-{$comment.id_smart_blog_comment}', '{$comment.id_smart_blog_comment}', 'respond', '{$smarty.get.id_post}')" class="comment-reply-link link">{l s='Reply' mod='smartblog'}</a>
              </div>
            {/if}
          {/if}
        </div>
      </div>
      {if isset($childcommnets.child_comments)}
        {foreach from=$childcommnets.child_comments item=comment}
          {if isset($childcommnets.child_comments)}
            {include file="./comment_loop.tpl" childcommnets=$comment}
            {$i=$i+1}
          {/if}
        {/foreach}
      {/if}
    </li>
  </ul>
{/if} 