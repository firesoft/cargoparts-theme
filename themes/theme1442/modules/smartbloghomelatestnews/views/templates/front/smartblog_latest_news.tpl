{*define numbers of product per line in other page for desktop*}
{if ($hide_left_column || $hide_right_column) && ($hide_left_column !='true' || $hide_right_column !='true')}   {* left or right column *}
  {assign var='nbItemsPerLine' value=3}
  {assign var='nbItemsPerLineTablet' value=2}
  {assign var='nbItemsPerLineMobile' value=2}
{elseif ($hide_left_column && $hide_right_column) && ($hide_left_column =='true' && $hide_right_column =='true')} {* no columns *}
  {assign var='nbItemsPerLine' value=4}
  {assign var='nbItemsPerLineTablet' value=3}
  {assign var='nbItemsPerLineMobile' value=2}
{else}                                                        {* left and right column *}
  {assign var='nbItemsPerLine' value=2}
  {assign var='nbItemsPerLineTablet' value=1}
  {assign var='nbItemsPerLineMobile' value=2}
{/if}

{*define numbers of product per line in other page for tablet*}

{assign var='nbLi' value=$view_data|@count}
{math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
{math equation="nbLi/nbItemsPerLineTablet" nbLi=$nbLi nbItemsPerLineTablet=$nbItemsPerLineTablet assign=nbLinesTablet}

<section id="homepage-blog" class="block">
  <div class="head clearfix">
    <h3 class='head-block pull-left'>{l s='[1]news[/1] and articles' mod='smartbloghomelatestnews' tags=['<strong>']}</h3>
    <a href="{smartblog::GetSmartBlogLink('smartblog')}" class="link pull-right hidden-xs">{l s='see all news' mod='smartbloghomelatestnews'}</a>
  </div>
  <div class="block_content">
    <ul class="row">
      {if isset($view_data) && !empty($view_data)}
        {foreach from=$view_data item=post name=post}
          {math equation="(total%perLine)" total=$smarty.foreach.post.total perLine=$nbItemsPerLine assign=totModulo}
          {math equation="(total%perLineT)" total=$smarty.foreach.post.total perLineT=$nbItemsPerLineTablet assign=totModuloTablet}
          {math equation="(total%perLineT)" total=$smarty.foreach.post.total perLineT=$nbItemsPerLineMobile assign=totModuloMobile}
          {if $totModulo == 0}{assign var='totModulo' value=$nbItemsPerLine}{/if}
          {if $totModuloTablet == 0}{assign var='totModuloTablet' value=$nbItemsPerLineTablet}{/if}
          {if $totModuloMobile == 0}{assign var='totModuloMobile' value=$nbItemsPerLineMobile}{/if}
          {$options.id_post = $post.id}
          {$options.slug = $post.link_rewrite}
          <li class="col-xs-12 col-sm-{12/$nbItemsPerLineTablet} col-lg-{12/$nbItemsPerLine}">
            <div class="blog-image">
              <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">
                <img alt="{$post.title}" class="img-responsive" src="{$modules_dir}smartblog/images/{$post.post_img}-home-default.jpg">
              </a>
            </div>

            <p class="post-info">
              {if isset($post.firstname)}
                <span>{l s='by' mod='smartbloghomelatestnews'} {$post.firstname}</span> -
              {/if}
              {if isset($post.cat_name)}
                {$post.cat_name}
              {/if}
              <span class="date-added">{$post.date_added|date_format:"%B %d, %Y"}</span>
            </p>
            <h5><a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.title}</a></h5>
            <p class="post-descr">
              {$post.short_description|escape:'htmlall':'UTF-8'}
            </p>
            <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" class="btn btn-primary btn-md">{l s='Read More' mod='smartbloghomelatestnews'}</a>
          </li>
        {/foreach}
      {/if}
    </ul>
  </div>
</section>