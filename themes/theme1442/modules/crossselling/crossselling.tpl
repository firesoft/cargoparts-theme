{if isset($orderProducts) && count($orderProducts)}
  <section id="crossselling" class="page-product-box">
    <h3 class="productscategory_h2 head-block">
      {if $page_name == 'product'}
        {l s='[1]Customers who bought this product[/1] also bought:' mod='crossselling' tags=['<strong>']}
      {else}
        <strong>{l s='We recommend' mod='crossselling'}</strong>
      {/if}
    </h3>
    <div id="crossselling_list">
      <ul id="crossselling_list_car" class="clearfix">
        {foreach from=$orderProducts item='orderProduct' name=orderProduct}
          <li class="product-box item" itemprop="isRelatedTo" itemscope itemtype="https://schema.org/Product">
            <div class="product">
              <div class="image-block">
                <a class="lnk_img product-image" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}" >
                  <img itemprop="image" src="{$orderProduct.image}" alt="{$orderProduct.name|htmlspecialchars}" />
                </a>
                {if !$PS_CATALOG_MODE && ($orderProduct.allow_oosp || $orderProduct.quantity > 0)}
                  <div class="buttons">
                    <a class="btn btn-primary ajax_add_to_cart_button btn-md" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$orderProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$orderProduct.id_product|intval}" title="{l s='Add to cart' mod='crossselling'}">
                      <span>{l s='Add to cart' mod='crossselling'}</span>
                    </a>
                  </div>
                {/if}
              </div>
              <h5 itemprop="name" class="product-name">
                <a itemprop="url" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}">
                  {$orderProduct.name|escape:'html':'UTF-8'}
                </a>
              </h5>
              {if $crossDisplayPrice && $orderProduct.show_price == 1 && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
                <p class="price_display">
                  <span class="price">{convertPrice price=$orderProduct.displayed_price}</span>
                </p>
              {/if}
            </div>
          </li>
        {/foreach}
      </ul>
    </div>
  </section>
{/if}