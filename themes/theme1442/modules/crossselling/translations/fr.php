<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{crossselling}prestashop>crossselling_46532f43e161343763ff815e5208f7e9'] = 'Vente croisée';
$_MODULE['<{crossselling}prestashop>crossselling_83bcd48a4b3938f7cd10c898ece01adf'] = 'Ajoute une section \"Les clients qui ont acheté ce produit ont également acheté...\" sur toutes les fiches produit.';
$_MODULE['<{crossselling}prestashop>crossselling_462390017ab0938911d2d4e964c0cab7'] = 'Paramètres mis à jour avec succès';
$_MODULE['<{crossselling}prestashop>crossselling_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{crossselling}prestashop>crossselling_b6bf131edd323320bac67303a3f4de8a'] = 'Afficher le prix du produit';
$_MODULE['<{crossselling}prestashop>crossselling_70f9a895dc3273d34a7f6d14642708ec'] = 'Afficher le prix du produit dans le block';
$_MODULE['<{crossselling}prestashop>crossselling_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{crossselling}prestashop>crossselling_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{crossselling}prestashop>crossselling_19a799a6afd0aaf89bc7a4f7588bbf2c'] = 'Nombre de produits affichés';
$_MODULE['<{crossselling}prestashop>crossselling_02128de6a3b085c72662973cd3448df2'] = 'Indiquez le nombre de produits à afficher dans ce bloc.';
$_MODULE['<{crossselling}prestashop>crossselling_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{crossselling}prestashop>crossselling_434a53ee00f6ab63cb3ceb58dce07cc1'] = '[1]Les clients qui ont acheté ce produit[/1] ont également acheté...';
$_MODULE['<{crossselling}prestashop>crossselling_ef2b66b0b65479e08ff0cce29e19d006'] = 'Les clients qui ont acheté ce produit ont également acheté...';
