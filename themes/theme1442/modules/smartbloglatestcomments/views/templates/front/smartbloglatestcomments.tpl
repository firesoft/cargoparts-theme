{if isset($latesComments) AND !empty($latesComments)}
  <section id="latestComments" class="block">
    <h4 class='title_block'>{l s='Latest Comments' mod='smartbloglatestcomments'}</h4>
    <div class="block_content products-block">
      <ul>
        {foreach from=$latesComments item="comment" name=comments}
          {assign var="options" value=null}
          {$options.id_post= $comment.id_post}
          {$options.slug= $comment.slug}
          <li class="clearfix{if $smarty.foreach.comments.last} last_item{elseif $smarty.foreach.comments.first} first_item{else}{/if}">
            <blockquote class="product-content">
              <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$comment.content|truncate:125:'...'}</a>
              <cite>{$comment.name}</cite>
            </blockquote>
          </li>
        {/foreach}
      </ul>
    </div>
  </section>
{/if}