{**
* 2002-2017 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

{if $content.indexed}
  <form action="#" method="post" class="clearfix">
    <div id="{$content.type|escape:'htmlall':'UTF-8'}-filter" class="{$content.type|escape:'htmlall':'UTF-8'}-filter tmadvancedfilter">
      <div class="container">
        <div class="filter-body">
          {if $content.filter_name || $content.filter_description}
            <div class="filter-info">
              {if $page_name =='index'}
                {if $content.filter_name}<h3>{$content.filter_name|escape:'htmlall':'UTF-8'}</h3>{/if}
                {if $content.filter_description}<div class="filter-description">{$content.filter_description|escape:'quotes':'UTF-8'}</div>{/if}
              {else}
                <h3>{l s='[1]Advanced[/1] filter' mod='tmadvancedfilter' tags=['<strong>']}</h3>
              {/if}
            </div>
          {/if}
          <div class="clearfix">
            <div class="filter-content">
              <div class="content">
                {$content.filter|escape:'quotes':'UTF-8'}
              </div>
              <div class="filter-btn">
                <button name="submitTopTmAdvancedFilter" type="submit" class="btn btn-primary btn-md result">{l s='Show' mod='tmadvancedfilter'}</button>
                <p><span class="count">{if isset($result) && $result} {$result|escape:'htmlall':'UTF-8'}{/if}</span> {l s='Products' mod='tmadvancedfilter'}</p>
              </div>
            </div>
            <div class="filter-navigation">
              {if isset($filter_parameters) && $filter_parameters}
                <ul id="filter-selected-parameters" class="clearfix">
                  {foreach from=$filter_parameters key=field item=data}
                    {if $data}
                      {foreach from=$data item=parameter}
                        {if $parameter.field_type}
                          <li data-filter-field="{$field|escape:'htmlall':'UTF-8'}" data-filter-field-type="{$parameter.field_type|escape:'htmlall':'UTF-8'}" {if isset($parameter.id) && $parameter.id}data-filter-field-id="{$parameter.id|escape:'htmlall':'UTF-8'}"{/if}>{$parameter.name|escape:'htmlall':'UTF-8'}<i></i></li>
                        {/if}
                      {/foreach}
                    {/if}
                  {/foreach}
                </ul>
              {/if}
              {if $page_name !='index'}
                <span id="adv-filter" class="btn btn-md btn-primary">Advanced Search</span>
              {/if}
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" name="id_filter" value="{$content.id_filter|escape:'htmlall':'UTF-8'}" />
      <input type="hidden" name="type" value="top" />
    </div>
  </form>
{/if}