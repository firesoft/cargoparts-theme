{**
* 2002-2016 TemplateMonster
*
* TM Advanced Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
{*{$id_filter_layout} - get filter layout type*}
{*{$filter_item.column} - get item column in layout*}
<div class="{$type|escape:'htmlall':'UTF-8'}-filter-row">
  {if $filter_item.name}
    <label class="parameter-name">{$filter_item.name|escape:'htmlall':'UTF-8'}
      {if $filter_item.label}<small>{$filter_item.label|escape:'htmlall':'UTF-8'}</small>{/if}
    </label>{/if}
  {if $filter_item.description}<div class="parameter-description">{$filter_item.description|escape:'quotes':'UTF-8'}</div>{/if}
  <div class="select-range">
    {assign var='from' value=Tmadvancedfilter::getActiveRange($filter_item.type, $filter_item.id_item, 'from')|escape:'htmlall':'UTF-8'}
    {assign var='to' value=Tmadvancedfilter::getActiveRange($filter_item.type, $filter_item.id_item, 'to')|escape:'htmlall':'UTF-8'}
    <div class="form-group">
      <input
              class="form-control"
              data-type="price"
              data-element-type="input"
              data-type-value="from"
              name="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}[]"
              value="{if $from}{$from|escape:'htmlall':'UTF-8'}{/if}"
              autocomplete="off"
              placeholder="{l s='from' mod='tmadvancedfilter'} ({$currency_sign|escape:'htmlall':'UTF-8'})"/>
    </div>
    <div class="form-group">
      <input
              class="form-control"
              data-type="price"
              data-element-type="input"
              data-type-value="to"
              name="{$filter_item.type|escape:'htmlall':'UTF-8'}_{$filter_item.id_item|escape:'htmlall':'UTF-8'}[]"
              value="{if $to}{$to|escape:'htmlall':'UTF-8'}{/if}"
              autocomplete="off"
              placeholder="{l s='to' mod='tmadvancedfilter'} ({$currency_sign|escape:'htmlall':'UTF-8'})"/>
    </div>
  </div>
</div>