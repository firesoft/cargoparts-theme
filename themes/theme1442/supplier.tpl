{include file="$tpl_dir./errors.tpl"}

{if !isset($errors) || !sizeof($errors)}
  <h1 class="page-heading product-listing">
    {l s='List of products by supplier:'}&nbsp;{$supplier->name|escape:'html':'UTF-8'}
  </h1>
  {if !empty($supplier->description)}
    <div class="description_box rte">
      <p>{$supplier->description}</p>
    </div>
  {/if}

  {if $products}
    <div class="content_sortPagiBar">
      <div class="sortPagiBar clearfix">
        {if isset($p) && $p}
          <div class="product-count">
            {if ($n*$p) < $nb_products }
              {assign var='productShowing' value=$n*$p}
            {else}
              {assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
            {/if}
            {if $p==1}
              {assign var='productShowingStart' value=1}
            {else}
              {assign var='productShowingStart' value=$n*$p-$n+1}
            {/if}
            {if $nb_products > 1}
              {l s='%1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products]}
            {else}
              {l s='%1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing]}
            {/if}
          </div>
        {/if}
        {include file="./nbr-product-page.tpl"}
        {include file="./product-sort.tpl"}
      </div>
    </div>

    {include file="./product-list.tpl" products=$products}

    <div class="content_sortPagiBar">
      <div class="bottom-pagination-content clearfix">
        {include file="./pagination.tpl" paginationId='bottom' no_follow=1}
        {include file="./product-compare.tpl"}
      </div>
    </div>
  {else}
    <p class="alert alert-warning">{l s='No products for this supplier.'}</p>
  {/if} 
{/if}