{strip}
  <span class="heading-counter">
    {if (isset($category) && $category->id == 1) || (isset($nb_products) && $nb_products == 0)}
      {l s='0 Products'}
    {else}
      {if isset($nb_products) && $nb_products == 1}
        {l s='1 Product'}
      {elseif isset($nb_products)}
        {l s='%d Products' sprintf=$nb_products}
      {/if}
    {/if}
  </span>
{/strip}