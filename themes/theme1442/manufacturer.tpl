{include file="$tpl_dir./errors.tpl"}

{if !isset($errors) || !sizeof($errors)}
  <h1 class="page-heading product-listing">
    {l s='List of products by manufacturer'}&nbsp;{$manufacturer->name|escape:'html':'UTF-8'}
  </h1>
  {if !empty($manufacturer->description) || !empty($manufacturer->short_description)}
    <div class="description_box rte">
      {if !empty($manufacturer->short_description)}
        <div class="short_desc">
          {$manufacturer->short_description}
        </div>
        <div class="hide_desc">
          {$manufacturer->description}
        </div>
        {if !empty($manufacturer->description)}
          <a href="#" class="lnk_more" onclick="$(this).prev().slideDown('slow'); $(this).hide();$(this).prev().prev().hide(); return false;" title="{l s='More'}">
            {l s='More'}
          </a>
        {/if}
      {else}
        <div>
          {$manufacturer->description}
        </div>
      {/if}
    </div>
  {/if}

  {if $products}
    <div class="content_sortPagiBar">
      <div class="sortPagiBar clearfix">
        {if isset($p) && $p}
          <div class="product-count">
            {if ($n*$p) < $nb_products }
              {assign var='productShowing' value=$n*$p}
            {else}
              {assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
            {/if}
            {if $p==1}
              {assign var='productShowingStart' value=1}
            {else}
              {assign var='productShowingStart' value=$n*$p-$n+1}
            {/if}
            {if $nb_products > 1}
              {l s='%1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products]}
            {else}
              {l s='%1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing]}
            {/if}
          </div>
        {/if}
        {include file="./nbr-product-page.tpl"}
        {include file="./product-sort.tpl"}
      </div>
    </div>

    {include file="./product-list.tpl" products=$products}

    <div class="content_sortPagiBar">
      <div class="bottom-pagination-content clearfix">
        {include file="./pagination.tpl" no_follow=1 paginationId='bottom'}
        {include file="./product-compare.tpl"}
      </div>
    </div>
  {else}
    <p class="alert alert-warning">{l s='No products for this manufacturer.'}</p>
  {/if}
{/if}